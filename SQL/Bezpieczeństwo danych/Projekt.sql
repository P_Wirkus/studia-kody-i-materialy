-- Autor 1: Szymon Tokarski, 172478
-- Autor 2: Karolina Rymorz, 172530


-- Kto co zrobil?

-- Czesc I: praca wspolna
-- Czesc II: praca wspolna
-- Ciężko ocenić wkład pracy poszczególnej osoby, ponieważ pracowaliśmy razem poprzez aplikację Skype

-- Instrukcje tworzace strukture bazy danych

drop table if exists Dyplom;
drop table if exists Oceny_koncowe;
drop table if exists Oceny;
drop table if exists Student;
drop table if exists Kierunek;
drop table if exists Przedmiot;
drop table if exists Prowadzacy;
drop table if exists Wydzial;

CREATE TABLE Wydzial (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Nazwa CHAR(50)  NOT NULL  ,
  Uczelnia CHAR(15)  NOT NULL  ,
  Rok_zalozenia DATE  NOT NULL    ,
PRIMARY KEY(ID));

CREATE TABLE Prowadzacy (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Imie CHAR(20)  NULL  ,
  Nazwisko CHAR(20)  NULL  ,
  Stopien_Naukowy CHAR(15)  NULL  ,
  Stanowisko CHAR(25)  NULL    ,
PRIMARY KEY(ID));

CREATE TABLE Przedmiot (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Prowadzacy_ID INTEGER UNSIGNED  NOT NULL  ,
  Nazwa CHAR(50)  NOT NULL  ,
  Punkty_ECTS INTEGER UNSIGNED  NOT NULL  ,
  Semestr INTEGER UNSIGNED  NULL    ,
PRIMARY KEY(ID, Prowadzacy_ID)  ,
INDEX Przedmiot_FKIndex2(Prowadzacy_ID),
  FOREIGN KEY(Prowadzacy_ID)
    REFERENCES Prowadzacy(ID)
      ON DELETE CASCADE
      ON UPDATE RESTRICT);

CREATE TABLE Kierunek (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Wydzial_ID INTEGER UNSIGNED  NOT NULL  ,
  Nazwa CHAR(50)  NOT NULL  ,
  Stopien INTEGER UNSIGNED  NOT NULL  ,
  Liczba_Semestrow INTEGER UNSIGNED  NOT NULL  ,
  Liczba_ECTS INTEGER UNSIGNED  NOT NULL    ,
PRIMARY KEY(ID, Wydzial_ID)  ,
INDEX Kierunek_FKIndex1(Wydzial_ID),
  FOREIGN KEY(Wydzial_ID)
    REFERENCES Wydzial(ID)
      ON DELETE RESTRICT
      ON UPDATE CASCADE);

CREATE TABLE Student (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Kierunek_Wydzial_ID INTEGER UNSIGNED  NOT NULL  ,
  Kierunek_ID INTEGER UNSIGNED  NOT NULL  ,
  Imie CHAR(20)  NOT NULL  ,
  Nazwisko CHAR(20)  NOT NULL  ,
  Nr_Indeksu INTEGER UNSIGNED  NOT NULL  ,
  Dlug_ECTS INTEGER UNSIGNED  NOT NULL    ,
PRIMARY KEY(ID, Kierunek_Wydzial_ID, Kierunek_ID)  ,
INDEX Student_FKIndex1(Kierunek_ID, Kierunek_Wydzial_ID),
  FOREIGN KEY(Kierunek_ID, Kierunek_Wydzial_ID)
    REFERENCES Kierunek(ID, Wydzial_ID)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);

CREATE TABLE Oceny (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Przedmiot_Prowadzacy_ID INTEGER UNSIGNED  NOT NULL  ,
  Przedmiot_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_Kierunek_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_Kierunek_Wydzial_ID INTEGER UNSIGNED  NOT NULL  ,
  Nazwa CHAR(30)  NOT NULL  ,
  Punkty INTEGER UNSIGNED  NOT NULL  ,
  Data_Wystawienia DATE  NULL    ,
PRIMARY KEY(ID, Przedmiot_Prowadzacy_ID, Przedmiot_ID, Student_ID, Student_Kierunek_ID, Student_Kierunek_Wydzial_ID)  ,
INDEX Oceny_FKIndex1(Przedmiot_ID, Przedmiot_Prowadzacy_ID)  ,
INDEX Oceny_FKIndex2(Student_ID, Student_Kierunek_Wydzial_ID, Student_Kierunek_ID),
  FOREIGN KEY(Przedmiot_ID, Przedmiot_Prowadzacy_ID)
    REFERENCES Przedmiot(ID, Prowadzacy_ID)
      ON DELETE RESTRICT
      ON UPDATE RESTRICT,
  FOREIGN KEY(Student_ID, Student_Kierunek_Wydzial_ID, Student_Kierunek_ID)
    REFERENCES Student(ID, Kierunek_Wydzial_ID, Kierunek_ID)
      ON DELETE CASCADE
      ON UPDATE CASCADE);

CREATE TABLE Oceny_koncowe (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Student_ID INTEGER UNSIGNED  NOT NULL  ,
  Przedmiot_Prowadzacy_ID INTEGER UNSIGNED  NOT NULL  ,
  Przedmiot_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_Kierunek_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_Kierunek_Wydzial_ID INTEGER UNSIGNED  NOT NULL  ,
  Punkty INTEGER UNSIGNED ZEROFILL  NOT NULL  ,
  Ocena FLOAT  NOT NULL  ,
  Poprawa BOOL  NULL  ,
  Data_Wystawienia DATE  NOT NULL    ,
PRIMARY KEY(ID, Student_ID, Przedmiot_Prowadzacy_ID, Przedmiot_ID, Student_Kierunek_ID, Student_Kierunek_Wydzial_ID)  ,
INDEX Oceny_koncowe_FKIndex1(Student_ID, Student_Kierunek_Wydzial_ID, Student_Kierunek_ID)  ,
INDEX Oceny_koncowe_FKIndex2(Przedmiot_ID, Przedmiot_Prowadzacy_ID),
  FOREIGN KEY(Student_ID, Student_Kierunek_Wydzial_ID, Student_Kierunek_ID)
    REFERENCES Student(ID, Kierunek_Wydzial_ID, Kierunek_ID)
      ON DELETE RESTRICT
      ON UPDATE CASCADE,
  FOREIGN KEY(Przedmiot_ID, Przedmiot_Prowadzacy_ID)
    REFERENCES Przedmiot(ID, Prowadzacy_ID)
      ON DELETE RESTRICT
      ON UPDATE RESTRICT);

CREATE TABLE Dyplom (
  ID INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  Student_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_Kierunek_ID INTEGER UNSIGNED  NOT NULL  ,
  Student_Kierunek_Wydzial_ID INTEGER UNSIGNED  NOT NULL  ,
  Data_Wystawienia DATE  NULL  ,
  Ocena INTEGER UNSIGNED  NOT NULL    ,
PRIMARY KEY(ID, Student_ID, Student_Kierunek_ID, Student_Kierunek_Wydzial_ID)  ,
INDEX Dyplom_FKIndex2(Student_ID, Student_Kierunek_Wydzial_ID, Student_Kierunek_ID),
  FOREIGN KEY(Student_ID, Student_Kierunek_Wydzial_ID, Student_Kierunek_ID)
    REFERENCES Student(ID, Kierunek_Wydzial_ID, Kierunek_ID)
      ON DELETE RESTRICT
      ON UPDATE CASCADE);


-- Instrukcje dodajace przykladowe dane

INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('1','Inżynierii Lądowej i Środowiska','PG','1945-09-13');
INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('2','Inżynierii Mechanicznej i Okrętownictwa','PG','1904-05-21');
INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('3','Architektury','PG','1904-08-12');
INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('4','Elektrotechniki i Automatyki','PG','1996-03-23');
INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('5','Zarządzania i Ekonomii','PG','1992-07-04');
INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('6','Fizyki Technicznej i Matematyki Stosowanej','PG','1983-09-12');
INSERT INTO `wydzial`(`ID`, `Nazwa`, `Uczelnia`, `Rok_zalozenia`) VALUES ('7','Chemiczny','PG','1904-08-12');

INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('1','6','Matematyka','1','6','180');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('2','6','Fizyka techniczna','1','7','210');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('3','3','Gospodarka przestrzenna','1','8','240');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('4','5','Ekonomia','2','4','120');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('5','5','Analityka gospodarcza','1','6','180');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('6','6','Nanotechnologia','1','7','210');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('7','1','Budownictwo','1','7','210');
INSERT INTO `kierunek`(`ID`, `Wydzial_ID`, `Nazwa`, `Stopien`, `Liczba_Semestrow`, `Liczba_ECTS`) VALUES ('8','6','Matematyka','2','4','120');

INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('1','Ryszard','Brzęczyszczykiewicz','mgr','Asystent');
INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('2','Mieszko','Pierwszy','dr inż.','Adiunkt');
INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('3','Edward','Stachura','dr hab.','Profesor uczelni');
INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('4','Małgorzata','Cieślar','prof.','Profesor');
INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('5','Ewa','Brudny','mgr inż.','Asystent');
INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('6','Olga','Baranowska','prof. dr hab.','Profesor');
INSERT INTO `prowadzacy`(`ID`, `Imie`, `Nazwisko`, `Stopien_Naukowy`, `Stanowisko`) VALUES ('7','Adam','Macura','dr','Adiunkt');

INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('1','3','Analiza matematyczna','8','1');
INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('2','4','Algebra liniowa z geometrią','3','2');
INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('3','6','Mechanika klasyczna','7','3');
INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('4','7','Zaawansowane oprogramowanie','6','1');
INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('5','4','Geometria analityczna','4','3');
INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('6','3','Zarządzanie zasobami ludzkimi','5','5');
INSERT INTO `przedmiot`(`ID`, `Prowadzacy_ID`, `Nazwa`, `Punkty_ECTS`, `Semestr`) VALUES ('7','6','Krystalografia','6','2');

INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('1','3','3','Jan','Kowalski','124358','0');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('2','6','6','Barbara','Białkowicz','172412','2');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('3','6','1','Kuba','Podolski','154218','0');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('4','6','1','Marzena','Nowak','162473','0');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('5','1','7','Piotr','Nowak','171504','10');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('6','5','4','Marcin','Zalewski','130412','0');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('7','6','8','Aleksandra','Adamska','154023','5');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('8','5','5','Bożena','Lewandowska','136901','6');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('9','6','2','Mikołaj','Piątek','184932','7');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('10','3','3','Aleksandra','Polak','109433','0');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('11','6','1','Jan','Nowak','163452','0');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('12','6','1','Monika','Cholewa','162636','1');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('13','6','1','Ewelina','Maj','167815','5');
INSERT INTO `student`(`ID`, `Kierunek_Wydzial_ID`, `Kierunek_ID`, `Imie`, `Nazwisko`, `Nr_Indeksu`, `Dlug_ECTS`) VALUES ('14','6','6','Jan','Nowak','163452','0');

INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('1','3','1','4','1','6','Kolokwium 1','60','2020-12-01');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('2','3','1','3','1','6','Kolokwium 1','40','2020-12-01');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('3','6','7','2','6','6','Projekt 1','90','2018-03-30');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('4','3','6','8','5','5','Kolokwium 2','75','2017-06-15');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('5','7','4','7','8','6','Projekt 2','88','2008-01-12');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('6','6','3','9','2','6','Projekt 1','79','2015-12-05');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('7','4','2','9','2','6','Wejściówka','74','2015-11-10');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('8','3','6','8','5','5','Wejściówka','64','2004-05-10');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('9','4','2','9','2','6','Kolokwium 1','30','2015-12-01');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('10','3','1','11','1','6','Aktywność','90','2020-12-01');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('11','6','7','14','6','6','Kolokwium 1','74','2020-11-30');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('12','3','1','12','1','6','Kolokwium 1','80','2020-12-01');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('13','3','1','13','1','6','Kolokwium 1','39','2020-12-01');
INSERT INTO `oceny`(`ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Nazwa`, `Punkty`, `Data_Wystawienia`) VALUES ('14','3','1','11','1','6','Kolokwium 1','81','2020-12-01');

INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('1','4','3','1','1','6','75','4','0','2021-03-01');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('2','3','3','1','1','6','58','3','0','2021-03-01');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('3','2','6','7','6','6','83','4.5','0','2018-06-20');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('4','8','3','6','5','5','68','3.5','0','2017-06-23');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('5','8','3','6','5','5','73','4','1','2004-09-05');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('6','9','6','3','2','6','53','3','1','2016-02-29');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('7','7','7','4','8','6','92','5','0','2008-02-20');
INSERT INTO `oceny_koncowe`(`ID`, `Student_ID`, `Przedmiot_Prowadzacy_ID`, `Przedmiot_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Punkty`, `Ocena`, `Poprawa`, `Data_Wystawienia`) VALUES ('8','9','4','2','2','6','62','3.5','1','2016-03-05');

INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('1','1','3','3','2020-07-02','4');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('2','2','6','6','2021-02-04','3');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('3','4','1','6','2018-06-30','5');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('4','8','5','5','2004-03-02','4.5');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('5','3','1','6','2020-08-04','5');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('6','10','3','3','2003-02-25','4');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('7','9','2','6','2018-07-18','5');
INSERT INTO `dyplom`(`ID`, `Student_ID`, `Student_Kierunek_ID`, `Student_Kierunek_Wydzial_ID`, `Data_Wystawienia`, `Ocena`) VALUES ('8','5','7','1','2009-09-11','3.5');


-- Widok 1
-- Srednia wystawianych punktow przez prowadzacych

SELECT Imie, Nazwisko, IFNULL(srednia_pkt,0) AS srednia  FROM prowadzacy p
LEFT JOIN (SELECT Przedmiot_Prowadzacy_ID, Avg(Punkty) AS srednia_pkt FROM oceny GROUP BY Przedmiot_Prowadzacy_ID) o
ON p.ID = o.Przedmiot_Prowadzacy_ID;


-- Widok 2
-- Na jakim kierunku w poszczególnych wydzialach studiuje najwiecej studentow

SELECT w.Nazwa, IFNULL(kie.Nazwa, 'brak') AS Nazwa_Kierunku, IFNULL(liczba,0) AS studenci FROM wydzial w 
LEFT JOIN (SELECT id, Nazwa,Wydzial_ID, ma AS liczba FROM kierunek w 
JOIN (SELECT *, MAX(liczba) AS ma FROM (SELECT Kierunek_Wydzial_ID, Kierunek_ID, Count(Kierunek_ID) AS liczba FROM student GROUP BY Kierunek_ID) l GROUP BY l.Kierunek_Wydzial_ID) ll
ON w.id = ll.Kierunek_ID) kie
ON w.ID = kie.Wydzial_ID ORDER BY studenci DESC;


-- Widok 3
-- Ilosc osob, ktorzy ukonczyli dany kierunek po 2009 roku

SELECT Nazwa, IFNULL(l.Ilosc,0) AS Liczba FROM kierunek k
LEFT JOIN (SELECT Student_Kierunek_ID, Data_Wystawienia, COUNT(Student_Kierunek_ID) AS Ilosc FROM dyplom d GROUP BY Student_Kierunek_ID HAVING YEAR(Data_Wystawienia) > 2009) l
ON k.id = l.Student_Kierunek_ID ORDER BY Liczba DESC;


-- Widok 4
-- Ilosc poprawek u danego prowadzacego 

SELECT Imie, Nazwisko, Stopien_Naukowy, Stanowisko, IFNULL(liczba,0) AS Ilosc FROM prowadzacy p
LEFT JOIN (SELECT Przedmiot_Prowadzacy_ID, COUNT(Przedmiot_Prowadzacy_ID) as liczba FROM oceny_koncowe WHERE Poprawa = 1 GROUP BY Przedmiot_Prowadzacy_ID) l
ON p.ID = l.Przedmiot_Prowadzacy_ID ORDER BY Stanowisko;


-- Widok 5
-- Studenci, ktorzy zdali kolowium 1 z Analizy matematycznej w 2020r. na kierunku Matematyka

SELECT s.imie, s.nazwisko, o.punkty, o.nazwa, p.nazwa AS przedmiot, k.nazwa AS kierunek, o.Data_Wystawienia
FROM ((student s JOIN oceny o ON s.id=o.student_id) JOIN przedmiot p ON o.przedmiot_id=p.id) JOIN kierunek k ON o.Student_Kierunek_ID=k.ID
WHERE o.nazwa="Kolokwium 1" AND o.przedmiot_id=1 AND o.Student_Kierunek_ID=1 AND o.Data_Wystawienia like '%2020%'
HAVING o.punkty>50;


-- Widok 6
-- Prowadzacy posortowani po nazwisku w sposob alfabetyczny z informacja o ilosci studentow, ktorym wystawili ocene koncowa

SELECT b.przedmiot_Prowadzacy_ID AS ID_prowadzacego, p.Imie, p.Nazwisko, COUNT(b.przedmiot_Prowadzacy_ID) AS ile_studentow
FROM (SELECT Student_ID, przedmiot_Prowadzacy_ID FROM oceny_koncowe GROUP BY przedmiot_Prowadzacy_ID, Student_ID) b JOIN prowadzacy p ON b.przedmiot_Prowadzacy_ID=p.ID
GROUP BY b.przedmiot_Prowadzacy_ID
ORDER BY p.Nazwisko;


-- Widok 7
-- Najwyzsza srednia wsrod studentow, ktorzy otrzymali dyplom

SELECT s.id, s.imie, s.nazwisko, s.nr_indeksu, AVG(o.Ocena) AS srednia
FROM student s JOIN oceny_koncowe o ON s.id=o.Student_ID
WHERE s.id IN (SELECT d.Student_ID FROM dyplom d)
GROUP BY o.Student_ID
ORDER BY srednia DESC
LIMIT 1;


-- Widok 8
-- Ilosc kierunkow oraz studiujacych studentow na danym wydziale

SELECT a.nazwa_wydzialu, a.ile_studentow, b.ile_kierunkow
FROM (SELECT w.ID AS id_wydzialu, w.Nazwa AS nazwa_wydzialu, IFNULL(COUNT(s.id),0) AS ile_studentow FROM student s JOIN wydzial w ON s.Kierunek_Wydzial_ID=w.ID GROUP BY w.ID) a JOIN
(SELECT w.ID, w.Nazwa, IFNULL(COUNT(k.ID),0) AS ile_kierunkow FROM kierunek k JOIN wydzial w ON k.Wydzial_ID=w.ID GROUP BY k.Wydzial_ID) b ON a.id_wydzialu=b.ID
UNION
SELECT w.nazwa, 0, 0
FROM wydzial w
WHERE w.Nazwa NOT IN (SELECT a.nazwa_wydzialu
FROM (SELECT w.ID AS id_wydzialu, w.Nazwa AS nazwa_wydzialu, IFNULL(COUNT(s.id),0) AS ile_studentow FROM student s JOIN wydzial w ON s.Kierunek_Wydzial_ID=w.ID GROUP BY w.ID) a JOIN
(SELECT w.ID, w.Nazwa, IFNULL(COUNT(k.ID),0) AS ile_kierunkow FROM kierunek k JOIN wydzial w ON k.Wydzial_ID=w.ID GROUP BY k.Wydzial_ID) b ON a.id_wydzialu=b.ID);
