/******************************************************************************/
/* Otrzymanie reprezentacji binarnej dla liczb dodatnich:                     */
/* "Prostym sposobem znajdowania kolejnych cyfr rozwiecia dwojkowego dodat-   */
/*  niej liczby dziesietnej jest znajdowanie reszt z kolejnych dzielen przez  */
/*  dwa ciagu liczb zaczynajacego od liczby, ktorej rozwiniecia dwojkowego    */
/*  poszukujemy, a kolejne wyrazy sa czesciami calkowitymi z poprzednich      */
/*  dzielen."                                                                 */
/* Otrzymanie reprezentacji binarnej dla liczb ujemnych (w kodzie uzupelnie-  */
/*  niowym do dwoch):                                                 */
/*            "Propedeutyka informatyki" Wl.Turski, PWN, 1975                 */
/* "Bity  liczby binarnej (otrzymanej z  modulu ujemnej liczby dziesietnej    */
/*  nalezy przegladac zaczynajac od prawej pozycji. Dla kazdego 0 az do       */
/*  piewrszej jedynki nalezy przepisac 0. Pierwsza napotkana jedynke nalezy   */
/*  przepisac bez zmian. Nastepnie dla kazdego napotkanego 0 nalezy napisac 1,*/
/*  a dla kazdego 1 napisac 0. Po przepisaniu wartosci modulu nalezy zapis    */
/*  poprzedzic jedynka oznaczajaca liczbe ujemna."                            */
/*      "Arytmetyka maszyn cyfrowych" Ivan Flores, WNT, 1970                  */
/* Wersja: Data:       Opis:                                                  */
/*   1           1999                                                         */
/*   2     19.08.2003  Uporzadkowanie komentarza objasniajacego               */
/*                                                                            */
/* Autor:                                                                     */
/*   R.Sobczak  Katedra Matematyki Dyskretnej Wydzial FTiMS PG                */
/******************************************************************************/
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

int dziesietna, bit, i = 15;
char binarna[17] = {'0','0','0','0','0','0','0','0',\
		    '0','0','0','0','0','0','0','0','\0'};
		    // lancuch znakowy z liczba binarna
int nie_bylo_jedynki;

void main () {
  printf("Podaj liczbe dziesietna : ");
  scanf("%d",&dziesietna);

  if (dziesietna >= 0) { // liczba dziesietna jest dodatnia lub rowna zero
    do {
      bit = dziesietna % 2;
      binarna[i--] = bit+'0';
      dziesietna >>= 1;
    } while (dziesietna);
  }
  else {          // liczba dziesietna jest ujemna
    dziesietna = abs(dziesietna);
    nie_bylo_jedynki = 1;
    do {
      bit = dziesietna % 2;
      if (nie_bylo_jedynki) { // czy byla juz jedynka w liczbie binarnej
	if (bit)   // czy trafiono na jedynke
	    nie_bylo_jedynki = 0;
      }
      else {
	if (!bit)    // zanegowanie bitu
	     bit = 1;
	else bit = 0;
      };
      binarna[i--] = bit+'0';
      dziesietna >>= 1;
    } while (dziesietna);

    while (i >= 0)
      binarna[i--] = '1';
  }; // if

  // Wyswietlenie liczby binarnej
  printf("Liczba binarna          : %s\n",binarna);
  system("pause");
}
