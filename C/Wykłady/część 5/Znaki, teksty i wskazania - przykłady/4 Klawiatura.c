/******************************************************************************/
/*  Zastosowanie funkcji getchar(), putchar() w konstruowaniu warunkow        */
/*    zakonczenia petli. Szyfr Cezara                                         */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*  Wersja 1.10 Ryszard Sobczak 2017-04-09                                    */                                                   
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include "stdio.h"
#include <stdlib.h>

int c;

int main(){
  // Petla z warunkiem wyjsciowym
/*
  do {
    c = getchar(); // wprowadzanie znakow do bufora (pamieci) klawiatury i
                   // wczytywanie pojedynczych znakow z bufora klawiatury
                   // po wcisnieciu ENTER. Przy wpisywaniu mozliwosc
                   // kasowania znakow z bufora. Pojemnosc bufora ok.
                   // 135 znakow. Funkcja wczytuje tylko znaki klawiatury
	 							   // podstawowej.
    putchar(c);
  } while (c != 'a');
*/

/*
  // Petla z warunkiem wejsciowym
  c = getchar();
  while (c != 'a') {
    putchar(c);
    c = getchar();
  };
*/

/*
  // Konstrukcja rownowazna poprzedniej
  while ((c = getchar()) != 'a') {
    putchar(c);
  };
*/

/*
  // Wyswietlanie do konca wczytanego ciagu znakow
  while ((c = getchar()) != '\n') {
    putchar(c);
  };
  putchar(c);

*/

  // Na wczytanych znakach moga byc wykonywane operacje arytmetyczne
  // dopuszczalne dla liczb calkowitych. Szyfr Cezara
  while ((c = getchar()) != '\n') {
    putchar(c + 3);
  };
  putchar(c);    
  return 0;
}
