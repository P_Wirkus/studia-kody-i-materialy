/******************************************************************************/
/* Zamiana lancucha znakow na wartosc liczbowa                                */
/* Przyklady zastosowania funkcji atoi(), strtol(), atof() i strtod()         */
/* z biblioteki string.h do kontrolowania typu wczytywanych warto�ci.         */
/*                                                                            */
/* Wersja 1.0 2002 Ryszard Sobczak                                            */
/* Wersja 1.1 2017 Ryszard Sobczak - bez funkcji z biblioteki conio.h         */
/******************************************************************************/
// Przykladowe dane 123, 12e3, 89f3, 7kl8

#include <stdio.h>
#include <stdlib.h>

char* w;
char s[20];
int i,c;
char* blad;
long l;
double d;

int main() {
  /* Odczytanie tekstu z wartoscia liczbowa */
  w = &s[0]; // albo w = s;
  while (((c = getch()) != '\r') && ((w-s)<20)) {
    putch(c);
    *w = (char) c;
    w++;
  };
  printf("\n");
  
  /* Zamiana na liczbe calkowita */
  i = atoi(s);
  printf("atoi - calkowita            : %d\n",i);

  l = strtol(s,&blad,10);
  if (*blad == 0) 
    printf("strtol(,,10) - calkowita    : %ld\n",l);
  else
    printf("strtol(,,10)                : blad - %c znak nr %ld\n", 
		  *blad, blad - s + 1);

  /* Zamiana przy zalozeniu, ze byla to liczba szestnastkowa */
  l = strtol(s,&blad,16);  
  if (*blad == 0) {
	  printf("strtol(,,16) - dziesietnie  : %ld\n",l);
    printf("             szestnastkowo  : 0x%x\n",(int)l); 
  }
	else
    printf("strtol(,,16)                : blad - %c znak nr %ld\n",
		  *blad, blad - s + 1);

  /* Zamiana na liczbe zmiennoprzecinkowa */
  d = atof(s);
  printf("atof   - zmiennoprzecinkowa : %f\n",d);

  d = strtod(s,&blad);
  if (*blad == 0)
    printf("strtod - zmiennoprzecinkowa : %f\n",d);
  else
	  printf("strtod                      : blad - %c znak nr %ld\n",
		  *blad, blad - s + 1);

  system("pause");
  return 0;
}

