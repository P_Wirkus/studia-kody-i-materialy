/******************************************************************************/
/*  Przyklady stosowania podstawowych funkcji z biblioteki string.h           */
/*                                                                            */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 1.10 Ryszard Sobczak 2017-04-09                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdio.h>
#include <string.h>

char tekst[20];
char slowo1[10];
char slowo2[10];
int i;

int main() {
  printf ("\nPodaj tekst 1 : ");
  scanf("%s",slowo1);  // mozna tez     scanf("%s",&slowo1[0]);
                       // ale nie mozna scanf("%s",slowo[0]);

  //Kopiowanie tekstu (lancuchow znakowych)
  strcpy(tekst,slowo1);
  printf("%s\n",tekst);
  
  //Laczenie tekstow (lancuchow znakowych)
  strcat(tekst," ");  //nie mozna stosowac strcat(tekst,' ');
  printf("%s\n",tekst);

  printf ("Podaj tekst 2 : ");
  scanf("%s",slowo2);

  //Laczenie tekstow (lancuchow znakowych)
  strcat(tekst,slowo2);
  printf("%s\n",tekst);

  //Porownywanie tekstow
  i = strcmp(slowo1,slowo2);
  if (i < 0)
    printf("Slowo 1 < slowo 2\n");
  if (i == 0)
    printf("Slowo 1 = slowo 2\n");
  if (i > 0)
    printf("Slowo 1 > slowo 2\n");

  //Porownywanie dlugosci tekstow
  if (strlen(slowo1) > strlen(slowo2))
    printf("Slowo 1 dluzsze niz slowo 2\n");
  if (strlen(slowo1) == strlen(slowo2))
    printf("Slowo 1 i slowo 2 rownej dlugosci\n");
  if (strlen(slowo1) > strlen(slowo2))
    printf("Slowo 1 krotsze niz slowo 2\n");

  //Poszukiwanie polozenia znaku w tekscie
  printf("Spacja to tekst[%ld]",strchr(tekst,' ') - tekst);
  return 0;
}
