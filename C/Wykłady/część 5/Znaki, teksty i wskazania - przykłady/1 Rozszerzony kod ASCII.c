/******************************************************************************/
/*  Znaki na ekranie i wartosci przypisane klawiszom.                         */
/*  Program wy�wietla na ekranie znaki odpowiadajace kolejnym wartosciom      */
/*   liczbowym.                                                               */
/*   Program dziala poprawnie tylko w srodowisku MS DOS i Windows             */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*  Wersja 1.01 Ryszard Sobczak 2007-04-20                                    */                                                   
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdlib.h>
#include <stdio.h>

int main () {
  const int N = 255;
  int i;
  
  printf("%c",7);
  for (i = 0; i <= N; i++) {
    if (!(i % 8)) 
      printf("\n");         // przejscie do nowej linii na ekranie co 8 wartosci
	  if ((i == 7)  ||                                               // BL dzwonek
	      (i == 8)  ||                                     // BS cofniecie kursora
	      (i == 9)  ||                                     // HT tabulacja pozioma
	      (i == 10) ||                          // LF przejscie do nastepnej linii
	      (i == 13) )                                         // CR poczatek linii
	    printf (" (%2X   )",i);                   // tylko wartosci heksadecymalne                           
	  else 
      printf (" (%2X  %c)",i,i);// wyswietlenie wartosci heksadecymalnej i znaku
  };
  printf("\n");
  printf("Zadanie czesciowe: oddac lancuch i lodz\n");
  printf("%c%cdanie cz%c%cciowe: odda%c %ca%ccuch i %c%cd%c\n",
	        0xbd,0xa5,0xa9,0x98,0x8f,0x88,0xe4,0x88,0xa2,0xab);
  system("PAUSE");
  printf("\a");
  return 0;
}

