/******************************************************************************/
/*  Iteracyjny algorytm obliczania pola sinusoida y=sin(x)                    */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux:                         */
/*  Wersja 1.0 Ryszard Sobczak 2007-08-16                                     */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double S,x,dx;
int i,n;

int main() {
  printf("Podaj liczbe trapezow:");
  scanf("%d",&n);
  dx = M_PI/(2*n);
  printf("dx=%lf\n",dx);
  S = 0;
  x = 0;
  for (i = 0 ; i < n; i++){
    S = S + dx*(sin(x)+sin(x+dx))/2;
    x = x + dx;
  };
  printf("Pole = %lf\n",S);
  system("pause");   
	return 0;
}
