/******************************************************************************/
/* Obliczenia iteracyjne. Obliczanie wartosci wielomianu                      */
/*   w=1+x+x^2+x^3+..+x^25                                                    */
/*  Wersja 1.0 Ryszard Sobczak 2007-08-16                                     */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i Matematyki Stosowanej  */
/******************************************************************************/
#include <stdio.h>
#include <math.h>

int   i;
float x;
double w;

int main() {
  printf("Podaj x: ");
  scanf("%f",&x);
  // Obliczenia z wielokrotnym potegowaniem
  w = 1;
  for (i=1;i <= 25;i++)
    w+=pow(x,i);  //w=1+x+x^2+x^3+..+x^25
  printf("%f\n",w);
  // Obliczenia z zastosowaniem schematu Hornera
  //   (bez potegowania)
  w = 1;
  for (i=1; i<= 25; i++)
    w = 1 + x*w; //w=1+x+x^2+x^3+..+x^25
  printf("%f\n",w);
  system("pause");
  return 0;
}





















