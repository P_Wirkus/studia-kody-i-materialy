/******************************************************************************/
/* Poszukiwanie maksimum funkcji metoda przegladania zbioru  argumentow       */
/* Wersja 1.0 Ryszard Sobczak 08.2006                                         */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

double y;                                             // biezaca wartosc funkcji
float a;                                             // wspolczynnik a w funkcji
float x,                                    // biezaca wartosc argumentu funkcji
     dx;                                            // zmiana wartosci argumentu 
double max, xm;                                 // znaleziona wartosc maksymalna 
                                                 // i odpowiadajacy jej argument
int main() {
  // 1. odczytanie z klawiatury warto�ci wsp�czynnika a,
  printf ("Podaj wspolczynnik a                         : ");
  scanf("%f",&a);
  // 2. odczytanie z klawiatury warto�ci odst�pu dx
  do {
    printf ("Podaj wartosc zmiany argumentu               : ");
    scanf ("%f",&dx);
    if (dx <=0 )
      printf("Liczba powinna byc wieksza od zera !!!\n");
  } while (dx <= 0);
  // 3. ustawienie pocz�tku przegl�dania
  xm  = 0;
  x = 0;
  max = -1e36;                                 // najmniejsza wartosc typu float
  do {
    // 4. wyznaczenie warto�ci funkcji
    y = x*(a - x);
    // 5. sprawdzanie czy otrzymana warto�� y jest wi�ksza 
    //    ni� dotychczasowa maksymalna warto�� funkcji max,
    if (y > max) {
      // 6. je�li jest wi�ksza to zapami�ta� t� warto�� funkcji
      //    i odpowiadaj�c� jej warto�� argumentu x.
      max = y;
      xm  = x;
    };
    // 7. zwi�kszenie argumentu funkcji o warto�� dx
    x += dx;                   
    // 8. sprawdzenie czy nie przekroczono zakresu przegl�dania
    // 9. je�eli zakres przegl�dania nie zosta� przekroczony,
    //    to do  przej�� do punktu 4.
  } while (x <= a);
  // 10. je�eli zakres zosta� przekroczony,  to wy�wietli�
  //     wynik poszukiwania
  printf ("Wartosc maksymalna %f w punkcie %f\n", max, xm);
  system("PAUSE");	
  return 0;
}

