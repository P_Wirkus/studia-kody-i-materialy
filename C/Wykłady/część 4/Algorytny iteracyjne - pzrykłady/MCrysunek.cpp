/*****************************************************************************/
/*  Zastosowanie metody Monte Carlo - ilustracja                             */
/*  Ryszard Sobczak                                                          */
/*  Katedra Rachunku Prawdopodobienstwa i Biomatematyki                      */
/*  Wydzial Fizyki Technicznej i Matematyki Stosowanej                       */
/*  Politechnika Gda�ska                                                     */
/*  2016.04.13                                                               */
/*****************************************************************************/
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <time.h>

#define K RAND_MAX

/*  Declare Windows procedure  */
LRESULT CALLBACK WindowProcedure (HWND, UINT, WPARAM, LPARAM);

/*  Make the class name into a global variable  */
char szClassName[ ] = "WindowsApp";

int WINAPI WinMain (HINSTANCE hThisInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpszArgument,
                    int nFunsterStil)

{
    HWND hwnd;               /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */

    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof (WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hIconSm = LoadIcon (NULL, IDI_APPLICATION);
    wincl.hCursor = LoadCursor (NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    /* Use Windows's default color as the background of the window */
    wincl.hbrBackground = (HBRUSH) GetStockObject (WHITE_BRUSH) ;

    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx (&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    hwnd = CreateWindowEx (
           0,                   /* Extended possibilites for variation */
           szClassName,         /* Classname */
           "Windows App",       /* Title Text */
           WS_OVERLAPPEDWINDOW, /* default window */
           CW_USEDEFAULT,       /* Windows decides the position */
           CW_USEDEFAULT,       /* where the window ends up on the screen */
           600,                 /* The programs width */
           600,                 /* and height in pixels */
           HWND_DESKTOP,        /* The window is a child-window to desktop */
           NULL,                /* No menu */
           hThisInstance,       /* Program Instance handler */
           NULL                 /* No Window Creation data */
           );

    /* Make the window visible on the screen */
    ShowWindow (hwnd, nFunsterStil);

    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage (&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    /* The program return-value is 0 - The value that PostQuitMessage() gave */
    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
        
     HDC         hdc ;
     PAINTSTRUCT ps ;
     RECT        rect ;

    const int MINXE = 100;  // Odleglosc od brzegu ramki
    const int MINYE = 100;  // Odleglosc od brzegu ramki
    const int N = 400;      // Bok kwadratu
    long i, x, y, t = 0;
    char   Tekst[20];
    
    switch (message){                  
      case WM_PAINT :
        hdc = BeginPaint (hwnd, &ps) ;
        GetClientRect (hwnd, &rect) ;

        SetTextColor(hdc,RGB(0,0,255));
        TextOut(hdc,100, 50, "Wyznaczanie liczby PI metod� Monte Carlo (ESC i Down)",53);
        SelectObject(hdc, GetStockObject(DC_PEN));
        SetDCPenColor(hdc, RGB(0,0,255));
        // Kwadrat
        Rectangle(hdc,MINXE-1,MINYE-1,MINXE+N+1,MINYE+N+1);
        Rectangle(hdc,MINXE,MINYE,MINXE+N,MINYE+N);
        // Okrag
        Ellipse(hdc,MINXE-1,MINYE-1,MINXE+N+1,MINYE+N+1);
        Ellipse(hdc,MINXE,MINYE,MINXE+N,MINYE+N);
        SetDCPenColor(hdc, RGB(255,0,0));
        srand(time(NULL));
        for (i = 1; i <= K; i++){
         // Losowanie wspolrzednych kolejnego punktu
          x = rand()%N;
          y = rand()%N;
          if (sqrt((x-200)*(x-200) + (y-200)*(y-200)) <= 200){
            t++;
            SetDCPenColor(hdc, RGB(0,255,0));
            Rectangle(hdc,MINXE+x,MINYE+y,MINXE+x+2,MINYE+y+2);
          }
          else {
            SetDCPenColor(hdc, RGB(255,0,0));
            Rectangle(hdc,MINXE+x,MINYE+y,MINXE+x+2,MINYE+y+2);
          };
          if (i%10 == 0){
            sprintf(Tekst,"Wynik =  %10.8lf,\n",4.0*t/K);
            TextOut(hdc,150,520, Tekst,21);
          };
        };
        SetDCPenColor(hdc, RGB(0,0,0));
        EndPaint (hwnd, &ps) ;
        break;
      case WM_KEYDOWN:
           switch (wParam) {
                  case VK_DOWN:
                       InvalidateRect(hwnd,NULL,TRUE);
                       break;
                  case VK_ESCAPE:
                       SendMessage(hwnd,WM_DESTROY,0,0);
                       break;
           };
         break;
      case WM_DESTROY:
        PostQuitMessage (0);       /* send a WM_QUIT to the message queue */
        break;
      default:                      /* for messages that we don't deal with */
        return DefWindowProc (hwnd, message, wParam, lParam);
    }
    return 0;
}
