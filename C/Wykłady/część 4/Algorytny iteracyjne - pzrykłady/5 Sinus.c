/******************************************************************************/
/* Obliczanie funkcji sinus z rozwinieca jej w szereg                         */
/*   potegowy i z zastosowania schematu Hornera.                              */
/*                                                                            */
/* Wersja 1.0 Ryszard Sobczak 1999.10                                         */
/* Wersja 1.1 Tomasz Boinski, Adam Wojtkiewicz    2001.01                     */
/*          Poprawka w algorytmie realizujacym schemat Hornera                */
/* Wersja 1.2 Ryszard Sobczak 2002.11.28                                      */
/*          Uporzadkowanie komentarza                                         */
/* Wersja 1.3 Ryszard Sobczak 2004.12.13                                      */
/*          Dostosowanie do wymagan aplikacji konsolowej MS C++               */
/*            poprzez wstawienie stalej M_PI i usuniecie funkcji              */
/*            clrscr()                                                        */
/* Wersja 1.4 Ryszard Sobczak 2005.12.08                                      */
/*          Dostosowanie do srodowiska DevC++                                 */
/*                                                                            */
/* Wersja 1.5 Ryszard Sobczak 08.2006 Dostosowanie programu do tekstu         */
/*                                    skryptu                                 */
/*                                                                            */
/* Uwagi: Brak sprawdzania typu i zakresu wprowadznych wartosci               */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/*                                                                            */
/******************************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "math.h"

float  x,                                                        // Wartosc kata
       s;                                                     // Wartosc funkcji
int    i,                                       // Nr biezacej liczby skladnikow
       n;                                           // Liczba skladnikow szeregu

char  z;

void main() {
 do {
  system("cls");	
  printf("Podaj wartosc kata (od 0 do 90 stopni):");
  scanf("%f",&x);
  printf("sin(%5.2f)=%f         radianow=%f\n",x,sin(x*M_PI/180),x*M_PI/180);
  printf("Liczba         Wartosc             Roznica        Blad     \n");
  printf("iteracji     przyblizona                         wzgledny \n");
  x *=  M_PI / 180;                                        // zamiana na radiany
  for (n = 1; n <= 10; n++) {                       // liczba skladnikow szeregu

    s =1;                                                          // wartosc S0 
    for (i=0;i<=n-1;i++)                        
      s = 1-s*x*x/(2*(n-i)*(2*(n-i)+1));                  // kolejne wartosci Si

    s *= x;
    printf ("%2d           %f             %7.4f   ",n,s,sin(x)-s);
    if (sin(x)!=0.0) {
      printf("     %8.4f%\n",fabs(100*(sin(x)-s)/sin(x)));
    }
    else {
	    printf("dzielenie przez zero\n");
    };
  };
  printf("Czy kontynuowac (1 - tak /2 - nie)?");
  scanf("%d",&z);
 } while (1 == z);
}
