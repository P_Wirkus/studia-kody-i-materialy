/******************************************************************************/
/*  Wyznaczanie najwiekszego wspolnego podzielnika dwoch liczb                */
/*   naturalnych bez stosowania dzielenia (przypadek b).                      */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
                                                                        
#include <stdio.h>

int main() {

  int a,b;                // liczby o nieznanym najwiekszym wspolnym podzielniku

  printf("Podaj pierwsza liczbe             : ");
  scanf("%d",&a);
  printf("Podaj druga liczbe (!= pierwszej) : ");
  scanf("%d",&b);

  do {
    if (a > b) 
      a = a - b;
    else
      b = b - a;
    printf("a = %d, b = %d\n",a,b);
  } while (a != b);
  printf("Najwiekszy wspolny podzielnik = %d\n",a);
  
  system("PAUSE");
  return 0;
}

