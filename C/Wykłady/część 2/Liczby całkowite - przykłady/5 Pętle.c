/************************************************************************/
/* Rozne rodzaje petli z zastosowaniem do obliczania wartosci sumy      */
/*   kolejnych liczb naturalnych od 1 do n.                             */
/* Wersja 1.0 2005.12    Ryszard Sobczak                                */
/* Wersja 1.1 2017.04.09 Ryszard Sobczak                                */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                  */
/*    Matematyki Stosowanej. Zaklad Matematyki Dyskretnej. R.Sobczak    */
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int n,  // Liczba skladnikow
    i;  // Numer skladnika
    
long s; // Poszukiwania suma

int main () {

  printf ("Podaj liczbe skladnikow :");
  scanf("%d",&n);
  printf ("\n");
  
  // 1. wariant  
  s = 0;
  for (i=1; i<=n; i++)
    s += i;  //  rownowazne s = s + i;
  printf ("Wariant 1: suma = %d\n", s);

  // 2 . wariant
  s = 0;
  i = 1;
  while (i<=n) {
    s += (i++);  //  rownowazne s = s + i++; albo poleceniu zlozonemu:
                 //    s = s+i; 
                 //    i++;
  };
  printf ("Wariant 2: suma = %d\n", s);

  // 3. wariant
  s = 0;
  i = 1;
  do {
    s += (i++);  //  rownowazne s = s + i++; albo poleceniu zlozonemu:
                 //    s = s+i; 
                 //    i++;
  } while (i <=n);
  printf ("Wariant 3: suma = %d\n", s);

  // Sprawdzenie
  printf ("\n");
  printf ("Test = %d\n", n*(n+1)/2); // ciag arytmetyczny
  system("pause");  
}
