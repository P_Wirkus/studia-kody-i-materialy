/******************************************************************************/
/* Program 1_P1_9.cpp                                                           */
/*   Przyklad 1.9: Rozwiazywanie rownania kwadratowego (cz.1)                 */
/*   - obliczanie wartosci wyroznika rownania kwadratowego                    */
/*                                                                            */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*                                                                            */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*                                                                            */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/*                                                                            */
/******************************************************************************/
  // Po znakach //mozna do konca linii pisac swoje komentarze (obja�nienia)
	#include <iostream>																		// dyrektywa kompilatora

	using namespace std;
	
	float a,b,c,d; 										 //deklaracja zmiennych zmiennoprzecinkowych
	int main()     																							 // funkcja glowna
   {
		// Wczytanie wspolczynnikow rownania
		cout << "Podaj a:"; 																	 //wyswietlanie tekstu
		cin >> a;																 // wczytanie wartosci wspolczynnika
		cout << "Podaj b:";
		cin >> b;
		cout << "Podaj c:";
		cin >> c;
	
		d = b*b - 4*a*c; 																			//obliczenie wyroznika
		cout << "Delta = " << d << endl;
		system("PAUSE");
		return 0;
   }
