/******************************************************************************/
/*  Ilustracja sposobu formatowania liczb przy ich wyswietlaniu               */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

void main() {
  long   i = -297646;

  // Liczby calkowite   
  printf("  %8d  %8i %8u\n",i,i,i);
  printf("%010d\n",i);
  printf("  %*d\n",8,i);
  printf("x%x\nx%X\no%o\n",-i,-i,-i);
}

