/******************************************************************************/
/*  Wyznaczanie najwiekszego wspolnego podzielnika dwoch liczb                */
/*   naturalnych bez stosowania dzielenia (przypadek a).                      */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
                                                                        
#include <stdio.h>

int main() {

  int a,b;             // liczby o nieznanym najwiekszym wspolnym podzielniku
  int r;             

  printf("Podaj pierwsza liczbe : ");
  scanf("%d",&a);
  printf("Podaj druga liczbe    : ");
  scanf("%d",&b);

  do {
    r = a % b;
    printf("a=%d, b=%d, r=%d\n",a,b,r);  // do testowania
    a = b;
    b = r;
  } while (r > 0);
 
  printf("Najwiekszy wspolny podzielnik = %d\n",a);
  system("PAUSE");
  return 0;
}
