/******************************************************************************/
/*  Wyznaczanie najwiekszego wspolnego podzielnika dwoch liczb                */
/*   naturalnych bez stosowania dzielenia (przypadek c).                      */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
                                                                        
#include <stdio.h>

int main() {

  int a,b;                // liczby o nieznanym najwiekszym wspolnym podzielniku

  printf("Podaj pierwsza liczbe : ");
  scanf("%d",&a);
  printf("Podaj druga liczbe    : ");
  scanf("%d",&b);

  while (a != b) {
    if (a > b) 
      a = a - b;
    else
      b = b - a;
  };
  
  printf("Najwiekszy wspolny podzielnik = %d\n",a);
  system("PAUSE");
  return 0;
}
