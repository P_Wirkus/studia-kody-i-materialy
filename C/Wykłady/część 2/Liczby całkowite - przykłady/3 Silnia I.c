/******************************************************************************/
/*  Ograniczenie zakresu obliczania symbolu n!                                */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2017-03-02                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/******************************************************************************/
#include <stdio.h>

int main() {
  int silnia;

  // Krok 1 (13, 15, 17)
  silnia = 1*2*3*4*5*6*7*8*9*10*11*12;
  printf("Krok 1 12! = %d\n",silnia);  
  silnia = silnia*13;
  printf("Krok 1 13! = %d\n",silnia);  
  silnia = silnia*14*15;
  printf("Krok 1 15! = %d\n",silnia);  
  silnia = silnia*16*17;
  printf("Krok 1 17! = %d\n",silnia);  

  // Krok 2
  silnia = 1;
  silnia = silnia * 2;
  silnia = silnia * 3;
  silnia = silnia * 4;
  silnia = silnia * 5;
  silnia = silnia * 6;
  silnia = silnia * 7;
  silnia = silnia * 8;
  silnia = silnia * 9;
  silnia = silnia * 10;
  silnia = silnia * 11;
  silnia = silnia * 12;
  printf("Krok 2 12! = %d\n",silnia);  

  unsigned long long ll_silnia;
  int i; 
  const int N = 20;
  
  ll_silnia = 1;
	for (i = 1; i <= N; i++)
    ll_silnia *= i;  // ll_silnia = ll_silnia * i;
  printf("Krok 3 %2d! = %llu\n\n",N,ll_silnia);  

  int n;
  ll_silnia = 1; 
  printf("Podaj n: ");
  scanf("%d",&n);  
  for (i = 1; i <= n; i++){
    ll_silnia *= i;
    printf("T1: %2d! = %llu\n",i,ll_silnia);  
  };
	printf("Krok 3 %2d! = %llu\n",n,ll_silnia);  

  return 0;
}
