/******************************************************************************/
/* Program 1_P1_8.c                                                           */
/*   Przyklad 1.8: Rozwiazywanie rownania kwadratowego (cz.1)                 */
/*   - obliczanie wartosci wyroznika rownania kwadratowego                    */
/*                                                                            */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*                                                                            */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*                                                                            */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/*                                                                            */
/******************************************************************************/

#include <stdio.h>   
                                                        // dyrektywa kompilatora

float a,b,c,d;                      // deklaracja zmiennych zmiennoprzecinkowych
int main()                                                     // funkcja glowna
{        
  // Wczytanie wspolczynnikow rownania
  printf("Podaj wspolczynnik a:");                        // wyswietlanie tekstu
  scanf("%f",&a);                                          // wczytanie wartosci
  printf("Podaj wspolczynnik b:");
  scanf("%f",&b);
  printf("Podaj wspolczynnik c:");
  scanf("%f",&c);
  d = b*b - 4*a*c;                                       // obliczenie wyroznika
  printf("Delta = %10.7f\n",d);                         // wyswietlenie wartosci
  system("PAUSE");
  return 0;
}
