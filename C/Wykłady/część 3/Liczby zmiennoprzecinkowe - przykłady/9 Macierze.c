/******************************************************************************/
/*  Mnozenie macierzy                                                         */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.0 Ryszsard Sobczak 2017-03-20                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define M 7      // 1. wersja
#define M 3      // 2. wersja

int main() {
	double a[M][M], b[M][M], c[M][M];
	
	int i,j,k;
	int n;  // 2. wersja
	
	FILE *dane;
	FILE *wyniki;

  dane = fopen("9 Macierze_dane.txt","rb");
  fscanf(dane,"%d",&n);  // 2. wersja
  printf("Macierz A\n");
  for (i = 0; i < n; i++){   // wiersze
    for (j = 0; j < n; j++){ // kolumny
      fscanf(dane,"%lf",&a[i][j]);
    };
  };
  printf("Macierz B\n");
  for (i = 0; i < n; i++){   // wiersze
    for (j = 0; j < n; j++){ // kolumny
      fscanf(dane,"%lf",&b[i][j]);
    };
  };
  fclose(dane);
  
  for (i = 0; i < n; i++){   // wiersze
    for (j = 0; j < n; j++){ // kolumny
      c[i][j] = 0;
      for (k = 0; k < n; k++){
        c[i][j] = c[i][j] + a[i][k]*b[k][j];
      };
    };
  };

	  printf("Macierz C\n");
  wyniki =fopen("9 Macierze_wynik.csv","w");
  for (i = 0; i < n; i++){   // wiersze
    for (j = 0; j < n; j++){ // kolumny
      fprintf(wyniki,"%6.2lf; ",c[i][j]);
    };
    fprintf(wyniki,"\n");
  };
  fclose(wyniki);
  system("pause");
	return 0;
}

