/******************************************************************************/
/*  Ilustracja sposobu odczytania pliku zawierajacego wspolrzedne wektorow    */
/*    i wyznaczenie wartosci iloczyny skalarnego tych wektorow.               */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2017-03-20                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
  int i,n;
  double a[1000],b[1000], Sk;
  FILE *plik_we;
  
  plik_we = fopen("8 Wektory_dane.txt","rb");
  if (plik_we != NULL) {
	  // Odczytanie wymiaru przestrzeni
	  fscanf(plik_we,"%d",&n);
		// Odczytanie wspolrzednych wektora
	  for (i = 0;i < n; i++) {
		  fscanf(plik_we,"%lf",&a[i]);
		};
	  for (i = 0;i < n; i++) {
		  fscanf(plik_we,"%lf",&b[i]);
		};
    Sk = 0;
		for (i = 0; i < n; i++)
		  Sk += a[i]*b[i];

	  // Wyznaczenie dlugosci wektora
	  printf("Iloczyn skalarny wektora a i b = %e", Sk);
  }
  else
    printf("Brak pliku\n");
	return 0;
}

