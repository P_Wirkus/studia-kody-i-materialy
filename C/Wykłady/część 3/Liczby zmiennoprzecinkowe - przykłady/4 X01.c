/******************************************************************************/
/*  Przyklad: Ilustracja roznicy w rozwinieciu dwojkowym i dziesietnym ulamkow*/
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.0 Ryszsard Sobczak 2017-03-20                                    */
/*                   - uproszczenie przyk�adu                                 */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdio.h>

double x = 0.0;
double y = 0.1;
//double y = 0.125;

void main() 
{
  printf("0.100  -> %50.48Lf\n",0.1);
  printf("0.125  -> %50.48Lf\n\n",0.125);

// Dwie operacje
  x = 0.0;
  printf("x=0.0    -> %50.48Lf\n",x);
  x += y;
  printf("x+=y     -> %50.48Lf\n",x);
  x -= y;
  printf("x-=y     -> %50.48Lf\n\n",x);

// Trzy operacje
  x = 0.0;
  printf("x=0.0    -> %50.48Lf\n",x);
  x += y;
  printf("x+=y     -> %50.48Lf\n",x);
  x += y;
  printf("x+=y     -> %50.48Lf\n",x);
  x += y;
  printf("x+=y     -> %50.48Lf\n",x);
  x -= y;
  printf("x-=y     -> %50.48Lf\n",x);
  x -= y;
  printf("x-=y     -> %50.48Lf\n",x);
  x -= y;
  printf("x-=y     -> %50.48Lf\n\n",x);

// Piec operacji
  x = 0.0;
  printf("x=0.0    -> %50.48Lf\n",x);
  x += y;
  printf("x+y      -> %50.48Lf\n",x);
  x += y;
  printf("x+y      -> %50.48Lf\n",x);
  x += y;
  printf("x+y      -> %50.48Lf\n",x);
  x += y;
  printf("x+y      -> %50.48Lf\n",x);
  x += y;
  printf("x+y      -> %50.48Lf\n",x);
  x -= y;
  printf("x-y      -> %50.48Lf\n",x);
  x -= y;
  printf("x-y      -> %50.48Lf\n",x);
  x -= y;
  printf("x-y      -> %50.48Lf\n",x);
  x -= y;
  printf("x-y      -> %50.48Lf\n",x);
  x -= y;
  printf("x-y      -> %50.48Lf\n\n",x);

// Pojedyncze dodawanie
  x = 0.0;
  printf("x=0.0    -> %50.48Lf\n",x);
  x = x + y + y + y;
  printf("x+y+..   -> %50.48Lf\n",x);
  x = x - y - y - y; 
  printf("x-y-..   -> %50.48Lf\n\n",x);

 // system("PAUSE");
}
