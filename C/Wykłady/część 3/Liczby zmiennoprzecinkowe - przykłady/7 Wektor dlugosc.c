/******************************************************************************/
/*  Wyznaczenie dlugosci wektora                                              */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.00 Ryszard Sobczak 2017-03-20                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
  int i,n;
  double a[1000], Sk;
  FILE *plik_we;

  plik_we = fopen("7 Wektor_dane.txt","rb");
  if (plik_we != NULL) {
	  // Odczytanie wymiaru przestrzeni
	  //printf("Podaj wymiar (n < 1000): ");
	  fscanf(plik_we,"%d",&n);
		// Odczytanie wspolrzednych wektora
    Sk = 0;
	  for (i=0;i<n;i++) {
		//	printf("Podaj wspolrzedna: ");
		  fscanf(plik_we,"%lf",&a[i]);
		  Sk = Sk + a[i]*a[i];
		};

//  Prostsze rozwiazanie
//	  Sk = 0;
//		for (i=0;i<n;i++)
//		  Sk = Sk + a[i]*a[i];
				
	  // Wyznaczenie dlugosci wektora
	  printf("Dlugosc = %lf", sqrt(Sk));
  }
  else
    printf("Brak pliku\n");
	return 0;
}

