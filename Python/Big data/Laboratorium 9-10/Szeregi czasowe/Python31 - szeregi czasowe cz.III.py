#Temat: szeregi czasowe

# In[1]:


#ładowanie bibliotek
from pandas import read_csv
from pandas import datetime
import random as rnd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sklearn.metrics as metr


# In[13]:


#ustawienia wykresów i wyświetlanie liczb zmiennoprzecinkowych
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rc('figure', figsize=(10, 6))
np.set_printoptions(precision=8, suppress=True)


# In[3]:


#tworzymy funkcję, która będzie interpretowała daty w zadany przez nas sposób
def parser(x):
    return datetime.strptime('200'+x, '%Y-%m')


# In[20]:


#wczytujemy plik z danymi
series = read_csv('dane/shampoo-sales.csv', header=0,
                  index_col=0, parse_dates=True, squeeze=True, date_parser=parser)
series


# In[26]:


len(series)


# In[44]:


#dokonujemy zmiany próbkowania szergu (upsample -> z miesięcy na dni)
upsampled = series.resample('D').mean()
print(upsampled.head(34))


# In[25]:


len(upsampled)


# In[42]:


type(upsampled)


# In[36]:


#do wypełnienia pustych wartości wykorzystujemy różne rodzaje interpolacji
#interpolated = upsampled.interpolate(method='linear')
interpolated = upsampled.interpolate(method='spline', order=3)
print(interpolated.head(34))


# In[38]:


#na koniec wykres
#interpolated.plot()
interpolated[0:66].plot()
plt.show()


# In[45]:


#możemy również zmniejszyć częstotliwość próbkowania szergu (downsample -> z miesięcy na kwartały lub lata)
#resample = series.resample('Q')
resample = series.resample('A')
resample


# In[46]:


#dla kwartałów wyznaczamy średnią, natomiast dla lat sumujemy dane
#quarterly_mean_sales = resample.mean()
yearly_sum_sales = resample.sum()
#quarterly_mean_sales.head()
yearly_sum_sales


# In[50]:


#na koniec wykres
#quarterly_mean_sales.plot()
yearly_sum_sales.plot()
plt.show()


# In[51]:


#transformacje szeregów czasowych w celu redukcji szumów i poprawieniu jakości sygnału
#Wczytanie danych
series = read_csv('dane/airline-passengers.csv', header=0,
                  index_col=0, parse_dates=True, squeeze=True)
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(series)
# histogram
plt.subplot(212)
plt.hist(series)
plt.show()


# In[42]:


#jak widzimy szereg czasowy jest niestacjonarny, czyli średnia i wariancja zmieniają się w czasie
#takie szeregi są problematyczne w zagadnieniach związanych z przewidywaniem, ponieważ zmienia się trend oraz sezonowść
series


# In[54]:


#przykład 1: trend zmienia się jak funkcja kwadratowa
series = [np.exp(i) for i in range(1,100)]
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(series)
# histogram
plt.subplot(212)
plt.hist(series)
plt.show()


# In[56]:


#Przykład 1
series = [np.exp(i) for i in range(1,100)]
#usunięcie trendu polega na spierwiastkowaniu szeregu
transform = np.log(series)
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(transform)
# histogram
plt.subplot(212)
plt.hist(transform)
plt.show()


# In[65]:


#poprzez pierwiastkowanie próbujemy zmienić trend na liniowy, a rozkład na gaussowski
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True,
                  squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = np.sqrt(dataframe['passengers'])
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# In[61]:


#jeżeli jednak trend rośnie wykładniczo możemy zlogarytmować szereg
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = np.log(dataframe['passengers'])
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# In[71]:


# transformacja Box-Cox jest transformacją parametryczną, w której sterujemy parametrem lambda
#w zależności od wartości tego parametru otrzymujemy różne przekształcenia szeregów
#np. lambda = 0.0 logarytmowanie, lambda = 0.5 pierwiastkowanie, lambda = 1.0 brak transformacji
from scipy.stats import boxcox
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = boxcox(dataframe['passengers'], lmbda=0.3)
plt.figure(1)
# line plot
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# In[73]:


series


# In[74]:


#możemy również automatycznie wyznaczyć najlepszy parametr lambda dla rozważanego szeregu
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'], lam = boxcox(dataframe['passengers'])
print('Lambda: %f' % lam)
plt.figure(1)
# line plot
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# In[75]:


# zastosowanie średniej kroczącej w celu przygotowania danych
#wczytanie danych
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

# wyznaczenie wartości średniej dla trzech wartości t-2, t-1 i t
rolling = series.rolling(window=3)
print(rolling)
rolling_mean = rolling.mean()
print(rolling_mean.head(10))

#wykres danych oryginalnych i po zastosowaniu średniej kroczącej
series.plot()
rolling_mean.plot(color='red')
plt.show()

# wykres części danych oryginalnych i po zastosowaniu średniej kroczącej
series[:100].plot()
rolling_mean[:100].plot(color='red')
plt.show()


# In[85]:


# średnia krocząca w celu tworzenia nowych cech szeregu (feature engineering)

#wczytanie danych
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
df = pd.DataFrame(series.values)
width = 3 #szerokość okna
lag1 = df.shift(1) #przesunięcie oryginalnego szeregu o jeden krok czasowy
lag3 = df.shift(width - 1) #przesunięcie oryginalnego szeregu o krok czasowy zależny od rozmiaru okna
window = lag3.rolling(window=width)
means = window.mean() #wyznaczenie średniej kroczącej

dataframe = pd.concat([means, lag1, df], axis=1) #połączenie kolumn w jedną ramkę danych
dataframe.columns = ['mean', 't', 't+1']
dataframe


# In[82]:


df.shift(1)


# In[93]:


#średnia krocząca jako naiwny podel predykcyjny
from sklearn.metrics import mean_squared_error

series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

# przygotowanie zmiennych
X = series.values
window = 3
history = [X[i] for i in range(window)]
test = [X[i] for i in range(window, len(X))]
predictions = list()

# wyznaczanie średniej kroczącej w celu obliczenia przyszłych wartości szeregu czasowego
for t in range(len(test)):
    length = len(history)
    yhat = np.mean([history[i] for i in range(length-window,length)])
    obs = test[t]
    predictions.append(yhat)
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))
rmse = np.sqrt(mean_squared_error(test, predictions))
print('Test RMSE: %.3f' % rmse)

# wykres
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()

# wykres z mniejszą liczbą danych
plt.plot(test[:100])
plt.plot(predictions[:100], color='red')
plt.show()


# In[99]:


#biały szum
#jeżeli szereg czasowy okaże się być białym szumem (całkowita losowość) wówczas rozsądna
#predykcja nie jest możliwa
#błąd predykcji szeregu czasowego powinien być białym szumem, wówczas jesteśmy pewni,
#że model został zbudowany na esencji sygnału
#szereg czasowy nie jest białym szumem jeżeli któreś z poniższych pytań jest prawdziwe:
#1. czy średnia jest niezerowa?
#2. czy wariancja zmienia się w czasie?
#3. czy wartości szeregu korelują ze sobą po przesunięciu o lag?

#przykład białego szumu

from random import gauss
from random import seed

seed(1)
series = [gauss(0.0, 1.0) for i in range(1000)]
series = pd.Series(series)

# statystyki
print(series.describe())

# wykres
series.plot()
plt.show()

# histogram
series.hist()
#plt.xlim(-10,10)
#plt.ylim(0,1)
plt.show()

# funkcja autokorelacji
pd.plotting.autocorrelation_plot(series)
plt.show()


# In[103]:


#błądzenie losowe (random walk)
#błądzenie losowe pozwala na zrozumienie przewidywalności szeregu czasowego

# tworzymy i rysujemy przykładowe błądzenie losowe
seed(123)
random_walk = list()
random_walk.append(-1 if rnd.random() < 0.5 else 1)
for i in range(1, 1000):
    movement = -1 if rnd.random() < 0.5 else 1
    value = random_walk[i-1] + movement
    random_walk.append(value)
plt.plot(random_walk)
plt.show()


# In[104]:


random_walk


# In[105]:


# funkcja autokorelacji (FA) błądzenia losowego
# FA pokazuje, że błądzenie losowe nie jest szeregiem stacjonarnym
pd.plotting.autocorrelation_plot(random_walk)
plt.show()


# In[106]:


# do wiarygodnego sprawdzenia stacjonarności szeregu służy rozszerzony test Dickey-Fullera (ADF)
# hipoteza zerowa testu to szereg czasowy jest niestacjonarny
# w wyniku testu dostajemy wartość większą od wszystkich krytycznych poziomów ufności
# zatem szereg czasowy jest niestacjonarny
from statsmodels.tsa.stattools import adfuller

result = adfuller(random_walk)
print('ADF Statistic: %f' % result[0])
print('p-value: %f' % result[1])
print('Critical Values:')
for key, value in result[4].items():
    print('\t%s: %.3f' % (key, value))


# In[135]:


#z szeregu, który jest błądzeniem losowym możemy stworzyć szereg stacjonarny

#skoro w błądzeniu losowym kolejne wartości zależą od poprzednich, usunięcie tych róźnic
#stworzy szereg stacjonarny
#szereg nie zawiera teraz żadnych struktur które można wykorzystać w uczeniu maszynowym
diff = list()
for i in range(1, len(random_walk)):
    value = random_walk[i] - random_walk[i - 1]
    diff.append(value)
# wykres różnic
plt.plot(diff)
plt.show()


# In[138]:


#i jeszcze FA
#wyraźnie widać, że korelacje są małe i poniżej 95% i 99% przedziału ufności
pd.plotting.autocorrelation_plot(diff)
plt.ylim(-0.5,0.5)
plt.show()


# In[107]:


#nie można przeprowadzić rozsądnej predykcji błądzenia losowego
#można jedynie wykonać naiwną predykcję bazując na najbliższych wartościach szeregu t-1 -> t

# dzielimy dane na zbiór uczący i testowy
train_size = int(len(random_walk) * 0.66)
train, test = random_walk[0:train_size], random_walk[train_size:]

# naiwna predykcja uwzględniająca zmianę między kolejnymi elementami równą 1
predictions = list()
history = train[-1]
for i in range(len(test)):
    yhat = history
    predictions.append(yhat)
    history = test[i]
rmse = np.sqrt(mean_squared_error(test, predictions))
print('RMSE: %.3f' % rmse)


# In[152]:


# naiwna predykcja uwzględniająca kierunek i wartość zmiany między kolejnymi elementami równą +/-1
predictions = list()
history = train[-1]
for i in range(len(test)):
    yhat = history + (-1 if rnd.random() < 0.5 else 1)
    predictions.append(yhat)
    history = test[i]
rmse = np.sqrt(mean_squared_error(test, predictions))
print('RMSE: %.3f' % rmse)

#naiwna predykcja to najlepsza i jedyna metoda, którą możemy zastosować dla błądzenia losowego


# In[163]:


#dekompozycja szeregów czasowych
#szereg czasowy można rozłożyć na dwa główne komponenty
#1. systematyczne - elementy szeregu, które są konsystentne lub rekurencyjne i mogą być 
#opisane oraz modelowane
#2. niesystematyczne - elementy szeregu, które nie mogą być bezpośrednio modelowane

#Komponenty systematyczne to: poziom (wartość średnia), trend (występuje opcjonalnie) i 
#sezonowość (występuje opcjonalnie)
#Komponent niesystematyczny to szum.

#Możemy rozważać dwa modele szeregów czasowych:
#1. addytywne (liniowe): poziom + trend + sezonowość + szum
#2. multiplikatywne (nieliniowe): poziom * trend * sezonowość * szum


# In[114]:


# przykład dekompozycji szeregu czasowego przy założeniu modelu addytywnego

from statsmodels.tsa.seasonal import seasonal_decompose
series = [i + rnd.randrange(10) for i in range(1,100)]
result_add = seasonal_decompose(series, model='additive', freq=1)
result_add.plot()
plt.show()


# In[112]:


#poszczególne składowe dekompozycji są zapisywane w odpowiednich zmiennych
print(result_add.trend)
print(result_add.seasonal)
print(result_add.resid)
print(result_add.observed)


# In[117]:


# przykład dekompozycji szeregu czasowego przy założeniu modelu multiplikatywnego

series = [i**2.0 for i in range(1,100)]
result_multi = seasonal_decompose(series, model='multiplicative', freq=1)
result_multi.plot()
plt.show()


# In[118]:


# dekompozycja rzeczywistego szeregu czasowego - model multiplikatywny
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True,
                  squeeze=True)
result = seasonal_decompose(series, model='multiplicative')
result.plot()
plt.show()


# In[119]:


# dekompozycja rzeczywistego szeregu czasowego - model addytywny
# sprawdźmy różnice pomiędzy prezentowanymi wynikami
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True,
                  squeeze=True)
result = seasonal_decompose(series, model='additive')
result.plot()
plt.show()


# In[120]:


type(series)


# In[ ]:





# In[166]:


#Usuwanie trendu z szeregu czasowego
#Możemy wyróżnić kilka cech trendów:
#1. Deterministyczne (monotonicznie rosnące lub malejące) i 
#stochastyczne (monotoniczność trendu się zmienia)
#2. Globalne i lokalne

#Szereg czasowy z trendem to szereg niestacjonarny.
#Usunięcie trendu z szeregu czasowego powoduje, że staje się on stacjonarny.

#Z punktu widzenia uczenia maszynowego usunięcie trendu zmniejsza zaburzenia
#pomiędzy danymi wyjściowymi i wyjściowymi.
#Z drugiej strony dodanie trendu może poprawić relację pomiędzy predyktorami a zmienną celu.


# In[121]:


#Usuwanie trendu poprzez różnicowanie szeregu czasowego

def parser(x):
    return datetime.strptime('200'+x, '%Y-%m')

series = read_csv('dane/shampoo-sales.csv', header=0, index_col=0, parse_dates=True,
                  squeeze=True, date_parser=parser)
X = series.values
print(np.mean(X))

#trend wznoszący jest wyraźnie widoczny na wykresie
plt.plot(X)
plt.show()


# In[122]:


# tworzymy nową zmienną, która jest różnicą dwóch kolejnych wartości szeregu
# new_x(t) = x(t) - x(t-1)
diff = list()
for i in range(1, len(X)):
    value = X[i] - X[i - 1]
    diff.append(value)

print(np.mean(diff))
#plt.plot(X)
plt.plot(diff)
plt.show()


# In[125]:


#Usuwanie trendu poprzez budowanie modelu regresyjnego 

from sklearn.linear_model import LinearRegression

# budujemy model regrsji liniowej
X = [i for i in range(0, len(series))]
X = np.reshape(X, (len(X), 1))
y = series.values
model = LinearRegression()
model.fit(X, y)


# In[126]:


# wyznaczamy trend liniowy korzystając z modelu
trend = model.predict(X)

# wykres danych i trendu
plt.plot(y)
plt.plot(trend)
plt.show()


# In[127]:


# usuwamy trend 
detrended = [y[i]-trend[i] for i in range(0, len(series))]

# rusujemy dane bez trendu
print(np.mean(detrended))
plt.plot(detrended)
plt.show()


# In[3]:


#Wyodrębnianie sezonowości

#Sezonowość może mieć znaczny wpływ na modelowanie szeregów czasowych
#1. Czysty sygnał - usunięcie sezonowości może poprawić związek pomiędzy
#danymi wejściowymi i wyjąciowymi
#2. Dodatkowe informacja - sezonowść może stać się kolejnym predyktorem

#Szereg czasowy z usuniętą sezonowością nazywamy sezonowo stacjnarnym.


# In[10]:


#wczytanie danych i wyświetlenie wykresu

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
plt.plot(series)
plt.show()


# In[13]:


series


# In[36]:


# usunięcie sezonowości poprzez różnicowanie dnia w roku do dnia w roku kolejnym
#uwaga na lata przestępne
X = series.values
diff = list()
days_in_year = 365
for i in range(days_in_year, len(X)):
    value = X[i] - X[i - days_in_year]
    diff.append(value)
plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[43]:


#może warto rozważyć sezonowść w odstępach miesięcznych
series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
resample = series.resample('M')
monthly_mean = resample.mean()
print(monthly_mean.head(18))
monthly_mean.plot()
plt.show()


# In[44]:


# usunięcie sezonowości poprzez różnicowanie miesiąca w roku do miesiąca w roku kolejnym

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
resample = series.resample('M')
monthly_mean = resample.mean()
X = series.values
diff = list()
months_in_year = 12
for i in range(months_in_year, len(monthly_mean)):
    value = monthly_mean[i] - monthly_mean[i - months_in_year]
    diff.append(value)
plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[53]:


series['1983-05']


# In[34]:


# usunięcie sezonowości poprzez różnicowanie średniej miesięcznej w roku
#do dnia w roku kolejnym

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
X = series.values
diff = list()
days_in_year = 365
for i in range(days_in_year, len(X)):
    month_str = str(series.index[i].year-1)+'-'+str(series.index[i].month)
    month_mean_last_year = series[month_str].mean()
    value = X[i] - month_mean_last_year
    diff.append(value)

plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[20]:


#podobnie jak trend, sezonowość możemy modelować za pomocą fitowania
#funkcji okresowych (np. sinus lub cosinus) lub wielomianem w ramach sezonu (okresu)

#jeżeli znajdziemy funkcję szacującą sezonowść, możemy jej użyć do predykcji 


# In[54]:


series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
# znajdujemy wielomian postaci: x^2*b1 + x*b2 + ... + bn dla k=2
X = [i%365 for i in range(0, len(series))]
y = series.values


# In[56]:


len(X)


# In[78]:


# modelowanie sezonowości poprzez wielomian wybranego stopnia - super sprawa!!!
#uwaga na lata przestępne

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
# znajdujemy wielomian postaci: x^2*b1 + x*b2 + ... + bn dla k=2
X = [i%365 for i in range(0, len(series))]
y = series.values
degree = 4
coef = np.polyfit(X, y, degree)
print('Coefficients: %s' % coef)
# wyznaczamy krzywą
curve = list()
for i in range(len(X)):
    value = coef[-1]
    for d in range(degree):
        value += X[i]**(degree-d) * coef[d]
    curve.append(value)
# tworzymy wykres porównawczy
plt.plot(series.values)
plt.plot(curve, color='red', linewidth=3)
plt.show()


# In[59]:


coef[-1]


# In[79]:


#posiadając wymodelowaną sezonowość możemy ją usunąć z szeregu czasowego
#poprzez różnicowanie wartości rzeczywistej i oszacowanej

values = series.values
diff = list()
for i in range(len(values)):
    value = values[i] - curve[i]
    diff.append(value)
plt.plot(diff)
plt.show()
print(np.mean(diff))
print(np.std(diff))


# In[37]:


#Modelowanie szeregów czasowych
#klasyczny podział na zbiór treningowy i testowy oraz walidacja krzyżowa nie będę tutaj skuteczne


# In[84]:


#Podział szeregu na zbiór treningowy i testowy - backtesting lub hindcasting
#train-test split - z zachowaniem chronologii szeregu

series = read_csv('dane/sunspots.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
X = series.values
train_size = int(len(X) * 0.80)
train, test = X[0:train_size], X[train_size:len(X)]
print('Observations: %d' % (len(X)))
print('Training Observations: %d' % (len(train)))
print('Testing Observations: %d' % (len(test)))


# In[85]:


#i jeszcze wykres zbioru treningowego i testowego
plt.plot(train)
plt.plot([None for i in train] + [x for x in test])
plt.show()


# In[88]:


#multi train-test split - podział szeregu czasowego na kilka zbiorów
#coś w rodzaju walidacji krzyżowej - metoda KFold

from sklearn.model_selection import TimeSeriesSplit

series = read_csv('dane/sunspots.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
X = series.values
splits = TimeSeriesSplit(n_splits=4)
plt.figure(1)
index = 1
for train_index, test_index in splits.split(X):
    train = X[train_index]
    test = X[test_index]
    print('Observations: %d' % (len(train) + len(test)))
    print('Training Observations: %d' % (len(train)))
    print('Testing Observations: %d' % (len(test)))
    plt.subplot(410 + index)
    plt.plot(train)
    plt.plot([None for i in train] + [x for x in test])
    index += 1
plt.show()


# In[90]:


#walidacja postępująca (Walk Forward Validation) - dobra dla prognoz krótkoterminowych
#mamy do wyboru dwie opcje:
#1. zbiór treningowy powiększa się wraz z napływem danych - zbiór testowy jest stały
#2. zbiór treningowy jest stały i przemieszcza się wraz z napływem danych (okno),
#zbiór testowy jest stały

series = read_csv('dane/sunspots.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
X = series.values
n_train = 500
n_records = len(X)
for i in range(n_train, n_records):
    train, test = X[0:i], X[i:i+10]
    print('train=%d, test=%d' % (len(train), len(test)))


# In[ ]:





# In[50]:


#konstrukcja metody postępowania w przewidywaniu wartości szeregu czasowego
#budowa modelu referencyjnego i określenie wartości referencyjnej (bazowej) błędu

#1. wczytanie zbioru
def parser(x):
    return datetime.strptime('201'+x, '%Y-%m')

series = read_csv('dane/shampoo-sales.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True, date_parser=parser)

#2. tworzymy zmienną, która jest naszym szeregiem przesuniętym o jeden krok czasowy
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']
print(dataframe.head(5))

#3. dzielimy szereg na część uczącą i testową
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

#4. tworzymy funkcję zwracającą wartość predykcji modelu naiwnego
def model_persistence(x):
    return x

#5. testujemy model za pomocą metody walidacji postępującej
predictions = list()
for x in test_X:
    yhat = model_persistence(x)
    predictions.append(yhat)

rmse = np.sqrt(metr.mean_squared_error(test_y, predictions))
print('Test RMSE: %.3f' % rmse)

#6. wyświtalmy wyniki predykcji i dane oryginalne
plt.plot(train_y)
plt.plot([None for i in train_y] + [x for x in test_y])
plt.plot([None for i in train_y] + [x for x in predictions])
plt.show()


# In[52]:


#ocena jakości modeli predykcyjnych poprzez wizualizację

#budujemy naiwny model predykcyjny
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

# tworzymy nową zmienną 
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

# dzielimy szereg na zbiór treningowy i testowy
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

# predykcja
predictions = [x for x in test_X]

# wyznaczamy błędy (residuals)
residuals = [test_y[i]-predictions[i] for i in range(len(predictions))]
residuals = pd.DataFrame(residuals)
print(residuals.head())


# In[53]:


# liniowy wykres błędów
residuals.plot()
plt.show()


# In[54]:


# statystyki błędów predykcji
print(residuals.describe())


# In[55]:


#histogram błędów i ich rozkład
residuals.hist()
plt.show()

residuals.plot(kind='kde')
plt.show()


# In[60]:


#wykres Q-Q albo wykres kwantylowy
#możemy sprawdzić w ten sposób nomralność rozkładu błędów

from statsmodels.graphics.gofplots import qqplot

series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

predictions = [x for x in test_X]

residuals = [test_y[i]-predictions[i] for i in range(len(predictions))]
residuals = np.array(residuals)

qqplot(residuals, line='r')
plt.show()


# In[62]:


#wykres autokorelacji

from pandas.plotting import autocorrelation_plot

autocorrelation_plot(residuals)
plt.show()


# In[3]:


#inżynieria cech dla szeregów czasowych

#zagadnienie regresyjne
#wczytujemy dane
series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)

#tworzymy nową cechę poprzez przesunięcie danych w czasie
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

#budujemy cechę jako najbliższą mniejszą liczbę podzielną przez 5 
for i in range(len(dataframe['t+1'])):
    dataframe['t+1'][i] = int(dataframe['t+1'][i] / 5) * 5.0

print(dataframe.head(5))


# In[6]:


#budowanie cech dla klasyfikacji

series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                  index_col=0, parse_dates=True, squeeze=True)

values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

#dyskretyzujemy zmienną ciągłą
for i in range(len(dataframe['t+1'])):
    value = dataframe['t+1'][i]
    if value < 10.0:
        dataframe['t+1'][i] = 0
    elif value >= 25.0:
        dataframe['t+1'][i] = 2
    else:
        dataframe['t+1'][i] = 1

dataframe['t+1'].value_counts()


# In[9]:


#budowanie cech dla predykcji dłuższych horyzontów czasowych

series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)

# tworzymy zbiór przesuniętych w czasie cech
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values, values.shift(-1),
                       values.shift(-2), values.shift(-3),
                       values.shift(-4), values.shift(-5),
                       values.shift(-6)], axis=1)
dataframe.columns = ['t', 't+1', 't+2', 't+3', 't+4', 't+5', 't+6', 't+7']

print(dataframe)


# In[10]:


#MODELOWANIE

#Model autoregresyjny -> X(t+1) = b0 + b1*X(t) + b2*X(t-1)


# In[91]:


#przed przystąpieniem do modelowania sprawdzamy w szeregu występują korelacje

from pandas.plotting import lag_plot
series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)
lag_plot(series)
plt.show()


# In[92]:


#sprawdzamy wielkość korelacji i tworzymy wykres autokorelacji (dwie wersje)
from pandas.plotting import autocorrelation_plot
from statsmodels.graphics.tsaplots import plot_acf

series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']
result = dataframe.corr()
print(result)

autocorrelation_plot(series)
plt.show()

plot_acf(series, lags=30)
plt.show()


# In[39]:


#budujemy naiwny model bazowy

from sklearn.metrics import mean_squared_error
from math import sqrt

#wczytywanie danych
series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)

#tworzymy zmienną przesuniętą w czasie
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

#tworzymy zbiór treningowy i testowy
X = dataframe.values
train, test = X[1:len(X)-7], X[len(X)-7:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

# model bazowy
def model_persistence(x):
    return x

#walidacja krocząca
predictions = list()
for x in test_X:
    yhat = model_persistence(x)
    predictions.append(yhat)

#obliczamy błąd predykcji
rmse = sqrt(mean_squared_error(test_y, predictions))
print('Test RMSE: %.3f' % rmse)

#wizualiazacja predykcji
plt.plot(test_y)
plt.plot(predictions, color='red')
plt.show()


# In[42]:


#Model AR

#wczytujemy odpowiedni pakiet
from statsmodels.tsa.ar_model import AR

#wczytywanie danych
series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)

#tworzymy zbiór treningowy i testowy
X = series.values
train, test = X[1:len(X)-7], X[len(X)-7:]

#budujemy model autoregresyjny
model = AR(train)
model_fit = model.fit()
print('Lag: %s' % model_fit.k_ar)
print('Coefficients: %s' % model_fit.params)
print(len(model_fit.params))

#predykcja
predictions = model_fit.predict(start=len(train), end=len(train)+len(test)-1, dynamic=False)
for i in range(len(predictions)):
    print('predicted=%f, expected=%f' % (predictions[i], test[i]))
rmse = sqrt(mean_squared_error(test, predictions))
print('Test RMSE: %.3f' % rmse)

# wykres predykcji
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()


# In[41]:


#budowanie modelu AR dla nowych wartości
#korzystamy ze współczynników bazowego modelu AR
#yhat = b0 + b1*X1 + b2*X2 + ... + bn*Xn

#wczytywanie danych
series = pd.read_csv('dane/daily-minimum-temperatures.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)

#tworzymy zbiór treningowy i testowy
X = series.values
train, test = X[1:len(X)-7], X[len(X)-7:]

#budujemy model autoregresyjny
model = AR(train)
model_fit = model.fit()
window = model_fit.k_ar
coef = model_fit.params


#robimy predykcję kolejnych punktów ze zbioru testowego,
#korzystając ze współczynników wyznaczonych dla zbioru treningowego
history = train[len(train)-window:]
history = [history[i] for i in range(len(history))]
predictions = list()
for t in range(len(test)):
    length = len(history)
    lag = [history[i] for i in range(length-window,length)]
    yhat = coef[0]
    for d in range(window):
        yhat += coef[d+1] * lag[window-d-1]
    obs = test[t]
    predictions.append(yhat)
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))

rmse = sqrt(mean_squared_error(test, predictions))
print('Test RMSE: %.3f' % rmse)

#wykres predykcji
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()


# In[93]:


#Model średniej ruchomej - nie mylić z wygładzaniem za pomocą średniej ruchomej
#błędy predykcji tworzą kolejny szereg czasowy, którego modelowanie
#może zostać wykorzystane do poprawy jakości predykcji

#dla przypomnienia: residual_error = expected - predicted

#jeżeli błędy (reszty) nie tworzą białego szumu, to mogą być modelowane

#model autoregresji dla błędów (reszt - residual errors)
#jest nazywany modelem średniej kroczącej


# In[95]:


#tworzymy model naiwny

# wczytywanie danych
series = pd.read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

# tworzenie cechy dla modelu naiwnego
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

# zbiór uczący i testowy
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

# model naiwny
predictions = [x for x in test_X]

# sprawdzamy jakość modelu naiwnego
rmse = sqrt(mean_squared_error(test_y, predictions))
print('Test RMSE: %.3f' % rmse)

# wyznaczamy błędy (reszty)
residuals = [test_y[i]-predictions[i] for i in range(len(predictions))]
residuals = pd.DataFrame(residuals)
print(residuals.head())


# In[96]:


#mamy teraz szereg reszt, który możemy modelować za pomocą modeli AR

#error(t+1) = b0 + b1*error(t) + b2*error(t-1) + ... + bn*error(t-n)

#wczytywanie danych
series = pd.read_csv('dane/daily-total-female-births.csv', header=0,
                     index_col=0, parse_dates=True, squeeze=True)

# tworzenie cechy dla modelu naiwnego
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

# zbiór uczący i testowy
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

# model naiwny
train_pred = [x for x in train_X]

# obliczamy reszty
train_resid = [train_y[i]-train_pred[i] for i in range(len(train_pred))]

# trenujemy model AR dla reszt
model = AR(train_resid)
model_fit = model.fit()
window = model_fit.k_ar
coef = model_fit.params
print('Lag=%d, Coef=%s' % (window, coef))


# In[97]:


#wykorzystamy wyniki do predykcji błędu (reszt)

# wykorzystujemy walidację kroczącą dla zbioru testowego z wykorzystaniem okna
history = train_resid[len(train_resid)-window:]
history = [history[i] for i in range(len(history))]
predictions = list()
expected_error = list()
for t in range(len(test_y)):
    # model naiwyny
    yhat = test_X[t]
    error = test_y[t] - yhat
    expected_error.append(error)
    # predykcja błędu
    length = len(history)
    lag = [history[i] for i in range(length-window,length)]
    pred_error = coef[0]
    for d in range(window):
        pred_error += coef[d+1] * lag[window-d-1]
    predictions.append(pred_error)
    history.append(error)
    print('predicted error=%f, expected error=%f' % (pred_error, error))

# wykres reszt
plt.plot(expected_error)
plt.plot(predictions, color='red')
plt.show()


# In[100]:


#poprawiamy naiwną predykcję przy użyciu predykcji reszt
#improved_forecast = forecast + estimated_error

#wczytywanie danych
series = pd.read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

# tworzenie cechy dla modelu naiwnego
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

# zbiór uczący i testowy
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

# model naiwny
train_pred = [x for x in train_X]

# obliczamy reszty
train_resid = [train_y[i]-train_pred[i] for i in range(len(train_pred))]

# trenujemy model AR dla reszt
model = AR(train_resid)
model_fit = model.fit()
window = model_fit.k_ar
coef = model_fit.params

# wykorzystujemy walidację kroczącą dla zbioru testowego z wykorzystaniem okna
history = train_resid[len(train_resid)-window:]
history = [history[i] for i in range(len(history))]
predictions = list()
for t in range(len(test_y)):
    # model naiwyny
    yhat = test_X[t]
    error = test_y[t] - yhat
    # predykcja reszt
    length = len(history)
    lag = [history[i] for i in range(length-window,length)]
    pred_error = coef[0]
    for d in range(window):
        pred_error += coef[d+1] * lag[window-d-1]
    # poprawiamy predykcję przy użyciu reszt
    yhat = yhat + pred_error
    predictions.append(yhat)
    history.append(error)
    print('predicted=%f, expected=%f' % (yhat, test_y[t]))

# błąd predykcji
rmse = sqrt(mean_squared_error(test_y, predictions))
print('Test RMSE: %.3f' % rmse)

# wykres predykcji
plt.plot(test_y)
plt.plot(predictions, color='red')
plt.show()


# In[101]:


#ARIMA - AutoRegressive Integrated Moving Average
#AR I(różnicowanie) MA
#ARIMA(p,d,q)
#p - rzęd autoregresji (liczba wcześniejszych wartości)
#d - rząd różnicowania
#q - rząd średniej ruchomej


# In[104]:


# wczytujemy dane
def parser(x):
    return datetime.strptime('201'+x, '%Y-%m')

series = pd.read_csv('dane/shampoo-sales.csv', header=0, index_col=0,
                     parse_dates=True, squeeze=True, date_parser=parser)

print(series.head())

# wykresy
series.plot()
plt.show()

autocorrelation_plot(series)
plt.show()


# In[107]:


#na podstawie wykresu autokorelacji możemy ustalić lag dla modelu ARIMA (jaki?)

from statsmodels.tsa.arima_model import ARIMA

# budujemy model i trenujemy
model = ARIMA(series, order=(5,1,0))
model_fit = model.fit()

# podsumowanie trenowania
print(model_fit.summary())

# wykresy reszt
residuals = pd.DataFrame(model_fit.resid)
residuals.plot()
plt.show()

# rozkład reszt
residuals.plot(kind='kde')
plt.show()

# statystyki reszt
print(residuals.describe())


# In[108]:


#na wykresach powyżej widać, że sezonowść nie została do końca usunięta,
#a średnia reszt nie jest równa zero


# In[114]:


# zbiór uczący i testowy
X = series.values
size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]
history = [x for x in train]
predictions = list()

# walidacja krocząca
for t in range(len(test)):
    model = ARIMA(history, order=(5,1,0))
    model_fit = model.fit(disp=0)
    output = model_fit.forecast()
    yhat = output[0]
    predictions.append(yhat)
    obs = test[t]
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))

# błąd predykcji
rmse = sqrt(mean_squared_error(test, predictions))
print('Test RMSE: %.3f' % rmse)

# wykres predykcji
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()


# In[ ]:




