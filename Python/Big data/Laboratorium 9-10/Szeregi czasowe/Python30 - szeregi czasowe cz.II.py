#Temat: szeregi czasowe

#ładowanie bibliotek
from pandas import read_csv
from pandas import datetime
import random as rnd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sklearn.metrics as metr


#ustawienia wykresów i wyświetlanie liczb zmiennoprzecinkowych
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rc('figure', figsize=(10, 6))
np.set_printoptions(precision=8, suppress=True)


#tworzymy funkcję, która będzie interpretowała daty w zadany przez nas sposób
def parser(x):
    return datetime.strptime('200'+x, '%Y-%m')


#wczytujemy plik z danymi
series = read_csv('dane/shampoo-sales.csv', header=0,
                  index_col=0, parse_dates=True, squeeze=True, date_parser=parser)
series


len(series)


#dokonujemy zmiany próbkowania szergu (upsample -> z miesięcy na dni)
upsampled = series.resample('D').mean()
print(upsampled.head(34))


len(upsampled)


type(upsampled)


#do wypełnienia pustych wartości wykorzystujemy różne rodzaje interpolacji
#interpolated = upsampled.interpolate(method='linear')
interpolated = upsampled.interpolate(method='spline', order=3)
print(interpolated.head(34))


#na koniec wykres
#interpolated.plot()
interpolated[0:66].plot()
plt.show()


#możemy również zmniejszyć częstotliwość próbkowania szergu (downsample -> z miesięcy na kwartały lub lata)
#resample = series.resample('Q')
resample = series.resample('A')
resample


#dla kwartałów wyznaczamy średnią, natomiast dla lat sumujemy dane
#quarterly_mean_sales = resample.mean()
yearly_sum_sales = resample.sum()
#quarterly_mean_sales.head()
yearly_sum_sales


#na koniec wykres
#quarterly_mean_sales.plot()
yearly_sum_sales.plot()
plt.show()


#transformacje szeregów czasowych w celu redukcji szumów i poprawieniu jakości sygnału
#Wczytanie danych
series = read_csv('dane/airline-passengers.csv', header=0,
                  index_col=0, parse_dates=True, squeeze=True)
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(series)
# histogram
plt.subplot(212)
plt.hist(series)
plt.show()


#jak widzimy szereg czasowy jest niestacjonarny, czyli średnia i wariancja zmieniają się w czasie
#takie szeregi są problematyczne w zagadnieniach związanych z przewidywaniem, ponieważ zmienia się trend oraz sezonowść
series


#przykład 1: trend zmienia się jak funkcja kwadratowa
series = [np.exp(i) for i in range(1,100)]
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(series)
# histogram
plt.subplot(212)
plt.hist(series)
plt.show()


#Przykład 1
series = [np.exp(i) for i in range(1,100)]
#usunięcie trendu polega na spierwiastkowaniu szeregu
transform = np.log(series)
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(transform)
# histogram
plt.subplot(212)
plt.hist(transform)
plt.show()


#poprzez pierwiastkowanie próbujemy zmienić trend na liniowy, a rozkład na gaussowski
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = np.sqrt(dataframe['passengers'])
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


#jeżeli jednak trend rośnie wykładniczo możemy zlogarytmować szereg
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = np.log(dataframe['passengers'])
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# transformacja Box-Cox jest transformacją parametryczną, w której sterujemy parametrem lambda
#w zależności od wartości tego parametru otrzymujemy różne przekształcenia szeregów
#np. lambda = 0.0 logarytmowanie, lambda = 0.5 pierwiastkowanie, lambda = 1.0 brak transformacji
from scipy.stats import boxcox
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = boxcox(dataframe['passengers'], lmbda=0.0)
plt.figure(1)
# line plot
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


#możemy również automatycznie wyznaczyć najlepszy parametr lambda dla rozważanego szeregu
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'], lam = boxcox(dataframe['passengers'])
print('Lambda: %f' % lam)
plt.figure(1)
# line plot
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# zastosowanie średniej kroczącej w celu przygotowania danych
#wczytanie danych
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0, parse_dates=True, squeeze=True)

# wyznaczenie wartości średniej dla trzech wartości t-2, t-1 i t
rolling = series.rolling(window=3)
print(rolling)
rolling_mean = rolling.mean()
print(rolling_mean.head(10))

#wykres danych oryginalnych i po zastosowaniu średniej kroczącej
series.plot()
rolling_mean.plot(color='red')
plt.show()

# wykres części danych oryginalnych i po zastosowaniu średniej kroczącej
series[:100].plot()
rolling_mean[:100].plot(color='red')
plt.show()


# średnia krocząca w celu tworzenia nowych cech szeregu (feature engineering)

#wczytanie danych
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
df = pd.DataFrame(series.values)
width = 3 #szerokość okna
lag1 = df.shift(1) #przesunięcie oryginalnego szeregu o jeden krok czasowy
lag3 = df.shift(width - 1) #przesunięcie oryginalnego szeregu o krok czasowy zależny od rozmiaru okna
window = lag3.rolling(window=width) 
means = window.mean() #wyznaczenie średniej kroczącej
dataframe = pd.concat([means, lag1, df], axis=1) #połączenie kolumn w jedną ramkę danych
dataframe.columns = ['mean', 't', 't+1']
dataframe


#średnia krocząca jako naiwny podel predykcyjny
from sklearn.metrics import mean_squared_error

series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0, parse_dates=True, squeeze=True)

# przygotowanie zmiennych
X = series.values
window = 3
history = [X[i] for i in range(window)]
test = [X[i] for i in range(window, len(X))]
predictions = list()

# wyznaczanie średniej kroczącej w celu obliczenia przyszłych wartości szeregu czasowego
for t in range(len(test)):
    length = len(history)
    yhat = np.mean([history[i] for i in range(length-window,length)])
    obs = test[t]
    predictions.append(yhat)
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))
rmse = np.sqrt(mean_squared_error(test, predictions))
print('Test RMSE: %.3f' % rmse)

# wykres
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()

# wykres z mniejszą liczbą danych
plt.plot(test[:100])
plt.plot(predictions[:100], color='red')
plt.show()


#biały szum
#jeżeli szereg czasowy okaże się być białym szumem (całkowita losowość) wówczas rozsądna predykcja nie jest możliwa
#błąd predykcji szeregu czasowego powinien być białym szumem, wówczas jesteśmy pewni,
#że model został zbudowany na esencji sygnału
#szereg czasowy nie jest białym szumem jeżeli któreś z poniższych pytań jest prawdziwe:
#1. czy średnia jest niezerowa?
#2. czy wariancja zmienia się w czasie?
#3. czy wartości szeregu korelują ze sobą po przesunięciu o lag?

#przykład białego szumu

from random import gauss
from random import seed

seed(1)
series = [gauss(0.0, 1.0) for i in range(1000)]
series = pd.Series(series)

# statystyki
print(series.describe())

# wykres
series.plot()
plt.show()

# histogram
series.hist()
plt.show()

# funkcja autokorelacji
pd.plotting.autocorrelation_plot(series)
plt.show()


# In[131]:


#błądzenie losowe (random walk)
#błądzenie losowe pozwala na zrozumienie przewidywalności szeregu czasowego

# tworzymy i rysujemy przykładowe błądzenie losowe
seed(123)
random_walk = list()
random_walk.append(-1 if rnd.random() < 0.5 else 1)
for i in range(1, 1000):
    movement = -1 if rnd.random() < 0.5 else 1
    value = random_walk[i-1] + movement
    random_walk.append(value)
plt.plot(random_walk)
plt.show()


# In[132]:


# funkcja autokorelacji (FA) błądzenia losowego
# FA pokazuje, że błądzenie losowe nie jest szeregiem stacjonarnym
pd.plotting.autocorrelation_plot(random_walk)
plt.show()


# In[133]:


# do wiarygodnego sprawdzenia stacjonarności szeregu służy rozszerzony test Dickey-Fullera (ADF)
# hipoteza zerowa testu to szereg czasowy jest niestacjonarny
# w wyniku testu dostajemy wartość większą od wszystkich krytycznych poziomów ufności
# zatem szereg czasowy jest niestacjonarny
from statsmodels.tsa.stattools import adfuller

result = adfuller(random_walk)
print('ADF Statistic: %f' % result[0])
print('p-value: %f' % result[1])
print('Critical Values:')
for key, value in result[4].items():
    print('\t%s: %.3f' % (key, value))


# In[135]:


#z szeregu, który jest błądzeniem losowym możemy stworzyć szereg stacjonarny

#skoro w błądzeniu losowym kolejne wartości zależą od poprzednich, usunięcie tych róźnic stworzy szereg stacjonarny
#szereg nie zawiera teraz żadnych struktur które można wykorzystać w uczeniu maszynowym
diff = list()
for i in range(1, len(random_walk)):
    value = random_walk[i] - random_walk[i - 1]
    diff.append(value)
# wykres różnic
plt.plot(diff)
plt.show()


# In[138]:


#i jeszcze FA
#wyraźnie widać, że korelacje są małe i poniżej 95% i 99% przedziału ufności
pd.plotting.autocorrelation_plot(diff)
plt.ylim(-0.5,0.5)
plt.show()


# In[151]:


#nie można przeprowadzić rozsądnej predykcji błądzenia losowego
#można jedynie wykonać naiwną predykcję bazując na najbliższych wartościach szeregu t-1 -> t

# dzielimy dane na zbiór uczący i testowy
train_size = int(len(random_walk) * 0.66)
train, test = random_walk[0:train_size], random_walk[train_size:]

# naiwna predykcja uwzględniająca zmianę między kolejnymi elementami równą 1
predictions = list()
history = train[-1]
for i in range(len(test)):
    yhat = history
    predictions.append(yhat)
    history = test[i]
rmse = np.sqrt(mean_squared_error(test, predictions))
print('RMSE: %.3f' % rmse)


# In[152]:


# naiwna predykcja uwzględniająca kierunek i wartość zmiany między kolejnymi elementami równą +/-1
predictions = list()
history = train[-1]
for i in range(len(test)):
    yhat = history + (-1 if rnd.random() < 0.5 else 1)
    predictions.append(yhat)
    history = test[i]
rmse = np.sqrt(mean_squared_error(test, predictions))
print('RMSE: %.3f' % rmse)

#naiwna predykcja to najlepsza i jedyna metoda, którą możemy zastosować dla błądzenia losowego


# In[163]:


#dekompozycja szeregów czasowych
#szereg czasowy można rozłożyć na dwa główne komponenty
#1. systematyczne - elementy szeregu, które są konsystentne lub rekurencyjne i mogą opisane i modelowane
#2. niesystematyczne - elementy szeregu, które nie mogą być bezpośrednio modelowane

#Komponenty systematyczne to: poziom (wartość średnia), trend (występuje opcjonalnie) i sezonowość (występuje opcjonalnie)
#Komponent niesystematyczny to szum.

#Możemy rozważać dwa modele szeregów czasowych:
#1. addytywne (liniowe): poziom + trend + sezonowość + szum
#2. multiplikatywne (nieliniowe): poziom * trend * sezonowość * szum


# In[159]:


# przykład dekompozycji szeregu czasowego przy założeniu modelu addytywnego

from statsmodels.tsa.seasonal import seasonal_decompose
series = [i + randrange(10) for i in range(1,100)]
result_add = seasonal_decompose(series, model='additive', freq=1)
result_add.plot()
plt.show()


# In[162]:


#poszczególne składowe dekompozycji są zapisywane w odpowiednich zmiennych
print(result_add.trend)
print(result_add.seasonal)
print(result_add.resid)
print(result_add.observed)


# In[164]:


# przykład dekompozycji szeregu czasowego przy założeniu modelu multiplikatywnego

series = [i**2.0 for i in range(1,100)]
result_multi = seasonal_decompose(series, model='multiplicative', freq=1)
result_multi.plot()
plt.show()


# In[165]:


# dekompozycja rzeczywistego szeregu czasowego - model multiplikatywny
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
result = seasonal_decompose(series, model='multiplicative')
result.plot()
plt.show()


# In[158]:


# dekompozycja rzeczywistego szeregu czasowego - model addytywny
# sprawdźmy różnice pomiędzy prezentowanymi wynikami
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
result = seasonal_decompose(series, model='additive')
result.plot()
plt.show()


# In[166]:


#Usuwanie trendu z szeregu czasowego
#Możemy wyróżnić kilka cech trendów:
#1. Deterministyczne (monotonicznie rosnące lub malejące) i stochastyczne (monotoniczność trendu się zmienia)
#2. Globalne i lokalne

#Szereg czasowy z trendem to szereg niestacjonarny.
#Usunięcie trendu z szeregu czasowego powoduje, że staje się on stacjonarny.

#Z punktu widzenia uczenia maszynowego usunięcie trendu zmniejsza zaburzenia pomiędzy danymi wyjściowymi i wyjściowymi.
#Z drugiej strony dodanie trendu może poprawić relację pomiędzy predyktorami a zmienną celu.


# In[176]:


#Usuwanie trendu poprzez różnicowanie szeregu czasowego

def parser(x):
    return datetime.strptime('200'+x, '%Y-%m')

series = read_csv('dane/shampoo-sales.csv', header=0, index_col=0, parse_dates=True, squeeze=True, date_parser=parser)
X = series.values
print(np.mean(X))

#trend wznoszący jest wyraźnie widoczny na wykresie
plt.plot(X)
plt.show()


# In[179]:


# tworzymy nową zmienną, która jest różnicą dwóch kolejnych wartości szeregu
# new_x(t) = x(t) - x(t-1)
diff = list()
for i in range(1, len(X)):
    value = X[i] - X[i - 1]
    diff.append(value)

print(np.mean(diff))
#plt.plot(X)
plt.plot(diff)
plt.show()


# In[181]:


#Usuwanie trendu poprzez budowanie modelu regresyjnego 

from sklearn.linear_model import LinearRegression

# budujemy model regrsji liniowej
X = [i for i in range(0, len(series))]
X = np.reshape(X, (len(X), 1))
y = series.values
model = LinearRegression()
model.fit(X, y)


# In[184]:


# wyznaczamy trend liniowy korzystając z modelu
trend = model.predict(X)

# wykres danych i trendu
plt.plot(y)
plt.plot(trend)
plt.show()


# In[186]:


# usuwamy trend 
detrended = [y[i]-trend[i] for i in range(0, len(series))]

# rusujemy dane bez trendu
print(np.mean(detrended))
plt.plot(detrended)
plt.show()


# In[3]:


#Wyodrębnianie sezonowości

#Sezonowość może mieć znaczny wpływ na modelowanie szeregów czasowych
#1. Czysty sygnał - usunięcie sezonowości może poprawić związek pomiędzy
#danymi wejściowymi i wyjąciowymi
#2. Dodatkowe informacja - sezonowść może stać się kolejnym predyktorem

#Szereg czasowy z usuniętą sezonowością nazywamy sezonowo stacjnarnym.


# In[10]:


#wczytanie danych i wyświetlenie wykresu

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
plt.plot(series)
plt.show()


# In[13]:


series


# In[36]:


# usunięcie sezonowości poprzez różnicowanie dnia w roku do dnia w roku kolejnym
#uwaga na lata przestępne
X = series.values
diff = list()
days_in_year = 365
for i in range(days_in_year, len(X)):
    value = X[i] - X[i - days_in_year]
    diff.append(value)
plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[16]:


#może warto rozważyć sezonowść w odstępach miesięcznych
series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
resample = series.resample('M')
monthly_mean = resample.mean()
print(monthly_mean.head(18))
monthly_mean.plot()
plt.show()


# In[35]:


# usunięcie sezonowości poprzez różnicowanie miesiąca w roku do miesiąca w roku kolejnym

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
resample = series.resample('M')
monthly_mean = resample.mean()
X = series.values
diff = list()
months_in_year = 12
for i in range(months_in_year, len(monthly_mean)):
    value = monthly_mean[i] - monthly_mean[i - months_in_year]
    diff.append(value)
plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[34]:


# usunięcie sezonowości poprzez różnicowanie średniej miesięcznej w roku
#do dnia w roku kolejnym

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
X = series.values
diff = list()
days_in_year = 365
for i in range(days_in_year, len(X)):
    month_str = str(series.index[i].year-1)+'-'+str(series.index[i].month)
    month_mean_last_year = series[month_str].mean()
    value = X[i] - month_mean_last_year
    diff.append(value)

plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[20]:


#podobnie jak trend, sezonowość możemy modelować za pomocą fitowania
#funkcji okresowych (np. sinus lub cosinus) lub wielomianem w ramach sezonu (okresu)

#jeżeli znajdziemy funkcję szacującą sezonowść, możemy jej użyć do predykcji 


# In[30]:


# modelowanie sezonowości poprzez wielomian wybranego stopnia - super sprawa!!!
#uwaga na lata przestępne

series = read_csv('dane/daily-minimum-temperatures.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)
# znajdujemy wielomian postaci: x^2*b1 + x*b2 + ... + bn dla k
X = [i%365 for i in range(0, len(series))]
y = series.values
degree = 4
coef = np.polyfit(X, y, degree)
print('Coefficients: %s' % coef)
# wyznaczamy krzywą
curve = list()
for i in range(len(X)):
    value = coef[-1]
    for d in range(degree):
        value += X[i]**(degree-d) * coef[d]
    curve.append(value)
# tworzymy wykres porównawczy
plt.plot(series.values)
plt.plot(curve, color='red', linewidth=3)
plt.show()


# In[33]:


#posiadając wymodelowaną sezonowość możemy ją usunąć z szeregu czasowego
#poprzez różnicowanie wartości rzeczywistej i oszacowanej

values = series.values
diff = list()
for i in range(len(values)):
    value = values[i] - curve[i]
    diff.append(value)
plt.plot(diff)
plt.show()
print(np.mean(diff))


# In[37]:


#Modelowanie szeregów czasowych
#klasyczny podział na zbiór treningowy i testowy oraz walidacja krzyżowa nie będę tutaj skuteczne


# In[38]:


#Podział szeregu na zbiór treningowy i testowy - backtesting lub hindcasting
#train-test split - z zachowaniem chronologii szeregu

series = read_csv('dane/sunspots.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
X = series.values
train_size = int(len(X) * 0.66)
train, test = X[0:train_size], X[train_size:len(X)]
print('Observations: %d' % (len(X)))
print('Training Observations: %d' % (len(train)))
print('Testing Observations: %d' % (len(test)))


# In[39]:


#i jeszcze wykres zbioru treningowego i testowego
plt.plot(train)
plt.plot([None for i in train] + [x for x in test])
plt.show()


# In[40]:


#multi train-test split - podział szeregu czasowego na kilka zbiorów
#coś w rodzaju walidacji krzyżowej - metoda KFold

from sklearn.model_selection import TimeSeriesSplit

series = read_csv('dane/sunspots.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
X = series.values
splits = TimeSeriesSplit(n_splits=3)
plt.figure(1)
index = 1
for train_index, test_index in splits.split(X):
    train = X[train_index]
    test = X[test_index]
    print('Observations: %d' % (len(train) + len(test)))
    print('Training Observations: %d' % (len(train)))
    print('Testing Observations: %d' % (len(test)))
    plt.subplot(310 + index)
    plt.plot(train)
    plt.plot([None for i in train] + [x for x in test])
    index += 1
plt.show()


# In[44]:


#walidacja postępująca (Walk Forward Validation) - dobra dla prognoz krótkoterminowych
#mamy do wyboru dwie opcje:
#1. zbiór treningowy powiększa się wraz z napływem danych - zbiór testowy jest stały
#2. zbiór treningowy jest stały i przemieszcza się wraz z napływem danych (okno),
#zbiór testowy jest stały

series = read_csv('dane/sunspots.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
X = series.values
n_train = 500
n_records = len(X)
for i in range(n_train, n_records):
    train, test = X[0:i], X[i:i+1]
    print('train=%d, test=%d' % (len(train), len(test)))


# In[50]:


#konstrukcja metody postępowania w przewidywaniu wartości szeregu czasowego
#budowa modelu referencyjnego i określenie wartości referencyjnej (bazowej) błędu

#1. wczytanie zbioru
def parser(x):
    return datetime.strptime('201'+x, '%Y-%m')

series = read_csv('dane/shampoo-sales.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True, date_parser=parser)

#2. tworzymy zmienną, która jest naszym szeregiem przesuniętym o jeden krok czasowy
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']
print(dataframe.head(5))

#3. dzielimy szereg na część uczącą i testową
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

#4. tworzymy funkcję zwracającą wartość predykcji modelu naiwnego
def model_persistence(x):
    return x

#5. testujemy model za pomocą metody walidacji postępującej
predictions = list()
for x in test_X:
    yhat = model_persistence(x)
    predictions.append(yhat)

rmse = np.sqrt(metr.mean_squared_error(test_y, predictions))
print('Test RMSE: %.3f' % rmse)

#6. wyświtalmy wyniki predykcji i dane oryginalne
plt.plot(train_y)
plt.plot([None for i in train_y] + [x for x in test_y])
plt.plot([None for i in train_y] + [x for x in predictions])
plt.show()


# In[52]:


#ocena jakości modeli predykcyjnych poprzez wizualizację

#budujemy naiwny model predykcyjny
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

# tworzymy nową zmienną 
values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

# dzielimy szereg na zbiór treningowy i testowy
X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

# predykcja
predictions = [x for x in test_X]

# wyznaczamy błędy (residuals)
residuals = [test_y[i]-predictions[i] for i in range(len(predictions))]
residuals = pd.DataFrame(residuals)
print(residuals.head())


# In[53]:


# liniowy wykres błędów
residuals.plot()
plt.show()


# In[54]:


# statystyki błędów predykcji
print(residuals.describe())


# In[55]:


#histogram błędów i ich rozkład
residuals.hist()
plt.show()

residuals.plot(kind='kde')
plt.show()


# In[60]:


#wykres Q-Q albo wykres kwantylowy
#możemy sprawdzić w ten sposób nomralność rozkładu błędów

from statsmodels.graphics.gofplots import qqplot

series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0,
                  parse_dates=True, squeeze=True)

values = pd.DataFrame(series.values)
dataframe = pd.concat([values.shift(1), values], axis=1)
dataframe.columns = ['t', 't+1']

X = dataframe.values
train_size = int(len(X) * 0.66)
train, test = X[1:train_size], X[train_size:]
train_X, train_y = train[:,0], train[:,1]
test_X, test_y = test[:,0], test[:,1]

predictions = [x for x in test_X]

residuals = [test_y[i]-predictions[i] for i in range(len(predictions))]
residuals = np.array(residuals)

qqplot(residuals, line='r')
plt.show()


# In[62]:


#wykres autokorelacji

from pandas.plotting import autocorrelation_plot

autocorrelation_plot(residuals)
plt.show()


# In[ ]:




