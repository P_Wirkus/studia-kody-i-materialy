#Temat: szeregi czasowe

#ładowanie bibliotek
from pandas import read_csv
from pandas import datetime
import random as rnd
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


#ustawienia wykresów i wyświetlanie liczb zmiennoprzecinkowych
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rc('figure', figsize=(10, 6))
np.set_printoptions(precision=4, suppress=True)


#tworzymy funkcję, która będzie interpretowała daty w zadany przez nas sposób
def parser(x):
    return datetime.strptime('200'+x, '%Y-%m')


#wczytujemy plik z danymi
series = read_csv('dane/shampoo-sales.csv', header=0, index_col=0, parse_dates=True, squeeze=True, date_parser=parser)
series


#dokonujemy zmiany próbkowania szergu (upsample -> z miesięcy na dni)
upsampled = series.resample('D').mean()
print(upsampled.head(32))


#do wypełnienia pustych wartości wykorzystujemy różne rodzaje interpolacji
interpolated = upsampled.interpolate(method='linear')
#interpolated = upsampled.interpolate(method='spline', order=2)
print(interpolated.head(32))


#na koniec wykres
interpolated.plot()
#interpolated[0:66].plot()
plt.show()


#możemy również zmniejszyć częstotliwość próbkowania szergu (downsample -> z miesięcy na kwartały lub lata)
resample = series.resample('Q')
#resample = series.resample('A')
resample


#dla kwartałów wyznaczamy średnią, natomiast dla lat sumujemy dane
quarterly_mean_sales = resample.mean()
#yearly_sum_sales = resample.sum()
quarterly_mean_sales.head()
#yearly_sum_sales


#na koniec wykres
quarterly_mean_sales.plot()
#yearly_sum_sales.plot()
plt.show()


#transformacje szeregów czasowych w celu redukcji szumów i poprawieniu jakości sygnału
#Wczytanie danych
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(series)
# histogram
plt.subplot(212)
plt.hist(series)
plt.show()


#jak widzimy szereg czasowy jest niestacjonarny, czyli średnia i wariancja zmieniają się w czasie
#takie szeregi są problematyczne w zagadnieniach związanych z przewidywaniem, ponieważ zmienia się trend oraz sezonowść
series


#przykład 1: trend zmienia się jak funkcja kwadratowa
series = [i**2 for i in range(1,100)]
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(series)
# histogram
plt.subplot(212)
plt.hist(series)
plt.show()


#Przykład 1
series = [i**2 for i in range(1,100)]
#usunięcie trendu polega na spierwiastkowaniu szeregu
transform = np.sqrt(series)
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(transform)
# histogram
plt.subplot(212)
plt.hist(transform)
plt.show()


#poprzez pierwiastkowanie próbujemy zmienić trend na liniowy, a rozkład na gaussowski
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = np.sqrt(dataframe['passengers'])
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


#jeżeli jednak trend rośnie wykładniczo możemy zlogarytmować szereg
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = np.log(dataframe['passengers'])
plt.figure(1)
# linia
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# transformacja Box-Cox jest transformacją parametryczną, w której sterujemy parametrem lambda
#w zależności od wartości tego parametru otrzymujemy różne przekształcenia szeregów
#np. lambda = 0.0 logarytmowanie, lambda = 0.5 pierwiastkowanie, lambda = 1.0 brak transformacji
from scipy.stats import boxcox
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'] = boxcox(dataframe['passengers'], lmbda=0.0)
plt.figure(1)
# line plot
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


#możemy również automatycznie wyznaczyć najlepszy parametr lambda dla rozważanego szeregu
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
dataframe = pd.DataFrame(series.values)
dataframe.columns = ['passengers']
dataframe['passengers'], lam = boxcox(dataframe['passengers'])
print('Lambda: %f' % lam)
plt.figure(1)
# line plot
plt.subplot(211)
plt.plot(dataframe['passengers'])
# histogram
plt.subplot(212)
plt.hist(dataframe['passengers'])
plt.show()


# zastosowanie średniej kroczącej w celu przygotowania danych
#wczytanie danych
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0, parse_dates=True, squeeze=True)

# wyznaczenie wartości średniej dla trzech wartości t-2, t-1 i t
rolling = series.rolling(window=3)
print(rolling)
rolling_mean = rolling.mean()
print(rolling_mean.head(10))

#wykres danych oryginalnych i po zastosowaniu średniej kroczącej
series.plot()
rolling_mean.plot(color='red')
plt.show()

# wykres części danych oryginalnych i po zastosowaniu średniej kroczącej
series[:100].plot()
rolling_mean[:100].plot(color='red')
plt.show()


# średnia krocząca w celu tworzenia nowych cech szeregu (feature engineering)

#wczytanie danych
series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
df = pd.DataFrame(series.values)
width = 3 #szerokość okna
lag1 = df.shift(1) #przesunięcie oryginalnego szeregu o jeden krok czasowy
lag3 = df.shift(width - 1) #przesunięcie oryginalnego szeregu o krok czasowy zależny od rozmiaru okna
window = lag3.rolling(window=width) 
means = window.mean() #wyznaczenie średniej kroczącej
dataframe = pd.concat([means, lag1, df], axis=1) #połączenie kolumn w jedną ramkę danych
dataframe.columns = ['mean', 't', 't+1']
dataframe


#średnia krocząca jako naiwny podel predykcyjny
from sklearn.metrics import mean_squared_error

series = read_csv('dane/daily-total-female-births.csv', header=0, index_col=0, parse_dates=True, squeeze=True)

# przygotowanie zmiennych
X = series.values
window = 3
history = [X[i] for i in range(window)]
test = [X[i] for i in range(window, len(X))]
predictions = list()

# wyznaczanie średniej kroczącej w celu obliczenia przyszłych wartości szeregu czasowego
for t in range(len(test)):
    length = len(history)
    yhat = np.mean([history[i] for i in range(length-window,length)])
    obs = test[t]
    predictions.append(yhat)
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))
rmse = np.sqrt(mean_squared_error(test, predictions))
print('Test RMSE: %.3f' % rmse)

# wykres
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()

# wykres z mniejszą liczbą danych
plt.plot(test[:100])
plt.plot(predictions[:100], color='red')
plt.show()


#biały szum
#jeżeli szereg czasowy okaże się być białym szumem (całkowita losowość) wówczas rozsądna predykcja nie jest możliwa
#błąd predykcji szeregu czasowego powinien być białym szumem, wówczas jesteśmy pewni,
#że model został zbudowany na esencji sygnału
#szereg czasowy nie jest białym szumem jeżeli któreś z poniższych pytań jest prawdziwe:
#1. czy średnia jest niezerowa?
#2. czy wariancja zmienia się w czasie?
#3. czy wartości szeregu korelują ze sobą po przesunięciu o lag?

#przykład białego szumu

from random import gauss
from random import seed

seed(1)
series = [gauss(0.0, 1.0) for i in range(1000)]
series = pd.Series(series)

# statystyki
print(series.describe())

# wykres
series.plot()
plt.show()

# histogram
series.hist()
plt.show()

# funkcja autokorelacji
pd.plotting.autocorrelation_plot(series)
plt.show()


#błądzenie losowe (random walk)
#błądzenie losowe pozwala na zrozumienie przewidywalności szeregu czasowego

# tworzymy i rysujemy przykładowe błądzenie losowe
seed(123)
random_walk = list()
random_walk.append(-1 if rnd.random() < 0.5 else 1)
for i in range(1, 1000):
    movement = -1 if rnd.random() < 0.5 else 1
    value = random_walk[i-1] + movement
    random_walk.append(value)
plt.plot(random_walk)
plt.show()


# funkcja autokorelacji (FA) błądzenia losowego
# FA pokazuje, że błądzenie losowe nie jest szeregiem stacjonarnym
pd.plotting.autocorrelation_plot(random_walk)
plt.show()


# do wiarygodnego sprawdzenia stacjonarności szeregu służy rozszerzony test Dickey-Fullera (ADF)
# hipoteza zerowa testu to szereg czasowy jest niestacjonarny
# w wyniku testu dostajemy wartość większą od wszystkich krytycznych poziomów ufności
# zatem szereg czasowy jest niestacjonarny
from statsmodels.tsa.stattools import adfuller

result = adfuller(random_walk)
print('ADF Statistic: %f' % result[0])
print('p-value: %f' % result[1])
print('Critical Values:')
for key, value in result[4].items():
    print('\t%s: %.3f' % (key, value))


#z szeregu, który jest błądzeniem losowym możemy stworzyć szereg stacjonarny

#skoro w błądzeniu losowym kolejne wartości zależą od poprzednich, usunięcie tych róźnic stworzy szereg stacjonarny
#szereg nie zawiera teraz żadnych struktur które można wykorzystać w uczeniu maszynowym
diff = list()
for i in range(1, len(random_walk)):
    value = random_walk[i] - random_walk[i - 1]
    diff.append(value)
# wykres różnic
plt.plot(diff)
plt.show()


#i jeszcze FA
#wyraźnie widać, że korelacje są małe i poniżej 95% i 99% przedziału ufności
pd.plotting.autocorrelation_plot(diff)
plt.ylim(-0.5,0.5)
plt.show()


#nie można przeprowadzić rozsądnej predykcji błądzenia losowego
#można jedynie wykonać naiwną predykcję bazując na najbliższych wartościach szeregu t-1 -> t

# dzielimy dane na zbiór uczący i testowy
train_size = int(len(random_walk) * 0.66)
train, test = random_walk[0:train_size], random_walk[train_size:]

# naiwna predykcja uwzględniająca zmianę między kolejnymi elementami równą 1
predictions = list()
history = train[-1]
for i in range(len(test)):
    yhat = history
    predictions.append(yhat)
    history = test[i]
rmse = np.sqrt(mean_squared_error(test, predictions))
print('RMSE: %.3f' % rmse)


# naiwna predykcja uwzględniająca kierunek i wartość zmiany między kolejnymi elementami równą +/-1
predictions = list()
history = train[-1]
for i in range(len(test)):
    yhat = history + (-1 if rnd.random() < 0.5 else 1)
    predictions.append(yhat)
    history = test[i]
rmse = np.sqrt(mean_squared_error(test, predictions))
print('RMSE: %.3f' % rmse)

#naiwna predykcja to najlepsza i jedyna metoda, którą możemy zastosować dla błądzenia losowego


#dekompozycja szeregów czasowych
#szereg czasowy można rozłożyć na dwa główne komponenty
#1. systematyczne - elementy szeregu, które są konsystentne lub rekurencyjne i mogą opisane i modelowane
#2. niesystematyczne - elementy szeregu, które nie mogą być bezpośrednio modelowane

#Komponenty systematyczne to: poziom (wartość średnia), trend (występuje opcjonalnie) i sezonowość (występuje opcjonalnie)
#Komponent niesystematyczny to szum.

#Możemy rozważać dwa modele szeregów czasowych:
#1. addytywne (liniowe): poziom + trend + sezonowość + szum
#2. multiplikatywne (nieliniowe): poziom * trend * sezonowość * szum


# przykład dekompozycji szeregu czasowego przy założeniu modelu addytywnego

from statsmodels.tsa.seasonal import seasonal_decompose
series = [i + randrange(10) for i in range(1,100)]
result_add = seasonal_decompose(series, model='additive', freq=1)
result_add.plot()
plt.show()


#poszczególne składowe dekompozycji są zapisywane w odpowiednich zmiennych
print(result_add.trend)
print(result_add.seasonal)
print(result_add.resid)
print(result_add.observed)


# przykład dekompozycji szeregu czasowego przy założeniu modelu multiplikatywnego

series = [i**2.0 for i in range(1,100)]
result_multi = seasonal_decompose(series, model='multiplicative', freq=1)
result_multi.plot()
plt.show()


# dekompozycja rzeczywistego szeregu czasowego - model multiplikatywny
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
result = seasonal_decompose(series, model='multiplicative')
result.plot()
plt.show()


# dekompozycja rzeczywistego szeregu czasowego - model addytywny
# sprawdźmy różnice pomiędzy prezentowanymi wynikami
series = read_csv('dane/airline-passengers.csv', header=0, index_col=0, parse_dates=True, squeeze=True)
result = seasonal_decompose(series, model='additive')
result.plot()
plt.show()


#Usuwanie trendu z szeregu czasowego
#Możemy wyróżnić kilka cech trendów:
#1. Deterministyczne (monotonicznie rosnące lub malejące) i stochastyczne (monotoniczność trendu się zmienia)
#2. Globalne i lokalne

#Szereg czasowy z trendem to szereg niestacjonarny.
#Usunięcie trendu z szeregu czasowego powoduje, że staje się on stacjonarny.

#Z punktu widzenia uczenia maszynowego usunięcie trendu zmniejsza zaburzenia pomiędzy danymi wyjściowymi i wyjściowymi.
#Z drugiej strony dodanie trendu może poprawić relację pomiędzy predyktorami a zmienną celu.


#Usuwanie trendu poprzez różnicowanie szeregu czasowego

def parser(x):
    return datetime.strptime('200'+x, '%Y-%m')

series = read_csv('dane/shampoo-sales.csv', header=0, index_col=0, parse_dates=True, squeeze=True, date_parser=parser)
X = series.values
print(np.mean(X))

#trend wznoszący jest wyraźnie widoczny na wykresie
plt.plot(X)
plt.show()


# tworzymy nową zmienną, która jest różnicą dwóch kolejnych wartości szeregu
# new_x(t) = x(t) - x(t-1)
diff = list()
for i in range(1, len(X)):
    value = X[i] - X[i - 1]
    diff.append(value)

print(np.mean(diff))
#plt.plot(X)
plt.plot(diff)
plt.show()


#Usuwanie trendu poprzez budowanie modelu regresyjnego 

from sklearn.linear_model import LinearRegression

# budujemy model regrsji liniowej
X = [i for i in range(0, len(series))]
X = np.reshape(X, (len(X), 1))
y = series.values
model = LinearRegression()
model.fit(X, y)


# wyznaczamy trend liniowy korzystając z modelu
trend = model.predict(X)

# wykres danych i trendu
plt.plot(y)
plt.plot(trend)
plt.show()


# usuwamy trend 
detrended = [y[i]-trend[i] for i in range(0, len(series))]

# rusujemy dane bez trendu
print(np.mean(detrended))
plt.plot(detrended)
plt.show()

