#Temat: przekształcanie_danych_tworzenie_nowych_cech

# In[39]:


#ładowanie bibliotek
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


# In[28]:


#ustawienia dla wykresów
get_ipython().run_line_magic('matplotlib', 'inline')
plt.rc('figure', figsize=(10, 6))
plt.rc('font', family='Arial')


# In[46]:


#ustawienia wyświetlania
np.set_printoptions(precision=4, suppress=True)


# In[8]:


#kodujemy wartości zmiennej kategorycznej
categorical_feature = pd.Series(['sunny', 'cloudy', 'snowy', 'rainy', 'foggy'])
mapping = pd.get_dummies(categorical_feature)
mapping


# In[9]:


#przygotowane w ten sposób dane można zapisać w postaci tabeli
np.array([mapping.cloudy])


# In[24]:


#wykorzystanie funkcji LabelEncoder() i OneHotEncoder()
#do budowania zmiennych kategorycznych w postaci liczbowej
le = LabelEncoder()
ohe = OneHotEncoder(categories='auto')

levels = ['sunny', 'cloudy', 'snowy', 'rainy', 'foggy']
fit_levs = le.fit_transform(levels)
ohe.fit([[fit_levs[0]], [fit_levs[1]], [fit_levs[2]], [fit_levs[3]], [fit_levs[4]]])

sunny_var = ohe.transform([le.transform(['sunny'])]).toarray()
cloudy_var = ohe.transform([le.transform(['cloudy'])]).toarray()
snowy_var = ohe.transform([le.transform(['snowy'])]).toarray()
rainy_var = ohe.transform([le.transform(['rainy'])]).toarray()
foggy_var = ohe.transform([le.transform(['foggy'])]).toarray()

print (sunny_var)
print (cloudy_var)
print (snowy_var)
print (rainy_var)
print (foggy_var)


# In[16]:


fit_levs


# In[30]:


#EDA - Exploratory Data Analysis
iris_filename = 'dane\datasets-uci-iris.csv'

iris = pd.read_csv(iris_filename, header=None,
                   names= ['sepal_length',
                           'sepal_width',
                           'petal_length',
                           'petal_width',
                           'target'])
iris.head()


# In[31]:


iris.describe()


# In[32]:


boxes = iris.boxplot(return_type='axes')


# In[33]:


iris.quantile([0.1, 0.9])


# In[34]:


iris.target.unique()


# In[35]:


pd.crosstab(iris['petal_length'] > 3.758667, iris['petal_width'] > 1.198667)


# In[36]:


scatterplot = iris.plot(kind='scatter', x='petal_width', y='petal_length', 
                        s=64, c='blue', edgecolors='white')


# In[37]:


distr = iris.petal_width.plot(kind='hist', alpha=0.5, bins=20)


# In[49]:


#Tworzenie cech
#wczytywanie zbioru
cali = datasets.california_housing.fetch_california_housing()
cali


# In[47]:


#wyznaczenie zmiennych niezależnych i zmiennej celu
X = cali['data']
Y = cali['target']
print(X[0:5])
print(Y[0:5])


# In[48]:


#Dzielimy dane na zbiór treningowy (80%) i testowy (20%)
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, train_size=0.8)


# In[50]:


#budujemy model regresji liniowej
from sklearn.neighbors import KNeighborsRegressor #regresja przy użciu metody kNN
regressor = KNeighborsRegressor() #tworzenie modelu
regressor.fit(X_train, Y_train) #trenowanie modelu
Y_est = regressor.predict(X_test) #estymacja zmiennej celu dla zbioru testowego
print ("MAE=", mean_squared_error(Y_test, Y_est)) #wyznaczenie błędu


# In[51]:


#standaryzujemy dane -> (x - średnia)/(odch. stan.)
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)
regressor = KNeighborsRegressor()
regressor.fit(X_train_scaled, Y_train)
Y_est = regressor.predict(X_test_scaled)
print ("MAE=", mean_squared_error(Y_test, Y_est))


# In[61]:


print(X_train_scaled[:,0].mean())
print(X_train_scaled[:,0].std())


# In[ ]:


#Zadanie 1 Sprawdź metodę MinMaxScaler oraz MaxAbsScaler
#(x - min)/(max - min)


# In[62]:


#gdy dane posiadają punkty oddalone należy skorzystać z innych metod
from sklearn.preprocessing import RobustScaler
scaler2 = RobustScaler()
X_train_scaled = scaler2.fit_transform(X_train)
X_test_scaled = scaler2.transform(X_test)
regressor = KNeighborsRegressor()
regressor.fit(X_train_scaled, Y_train)
Y_est = regressor.predict(X_test_scaled)
print ("MAE=", mean_squared_error(Y_test, Y_est))


# In[64]:


#przykład przekształceń nieliniowych
non_linear_feat = 5 # Średnia liczba mieszkańców 
X_train_new_feat = np.sqrt(X_train[:,non_linear_feat])
X_train_new_feat.shape = (X_train_new_feat.shape[0], 1)
X_train_extended = np.hstack([X_train, X_train_new_feat])

X_test_new_feat = np.sqrt(X_test[:,non_linear_feat])
X_test_new_feat.shape = (X_test_new_feat.shape[0], 1)
X_test_extended = np.hstack([X_test, X_test_new_feat])

scaler = StandardScaler()
X_train_extended_scaled = scaler.fit_transform(X_train_extended)
X_test_extended_scaled = scaler.transform(X_test_extended)
regressor = KNeighborsRegressor()
regressor.fit(X_train_extended_scaled, Y_train)
Y_est = regressor.predict(X_test_extended_scaled)
print ("MAE=", mean_squared_error(Y_test, Y_est))


# In[ ]:


#Zadanie II
#Sprawdź wpływ stosunku podziału danych na zbiór treningowy i testowy 


# In[ ]:


#Zadanie III
#Stwórz model regresji przy użyciu kNN dla danych z wezla GPEC
#Określ predyktory i zmienną celu
#Podziel dane na zbiór treningowy i testowy
#Sprawdź różne metody normalizacji
#Dodaj samodzielnie stworzoną nową zmienną


# In[71]:


#macierz kowariancji określa poziom korelacji pomiędzy parami cech
iris = datasets.load_iris()
cov_data = np.corrcoef(iris.data.T)
print(iris.feature_names)
print(cov_data)


# In[72]:


#wykres macierzy kowariancji
img = plt.matshow(cov_data, cmap=plt.cm.rainbow)
plt.colorbar(img, ticks=[-1, 0, 1], fraction=0.045)
for x in range(cov_data.shape[0]):
    for y in range(cov_data.shape[1]):
        plt.text(x, y, "%0.2f" % cov_data[x,y], 
                 size=12, color='black', ha="center", va="center")
plt.show()


# In[74]:


iris.data.shape


# In[85]:


plt.scatter(iris.data[:,0], iris.data[:,1], c=iris.target, alpha=0.8, 
            s=60, marker='o', edgecolors='white')
plt.show()


# In[95]:


#Analiza głównych składowych
from sklearn.decomposition import PCA
pca_2c = PCA(n_components=2)
X_pca_2c = pca_2c.fit_transform(iris.data)
X_pca_2c.shape


# In[96]:


#wykres wyznaczonych składowych
plt.scatter(X_pca_2c[:,0], X_pca_2c[:,1], c=iris.target, alpha=0.8, 
            s=60, marker='o', edgecolors='white')
plt.show()
pca_2c.explained_variance_ratio_.sum()


# In[97]:


pca_2c.components_


# In[98]:


#PCA z wybieleniem, czyli wymuszana jest jednostkowa wariancja
pca_2cw = PCA(n_components=2, whiten=True)
X_pca_1cw = pca_2cw.fit_transform(iris.data)
plt.scatter(X_pca_1cw[:,0], X_pca_1cw[:,1], c=iris.target,
            alpha=0.8, s=60, marker='o', edgecolors='white')
plt.show()
pca_2cw.explained_variance_ratio_.sum()


# In[103]:


#wymuszenie wartości wariancji
pca_95pc = PCA(n_components=0.95)
X_pca_95pc = pca_95pc.fit_transform(iris.data)
print (pca_95pc.explained_variance_ratio_.sum())
print (X_pca_95pc.shape)


# In[ ]:




