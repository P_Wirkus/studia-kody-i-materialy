﻿#Temat: algorytmy uczenia maszynowego

#wczytujemy biblioteki i dane
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn

wine = pd.read_csv("dane/winequality-all.csv", comment="#")
wine.head()

#sprawdzamy typy
wine.info()

#zmienną kolor ustawiamy jako zmienną kategoryczną
wine.color = wine.color.astype("category")

#algorytm k-średnich - jako przypadek uczenia się bez nadzoru
#czasami nie mamy dostępu do zmiennej objaśnianej y, ale wciąż chcemy dokonać sensownego podziału zbioru na rozłączne klasy
#analiza skupień - chcemy, aby elementy należące do tego samego skupienia były do siebie podobne jak to tylko możliwe
#obserwacje należące do różnych podzbiorów mają się różnić jak najbardziej

#do celów analizy będziemy działać na wystandaryzowanym losowym podzbiorze zbioru winequality
np.random.seed(123)
wine_sample = wine.sample(n=500, random_state=123)
X = wine_sample.iloc[:, :11]
X[0:10] #próbka

#standaryzacja
X = (X - X.mean())/X.std(ddof=1)
X[0:10]

#działanie algorytmu k-średnich (k-means)
#1) określamy żadaną liczbę skupień
#2) dokonujemy losowego podziału zbioru obserwacji X na rozłączne i niepuste skupienia C1, ..., Ck
#3) wyznaczamy środek każdego skupinia jako średnią arytmetyczną po współrzędnych obserwacji przydzielonych 
#do danego skupienia
#4) aktualizujemy przydział każdej obserwacji do skupień, przypisując każdy z punktów do takiego skupienia Ci,
#że odległość między tym punktem a środkiem skupienia jest najmniejsza
#5) powtarzamy kroki 2)-3) tak długo aż zmiana współrzędnych środków będzie wystarczająco mała

#algorytm k-średnich dla k=2
import sklearn.cluster
km = sklearn.cluster.KMeans(n_clusters=2, random_state=123)
km.fit(X)

#znalezione środki centroidów
km.cluster_centers_

#miara zwartości skupień
km.inertia_

#etykiety klas przypisane poszczególnym punktom
km.labels_

#grupowanie punktów zbioru wejściowego
y_pred = km.fit_predict(X)
y_pred[:15]

#musimy znaleźć punkt odniesienia - interesuje nas zgodność między wyznaczonym podziałem a kolorem wina
y_test = wine_sample["color"].cat.codes.values
y_test[:15]

#macierz pomyłek
sklearn.metrics.confusion_matrix(y_test, y_pred)

#zgodność między dwoma podziałami zbioru na skupienia można zmierzyć za pomocą indeksu Fowlkesa-Mallowsa :)
def indeks_fm(Y1, Y2):
    assert len(Y1) == len(Y2)
    assert min(Y1.min(), Y2.min()) >= 0
    k = max(Y1.max(), Y2.max()) + 1
    n = len(Y1)
    
    C1 = [set() for i in range(k)] #k zbiorów
    C2 = [set() for i in range(k)] #k zbiorów
    for i in range(n):
        C1[ Y1[i] ].add(i) #dodaj i-tą obserwację
        C2[ Y2[i] ].add(i) # do Y2[i]-tego zbioru z C2
        
    M = np.zeros((k,k))
    for i in range(k):
        for j in range(k):
            M[i, j] = len(C1[i]&C2[j])
        
    return ((M**2).sum() - n)/(np.sqrt(((M.sum(axis=0)**2).sum() - n) * ((M.sum(axis=1)**2).sum() - n)))
	
#testowanie ideksu FM na równoważnych podziałach
indeks_fm(np.r_[0, 0, 0, 1, 1], np.r_[1, 1, 1, 0, 0])

#test indeksu FM dla kilku losowych podziałów
los = lambda k, n: np.random.randint(0, k, n)
[ indeks_fm(los(k, 1000), los(k, 1000)) for k in range(2, 8) ]

#zgodność między podziałem generowanym przez algorytm k-średnich, a tym który wynika z koloru wina
indeks_fm(y_test, y_pred)
#indeks jest wysoki co oznacza, że zmienna color wyznacza naturalny podział zbioru win na dwie podgrupy

#można oczywiście wykorzystać zaimplementowane metody
#indeks FM
from sklearn.metrics.cluster import fowlkes_mallows_score
#skorygowany indeks Randa
from sklearn.metrics.cluster import adjusted_rand_score 

fowlkes_mallows_score(y_test, y_pred)
adjusted_rand_score(y_test, y_pred)

#odwzorowanie referencyjnych etykiet
kategoria1 = wine_sample["color"].cat.categories
kategoria2 = wine_sample["color"].cat.categories + "_clust"

if np.mean(y_test == y_pred) < np.mean(y_test != y_pred):
    kategoria2 = kategoria2[::-1]
    
kategoria1.values # 0 lub 1 w y_test
kategoria2.values # 0 lub 1 w y_pred

#wyznaczanie środków skupień znalexionych przez algorytm k-średnich oraz tych generowanych przez zmienną kolor
srodki1 = pd.DataFrame(km.cluster_centers_.T, columns=kategoria2, index=X.columns)
srodki2 = X.groupby(wine_sample.color).mean().T
srodki12 = pd.concat([srodki1, srodki2], axis=1)
srodki12

srodki12 = srodki12[['white_clust', 'white', 'red_clust', 'red']]

#wykres wsp środków skupień wyznaczonych metodą k-średnich
srodki12["red_clust"].sort_values(ascending=False).plot()
srodki12["white_clust"].sort_values(ascending=True).plot()
plt.xticks(rotation=85)
plt.show()

#odległość między szacowanymi i referencyjnymi środkami jest niewielka
np.sqrt(np.sum((srodki12.white - srodki12.white_clust)**2))
np.sqrt(np.sum((srodki12.red - srodki12.red_clust)**2))

#analiza głównych składowych
import sklearn.decomposition
pca = sklearn.decomposition.PCA(n_components=2)
pca.fit(X)

#intersują nas dwie pierwsze składowe główne
#ponieważ chcemy zrzutować 11-wymiarową przestrzeń na dwuwymiarową
pd.DataFrame(pca.components_.T, index=X.columns)

pca.explained_variance_ratio_
np.cumsum(pca.explained_variance_ratio_)

#wykres współrzędnych punktów z dwóch skupień wyznaczonych za pomocą algorytmu k-średnich - rzut zbioru danych na dwa kierunki główne
X2 = pca.transform(X) #wyznacz główne składowe
grp = (y_pred==0) #0 -> color

plt.figure(figsize=(10,6))
plt.plot(X2[ grp, 0], X2[ grp, 1], "ko", label=kategoria2[0])
plt.plot(X2[~grp, 0], X2[~grp, 1], "ws", label=kategoria2[1], markeredgecolor="k", markeredgewidth=1)
plt.legend(loc=9)
plt.show()

#hierarchiczna analiza skupień
hw = sklearn.cluster.AgglomerativeClustering(n_clusters=2, affinity="euclidean", linkage="ward")

#trenujemy model
y_pred2 = hw.fit(X).labels_

#macierz pomyłek
sklearn.metrics.confusion_matrix(y_test, y_pred2)

#podsumowanie wyników dla dwóch metod klasteryzacji
pd.DataFrame({
    "k-means": fowlkes_mallows_score(y_test, y_pred),
    "ward": fowlkes_mallows_score(y_test, y_pred2)}, index=["Indeks FM"])

#rysujemy dendrogram
import scipy.cluster.hierarchy
hw2 = scipy.cluster.hierarchy.ward(X)

plt.figure(figsize=(16,8))
den = scipy.cluster.hierarchy.dendrogram(hw2, no_labels=True)
plt.show()

