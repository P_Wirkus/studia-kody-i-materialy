DODAWANIE REPOZYTORIUM NA GITLAB:
1. Ustalam, gdzie chce by był folder ze sklonowanym repozytorium. Wybieram pulpit u mnie, więc tak:
cd C:\Users\Patryk\Desktop
2. Kopiuję link HTTPS z nowoutworzonego na Gitlab repozytorium:
git clone link_HTTPS
3. Na pulpicie (bo to wybrane przeze mnie miejsce) utworzył się folder, który ma tę samą nazwę jak repozytorium na Gitlab
4. Żeby wejść we wnętrze tego sklonowanego repozytorium, muszę napisać:
cd nazwa_folderu
5. Sprawdzam status
git status
6. Przerzucam pliki, które chcę zamieścić na repozytorium do folderu
7. Aby pliki dodały się do repozytorium na Gitlab (a nie tylko były w folderze) klikam:
git add .
8. Sprawdzam status, by sprawdzić, czy i ile plików się dodało:
git status
9. Dodaję opis commitu:
git commit -m "dodałem plik"
10. Odświeżam repozytorium na Gitlab po wcześniejszym wpisaniu komendy:
git push
11. Sprawdzam, czy na Gitlab nastąpiła aktualizacja repozytorium
*. Opcjonalnie: dodaję członków do repozytorium ustalając ich rolę:
member -> członek (nazwa osoby na Gitlab) -> funkcja (np. Reporter)

JAK DODAĆ PLIK DO ISTNIEJĄCEGO JUŻ REPOZYTORIUM, BY NIE TWORZYĆ NOWEGO ?
12. Wrzuć nowy plik do folderu, a następnie daj komendę:
git add "nazwa_pliku.rozszerzenie_pliku" (np.: git add "1.png" albo git add "tekst.txt")
13. Sprawdź, czy plik się dodał:
git status
14. Dodaj commit:
git commit -m "dodany tekst"
15. Sprawdź, czy się commit dodał
git status
16. git push
17. Odśwież repozytorium na Gitlab i sprawdź, czy się dodał plik

DODANIE 2+ PLIKÓW JEDNOCZEŚNIE DO ISTNIEJĄCEGO REPOZYTORIUM NA GITLAB:
18. git add .
19. git commit -m "opis commita"
20. git push
21. Odśwież repozytorium na Gitlab i sprawdź, czy się dodał plik


INNE:

UWAGA: Docelowo stwórz folder 'Kody z zajęć' i w nim foldery z kodami w: Python, R, SAS itp.
UWAGA: Dane do Gitlab: Username-P_Wirkus, Password-Piktus07$

Dodawanie plików do nowego repozytorium:
- cd C:\Users\Patryk\Desktop
- git clone HTTPS
- cd nazwa folderu z pulpitu ; będzie ta sama jak w repozytorium na Gitlab
- git status
- wrzuć pliki w floder
- git add .
- git status
- git commit -m "własny komentarz opisujący (najlepiej) status wykonywanego zadania"
- git status
- git push
- odśwież Gitlab, by sprawdzić czy pliki się dodały i dodaj członków (zwykle jako 'Reporter') 

Kod w Anaconda Powershell Prompt (anaconda3) do tworzenia repozytorium 'KodywPython':
- cd C:\Users\Patryk\Desktop
- git clone HTTPS (https://gitlab.com/P_Wirkus/kodywpython.git) z repozytorium na Gitlab
- cd kodywpython (kodywpython - nazwa folderu utworzonego na pulpicie po sklonowaniu repozytorium z Gitlab)
- wrzucasz pliki w folder kodywpython
- git add .
- git commit -m "Kody w Python z przedmiotów: Big data, Statystyka II i ZP"
- git status
- git push
- odśwież Gitlab, by sprawdzić czy pliki się dodały i dodaj członków (zwykle jako 'Reporter')