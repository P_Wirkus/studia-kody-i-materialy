LIBNAME mylib "C:\Users\student\Desktop\";
RUN;
DATA mylib.students;
INPUT Name$ Surname$ ShoeSize;
DATALINES;
Jan Kowalski 45 
Maja Harda 36 Kasia Kil 39 
Honorata Mobpke 41 
Kajetan Straszny 49 
;
 RUN;

/* �wiczenie 1 */
Proc sort data=mylib.students out=mylib.by_surname_asc;
BY Surname;
RUN;

/* �wiczenie 2 */
Proc sort data=mylib.students out=mylib.by_surname_desc;
BY DESCENDING Surname;
RUN;

/* �wiczenie 3 */
Proc sort data = sashelp.electric out=mylib.sorted_electric
(Where = (year=1999)
KEEP = customer revenue year);
BY revenue;
BY DESCENDING customer ;
RUN;
DATA mylib.zad3_koniec;
SET mylib.sorted_electric;
KEEP customer revenue;
RUN;

/* �wiczenie 4 */
data account;
   input Company $ 1-22 Debt 25-30 AccountNumber 33-36
         Town $ 39-51;
   datalines;
Paul's Pizza             83.00  1019  Apex
World Wide Electronics  119.95  1122  Garner
Strickland Industries   657.22  1675  Morrisville
Ice Cream Delight       299.98  2310  Holly Springs
Watson Tabor Travel      37.95  3131  Apex
Boyd & Sons Accounting  312.49  4762  Garner
Bob's Beds              119.95  4998  Morrisville
Tina's Pet Shop          37.95  5108  Apex
Elway Piano and Organ    65.79  5217  Garner
Tim's Burger Stand      119.95  6335  Holly Springs
Peter's Auto Parts       65.79  7288  Apex
Deluxe Hardware         467.12  8941  Garner
Pauline's Antiques      302.05  9112  Morrisville
Apex Catering            37.95  9923  Apex
;
RUN;
Proc sort DATA = mylib.account
OUT = mylib.zad4a
DUPOUT = mylib.zad4b NODUPKEY;
BY Town;
RUN;

/* �wiczenie 5 */
DATA mylib.studenci;
LENGTH Imie$9, Nazwisko$10, NrButa 3.;
INPUT Imie$ Nazwisko$ NrButa;
DATALINES;
DATALINES;
Jean Baptiste 49
Kate Zweerink 37
Helga Jensen 40
Yo Tang 49
;
RUN;
DATA mylib.zad5;
SET mylib.syudents mylib.studenci
(RENAME=(Imie=Name Nazwisko=Surname Nrbuta=Shoesize));
RUN;

/* �wiczenie 6 */
data mylib.publications;
length Department Manager $ 10;
INPUT Manager $ Department $ Project $ StaffCount;
datalines;
Cook Writing WP057 5
Deakins Writing SL827 7
Franscombe Editing MP971 4
Henry Editing WP057 3
King Production SL827 5
Krysonski Production WP057 3
Lassiter Graphics SL827 3
Miedema Editing SL827 5
Morard Writing MP971 6
Posey Production MP971 4
Spackle Graphics WP057 2
;
run;
data mylib.research_development;
length Department Manager $ 10;
INPUT Project $ Department $ Manager $ StaffCount;
datalines;
MP971 Designing Daugherty 10
MP971 Coding Newton 8
MP971 Testing Miller 7
SL827 Designing Ramirez 8
SL827 Coding Cho 10
SL827 Testing Baker 7
WP057 Designing Hascal 11
WP057 Coding Constant 13
WP057 Testing Slivko 10
;
run;
PROC SORT DATA = mylib.publications;
BY Project;
RUN;
PROC SORT DATA = mylib.research_development;
BY Project;
RUN;
DATA mylib.zad6;
SET mylib.publications mylib.research_development;
BY Project;
RUN;
