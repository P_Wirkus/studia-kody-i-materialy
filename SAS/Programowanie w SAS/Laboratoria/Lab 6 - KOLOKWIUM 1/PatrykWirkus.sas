/* KOLOKWIUM_1 - Patryk Wirkus */
/* �wiczenie 1 */
LIBNAME KOLO BASE "C:\Users\student\Desktop\Kolokwium" ;
RUN;


/* �wiczenie 2 */
data kolo.zadanie_2;
length imie_lekarza$30. nazwisko_lekarza$12. specjalista$10.;
input imie_lekarza$ nazwisko_lekarza$ specjalista$;
datalines;
Felicja Zdrowotko internista
Marianna Hart-Ducha geriatra 
Milena Dobrotliwa pediatra
;
RUN;


/* �wiczenie 3 */
data KOLO.pacjenci;
input lp$  imie$  nazwisko$  rok_urodzenia ;
dlugosc_zycia = 2019-rok_urodzenia;
datalines;
101 Maciej Banach 1945
102 Eugeniusz Sobolewski 2017
103 Karol Orlicz 2005
104 Mateusz Olach 1995
105 Emilia Robolewska 1978
106 Karolina Liczman 2010
107 Leon Kowalski 1978
108 Tomasz Polowski 1989
109 Remigiusz Terlicz 1939
110 Miron Olaf 1995
111 Emma Tobolewska 1928
112 Ewelina Licz-Pan 1969
113 Emma Tobolewska 1976
114 Ewa Chorowita 1940
;
run;
data KOLO.dzieci;
set pacjenci;
WHERE rok_urodzenia>2000;
RUN;
data KOLO.sredni;
set KOLO.pacjenci;
WHERE rok_urodzenia<1999 & dlugosc_zycia>1948;
RUN;
data KOLO.starsi;
set pacjenci;
WHERE rok_urodzenia<1949;
RUN;


/*
�wiczenie 4 
data KOLO.do_internisty; 
set KOLO.sredni;
specjalista = "internista";
length internista$10.;
input internista$;
datalines;
internista
internista
internista
internista
internista
internista
;
RUN;
*/

/* Drugi spos�b: */
/* �wiczenie 4 */
/*
data KOLO.do_internisty;
set KOLO.sredni;
specjalista = "internista";
RUN;
/* 


/* �wiczenie 5 */ 
data kolo.lekarze_Razem;
merge kolo.zadanie_2 kolo.do_internisty;  
by specjalista;
if a=b;
run;


/* �wiczenie 6 */
ods rtf;
proc print data=kolo.Zadanie_5;
run;
ods rtf close;

