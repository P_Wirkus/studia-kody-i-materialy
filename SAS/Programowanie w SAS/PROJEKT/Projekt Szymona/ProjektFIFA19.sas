/*
	Autor : Szymon Tokarski
	Tytu� : Analiza bazdy danych FIFA 19
	Opis  : 
		Projekt ma na celu analize danych zawodnik�w kt�rzy znale�li si� w grze FIFA 19.
		W projektcjie zawarte jest m.in : analiza zawodnik�w i ich umiej�tno�ci pod wzgl�dem narodowo�ci oraz przynale�no�ci do klubu.
*/

*Scie�a do bilioteki;
LIBNAME wynik 'D:\Projekty\Programowanie w SAS\';


*Zanim bedziemy mogli korzysta� z bazy danych to musimy ja sformatowa�.;
PROC IMPORT DATAFILE = 'D:\Projekty\Programowanie w SAS\data.csv'
	OUT=wynik.ParseData
    DBMS=csv
    REPLACE
;
RUN;

*Dane potrzebene przy tworzeniu raportu podzbior�w bazy danych;
PROC MEANS DATA = wynik.ParseData
	N
	NMISS
	NOPRINT
;
OUTPUT OUT = ZrodloDane;
RUN;

*Makro to s�u�y do wypisania obserwacji z pliku �r�d�owego. F oznacza pocz�tek a L ilo�� obserwacji;
%macro Napisz(f,l, data);
PROC PRINT DATA = &data (FIRSTOBS=&f OBS=&l);
RUN;
%mend print;

*Makro to s�u�y do stworzenia podzbioru z pliku �r�d�owego. Nazwa to nazwa naszego podzbioru, ilosc to ilosc elementow przypadku gdy chcemy
losowa� elementy z bazy danych. Jezeli w warunku wpiszemy losuj to elementy beda losowane z bazy danych w innym wypadku warunek dziala normalnie
tzn. np. ID < 100. W raporcie nale�y wpisa� TAK lub NIE.;
%macro StworzBaze(nazwa,ilosc,warunek, raport);
%IF &warunek=losuj %THEN 
	%DO;
		DATA temp(keep = k keep = VAR1);		
			DO i = 1 TO &ilosc;
				u = rand("Uniform");
				k = ceil( 18207*u );
				VAR1 = ceil( 18207*u );
				output;
			END;
		RUN;

		PROC SORT DATA = temp;
   			BY VAR1;
		RUN;

		PROC SORT DATA = wynik.ParseData;
 		  BY VAR1;
		RUN;

		DATA &nazwa;		
			Merge Temp(IN = k) wynik.ParseData;
			BY VAR1;
			IF k = VAR1;
		RUN;
		%GOTO &raport;
	%END;
%ELSE
	%DO;
		%IF &warunek!=brak %THEN 
		%DO;
			%GOTO &warunek;
		%END;

		DATA &nazwa;
			SET wynik.ParseData(WHERE=(&warunek) OBS = &ilosc); 
		RUN;
		%GOTO &raport;

		%brak:
		DATA &nazwa;
			SET wynik.ParseData(OBS = &ilosc); 
		RUN;
			
		%GOTO &raport;
	%END;
	
%tak:
	PROC CONTENTS DATA = &nazwa;
	RUN; 

	PROC MEANS DATA = &nazwa
		N
		NMISS
		NOPRINT
	;
	OUTPUT OUT = ZrodloTemp;
	RUN;

	PROC COMPARE BASE = ZrodloDane
		COMPARE = ZrodloTemp
		METHOD=RELATIVE 
	;
	RUN;	
%nie:;
%mend StworzBaze;

%Napisz(1,10, dane);
%StworzBaze(dane,1000, losuj, nie);

*Waga pi�karzy;
PROC FREQ DATA = dane;
	TABLES Weight;
RUN;

*Rozk�ad Pensji pi�karzy w stosunku do ich wieku
Gdzie format odnosi sie do etapu kariery;
PROC FORMAT ;
	value age
		low-25 = "Poczatek"
		25 < -30 = "Najlepszy Okres"
		30 < -high = "Schylek"
	;
RUN;

PROC FREQ DATA = dane;
	tables Value*Age;
	format Age age.;
RUN;

*Zale�no�� d�ugo�ci kontraktu od reputacji mi�dzynarodowej;
PROC FREQ DATA = dane;
	tables Contract_Valid_Until*International_Reputation;
RUN;

*Wykres przedstawia rozk�ad wzrostu pi�karzy;
PROC SORT DATA = dane;
	BY Height;
RUN;

ods graphics on;
PROC FREQ DATA = dane order=DATA;
	TABLES Height /plots=freqplot(type=bar scale=percent);
RUN;
ods graphics off;

*Wykres przedstawiwa ilosc graczy na poszczegolnych pozycjach;
PROC SGPLOT DATA = dane;
  	yaxis label="Ilosc graczy na poszczegolnych pozycjach";
  	vbar Position;
RUN;

*Wykres przedstawiwa stosunek warto�ci do zarobk�w;
proc gplot data=dane;  
	title 'Stosunek warto�ci do zarobk�w'; 
	plot Value*Wage;   
run; 

*Wykres przedstawiwa stosunek wieku do og�lnych umiej�tno�ci;
title "Boxplot Of Sepal Lengths";
proc sgplot data=dane noautolegend;
   vbox Age / category=Overall connect=mean connectattrs=(color=black pattern=mediumdash thickness=1)
                                                    meanattrs=(symbol=plus color=red size=20)
                                                    lineattrs=(color=black)
                                                    medianattrs=(color=black)
                                                    whiskerattrs=(color=black)
                                                    outlierattrs=(color=green symbol=starfilled size=12);
   xaxis display=(noline noticks nolabel);
   yaxis display=(noline noticks) labelattrs=(weight=bold);
run;
title;

*Dane przedstawiaja wiek,og�lne umiej�tno�ci i potencjia� pi�karzy pochodz�cych z konkretnych kraj�w;
PROC MEANS DATA = dane
	MAX
	MIN
	MEAN
;
	class Nationality;
	var Age Overall Potential;
RUN;

*Dane przedstawiaja wiek,og�lne umiej�tno�ci i potencjia� pi�karzy graj�cych w danych klubach;
PROC MEANS DATA = dane
	MAX
	MIN
;
	class Club;
	var International_Reputation Weak_Foot Skill_Moves;
RUN;

