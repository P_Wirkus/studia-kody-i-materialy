CREATE TABLE mezczyzni (pesel_m CHAR(11) PRIMARY KEY, imie CHAR(10), nazwisko CHAR(10));
CREATE TABLE kobiety (pesel_k CHAR(11) PRIMARY KEY, imie CHAR(10), nazwisko CHAR(10));

INSERT INTO mezczyzni VALUES ('83100612345','Adam','Nowak');
INSERT INTO mezczyzni VALUES('84051478787','Jan','Kowalski');
INSERT INTO mezczyzni VALUES('84022654545','Piotr','Sikora');
INSERT INTO mezczyzni VALUES('82121598098','Karol','Karolak');
INSERT INTO mezczyzni VALUES('83060214144','Marek','Marecki');
INSERT INTO kobiety VALUES ('82040512211','Anna','�roda');
INSERT INTO kobiety VALUES ('83040512211','Bo�ena','Czwartek');
INSERT INTO kobiety VALUES ('82110512211','Justyna','Piatek');
INSERT INTO kobiety VALUES ('83101212211','Magda','Sobota');
INSERT INTO kobiety VALUES ('82040512299','Kinga','Niedziela');

CREATE TABLE sluby1 (lp number PRIMARY KEY,pesel_m CHAR(11) REFERENCES mezczyzni(pesel_m), pesel_k CHAR(11) REFERENCES kobiety (pesel_k),data_s DATE, data_r DATE);
INSERT INTO sluby1 VALUES (1,'83100612345','82040512211',TO_DATE('02/05/2009','DD/MM/YYYY'),NULL);
INSERT INTO sluby1 VALUES (2,'82121598098','82040512299',TO_DATE('17/04/2011','DD/MM/YYYY'),NULL);
INSERT INTO sluby1 VALUES (3,'83060214144','82110512211',TO_DATE('12/07/2008','DD/MM/YYYY'),TO_DATE('03/01/2012','DD/MM/YYYY'));
INSERT INTO sluby1 VALUES (4,'83060214144','83101212211',TO_DATE('12/02/2012','DD/MM/YYYY'),NULL);

CREATE TABLE sluby2 (lp number PRIMARY KEY,pesel_m CHAR(11) REFERENCES mezczyzni(pesel_m) ON DELETE SET NULL, pesel_k CHAR(11) REFERENCES kobiety (pesel_k) ON DELETE SET NULL,data_s DATE, data_r DATE);
INSERT INTO sluby2 VALUES (1,'83100612345','82040512211',TO_DATE('02/05/2009','DD/MM/YYYY'),NULL);
INSERT INTO sluby2 VALUES (2,'82121598098','82040512299',TO_DATE('17/04/2011','DD/MM/YYYY'),NULL);
INSERT INTO sluby2 VALUES (3,'83060214144','82110512211',TO_DATE('12/07/2008','DD/MM/YYYY'),TO_DATE('03/01/2012','DD/MM/YYYY'));
INSERT INTO sluby2 VALUES (4,'83060214144','83101212211',TO_DATE('12/02/2012','DD/MM/YYYY'),NULL);

CREATE TABLE sluby3 (lp number PRIMARY KEY,pesel_m CHAR(11) REFERENCES mezczyzni(pesel_m) ON DELETE CASCADE, pesel_k CHAR(11) REFERENCES kobiety (pesel_k) ON DELETE CASCADE,data_s DATE, data_r DATE);
INSERT INTO sluby3 VALUES (1,'83100612345','82040512211',TO_DATE('02/05/2009','DD/MM/YYYY'),NULL);
INSERT INTO sluby3 VALUES (2,'82121598098','82040512299',TO_DATE('17/04/2011','DD/MM/YYYY'),NULL);
INSERT INTO sluby3 VALUES (3,'83060214144','82110512211',TO_DATE('12/07/2008','DD/MM/YYYY'),TO_DATE('03/01/2012','DD/MM/YYYY'));
INSERT INTO sluby3 VALUES (4,'83060214144','83101212211',TO_DATE('12/02/2012','DD/MM/YYYY'),NULL);