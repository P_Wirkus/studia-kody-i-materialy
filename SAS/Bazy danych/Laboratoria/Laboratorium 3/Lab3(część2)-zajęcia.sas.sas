/* a) */
proc sql;
describe table sashelp.shoes;	 
SELECT * FROM sashelp.shoes;
quit;

proc sql;
create table work.Buty LIKE sashelp.shoes;
quit;

proc sql;
SELECT * FROM work.Buty
quit;

proc sql;
insert into work.Buty
VALUES ('Africa', 'Boot', 'Addis Ababa', 12, 30, 20, 100)
VALUES ('Pacific', 'Sandal', 'Auckland', 22, 60, 40, 200);
quit;

/* b) */
proc sql;
insert into work.Buty
SELECT * FROM sashelp.shoes WHERE REGION in("Asia");
quit;

/* c) */
proc sql;
create table work.Buty1 like sashelp.shoes;
quit;
proc sql;
insert into work.Buty1
SELECT Region, Product, Stores FROM sashelp.shoes WHERE Stores BETWEEN 1 AND 10;
quit;
proc sql;
create table work.Buty2 as SELECT Region, Product, Stores FROM sashelp.shoes WHERE Stores BETWEEN 1 AND 10;
quit;
proc sql;
SELECT * FROM work.Buty2;
quit;
