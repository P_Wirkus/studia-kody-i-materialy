a)
CREATE TABLE Studenci (
Indeks NUMBER(6),
Nazwisko VARCHAR2(20),
Imie VARCHAR2(25),
Ocena NUMBER(2,1),
Data_zal  DATE DEFAULT SYSDATE
);

b)
INSERT INTO Studenci VALUES
(123456, 'Kowalski', 'Jan', 4, TO_DATE('01/02/2019','DD/MM/YYYY'));
INSERT INTO Studenci VALUES
(135791, 'Nowak', 'Piotr', 2, TO_DATE('02/02/2019','DD/MM/YYYY'));
INSERT INTO Studenci VALUES
(147036, 'Lewandowski', 'Robert', 5, TO_DATE('03/02/2019','DD/MM/YYYY'));
INSERT INTO Studenci VALUES
(159371, 'Milik', 'Arkadiusz', 3, TO_DATE('04/02/2019','DD/MM/YYYY'));
INSERT INTO Studenci VALUES
(161616, 'Pi�tek', 'Krzysztof', 4, TO_DATE('05/02/2019','DD/MM/YYYY'));

c)
INSERT INTO Studenci(Indeks, Nazwisko, Imie) VALUES (112233, 'Nawotka', 'Jakub');
INSERT INTO Studenci(Indeks, Nazwisko, Imie) VALUES (114477, 'Knop', 'Michal');

d)
SELECT * FROM  Studenci ORDER BY Nazwisko;
e)
SELECT Nazwisko, Imie FROM Studenci WHERE Ocena>=3 ;




