CREATE TABLE laborki 
(
nr_indeksu VARCHAR2(6) PRIMARY KEY,
imie VARCHAR2(25),
nazwisko VARCHAR2(25),
grupa NUMBER(1) DEFAULT 1 CONSTRAINT check_gr
          CHECK (grupa IN (1,2,3,4))
);

INSERT INTO laborki VALUES
('123456', 'Jan', 'Kowalski',1);
INSERT INTO laborki VALUES
('135791', 'Piotr', 'Nowak',2);
INSERT INTO laborki VALUES
('147036', 'Robert', 'Lewandowski',3);
INSERT INTO laborki VALUES
('159371', 'Arkadiusz', 'Milik',4);
INSERT INTO laborki VALUES ('161616', 'Krzysztof','Piatek',1);

ALTER TABLE laborki MODIFY nazwisko NOT NULL;
ALTER TABLE laborki ADD ocena NUMBER(2);
ALTER TABLE laboratorium DROP(imie);

INSERT INTO laborki VALUES
(723456, 'Kowalczyk',5);
INSERT INTO laborki VALUES
(735791, 'Nowaczyk',6);

DELETE FROM laborki WHERE grupa>3;

RENAME laborki TO laboratorium;

SELECT * FROM laboratorium;