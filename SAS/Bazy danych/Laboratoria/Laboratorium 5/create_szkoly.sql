CREATE TABLE szkoly (lp NUMBER PRIMARY KEY, wojewodztwo VARCHAR2(25) NOT NULL,
miasto VARCHAR2(15),rodzaj_szk VARCHAR2(10), liczba_uczniow NUMBER, liczba_szk
NUMBER);

INSERT INTO szkoly VALUES (1,'pomorskie','Gdynia','podstawowa',120,15);
INSERT INTO szkoly VALUES (2,'pomorskie','Gdynia','gimnazjum',152,18);
INSERT INTO szkoly VALUES (3,'pomorskie','Gdynia','�rednia',40,5);
INSERT INTO szkoly VALUES (4,'pomorskie','Gda�sk','podstawowa',230,21);
INSERT INTO szkoly VALUES (5,'pomorskie','Gda�sk','gimnazjum',144,7);
INSERT INTO szkoly VALUES (6,'pomorskie','Tczew','podstawowa',56,3);
INSERT INTO szkoly VALUES (7,'pomorskie','Tczew','�rednia',10,1);
INSERT INTO szkoly VALUES (8,'pomorskie','Wejherowo','podstawowa',4,35);
INSERT INTO szkoly VALUES (9,'pomorskie','Sopot','�rednia',23,2);
INSERT INTO szkoly VALUES (10,'pomorskie','Sopot','podstawowa',121,3);
INSERT INTO szkoly VALUES (11,'ma�opolskie','Krak�w','podstawowa',340,15);
INSERT INTO szkoly VALUES (12,'ma�opolskie','Krak�w','�rednia',120,4);
INSERT INTO szkoly VALUES (13,'ma�opolskie','Krak�w','gimnazjum',124,6);
INSERT INTO szkoly VALUES (14,'ma�opolskie','Zakopane','podstawowa',187,3);
INSERT INTO szkoly VALUES (15,'zachodniopomorskie','Koszalin','�rednia',56,1);
INSERT INTO szkoly VALUES (16,'zachodniopomorskie','Koszalin','gimnazjum',127,2);
INSERT INTO szkoly VALUES (17,'zachodniopomorskie','Szczecin','�rednia',520,6);
INSERT INTO szkoly VALUES (18,'zachodniopomorskie','Szczecin','podstawowa',343,7);
INSERT INTO szkoly VALUES (19,'zachodniopomorskie','Szczecin','gimnazjum',298,5);
INSERT INTO szkoly VALUES (20,'wielkopolskie','Pozna�','�rednia',475,6);
INSERT INTO szkoly VALUES (21,'wielkopolskie','Pozna�','gimnazjum',345,8);
INSERT INTO szkoly VALUES (22,'wielkopolskie','Pi�a','podstawowa',55,3);
INSERT INTO szkoly VALUES (23,'wielkopolskie','Pi�a','�rednia',78,4);
INSERT INTO szkoly VALUES (24,'wielkopolskie','Leszno','podstawowa',98,1);
INSERT INTO szkoly VALUES (25,'wielkopolskie','Konin','gimnazjum',75,2);
INSERT INTO szkoly VALUES (26,'wielkopolskie','Kalisz','podstawowa',230,3);
INSERT INTO szkoly VALUES (27,'wielkopolskie','Kalisz','gimnazjum',198,4);
INSERT INTO szkoly VALUES (28,'wielkopolskie','Wrze�nia','podstawowa',150,2);
INSERT INTO szkoly VALUES (29,'dolno�l�skie','Wroc�aw','podstawowa',430,5);
INSERT INTO szkoly VALUES (30,'dolno�l�skie','Wroc�aw','�rednia',130,2);
INSERT INTO szkoly VALUES (31,'dolno�l�skie','Wroc�aw','gimnazjum',140,3);
INSERT INTO szkoly VALUES (32,'dolno�l�skie','Jelenia G�ra','podstawowa',80,2);
INSERT INTO szkoly VALUES (33,'dolno�l�skie','Jelenia G�ra','gimnazjum',130,3);