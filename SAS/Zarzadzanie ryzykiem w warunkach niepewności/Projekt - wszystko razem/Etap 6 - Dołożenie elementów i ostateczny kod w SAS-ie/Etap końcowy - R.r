# CZʌ� PIERWSZA - wczytanie zbior�w danych oraz wyznaczanie ich parametr�w

library(dplyr)
library(moments)
library(ggplot2)

#Microsoft

microsoft <- read.csv("C:/Users/Patryk/Desktop/MICROSOFT.csv")
microsoft <- microsoft[-61,c(1,2,5)]

microsoft <- microsoft %>% 
  mutate(Stopa_straty_Microsoft = round((microsoft[,2]-microsoft[,3])*100/microsoft[,2],4)) %>%
  rename('Data'='Date','Cena_otwarcia' = 'Open', 'Cena_zamkni�cia' = 'Close')

summary(microsoft[,4])
m_Odchylenie_standardowe <- sd(microsoft[,4])
m_Średnia <- mean(microsoft[,4])
m_Wariancja <- var(microsoft[,4])
m_Skośność <- skewness(microsoft[,4])
m_Kurtoza <- kurtosis(microsoft[,4])

Charakterystyki_liczbowe_Microsoft <- data.frame(m_Odchylenie_standardowe, m_Średnia, m_Wariancja, m_Skośność, m_Kurtoza)

Charakterystyki_liczbowe_Microsoft

Hist_stopy_m <- ggplot(microsoft, aes(x=Stopa_straty_Microsoft)) +
  geom_histogram(color="white", fill="blue", bins = 10) +
  ylab('')

Hist_stopy_m

microsoft[,1] <- as.Date(microsoft[,1])
Wykres_cena_m <- ggplot(microsoft, aes(x=Data, y=Cena_otwarcia, group = 1)) +
  geom_line(color='blue', size = 2) +
  scale_x_date() +
  coord_cartesian(ylim=c(0,150))

Wykres_cena_m

#Apple

apple <- read.csv("C:/Users/Patryk/Desktop/APPLE.csv")
apple <- apple[-61,c(1,2,5)]

apple <- apple %>%
  mutate(Stopa_straty_Apple = round((apple[,2]-apple[,3])*100/apple[,2],4)) %>%
  rename('Data'='Date','Cena_otwarcia' = 'Open', 'Cena_zamkni�cia' = 'Close')

summary(apple[,4])
a_Odchylenie_standardowe <- sd(apple[,4])
a_Średnia <- mean(apple[,4])
a_Wariancja <- var(apple[,4])
a_Skośność <- skewness(apple[,4])
a_Kurtoza <- kurtosis(apple[,4])

Charakterystyki_liczbowe_Apple <- data.frame(a_Odchylenie_standardowe, a_Średnia, a_Wariancja, a_Skośność, a_Kurtoza)

Charakterystyki_liczbowe_Apple

Hist_stopy_a <- ggplot(apple, aes(x=Stopa_straty_Apple)) +
  geom_histogram(color="white", fill="darkgreen", bins = 10) +
  ylab('') 

Hist_stopy_a

apple[,1]<- as.Date(apple[,1])
Wykres_cena_a <- ggplot(apple, aes(x=Data, y=Cena_otwarcia, group = 1)) +
  geom_line(color='darkgreen', size = 2) +
  scale_x_date() +
  coord_cartesian(ylim=c(0,250))

Wykres_cena_a

#Amazon

amazon <- read.csv("C:/Users/Patryk/Desktop/AMAZON.csv")
amazon <- amazon[-61,c(1,2,5)]

amazon <- amazon %>%
  mutate(Stopa_straty_Amazon = round((amazon[,2]-amazon[,3])*100/amazon[,2],4)) %>%
  rename('Data'='Date','Cena_otwarcia' = 'Open', 'Cena_zamkni�cia' = 'Close')

summary(amazon[,4])
am_Odchylenie_standardowe <- sd(amazon[,4])
am_Średnia <- mean(amazon[,4])
am_Wariancja <- var(amazon[,4])
am_Skośność <- skewness(amazon[,4])
am_Kurtoza <- kurtosis(amazon[,4])

Charakterystyki_liczbowe_Amazon <- data.frame(am_Odchylenie_standardowe, am_Średnia, am_Wariancja, am_Skośność, am_Kurtoza)

Charakterystyki_liczbowe_Amazon

Hist_stopy_am <- ggplot(amazon, aes(x=Stopa_straty_Amazon)) +
  geom_histogram(color="white", fill="orange", bins = 10) +
  ylab('')  

Hist_stopy_am

amazon[,1]<- as.Date(amazon[,1])
Wykres_cena_am <- ggplot(amazon, aes(x=Data, y=Cena_otwarcia, group = 1)) +
  geom_line(color='orange', size = 2) +
  scale_x_date() +
  coord_cartesian(ylim=c(0,2000))

Wykres_cena_am

###

ceny <- data.frame(microsoft[,1], microsoft[,2], apple[,2], amazon[,2])
Wykresy_razem <- ggplot(ceny, aes(x=ceny[,1]))+
  geom_line(aes(y=ceny[,2]), color='blue', size=2)+
  geom_line(aes(y=ceny[,3]), color='darkgreen', size=2)+
  geom_line(aes(y=ceny[,4]), color='orange', size=2)

Wykresy_razem


# CZ�� DRUGA - Mardia test
library(MVN)
stopy <- read.table("C:/Users/Patryk/Desktop/stopy.csv", dec=",")
#Rozkład
stopy <- data.frame("Amazon" = amazon[,4], "Apple" = apple[,4], "Microsoft" = microsoft[,4])
mvn(stopy, mvnTest = "mardia")
#Nie jest normalny

mvn(stopy, mvnTest = 'royston')
#Ze wszystkich testow tylko tu wyszedl normalny

sigma <- matrix(cov(stopy),3)
mu <- c(mean(stopy$Amazon), mean(stopy$Apple), mean(stopy$Microsoft))

sigma
mu

#Rozk�ad bez Amazonu

stopyBezAmazonu <- stopy[,-1]

mvn(stopyBezAmazonu, mvnTest = "mardia")

sigmaBA <- matrix(cov(stopyBezAmazonu),2)
muBA <- c(mean(stopyBezAmazonu$Apple), mean(stopyBezAmazonu$Microsoft))

sigmaBA
muBA

#3

library(MVN)
library(MASS)
library(dplyr)

stopy <- data.frame("Amazon" = amazon[,4], "Apple" = apple[,4], "Microsoft" = microsoft[,4])
write.csv(stopy,"C:/Users/Patryk/Desktop/nowe_stopy.csv")

mvn(stopy)

sigma <- matrix(cov(stopy),3)
mu <- c(mean(stopy$Amazon), mean(stopy$Apple), mean(stopy$Microsoft))

sigma
mu

set.seed(1235)
norm <- mvrnorm(10000,mu=rep(mu),Sigma = sigma, empirical=T)

norm <- data.frame("Amazon" = norm[,1], "Apple" = norm[,2], "Microsoft" = norm[,3])

L <- c()
L_Amazon <- c()
L_Apple <- c()
L_Microsoft <- c()
VaR <- c()
ES <- c()
k <- 0
for(i in 0:10){
  for(j in 0:10){
    a <- i*0.1
    b <- j*0.1
    if(a+b<=1){
      k <- k+1
      L <- a*norm[,1]+b*norm[,2]+(1-a-b)*norm[,3]
      L <- sort(L)
      VaR[k] <- L[9500]
      ES[k] <- mean(L[9501:10000])
      
      L_Amazon[k] <- a
      L_Apple[k] <- b
      L_Microsoft[k] <- 1-a-b
    }
  }
}

Portfel <- data.frame(VaR, ES, L_Amazon, L_Apple, L_Microsoft)

#Portfel ze wzgl�du na ES
Portfel %>% filter(ES==min(ES))

#Portfel ze wzgl�du na VaR
Portfel %>% filter(VaR==min(VaR))