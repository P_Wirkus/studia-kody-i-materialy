Symulacja niezawodności

Jaki wpływ na błąd I rodzaju ma:
- niejednorodność wariancji,
- asymetria rozkładu?

Link wyjaśniający to:
http://onlinestatbook.com/2/tests_of_means/robust_sim.html