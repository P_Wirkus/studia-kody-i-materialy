/* Porównanie dwoch grup.

PROBLEM: czy srednia wartosc w obu grupach jest taka sama?

PYTANIE 1. czy nasze grupy są niezalezne czy powiazane?

Grupy zalezne (paired):
zawieraja pomiary dla jednej próbki elementów,
ale dla każdej pozycji są dwa pomiary.

Przyklady grup zaleznych:
- waga przed i po diecie
- wyniki egzaminu
*/
/*
Niezalezne: zawieraja pomiary dla niepowiazanych ze soba probek.

Przyklady grup niezaleznych:
-kobiety i meczyzni
(np. przy badaniu zarobkow wzgledem plci), -dzieci uzywajace pasty z fluorem i
bez fluoru
(przy badaniu stopnia zaawansowania prochnicy). */
/*GRUPYY ZALEZNE - przyklad:*/
/*Rozwazmy pewien eksperyment.
Chemik bada paliwa syntetyczne produkowane z węgla i
za pomocą dwóch różnych metod
chce zmierzyć wartości naftalenu.
Czy srednia wartosc jest rozna od zera
na poziomie istotnosci alpha=0.05?
*/
data chromat;
	input hp std @@;

	/*wczytywanie calej linii*/
	methdiff=hp-std;
	datalines;
12.1 14.7 10.9 14.0 13.1 12.9 14.5 16.2 9.6 10.2 11.2 12.4
9.8 12.0 13.7 14.8 12.0 11.8 9.1 9.7
;
run;

/*Zalozenia do testu t-studenta dla zmiennych zaleznych:
I) każda para pomiarow jest niezalezna od innych par pomiarow
II) roznice pochodza z rozkladu normalnego
Test ten jest odpowiedni dla zmiennych ciaglych
*/
/*KOLEJNE KROKI:
0. postawienie hipotezy:

H0: srednia wartosc roznicy wynosi 0
H1: srednia wartosc jest rozna od  0 (na razie tak ale moze tez byc> albo <)

1. utworzenie zbioru i sprawdzanie bledow (mozna uzyc PROC UNIVARIATE  zeby sprawdzic czy nic dziwnego sie nie dzije )
2. wybor poziomu istotnosci
p=0.05
3. sprawdzamy zalozenia (
I)  Założenie o niezaleznosci par wydaje się uzasadnione,
poniewaz istnieje 10 roznych probek podzielonych na dwie jednostki.

II) Uzywamy PROC UNIVARIATE do sprawdzenia normalnosci	)
*/
proc univariate data=chromat normal;
	ods select TestsForNormality Histogram PPPlot;
	var methdiff;
	histogram methdiff;
	ppplot methdiff;
	title 'Testing for Normality of variable methdiff';
run;

/*nie mamy  podstaw do odrzucenia H0 o normalnosci rozkladu*/
/*W przypadku niepewnosci co do normalnosci lub jesli testy wskazuja na to
ze  dane nie pochodza z rozkladu normalnego mozna rozwazyc przeprowadzenie
testow nieparametrycznych*/
/*5. przeprowadzamy test:*/
/*1 sposob*/
ods select TestsForLocation;

proc univariate data=chromat;
	*domyslnie mu0=0, alpha=0.05 ;
	var methdiff;
	title 'Testing for Differences between Chromatography Methods';
run;

/*2 sposob -proc ttesr bez roznicy*/
proc ttest data=chromat;
	paired hp*std;
	title 'Paired Differences with PROC TTEST';
run;

/*6. Wysnuwamy wnioski na podstawie tabel wynikowych
Statystyka testowa wynosi -3.56511 -
mozemy sprawdzic w tabeli rozkladu i na tej podstawie wysnuc wniosek
albo skorzystac z p-value:
Wartosc p=0.0061<0.05
Odrzucamy wiec hipoteze zerowa 
*/