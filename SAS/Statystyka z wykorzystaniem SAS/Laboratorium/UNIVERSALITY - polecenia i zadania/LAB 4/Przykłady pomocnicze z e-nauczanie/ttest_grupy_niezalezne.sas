/*  test dwuprobkowy dla danych niezaleznych
przyklad z:
https://support.sas.com/documentation/cdl/en/statug/63347/HTML/default/viewer.htm#statug_ttest_a0000000135.htm

zobacz tez: http://onlinestatbook.com/2/tests_of_means/difference_means.html

*/
/*Dane na temat wypasu dla 32 wolow. Porownanie 2 metod wypasu.  
Połowa wołów może wypasać się w sposób ciągły, 
podczas gdy druga połowa jest poddawana kontrolowanemu czasowi wypasu.
 Naukowcy chcą wiedzieć, czy te dwie metody wypasania wpływają w różny sposób 
 na przyrost masy ciała (WtGain). */
data graze;
   length GrazeType $ 10;
   input GrazeType $ WtGain @@;
   datalines;
controlled  45   controlled  62
controlled  96   controlled 128
controlled 120   controlled  99
controlled  28   controlled  50
controlled 109   controlled 115
controlled  39   controlled  96
controlled  87   controlled 100
controlled  76   controlled  80
continuous  94   continuous  12
continuous  26   continuous  89
continuous  88   continuous  96
continuous  85   continuous 130
continuous  75   continuous  54
continuous 112   continuous  69
continuous 104   continuous  95
continuous  53   continuous  21
;
run;


/*Uzywajac PROC MEANS z instrukcja CLASS wyswietlimy  srednia 
i wariancje dla KONKRETNEJ PROBKI.*/
proc sort data=graze;
by GrazeType;
proc means data=WORK.graze mean var;
	class GrazeType;
	var WtGain;
run;

/*Srednia Z PROBY dla kontrolowanego wypasu jest wieksza, ale 
UWAGA!!!!
Roznica przyrostu masy w tej konkretnej probie nie jest
bardzo wazna. Celem naszego badania jest sprawdzenia
czy istnieje roznica dla POPULACJI.*/

/* ZALOZENIA:*/
/*
I. Jednorodnosc wariancji, czyli czy 
obie populacje (tutaj: wypasane w sposob kontrolowany i wypasane w sposob ciagly) 
maja te sama wariancje
*/


/*II. Normalnosc rozkladu populacji*/
proc univariate data=graze normal;
	ods select TestsForNormality Histogram;
	class GrazeType;
	var WtGain;
	histogram WtGain;
run;
/*W obu przypadkach nie mamy podstaw do odrzucenia hipotezy o normalnosci rozkladu */

/*O konsekwencjach naruszenia dwoch pierwszych zalozen mozna sie przekonac tutaj:
http://onlinestatbook.com/2/tests_of_means/robust_sim.html
 Niewielkie i umiarkowane naruszenia założeń I i II nie mają wiekszego znaczenia.
 Duze moga miec. Ciekawy przyklad na koncu strony: 
 https://statystykawpsychologii.blogspot.com/2018/06/testy-parametryczne-i-nieparametryczne.html
 
 Wazne jest natomiast aby nie naruszać założenia III.
 III. Obserwacje w probce sa niezalezne od siebie.
W tym przypadku zalozenie jest  dla kazdego wolu mamy jeden wynik.
*/

/*Przeprowadzmy test rownosci srednich korzystajac z PROC TTEST
H0=0    - nie ma roznicy pomiedzy srednimi
H1 - srednie roznia sie */

proc ttest data=graze h0=0 ;
class GrazeType;
var WtGain;
run; 

/*Jesli chodzi o zalozenie I, to w procedurze ttest przeprowadzany jest test rownosci
wariancji (tabelka nad wykresami). Poniewaz p=0.6981, wiec nie ma podstaw do odrzucenia 
hipotezy o rownosci wariancji. 

Ponadto statystyka testu na rownosc srednich jest podawana zarowno dla wariancji równych,
 jak i nierownych. W naszym przypadku oba testy
 (zarowno metoda Wariancji sumarycznej (p=0.4912) jak i Satterthwaite'a(p=0.4913)) 
 wskazuja na brak dowodow na istotna roznice miedzy srednim  przyrostem wagi 
 dla roznych metod wypasu. 
 */



