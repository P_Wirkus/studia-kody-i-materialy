# Zadanie 1 (Liczność próbki dla testu Pearson i Fishera):
Sprawdź jak liczną powinniśmy mieć próbkę dla testu Pearson chi2 i Fishera, tak aby moc testu nie była 
mniejsza niż 0.8. Załóż, że różnica proporcji między próbkami wynosi 0.05, alpha = 0.1. 
Przewidywany udział np. raka w grupie nieleczonej wynosi 0.3, zaś w leczonej 0.1.

Wskazówki: power, twosamplefreq, pchi, fisher

