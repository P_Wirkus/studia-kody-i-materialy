/*PROC REPORT z WARUNKOWYM KOLOROWANIEM OBSERWACJI*/

ods html file='/folders/myfolders/statystyka2020/lab3/raport.html' (url=none) ;
proc report data=sashelp.class nowd;
	column name age sex height weight;
	define name / display;
	define age / display;
	define sex / order;
	define height / sum;
	define weight / sum;
	compute age;

		if age  <=12 then
			call define(_row_, "style", "style={background=blue}");
		else if 12< age <14 then
			call define(_row_, "style", "style={background=green}");
		else
			call define(_row_, "style", "style={background=red}");
	endcomp;
run;

ods html close;