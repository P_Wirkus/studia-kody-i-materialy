/*Test Chi^2 niezaleznosci
na przykladzie z zadania  z  Lab2 (sastystyka2.pdf)

PROBLEM: czy plec ma wplyw na poziom cholesterolu?
*/


/*Kiedy stosujemy? 
-> gdy obie zmienne są jakosciowe i
-> gdy w tabeli licznosci liczebnosc w kazdej komorce jest >=5
*/
proc print data=sashelp.heart;
run;

/*W tabeli sashelp.heart mamy dwie zmienne zwiazane z poziomem cholesterolu
jest to Cholesterol i Chol_Status. Zmienna Chol_Status jest zmienna jakosciowa.
Sprawdzmy jak wygladaja jej tabele liczebnosci dla poszczegolnych plci.
*/

proc freq data=sashelp.heart ;
	tables Chol_Status*Sex /plots=freqplot;
run;

/*Widzimy, ze Chol_Status przyjmuje 3 wartosci:
Borderline, Desirable i High. 
Zmienna plec ma standardowo dwie wartosci.
Widzimy, ze w kazdej komorce tabeli liczebnosci 
wystepuje wiecej niz 5 elementow.
Wykorzystamy wiec test chi^2 Pearsona zeby zbadac czy istnieje
zaleznosc pomiedzy zmienna Chol_Status, a zmienna Sex.*/
/*
Hipoteza zerowa i alternatywna sa nastepujace:
H0: nie ma zaleznosci pomiedzy analizowanymi zmiennymi jakosciowymi
H1: jest zaleznosc pomiedzy analizowanymi zmiennymi jakosciowymi

Test ten mozemy przeprowadzic w procedurze FREQ - opcja chisq
*/

proc freq data=sashelp.heart ;
	tables Chol_Status*Sex /chisq;
run;

/*Z tabeli wynikowej
 "Statystyki dla tabeli przedstawiającej Chol_Status od Sex"*
 mozemy odczytac nastepujace informacje:
 
 - DF (liczba stopni swobody) wynosi 2
 w przypadku testu chi^2 df=(r-1)(k-1), gdzie
 	 r-liczba wierszy w tabeli licznosci (u nas 3)
	 k - liczba kolumn w tabeli licznosci (u nas 2)
 
 - Wartość Statystyki testowej Chi^2 Pearsona znajdujemy 
 na przecięciu wierszy Chi^2 i Wartosc. 
 Wynosi ona w tym przypadku 3.4541
 - W kolejnej kolumnie w tym samym wierszu mamy
 p -value=0.0631
 - Domyslnie przyjelismy alpha=0.05. Nie mamy wiec podstaw do 
 odrzucenia H0. Zaleznosci nie stwierdzono 

 */


/*Chi^2 jest wrazliwe na wielkosc proby. 
Im wieksza proba tym wieksza statystyka. 
Statystyka niewrazliwa na wielkosc proby 
jest statystyka V-Cramera, ktorej wartosc mozemy znalezc w 
ostatniej kolumnie tej samej tabeli.
Statystyka ta przyjmuje wartosci od 
-1 do 1 dla tabel liczebności 2x2.
Dla wiekszych przyjmuje wartosci od 0 do 1.
Statystyka bliska 1 oznacza silna zaleznosc. 
Im blizej zera tym zaleznosc jest slabsza.

W naszym przypadku jej wartosc wynosi 0.0515, mamy wiec do 
czynienia z bardzo slaba zaleznoscia, co potwierdza wyniki 
testu Chi^2. 

*/
