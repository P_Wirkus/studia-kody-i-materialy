libname sas '/folders/myfolders/sampledata';

proc print data=sas.klienci;
run;

/*w przypadku proc cluster możemy dokonać  standaryzacji w procedurze std tu już nie musimy znać początkowej liczby klastrów*/
/*standard/std - Standaryzacja zmiennych*/
/*method = complete - metoda hierarchiczna (pełnego wiązania) (complete linkage), odległość między skupeniami
jako odległość między najdalszymi sąsiadami z obu skupień(grup)*/


proc cluster data=sas.klienci method=complete outtree=tree standard pseudo ccc ;
	var satisfaction loyalty;
run;


/*przyporządkowanie do klastra*/
proc tree data=tree noprint ncl=4 out=out;
	copy satisfaction loyalty;
run;

/*porównanie*/



title 'complete';

proc sgplot data=out;
	scatter x=satisfaction y=loyalty / group=cluster  markerattrs=(symbol=circlefilled size=10);
run;


title 'k-means';

proc sgplot data=clusters4;
		scatter x=satisfaction y=loyalty /markerattrs=(symbol=circlefilled size=10) 
			group=cluster;
run;

/*Wniosek - podział na 4 klastry był odpowiedni - 1 obserwacja przynależy do innego klastra niż */