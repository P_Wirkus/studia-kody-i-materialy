/*Przeprowadzić regresję logistyczną umożliwiającą sprawdzenie, czy student zostanie 
przyjęty (admittance=Yes) na uczelnię na podstawie jego wyniku SAT 
(The SAT is a standardized test widely used for college admissions in the 
United States).*/




proc import datafile='/folders/myfolders/ECSTAT0/365/2.01. Admittance.csv' 
		dbms=csv replace out=admission;
	getnames=yes;
run;
/*1 sposób*/
proc logistic data=admission plots=oddsratio;
	model Admitted(event='Yes')=SAT;
	score out=przewidywany data=test;
run;


data test;
input SAT ;
datalines;
1300 
1700
1500
;
run; 
/*2. przewidywanie*/
proc logistic data=admission descending plots=oddsratio outmodel=mojmodel;
	model Admitted=SAT;
run;

proc logistic inmodel=mojmodel;
	score  data=test out=przewidywane;
run;




