libname libref "/folders/myfolders/statystyka2020/case_studies";

/*REGRESJA LOGISTYCZNA


MODEL:
logit(p)=log(odds)=log(p/(1-p))=b0+b1 x1+b2 x2 + ... bn xn

SZANSE:
odds= p/(1-p)=exp(b0+b1 x1+b2 x2 + ... bn xn)*/

/*Omówimy budowę modelu regresji logistycznej do przewidywania tego, że klient
nie spłaci kredytu bankowego*/
proc import datafile="/folders/myfolders/statystyka2020/case_studies/Chapter_02_loan_default_dataset.csv" 
		dbms=csv replace out=libref.loan_default;
	getnames=yes;
run;

proc contents data=libref.loan_default;
run;

/*
Mamy 1000 obserwacji (dotyczących 1000 wnioskodawców) i 16 zmiennych;
13 numerycznych i 3   kategoryczne.

Mamy opracować model regresji logistycznej w celu przewidzenia prawdopodobieństwa
niewywiązania się ze spłaty kredytu bankowego.
*/
proc freq data=libref.loan_default;
	tables Default;
run;

/* Default to zmienna zależna lub docelowa w danych,
1 oznacza  niespłacenie kredytu (30% obserwacji),
0 oznacza brak niespłacenia kredytu (70% obserwacji).*/
/*sprawdzamy czy są jakieś braki danych*/
proc means data=libref.loan_default NMISS N;
run;

/*nie ma*/
proc univariate data=libref.loan_default;
	var Saving_amount;
	histogram Saving_amount/normal;
run;

/*próbka duża, histogram w porządku (w książce literówka)*/
/*Przechodzimy do regresji logistycznej:*/
/*Będziemy modelować prawdopodobieństwo niespłacenia kredytu, czyli
Default=1 - używamy do tego opcji descending.
Domyślną opcją  jest ascending, więc domyślnie modelowalibyśmy prawdopodobieństwo
spłacenia kredytu Default= 0
(moglibyśmy też, zamiast descending w instrukcji model napisać model default(event='1'))*/
proc logistic data=libref.loan_default descending;
	class Gender Marital_status Emp_status / param=effect ref=first;
	model default=Checking_amount Term Credit_score Car_loan Personal_loan 
		Home_loan Education_loan Amount Saving_amount Emp_duration Gender 
		Marital_status Age No_of_credit_acc Emp_status / link=logit;
	score out=Logistic_result;
run;

/*Spójrzmy na instrukcję CLASS.
PROC LOGISTIC nie działa bezpośrednio ze zmiennymi kategorycznymi,
koduje każdą zmienną predykcyjną.
Instrukcja CLASS tworzy zestaw zmiennych projektowych (dummy variables)
które reprezentują informacje o każdej zmiennej klasyfikacyjnej
(podobnie możemy zrobić w regresji liniowej, ).
PROC LOGISTIC wykorzystuje zmienne projektowe, a nie oryginalne zmienne*/
/*

DOPASOWANIE:

Sommers_D
Gamma
Tau-a
c  (od 0.5(słabe przewidywanie) do 1(idealne przewidywanie))

Im większe wartości powyższych parametrów, tym większa zdolność predykcyjna

W regresji logistycznej używamy:
Maksimum Likelihood Estimation - Metoda Maksymalnej Wiarygodności
	
Im większe log-likelihood (przeważnie ujemne), tym bardziej jest prawdopodobne, że nasz model jest prawidłowy (szacuje rzeczywistą zależność zmiennych).  
W SASIE mamy -2logL -  im mniejsze, tym dopasowanie jest lepsze. 


*/
/*Sprawdźmy, które zmienne są nieistotne i usuńmy je z modelu */
proc logistic data=libref.loan_default descending plots=(oddsratio effect);
	model default=Checking_amount Term Credit_score Saving_amount Age/ link=logit;
	score out=Logistic_result;
run;



/*1. Dla każdej zmiany jednostkowej w Checking_amount, logarytm szansy na spłatę kredytu bankowego w stosunku do braku spłaty kredytu bankowego zmniejsza się o (-0,004).
2. Podobnie w przypadku Term zmiana jednostkowa w Term oznacza, że logarytm szansy na spłatę kredytu bankowego w stosunku do niewypłacalności kredytu bankowego wzrasta o (0,174).
3. W przypadku jednostronnej zmiany Credit_score w Credit_score logarytm szansy na spłatę kredytu bankowego w stosunku do braku spłaty kredytu bankowego zmniejsza się o (-0,011).
4. W przypadku zmiany jednostkowej Saving_amount w Saving_amount, logarytm szansy na spłatę kredytu bankowego w stosunku do braku spłaty kredytu bankowego zmniejsza się o (-0,004).
5. W przypadku zmiany wieku o jedną jednostkę Wiek, dzienne szanse niewypłacalności kredytu bankowego w stosunku do niewypłacalności kredytu bankowego zmniejszają się o (-0,628).*/