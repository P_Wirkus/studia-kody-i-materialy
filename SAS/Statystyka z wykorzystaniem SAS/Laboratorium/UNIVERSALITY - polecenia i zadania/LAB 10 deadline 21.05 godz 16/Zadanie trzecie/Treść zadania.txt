# Zadanie 3 (Predykcja z regresji):
Pewna firma produkuje wysokiej jakości pręty budowlane. 
Pręty powinny być zahartowane (nagrzane do odpowiedniej temperatury, a następnie schłodzone do niskiej temperatury).
Firma testuje procedurę hartowania prętów. 
Wyniki testu przedstawia poniższa tabelka:
7 1.0 0 10
7 1.7 0 17
7 2.2 0 7
7 2.8 0 12
7 4.0 0 9   
14 1.0 0 31
14 1.7 0 43    
14 2.2 2 33
14 3.8 0 31
14 4.0 0 19
27 1.0 1 56
27 1.7 4 44
27 2.2 0 21
27 3.8 1 22
27 4.0 1 16
51 1.0 3 13
51 2.7 0 1
51 3.2 0 1
51 4.0 0 1
Kolejne kolumny oznaczają: ogrzewanie, schładzanie, liczbę słabych prętów, liczbę prób.
Korzystając z tych danych wyznacz model regresji logarytmicznej opisujący prawdopodobieństwo tworzenia prętów złej jakości. 
Zapisz model tej regresji do nowej tablicy, a następnie dla danych testowych:
50 11.1
11 14.7
92 2.2
53 32.8
14 41.0
65 1.0
16 11.7
57 2.2
28 13.8
79 4.0
80 14.0
gdzie pierwsza kolumna określa parametr ogrzewania zaś druga chłodzenia. 
Oblicz przewidywaną wartość dla każdej obserwacji.

Wskazówki: proc logistic, score, inmodel, outmodel