#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# partutilities(xfrm, y, x, n, p, S) - funkcja obliczaj�ca macierz u�yteczno�ci cz�stkowych
# poziom�w atrybut�w w przekroju respondent�w usl[S, p]
# xfrm - wyra�enie reprezentuj�ce w modelu atrybuty
# y - wektor preferencji empirycznych y[n*S]
# x - macierz reprezentuj�ca profile
# n - liczba profil�w
# S - liczba respondent�w
# p - liczba poziom�w wszystkich atrybut�w z wyrazem wolnym (bez poziom�w odniesienia),
# liczba zmiennych sztucznych plus wyraz wolny
# wywo�anie - usl <- partutilities(xfrm, y, x, n, p, S)

partutilities <- function(xfrm, y, x, n, p, S)
{
   usl <- matrix(0, S, p)   #macierz u�yteczno�ci cz�stkowych poziom�w atrybut�w w przekroju respondent�w
   Y <- vector("numeric", n)
   Ys <- as.data.frame(Y)   #preferencje empiryczne s-tego respondenta
   for(s in 0:(S-1))
   {
      k <- n*s+1            #indeks wskazuj�cy wektor preferencji s-tego respondenta
      for (i in 1:n)
      {
         Ys[i,1] <- y[k,1]
         k <- k+1
      }
      frml <- as.formula(paste("Ys$Y~", paste(xfrm)))
      camodel <- lm(frml)
      u <- as.matrix(camodel$coeff)   #u�yteczno�ci cz�stkowe poziom�w atrybut�w s-tego respondenta
      for(l in 1:p) {usl[(s+1),l] <- u[l]}
   }
   return(usl)
}