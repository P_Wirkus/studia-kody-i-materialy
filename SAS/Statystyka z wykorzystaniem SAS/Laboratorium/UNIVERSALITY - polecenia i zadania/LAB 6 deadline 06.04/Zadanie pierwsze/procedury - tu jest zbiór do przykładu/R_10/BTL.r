#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# BTL(Usym, S, r) - funkcja obliczaj�ca udzia�y w rynku profil�w symulacyjnych na podstawie modelu BTL
# Usym - macierz u�yteczno�ci ca�kowitych profil�w symulacyjnych w przekroju respondent�w
# S - liczba respondent�w
# r - liczba profil�w symulacyjnych
# wywo�anie - Pbtl <- BTL(Usym, S, r)

BTL <- function(Usym, S, r)
{
   share <- matrix(0, r, S)   #macierz udzia��w
   Pbtl <- vector("numeric", r)
   for(s in 1:S) {share[,s] <- Usym[,s]/sum(Usym[,s])}
   for(i in 1:r) {Pbtl[i] <- mean(share[i,])*100}
   return(Pbtl)
}