#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Akademia Ekonomiczna we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza wielowymiarowa z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# utilities(u, Lj) - funkcja obliczaj�ca wszystkie u�yteczno�ci cz�stkowe poziom�w (kodowanie quasi-eksperymentalne - sum contrasts)
# u - u�yteczno�ci cz�stkowe (parametry) poziom�w atrybut�w (liczba parametr�w jest r�wna liczbie zmiennych sztucznych)
# Lj - liczba poziom�w j-tego atrybutu (wektor Lj o d�ugo�ci r�wnej liczbie atrybut�w zawiera liczby poziom�w ka�dego atrybutu)
# wywo�anie - ul <- utilities (u, Lj)

utilities <- function(u, Lj)
{
   m <- length(Lj)   #liczba atrybut�w
   L <- sum(Lj)      #��czna liczba poziom�w
   p <- length(u)    #liczba parametr�w (z wyrazem wolnym)
   b <- vector("numeric", p-1)  #wektor parametr�w bez wyrazu wolnego
   ul <- vector("numeric", L)   #u�yteczno�ci cz�stkowe poziom�w atrybut�w z poziomami odniesienia
   for(i in 1:(p-1)) {b[i] <- u[i+1]}
   i <- 0
   h <- 1
   for(j in 1:m)
   {
      tu <- 0
      l <- Lj[j]-1
      for (k in 1:l)
      {
         i <- i+1
         ul[i] <- b[h]
         tu <- tu+ul[i]
         h <- h+1
      }
      i <- i+1
      ul[i] <- -tu
   }
   return(ul)
}