#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# matexpand(m, n, S, x) - funkcja rozwija macierz uk�adu czynnikowego na zbi�r respondent�w
# w celu estymacji modelu na poziomie zagregowanym
# m - liczba atrybut�w
# n - liczba profil�w
# S - liczba respondent�w
# x - macierz atrybut�w (profile)
# wywo�anie - x <- as.data.frame(matexpand(m, n, S, x))

matexpand <- function(m, n, S, x)
{
   N <- n*S               #liczba obserwacji
   X <- matrix(0, N, m)   #rozwini�ta macierz uk�adu czynnikowego
   k <- 1
   for(s in 1:S)
   {
      for(i in 1:n)
      {
         for(j in 1:m) {X[k,j] <- x[i,j]}
         k <- k+1
      }
   }
   colnames(X) <- names(x)   #nazwy atrybut�w
   return(X)
}