#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Dorota Rozmus     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

wydatki_dane <- read.csv2("wydatki_dane.csv", header=TRUE, row.names=1)
wydatki_dane[,2] <- as.factor(wydatki_dane[,2])
options(OutDec=",")
attach(wydatki_dane)
m1 <- lm(wydatki~grupa*reklama)
m2 <- anova(m1)
print(m2)
detach(wydatki_dane)