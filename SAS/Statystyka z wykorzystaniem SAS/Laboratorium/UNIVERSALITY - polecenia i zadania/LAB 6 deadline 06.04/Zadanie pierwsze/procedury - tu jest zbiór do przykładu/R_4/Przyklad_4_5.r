#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Dorota Rozmus     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

czas_wykonania <- read.csv2("czas_wykonania.csv", header=TRUE, row.names=1)
czas_wykonania[,2] <- as.factor(czas_wykonania[,2])
options(OutDec=",")
attach(czas_wykonania)
srednie <- round(matrix(sapply(split(czas_wykonania$czas, czas_wykonania$plec:czas_wykonania$kwalifikacje), mean), ncol=2), 3)
odchylenia <- round(matrix(sapply(split(czas_wykonania$czas, czas_wykonania$plec:czas_wykonania$kwalifikacje), var), ncol=2), 3)
print("�rednie:", quote=FALSE)
print(srednie)
print("Odchylenia:", quote=FALSE)
print(odchylenia)
detach(czas_wykonania) 