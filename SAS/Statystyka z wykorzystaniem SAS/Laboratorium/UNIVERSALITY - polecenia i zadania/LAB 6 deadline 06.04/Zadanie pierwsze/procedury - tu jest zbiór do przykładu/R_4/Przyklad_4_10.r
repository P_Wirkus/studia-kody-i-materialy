#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Dorota Rozmus     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

wydatki_dane <- read.csv2("wydatki_dane.csv", header=TRUE, row.names=1)
wydatki_dane[,2] <- as.factor(wydatki_dane[,2])
options(OutDec=",")
attach(wydatki_dane)
par(mfrow=c(2,1))
# Zmniejsza margines na rysunku:
par(mar=c(4.2,4,0.8,1.1))
interaction.plot(grupa, reklama, wydatki, ylim=c(10,40), lty=c(1,10), ylab="�rednie wydatki", xlab="Grupa", lwd=2, trace.label="Reklama")
interaction.plot(reklama, grupa, wydatki, ylim=c(10,40), lty=c(1,5,10), ylab="�rednie wydatki", xlab="Reklama", lwd=2, trace.label="Grupa")
detach(wydatki_dane)