#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

x <- seq(0,20,2)
y <- 0:10
plot(x, y, type="n", xlab="", ylab="", axes=FALSE)
axis(1, at=c(0,10,20), labels=c(0,10,20))
axis(2, at=0:9, labels=0:9)
for(i in c(0,10,20)){
  for(j in 0:9){
    if(i+j<26){
      points(i, j, pch=i+j, cex=2)
    }
  }
}