#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Iwona Kasprzyk     Akademia Ekonomiczna w Katowicach
#*  
#*  Rysunek 13.1 do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(ca)
row<-c("a","b","c","d","e")
def.row<-row
column<-c("1","2","3")
datalabel<-list(litery=def.row,cyfry=column)
data1<-expand.grid(litery=def.row,cyfry=column)
dane<-c(11,10,10,9,10,10,11,9,9,11,9,9,10,12,10)
table1<-cbind(data1,count=dane)
tab<-xtabs(count~litery+cyfry, data=table1)

row<-c("a","b","c","d","e")
def.row<-row
column<-c("1","2","3")
datalabel<-list(litery=def.row,cyfry=column)
data1<-expand.grid(litery=def.row,cyfry=column)
dane<-c(20,0,24,2,4,1,24,2,0,23,0,1,0,47,2)
table1<-cbind(data1,count=dane)
tab2<-xtabs(count~litery+cyfry, data=table1)

par(mfrow=c(1,2),pty="s") 
#utworzenie obszaru dla dw�ch rysunk�w
plot(ca(tab), map="rowprincipal",what = c("all", "all"),label=0, main="inercja=0,0076")
options(OutDec=",")
text(ca(tab)$colcoord[,1]/1.1,ca(tab)$colcoord[,2]/1.2,ca(tab)$colnames,cex=0.8)
text(ca(tab)$rowcoord[,1]/5,ca(tab)$rowcoord[,2]/7,ca(tab)$rownames,cex=0.8)
options(OutDec=",")
plot(ca(tab2), map="rowprincipal",what = c("all", "all"),label=0, main="inercja=1,5715")
text(ca(tab2)$colcoord[,1]/1.1,ca(tab2)$colcoord[,2]/1.2,ca(tab2)$colnames,cex=0.8)
text(ca(tab2)$rowcoord[,1]/1.4,ca(tab2)$rowcoord[,2]/1.3,ca(tab2)$rownames,cex=0.8)