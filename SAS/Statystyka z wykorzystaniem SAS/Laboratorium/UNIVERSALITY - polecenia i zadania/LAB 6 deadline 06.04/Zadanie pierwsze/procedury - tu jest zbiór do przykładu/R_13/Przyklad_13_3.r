#***********************************************************************************************************************************************
#*  
#*  (C) 2009    Iwona Kasprzyk     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************
dane <- read.csv2("dane_13_1.csv", header=TRUE)
tab <- xtabs(~sklep+powod, data=dane)
options(OutDec=",")
N <- sum(tab)
liczba_w <- nrow(tab)
liczba_k <- ncol(tab)
suma_w <- rowSums(tab)
suma_k <- colSums(tab)
E <- outer(suma_w, suma_k, "*")/N
df <- (liczba_w-1)*(liczba_k-1)
lambda <- 2/3
z <- 2*(lambda*(lambda+1))^(-1)
CR <- z*sum(sort(tab*((tab/E)^lambda-1)), decreasing = TRUE) 
p <- pchisq(CR,df,lower=FALSE)
mdat <- matrix(c(CR,df,p), nrow = 1, ncol=3, byrow=TRUE, dimnames = list(c("Cressie_Read"), c("CR", "df", " P(> X^2)")))
print(mdat)