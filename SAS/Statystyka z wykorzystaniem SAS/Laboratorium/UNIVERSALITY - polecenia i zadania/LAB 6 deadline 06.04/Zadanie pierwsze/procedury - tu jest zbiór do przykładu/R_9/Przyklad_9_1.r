#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mlbench)
library(rpart)
library(adabag)
options(OutDec=",")
set.seed(123)
data(Vehicle)
l.obs <- nrow(Vehicle)
# podzia� zbioru
test <- sample(1:l.obs, round(l.obs/3), replace=FALSE)
veh.ucz <- Vehicle[-test,]
veh.test <- Vehicle[test,]
# budowa modelu w postaci pojedynczego drzewa klasyfikacyjnego
drzewo.veh <- rpart(Class~., data=veh.ucz, maxdepth=5)
y.pred <- predict(drzewo.veh, newdata=veh.test, type="class")
t <- table(y.pred, veh.test$Class)
blad.rpart <- 1-(sum(diag(t))/sum(t))
print("B��d dla pojedynczego modelu", quote=FALSE)
print(t)
print(blad.rpart)
# budowa modelu zagregowanego metod� bagging
bag.veh <- bagging(Class ~., data=veh.ucz, mfinal=50, maxdepth=5)
bag.veh.pred <- predict.bagging(bag.veh, newdata=veh.test)
print("B��d dla modelu zagregowanego metod� bagging:", quote=FALSE)
print(bag.veh.pred[-1])
# budowa modelu zagregowanego metod� boosting
boost.veh <- adaboost.M1(Class~., data=veh.ucz, mfinal=50, maxdepth=5)
boost.veh.pred <- predict.boosting(boost.veh, newdata=veh.test)
print("B��d dla modelu zagregowanego metod� boosting:", quote=FALSE)
print(boost.veh.pred[-1])