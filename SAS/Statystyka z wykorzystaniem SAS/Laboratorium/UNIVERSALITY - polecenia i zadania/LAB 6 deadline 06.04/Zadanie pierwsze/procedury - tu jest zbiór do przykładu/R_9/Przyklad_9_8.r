#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(ipred) 
library(MASS)
data(Boston)
options(OutDec=",")
set.seed(123)
# zakodowanie zmiennej zero-jedynkowej "chas"
Boston[,4] <- as.factor(Boston[,4])
# podzia� na zbi�r ucz�cy i testowy
l.obs <- nrow(Boston)
set.seed(123)
test <- sample(1:l.obs, size=round(l.obs/3), replace=FALSE)
bos.ucz <- Boston[-test,]
bos.test <- Boston[test,]
# klasyczny model bagging 
bag.bos <- bagging(medv ~ ., data=bos.ucz)
bag.bos.pred <- predict(bag.bos, newdata=bos.test)
mse.bg <- var(bos.test$medv-bag.bos.pred)
print("B��d MSE dla metody bagging:", quote=FALSE)
print(mse.bg)
# budowa modelu bundling
comb.lm <- list(list(model=lm, predict=predict.lm))
model.agr <- bagging(medv ~ ., data=bos.ucz, comb=comb.lm)
# b��d �redniokwadratowy
y.pred <- predict(model.agr, newdata=bos.test)
mse.bd <- var(bos.test$medv-y.pred)
print("B��d MSE dla metody bundling:", quote=FALSE)
print(mse.bd)