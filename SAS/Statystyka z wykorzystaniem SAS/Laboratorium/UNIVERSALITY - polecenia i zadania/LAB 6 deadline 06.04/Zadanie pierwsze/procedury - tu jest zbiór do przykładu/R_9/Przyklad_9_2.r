#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(randomForest)
library(ElemStatLearn)
data(spam)
options(OutDec=",")
set.seed(123)
l.obs <- nrow(spam)
l.zm <- ncol(spam)-1
l.zm2 <- floor(l.zm/2)
# podzia� zbioru
test <- sample(1:l.obs, round(l.obs/3), replace=FALSE)
spam.ucz <- spam[-test,]
spam.test <- spam[test,]
# budowa modelu w postaci drzew losowych
rf.spam <- randomForest(spam~., data=spam.ucz, mtry=l.zm2, do.trace=10, ntree=200, keep.forest=TRUE)
print(rf.spam)