#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(randomForest)
library(ElemStatLearn)
data(spam)
options(OutDec=",")
set.seed(123)
l.obs <- nrow(spam)
l.zm <- ncol(spam)-1
l.zm2 <- floor(l.zm/2)
# podzia� zbioru
test <- sample(1:l.obs, round(l.obs/3), replace=FALSE)
spam.ucz <- spam[-test,]
spam.test <- spam[test,]
x.test <- spam.test[,-58]
y.test <- spam.test$spam
l.mod <- 100
rf.spam2 <- randomForest(spam~., data=spam.ucz, xtest=x.test, ytest=y.test, ntree=l.mod, mtry=l.zm2)
# b��dy klasyfikacji
bl.oob <- rf.spam2$err.rate[,1]
bl.test <- rf.spam2$test$err.rate[,1]
# rysowanie wykresu
plot(1:l.mod, ylim=c(min(bl.test,bl.oob), max(bl.test,bl.oob)), xlab="Liczba drzew", ylab="B��d klasyfikacji", type="n")
points(1:l.mod, bl.oob, col="green", pch=20)
points(1:l.mod, bl.test, col="red", pch=20)
lines(1:l.mod, bl.oob, col="green")
lines(1:l.mod, bl.test, col="red")
# legenda
ll <- c("zbi�r testowy","OOB")
legend("topright", legend=ll, col=c("red","green"), lty=1, pch=20)