#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Micha� Trz�siok     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
library(class)
library(e1071)
data(iris)
names(iris) <- c("ddk","sdk","dp","sp","klasa")
attach(iris)
options(OutDec=",")
set.seed(111)
indeks <- sample(1:nrow(iris), 1)
print(paste("Ze zbioru ucz�cego wybrano obserwacj� nr:", indeks), quote=FALSE)
nowy <- iris[indeks,]
print(paste("Nale�y ona do gatunku:", nowy$klasa), quote=FALSE)
iris149 <- iris[-indeks,]
#Najlepszy model SVM z funkcj� j�drow� Gaussa:
svm.iris <- tune.svm(klasa ~ ., data=iris149, kernel = "radial", gamma=c(0.5, 1), cost=10^(-2:2), tunecontrol=tune.control(sampling="cross", cross=3))
print(summary(svm.iris))
#Funkcja signif daje dwie cyfry znacz�ce:
print(paste("Najmniejszy b��d klasyfikacji dla funkcji j�drowej Gaussa:", signif(svm.iris$best.performance, digits=2)), quote=FALSE)
print("Parametry tego modelu SVM:", quote=FALSE)
print(svm.iris$best.parameters)
#Zapami�tanie najlepszego z dotychczas zbudowanych modeli:
najl.model <- svm.iris$best.model
min.err <- svm.iris$best.performance
#Najlepszy model SVM z wykorzystaniem wielomianowej funkcji j�drowej:
svm.iris <- tune.svm(klasa ~ ., data=iris149, kernel = "polynomial", degree=(2:5), gamma=c(0.5, 1), coef0=10, cost=10^(-2:2), tunecontrol=tune.control(sampling="cross", cross=3))
print(paste("Najmniejszy b��d klasyfikacji dla wielomianowej funkcji j�drowej:", signif(svm.iris$best.performance, digits=2)), quote=FALSE)
print("Parametry tego modelu SVM:", quote=FALSE)
print(svm.iris$best.parameters)
#Je�li zbudowano model lepszy od najlepszego z wcze�niej rozwa�onych to zapami�tujemy go:
if (svm.iris$best.performance < min.err) {najl.model <- svm.iris$best.model; min.err <- svm.iris$best.performance}
#Najlepszy model SVM z wykorzystaniem sigmoidalnej funkcji j�drowej:
svm.iris <- tune.svm(klasa ~ ., data=iris149, kernel = "sigmoid", gamma=c(0.5, 1), coef0=10, cost=10^(-2:2), tunecontrol=tune.control(sampling="cross", cross=3))
print(paste("Najmniejszy b��d klasyfikacji dla sigmoidalnej funkcji j�drowej:", signif(svm.iris$best.performance, digits=2)), quote=FALSE)
print("Parametry tego modelu SVM:", quote=FALSE)
print(svm.iris$best.parameters)
#Je�li zbudowano model lepszy od najlepszego z wcze�niej rozwa�onych to zapami�tujemy go:
if (svm.iris$best.performance < min.err) {najl.model <- svm.iris$best.model; min.err <- svm.iris$best.performance}
#Najlepszy model SVM z wykorzystaniem liniowej funkcji j�drowej:
svm.iris <- tune.svm(klasa ~ ., data=iris149, kernel = "linear", cost=10^(-2:2), tunecontrol=tune.control(sampling="cross", cross=3))
print(paste("Najmniejszy b��d klasyfikacji dla liniowej funkcji j�drowej:", signif(svm.iris$best.performance, digits=2)), quote=FALSE)
print("Parametry tego modelu SVM:", quote=FALSE)
print(svm.iris$best.parameters)
#Je�li zbudowano model lepszy od najlepszego z wcze�niej rozwa�onych to zapami�tujemy go
if (svm.iris$best.performance < min.err) {najl.model <- svm.iris$best.model; min.err <- svm.iris$best.performance}
#Badanie przynale�no�ci do klasy wylosowanej wcze�niej obserwacji
nowy.bez.klasy <- subset(nowy, select=-klasa)
#Najlepsza kombinacja parametr�w i odpowiadaj�cy jej model
klasa.nowy <- predict(najl.model, nowy.bez.klasy)
print(paste("Najlepszy z modeli SVM sklasyfikowa� wybran� obserwacj� jako:", klasa.nowy), quote=FALSE)
print("Parametry najlepszego z wyznaczonych modeli:", quote=FALSE)
print(summary(najl.model))
detach(iris)