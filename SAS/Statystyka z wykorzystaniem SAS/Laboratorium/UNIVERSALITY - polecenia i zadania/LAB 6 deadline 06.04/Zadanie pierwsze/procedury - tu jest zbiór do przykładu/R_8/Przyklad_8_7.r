#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mlbench)
library(rpart)
options(OutDec=",")
set.seed(114)
# podzial zbioru na cz�� ucz�c� i testow�
data(Vehicle)
l.obs <- nrow(Vehicle)
test <- sample(1:l.obs, round(l.obs/3), replace=FALSE)
veh.ucz <- Vehicle[-test,]
veh.test <- Vehicle[test,]
# budowa modelu
drzewo.veh <- rpart(Class~., data=veh.ucz, control=rpart.control(xval=100, cp=0))
plot(drzewo.veh)
tab.cp <- drzewo.veh$cptable
print(tab.cp)