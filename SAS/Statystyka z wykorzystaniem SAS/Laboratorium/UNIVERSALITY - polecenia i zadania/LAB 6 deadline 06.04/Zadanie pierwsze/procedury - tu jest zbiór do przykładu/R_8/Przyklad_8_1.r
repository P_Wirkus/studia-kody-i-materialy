#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# zmiana nazw zmiennych
names(iris) <- c("ddk","sdk","dp","sp","klasa")
attach(iris)
# rozk�ad zmiennej zale�nej
tk <- table(klasa)
print(tk)
# budowa drzewa klasyfikacyjnego
library(tree)
options(OutDec=",")
drzewo.iris <- tree(klasa~., data=iris)
sd <- summary(drzewo.iris)
print(sd)
print(drzewo.iris)
detach(iris)