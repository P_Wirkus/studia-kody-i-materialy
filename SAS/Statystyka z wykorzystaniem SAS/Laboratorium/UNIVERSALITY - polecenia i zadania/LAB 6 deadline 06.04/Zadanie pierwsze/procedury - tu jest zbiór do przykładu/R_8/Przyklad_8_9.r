#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mlbench)
library(rpart)
options(OutDec=",")
set.seed(114)
# podzial zbioru na cz�� ucz�c� i testow�
data(Vehicle)
l.obs <- nrow(Vehicle)
test <- sample(1:l.obs, round(l.obs/3), replace=FALSE)
veh.ucz <- Vehicle[-test,]
veh.test <- Vehicle[test,]
# budowa modelu
drzewo.veh <- rpart(Class~., data=veh.ucz, control=rpart.control(xval=100, cp=0))
tab.cp <- drzewo.veh$cptable
# poszukiwanie modelu optymalnego
model.opt <- which.min(tab.cp[,4])
cp.opt <- tab.cp[model.opt,1] 
drzewo.veh.p <- prune(drzewo.veh, cp=cp.opt)
# rysunek pokazuj�cy zale�no�� mi�dzy wielko�ci� drzewa a b��dem w sprawdzaniu krzy�owym oraz b��dem zast�pienia
plot(tab.cp[,2]+1, tab.cp[,4], ylim=c(min(tab.cp[,3]),1), type="n", xlab="Wielko�� drzewa", ylab="B��d klasyfikacji")
lines(tab.cp[,2]+1, tab.cp[,4],lty=1)
lines(tab.cp[,2]+1, tab.cp[,3],lty=2)
points(tab.cp[,2]+1, tab.cp[,4],pch=21)
points(tab.cp[,2]+1, tab.cp[,3],pch=22)
legend("topright", legend=c("B��d w sprawdzaniu krzy�owym","B��d zast�pienia"), lty=1:2, pch=21:22)