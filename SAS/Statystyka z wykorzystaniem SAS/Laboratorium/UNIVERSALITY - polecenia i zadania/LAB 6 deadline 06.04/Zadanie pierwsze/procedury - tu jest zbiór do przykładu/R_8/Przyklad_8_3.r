#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

names(iris) <- c("ddk","sdk","dp","sp","klasa")
attach(iris)
# budowa drzewa klasyfikacyjnego
library(tree)
options(OutDec=",")
drzewo.iris <- tree(klasa~., data=iris)
# przyci�cie drzewa do 4 li�ci
drzewo2.iris <- prune.tree(drzewo.iris, best=4)
# rysunek drzewa przyci�tego
plot(drzewo2.iris)
text(drzewo2.iris)
detach(iris)