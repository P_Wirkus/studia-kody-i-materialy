#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

MSA <- function(z, cor, digits){
       if (cor==TRUE)
           { MSA <- array(2,c(1,ncol(z)))
           for(i in (1:ncol(z))){
                    l <- sum((z[,i])^2)-1
                    m <- sum((z[,i])^2)+sum((cor2pcor(z)[,i])^2)-2
                    MSA[i] <- l/m
           }
       }
       else{ 
           MSA <- array(2,c(1,ncol(z)))
           for(i in (1:ncol(z))){
                   l <- sum((cor(z)[,i])^2)-1
                   m <- sum((cor(z)[,i])^2)+sum((cor2pcor(cor(z))[,i])^2)-2
                   MSA[i] <- l/m
            }
       }
       return(MSA)
}