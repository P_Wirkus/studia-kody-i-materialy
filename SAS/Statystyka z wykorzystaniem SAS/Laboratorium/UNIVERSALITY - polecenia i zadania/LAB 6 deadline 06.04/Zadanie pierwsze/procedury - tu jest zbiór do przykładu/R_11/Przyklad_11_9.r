#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(psych)
library(Rgraphviz)
lab <- names(Harman23.cor$cov) <- c("height","arm.span","forearm","lower.leg","weight","bitro.diameter","chest.girth","chest.width")   
fa.graph(factor.pa(Harman23.cor$cov, nfactors=2, residuals=FALSE, n.obs=305, min.err=0.001, digits=3, max.iter=1000), labels=lab, title=NULL)