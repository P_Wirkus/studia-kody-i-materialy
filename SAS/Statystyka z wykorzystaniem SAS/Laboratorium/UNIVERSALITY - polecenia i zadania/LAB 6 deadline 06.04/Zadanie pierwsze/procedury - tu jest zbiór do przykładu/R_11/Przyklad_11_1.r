#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(corpcor)
z <- read.csv2("kawy_obs18_zm10.csv", header=TRUE, row.names=1)
source("KMO.r")
print(paste("KMO - wska�nik Kaisera-Meyera-Olkina:", round(KMO(z, cor=FALSE), digits=5)), quote=FALSE)
source("MSA.r")
M <- MSA(z, cor=FALSE)
dimnames(M)[[2]] <- names(z)
print("MSA", quote=FALSE)
print(M)