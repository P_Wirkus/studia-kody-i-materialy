#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

d <- read.csv2("dane_firma.csv", header=TRUE, row.names=1)
attach(d)
options(OutDec=",")
reg <- lm(y~x1+x2, data=d, x=TRUE, y=TRUE)
wyn <- summary.lm(reg)
print(wyn)
# Wykres p�aszczyzny regresji
library(KernSmooth)
x1v <- seq(min(x1)-0.1, max(x1)+0.1, by=0.1)
x2v <- seq(min(x2)-2, max(x2)+2, by=1)
f <- function(x1v, x2v) {wyn$coefficients[1,1]+wyn$coefficients[2,1]*x1v+wyn$coefficients[3,1]*x2v}
yv <- outer(x1v, x2v, f)
res <- persp(x=x1v, y=x2v, z=yv, theta=35, phi=25, xlab="x1", ylab="x2", zlab="y", ticktype="detailed", cex.axis=0.5)
# Wprowadzenie danych empirycznych
points(trans3d(x1, x2, y, res), col="red", lwd=2, pch=20)
# Linie ��cz�ce warto�ci empiryczne i teoretyczne
segments(trans3d(x1,x2,y,res)$x,trans3d(x1,x2,y,res)$y,trans3d(x1,x2,predict.lm(reg),res)$x,trans3d(x1,x2,predict.lm(reg),res)$y)
detach(d)