#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Joanna i Micha� Trz�siok     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
data(Boston)
attach(Boston)
options(OutDec=",")
model.smoothspline <- smooth.spline(Boston$rm, Boston$medv)
print(model.smoothspline)
print("Warto�ci teoretyczne zmiennej obja�nianej:", quote=FALSE)
print(fitted(model.smoothspline))
R2 <- NULL
R2 <- 1-sum((Boston$medv-fitted(model.smoothspline))^2)/sum((Boston$medv-mean(Boston$medv))^2)
print("Jako�� dopasowania mierzona R^2:", quote=FALSE)
print(R2)
detach(Boston)