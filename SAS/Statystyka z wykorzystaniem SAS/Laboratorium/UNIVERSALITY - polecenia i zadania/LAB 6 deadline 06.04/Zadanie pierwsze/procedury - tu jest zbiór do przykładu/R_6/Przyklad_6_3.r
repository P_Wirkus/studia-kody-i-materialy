#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Joanna i Micha� Trz�siok     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(polspline)
library(MASS)
data(Boston)
options(OutDec=",")
# Losowy podzia� zbioru ucz�cego na 5 cz�ci i obliczenie b��du �redniokwadratowego metod� sprawdzania krzy�owego 
MSE.cv <- c()
set.seed(111)   # Ustawienie generatora liczb losowych
wekt.losowy <- sample(1:nrow(Boston), nrow(Boston))
for (i in 1:5){
  indeks.walidac <- wekt.losowy[(floor((i-1)*length(wekt.losowy)/5)+1):floor(i*length(wekt.losowy)/5)]
  cv.walidac <- Boston[indeks.walidac,]
  cv.uczacy <- Boston[-indeks.walidac,]
  modelMARS <- polymars(cv.uczacy$medv, cv.uczacy[,names(cv.uczacy)!='medv'], gcv=3, factors=4)
  walidac.bez.y <- subset(cv.walidac, select=-medv) 
  y.teoret <- predict.polymars(modelMARS, walidac.bez.y) 
  MSE <- sum((cv.walidac$medv-y.teoret)^2)/length(y.teoret)
  MSE.cv <- c(MSE.cv,MSE)
  MSE <- NULL
}
print(paste("B��d �redniokwadratowy modelu POLYMARS:", round(mean(MSE.cv), digits=3)), quote=FALSE)
print("Charakterystyki modelu zbudowanego na ca�ym zbiorze ucz�cym:", quote=FALSE)
modelMARS <- polymars(Boston$medv, Boston[,names(Boston)!='medv'], gcv=3, factors=4, maxsize=20)
print(modelMARS)
par(mfrow=c(2,2))
plot.polymars(modelMARS, 6, 8)
plot.polymars(modelMARS, 6, 11)
plot.polymars(modelMARS, 6, 13)
plot.polymars(modelMARS, 8, 13)