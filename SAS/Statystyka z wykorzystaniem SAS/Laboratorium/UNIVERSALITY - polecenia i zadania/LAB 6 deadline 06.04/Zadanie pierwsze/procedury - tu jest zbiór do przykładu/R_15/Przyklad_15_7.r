#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Ewa Witek    Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mclust)
x <- read.csv2("dane_15_7.csv", header=TRUE, row.names=1)
options(OutDec=",")
MBC <- Mclust(x)
BIC <- mclustBIC(x)
wyniki <- summary(BIC, data=x)
print("Model optymalny:", quote=FALSE)
print(MBC)
print("Podzia� obiekt�w na klasy:", quote=FALSE)
print(wyniki)
print("Warto�ci kryterium informacyjnego BIC:", quote=FALSE)
print(BIC)
plot.mclustBIC(BIC, data=x)
print("Regularyzacja bayesowska w podej�ciu modelowym w analizie skupie�", qoute=FALSE)
BIC_a_priori <- mclustBIC(x, prior=priorControl())
print("Podzia� obiekt�w na klasy:", quote=FALSE)
print(summary(BIC_a_priori,x))
print("Warto�ci kryterium informacyjnego BIC dla zadanych g�sto�ci a priori:", qoute=FALSE)
print(BIC_a_priori)
print("Warto�ci parametr�w:", quote=FALSE)
print(defaultPrior(x, G=3, modelName="VVV"))
getOption("device")()
plot.mclustBIC(BIC_a_priori, legendArgs=list(x="bottom", ncol=2, cex=1.0))