#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Ewa Witek    Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(prabclus)
library(mclust)
x <- read.csv2("dane_15_6.csv", header=TRUE, row.names=1)
y <- read.csv2("klasy_15_6.csv", header=TRUE, row.names=1)
nnc <- NNclean(x, 15, plot=FALSE)
BIC <- mclustBIC(data=x, initialization=list(noise=nnc$z))
wyniki <- summary(BIC, data=x)
print("Podzia� obiekt�w na klasy:", quote=FALSE)
print(wyniki)
plot.mclustBIC(BIC, legendArgs=list(x="bottomright", horiz=FALSE, ncol=5, cex=.9))
Blad_klasyfikacji <- classError(summary(BIC, x)$classification, y[,1])
Sk_in_Randa <- adjustedRandIndex(summary(BIC, x)$classification, y[,1])
print("Ocena jako�ci klasyfikacji:", quote=FALSE)
print(Blad_klasyfikacji)
print("Skorygowany indeks Randa:", quote=FALSE)
print(Sk_in_Randa)