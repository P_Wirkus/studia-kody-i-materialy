#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Artur Zaborski     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
dane <- read.csv2("danesokpp.csv", header=TRUE, row.names=1)
options(OutDec=",")
d <- dist(dane)
cmdsA <- cmdscale(d, k=2, add=TRUE, eig=TRUE, x.ret=TRUE)
x <- cmdsA$points[,1]
y <- cmdsA$points[,2]
plot(x, y, type="n", xlab="Wymiar 1", ylab="Wymiar 2", main="")
text(x, y, labels=rownames(dane))
print(d)
print(cmdsA)
write.table(cmdsA$points, file="sokcmd.csv", sep=";", dec=",")