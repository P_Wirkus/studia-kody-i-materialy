#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Artur Zaborski     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
dane <- read.csv2("danesokpp.csv", header=TRUE, row.names=1)
x <- as.matrix(dane)
options(OutDec=",")
d <- dist(x, method="euclidean")
sok.mds <- isoMDS(d)
plot(sok.mds$points, xlab="Wymiar 1", ylab="Wymiar 2", type="n")
text(sok.mds$points, labels=as.character(rownames(x)))
print(sok.mds)
write.table(sok.mds$points, file="sokiso.csv", sep=";", dec=",")