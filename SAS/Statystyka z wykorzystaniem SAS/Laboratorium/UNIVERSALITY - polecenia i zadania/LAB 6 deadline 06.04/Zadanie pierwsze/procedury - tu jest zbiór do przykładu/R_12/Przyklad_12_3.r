#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Artur Zaborski     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(shapes)
iso_s <- read.csv2("sokiso.csv", header=TRUE, row.names=1)
cmd_s <- read.csv2("sokcmd.csv", header=TRUE, row.names=1)
options(OutDec=",")
A <- as.matrix(iso_s)
B <- as.matrix(cmd_s)
par(pty="s")
out <- procOPA(A, B, scale=TRUE, reflect=TRUE)
plot(out$Bhat, type="n", xlim=c(-0.8,0.9), ylim=c(-0.4,0.4), xlab="Wymiar 1", ylab="Wymiar 2")
text(out$Bhat, labels=as.character(rownames(iso_s)))
print(out)