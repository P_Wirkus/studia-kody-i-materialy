#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(clusterSim)
x <- read.csv2("dane_14_1.csv", header=TRUE, row.names=1)
r1 <- HINoV.Mod(x, type="metric", s=2, 3, distance="d4", method="pam", Index="cRAND")
options(OutDec=",")
plot(r1$stopri[,2], type="b", pch=0, xlab="Numer zmiennej", ylab="topri", xaxt="n")
axis(1, at=c(1:max(r1$stopri[,1])), labels=r1$stopri[,1])