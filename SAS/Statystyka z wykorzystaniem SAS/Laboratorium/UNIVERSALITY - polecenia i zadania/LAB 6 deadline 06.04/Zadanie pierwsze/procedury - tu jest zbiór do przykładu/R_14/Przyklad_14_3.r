#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(clusterSim)
set.seed(123)           # Ustawienie generatora liczb losowych
x <- read.csv2("dane_14_1a.csv", header=TRUE, row.names=1)
d <- dist(x, method="euclidean")^2
min_liczba_klas <- 1
max_liczba_klas <- 15
min <- 0
clopt<-NULL
wyn<-NULL
wyniki <- array(0, c(max_liczba_klas-min_liczba_klas+1,2))
wyniki[,1] <- min_liczba_klas:max_liczba_klas
znaleziono <- FALSE
for (liczba_klas in min_liczba_klas:max_liczba_klas){
  cl1 <- pam(d, liczba_klas, diss=TRUE)
  cl2 <- pam(d, liczba_klas+1, diss=TRUE)
  clall <- cbind(cl1$clustering, cl2$clustering)
  Gap <- index.Gap(x, clall, reference.distribution="pc", B=10, method="pam")
  wyniki[liczba_klas - min_liczba_klas+1,2] <- diffu <- Gap$diffu
  if ((wyniki[liczba_klas - min_liczba_klas+1,2]>=0) && (!znaleziono)){
    lk <- liczba_klas
    min <- diffu
    clopt <- cl1$cluster
    wyn <- cl1$clusinfo
    znaleziono <- TRUE
  }
}
if (znaleziono){
  print(paste("Minimalna liczba klas dla diffu>=0 wynosi", lk, "dla diffu=", round(min, 4)), quote=FALSE)
}else{
  print("Nie znalaz�em klasyfikacji, dla kt�rej diffu>=0", quote=FALSE)
}
write.table(wyniki, file="diffu.csv", sep=";", dec=",", row.names=TRUE, col.names=FALSE)
write.table(clopt, file="clustering.csv", sep=";", dec=",", row.names=TRUE, col.names=FALSE)
write.table(wyn, file="clusinfo.csv", sep=";", dec=",", row.names=TRUE, col.names=TRUE)
options(OutDec=",")
plot(wyniki, type="p", pch=0, xlab="Liczba klas", ylab="diffu", xaxt="n")
abline(h=0, untf=FALSE)
axis(1, c(min_liczba_klas:max_liczba_klas))