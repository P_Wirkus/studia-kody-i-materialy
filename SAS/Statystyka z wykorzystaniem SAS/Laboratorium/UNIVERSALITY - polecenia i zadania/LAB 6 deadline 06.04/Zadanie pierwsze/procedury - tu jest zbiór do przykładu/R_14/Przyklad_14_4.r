#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(clusterSim)
set.seed(123)           # Ustawienie generatora liczb losowych
x <- read.csv2("dane_14_1a.csv", header=TRUE, row.names=1)
options(OutDec=",")
w <- replication.Mod(x, v="m", u=3, centrotypes="medoids", normalization=NULL, distance="d4", method="pam", S=20, fixedAsample=NULL)
print(w$cRand)