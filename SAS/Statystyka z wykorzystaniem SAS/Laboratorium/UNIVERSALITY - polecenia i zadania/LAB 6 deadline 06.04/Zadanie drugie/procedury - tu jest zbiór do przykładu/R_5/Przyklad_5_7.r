#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

d <- read.csv2("Paap_124_obs.csv", header=TRUE, row.names=1)
alfa <- 0.05 # Podaj poziom istotno�ci
z <- as.ts(d)
t <- ts.intersect(z, C1=lag(z[,2],-1))
options(OutDec=",")
reg <- lm(log(z.S) ~ log(z.C) + log(C1) + z.E + z.K + z.P, data=t, x=TRUE)
s <- rstudent(reg)
z <- as.matrix(s)
sk <- qt(1-alfa/2, nrow(reg$x)-ncol(reg$x)-2)
w <- (abs(z[,1])> sk)
wyn <- as.matrix(w)
wyn_k <- data.frame(z, wyn)
plot(z, xlab="numer obserwacji", ylab="reszty studentyzowane")
abline(h=c(sk,-sk),lty=2)
identify(1:nrow(z), z, row.names(z))