#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

d <- read.csv2("Paap_124_obs.csv", header=TRUE, row.names=1)
alfa <- 0.05 # Podaj poziom istotno�ci
z <- as.ts(d)
t <- ts.intersect(z, C1=lag(z[,2],-1))
options(OutDec=",")
reg <- lm(log(z.S) ~ log(z.C) + log(C1) + z.E + z.K + z.P, data=t, x=TRUE)
wyn <- summary.lm(reg)
print(wyn)