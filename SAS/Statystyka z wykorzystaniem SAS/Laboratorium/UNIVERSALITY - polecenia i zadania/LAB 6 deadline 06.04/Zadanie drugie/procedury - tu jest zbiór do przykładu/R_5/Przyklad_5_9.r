#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

d <- read.csv2("dane_firma.csv", header=TRUE, row.names=1)
attach(d)
options(OutDec=",")
reg <- lm(y~x1+x2, data=d, x=TRUE, y=TRUE)
wyn <- summary.lm(reg)
uf <- confint(reg, level=0.95)
print(uf)
library(car)
# Utworzenie obszaru dla dw�ch rysunk�w
par(mfrow=c(1,2))
par(pty="s")
# Elipsy danych dla zmiennych x1 i x2
data.ellipse(x1, x2, levels=c(0.50,0.75,0.95))
title("a)", font.main=1)
# Elipsa obszaru ufno�ci dla parametr�w a1 i a2 (1-alfa=0.95)
confidence.ellipse(reg, levels=0.95)
title("b)", font.main=1)
detach(d)