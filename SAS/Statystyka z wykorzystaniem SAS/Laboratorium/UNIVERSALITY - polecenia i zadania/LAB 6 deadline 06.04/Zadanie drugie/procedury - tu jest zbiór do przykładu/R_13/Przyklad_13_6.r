#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Iwona Kasprzyk     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(ca)
dane <- read.csv2("dane_13_1.csv", header=TRUE)
tab <- xtabs(~sklep+powod, data=dane)
options(OutDec=",")
N <- sum(tab)
P <- round(((1/N)*tab), 4)
masa.w <- round(ca(tab)$rowmass, 4)
masa.k <- round(ca(tab)$colmass, 4)
#obliczanie profili wierszy
profile.w <- round(P/masa.w, 4)
print("Profile wierszy:", quote=FALSE)
print(profile.w)
#obliczanie profili kolumn
Pt <- t(P) 
profile.k <- round(Pt/masa.k, 4)
print("Profile kolumn:", quote=FALSE)
print(profile.k)