#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Iwona Kasprzyk     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(ca)
dane <- read.csv2("dane_13_1.csv", header=TRUE)
tab <- xtabs(~sklep+powod, data=dane)
F <- ca(tab)$rowcoord 
G <- ca(tab)$colcoord 
rn <- dimnames(tab)[[1]] 
cn <- dimnames(tab)[[2]] 
rownames(F) <- c(rn) 
rownames(G) <- c(cn) 
F2 <- cbind(F)[ ,c(1,2)]
G2 <- cbind(G)[ ,c(1,2)]
# po��czenie wsp�rz�dnych w jeden zbi�r
wymiar1 <- append(F2[,1], G2[,1])
wymiar2 <- append(F2[,2], G2[,2])
r <- cbind(wymiar1,wymiar2)
# wyznaczenie kwadratowej odleg�o�ci euklidesowej
odl <- dist(r, method="euclidean")^2
# dendrogram za pomoc� metody Warda
dend <- hclust(odl, method="ward")
plot(dend, main="", ylab="odleg�o��", xlab="", sub="")