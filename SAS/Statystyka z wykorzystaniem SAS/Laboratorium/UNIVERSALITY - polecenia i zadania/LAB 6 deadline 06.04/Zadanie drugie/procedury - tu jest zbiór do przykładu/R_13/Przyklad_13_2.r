#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Iwona Kasprzyk     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(vcd)
dane <- read.csv2("dane_13_1.csv", header=TRUE)
tab <- xtabs(~sklep+powod, data=dane)
options(OutDec=",")
w <- assocstats(tab)
print(w)