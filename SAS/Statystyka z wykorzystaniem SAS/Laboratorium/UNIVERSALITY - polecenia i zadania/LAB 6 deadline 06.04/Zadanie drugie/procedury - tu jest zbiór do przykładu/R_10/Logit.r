#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# Logit(Usym, S, r) - funkcja obliczaj�ca udzia�y w rynku profil�w symulacyjnych na podstawie modelu logitowego
# Usym - macierz u�yteczno�ci ca�kowitych profil�w symulacyjnych w przekroju respondent�w
# S - liczba respondent�w
# r - liczba profil�w symulacyjnych
# wywo�anie - Plogit <- Logit(Usym, S, r)

Logit <- function(Usym, S, r)
{
   ex <-  matrix(0, r, S)      #macierz logarytm�w naturalnych u�yteczno�ci ca�kowitych profil�w
   share <-  matrix(0, r, S)   #macierz udzia��w
   Plogit <- vector("numeric", r)
   for(s in 1:S)
   {
      ex[,s] <- exp(Usym[,s])
      share[,s] <- ex[,s]/sum(ex[,s])
   }
   for(i in 1:r) {Plogit[i] <- mean(share[i,])*100}
   return(Plogit)
}