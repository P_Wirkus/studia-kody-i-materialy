#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# generowanie profil�w (wyb�r losowy)
library(poLCA)
set.seed(123)
probs <- list(matrix(c(0.6,0.1,0.3, 0.6,0.3,0.1, 0.3,0.1,0.6 ), ncol=3, byrow=TRUE),
              matrix(c(0.3,0.6,0.1, 0.1,0.3,0.6, 0.3,0.6,0.1 ), ncol=3, byrow=TRUE), 
              matrix(c(0.2,0.8,     0.7,0.3,     0.3,0.7     ), ncol=2, byrow=TRUE))
dane <- poLCA.simdata(N=12, probs, classdist=c(0.2,0.3,0.5))
profile <- dane$dat
print(profile)