#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

source("partutilities.r")
source("MaxUtility.r")
source("BTL.r")
source("Logit.r")
#kodowanie quasi-eksperymentalne atrybut�w
options(contrasts=c("contr.sum","contr.poly"))
options(OutDec=",")
x <- read.csv2("Dane_10_1a.csv", header=TRUE)   #profile
y <- read.csv2("Dane_10_1c.csv", header=TRUE)   #preferencje empiryczne 
m <- length(x)     #liczba atrybut�w
n <- nrow(x)       #liczba profil�w
S <- nrow(y)/n     #liczba respondent�w
xnms <- names(x)   #nazwy zmiennych obja�niaj�cych (atrybuty)
Lj <- vector("numeric", m)   #liczba poziom�w j-tego atrybutu
for(j in 1:m) {Lj[j] <- nlevels(factor(x[[xnms[j]]]))}
p <- sum(Lj)-m+1   #liczba poziom�w wszystkich atrybut�w z wyrazem wolnym (bez poziom�w odniesienia), 
                   #liczba zmiennych sztucznych plus wyraz wolny
ynms <- names(y)   #nazwa zmiennej obja�nianej (preferencje)
xtmp <- paste("factor(x$", xnms, sep="", paste(")"))
xfrm <- paste(xtmp, collapse="+")
# u�yteczno�ci ca�kowite profil�w symulacyjnych (udzia�y w rynku profil�w symulacyjnych)
# Dane_10_1d.csv - plik zawieraj�cy macierz uk�adu czynnikowego ze zmiennymi sztucznymi profil�w symulacyjnych z wektorem wyrazu wolnego
usl <- partutilities(xfrm, y, x, n, p, S) 
Zsym <- read.csv2("Dane_10_1d.csv", header=TRUE)   #macierz uk�adu czynnikowego ze zmiennymi sztucznymi profil�w symulacyjnych      #z wektorem 1 wyrazu wolnego
Zsym <- as.matrix(Zsym)
Usym <- Zsym%*%t(usl)     #u�yteczno�ci ca�kowite profil�w symulacyjnych w przekroju respondent�w
r <- nrow(Zsym)           #liczba profil�w symulacyjnych
Pmu <- MaxUtility(Usym, S, r) 
print("Model maksymalnej u�yteczno�ci", quote=FALSE)
print(Pmu)
Pbtl <- BTL(Usym, S, r)
print("Model BTL", quote=FALSE)
print(Pbtl)
Plogit <- Logit(Usym, S, r) 
print("Model logitowy", quote=FALSE)
print(Plogit)