#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# MaxUtility(Usym, S, r) - funkcja obliczaj�ca udzia�y w rynku profil�w symulacyjnych na podstawie modelu maksymalnej u�yteczno�ci
# Usym - macierz u�yteczno�ci ca�kowitych profil�w symulacyjnych w przekroju respondent�w
# S - liczba respondent�w
# r - liczba profil�w symulacyjnych
# wywo�anie - Pmu <- MaxUtility(Usym, S, r)

MaxUtility <- function(Usym, S, r)
{
   count <- vector("numeric", r)   #wektor cz�sto�ci
   for(s in 1:S) {count[which.max(Usym[,s])] <- count[which.max(Usym[,s])]+1}
   Pmu <- (count/sum(count))*100
   return(Pmu)
}