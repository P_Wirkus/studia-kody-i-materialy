#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# importance(ul, Lj) - funkcja obliczaj�ca wa�no�ci atrybut�w
# ul - u�yteczno�ci cz�stkowe (parametry) poziom�w atrybut�w (liczba parametr�w = liczba zmiennych sztucznych + liczba atrybut�w)
# Lj - liczba poziom�w j-tego atrybutu (wektor Lj o d�ugo�ci r�wnej liczbie atrybut�w zawiera liczby poziom�w ka�dego atrybutu)
# wywo�anie - imp <- round(importance(ul, Lj)*100, 2)

importance <- function (ul, Lj)
{
   m <- length(Lj)               #liczba atrybut�w
   p <- length(u)                #liczba parametr�w (z wyrazem wolnym)
   imp <- vector("numeric", m)   #wektor wa�no�ci atrybut�w
   roz <- vector("numeric", m)   #wektor rozst�p�w
   i <- 0
   for(j in 1:m)
   {
      l <- Lj[j]                 #liczba poziom�w j-tego atrybutu
      a <- vector("numeric", l)
      for(k in 1:l)
      {
         i <- i+1
         a[k] <- ul[i]
      }
      roz[j] <- max(a)-min(a)
   }
   rs <- sum(roz)
   for(j in 1:m) {imp[j] <- roz[j]/rs}
   return(imp)
}