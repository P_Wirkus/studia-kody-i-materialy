#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

#kodowanie quasi-eksperymentalne atrybut�w
options(contrasts=c("contr.sum","contr.poly"))
options(OutDec=",")
x <- read.csv2("Dane_10_1a.csv", header=TRUE)   #profile
y <- read.csv2("Dane_10_1b.csv", header=TRUE)   #preferencje empiryczne 
xnms <- names(x)   #nazwy zmiennych obja�niaj�cych (atrybuty)
ynms <- names(y)   #nazwa zmiennej obja�nianej (preferencje)
xtmp <- paste("factor(x$", xnms, sep="", paste(")"))
xfrm <- paste(xtmp, collapse="+")
yfrm <- paste("y$", ynms, sep="", "~")
frml <- as.formula(paste(yfrm, xfrm))   #model
camodel <- lm(frml)
wyniki <- summary.lm(camodel)
print(wyniki)