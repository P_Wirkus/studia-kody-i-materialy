#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Andrzej B�k     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# generowanie danych (skale niemetryczne - skala rang)
# zmienna wielomianowa (12 profil�w)
set.seed(123)
preferencje <- as.matrix(sample(c(1:12), 12, replace=FALSE))
print(preferencje)