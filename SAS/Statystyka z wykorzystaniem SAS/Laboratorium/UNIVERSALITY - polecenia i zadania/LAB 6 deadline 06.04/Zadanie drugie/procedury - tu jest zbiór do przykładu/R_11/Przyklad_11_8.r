#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(psych)
og <- factor.pa(Harman23.cor$cov, nfactors=2, residuals=FALSE, n.obs=305, min.err=0.001, digits=3, max.iter=1000)
options(OutDec=",")
print("Analiza czynnikowa metod� osi g��wnych z rotacj� varimax", quote=FALSE)
print(og$loadings)