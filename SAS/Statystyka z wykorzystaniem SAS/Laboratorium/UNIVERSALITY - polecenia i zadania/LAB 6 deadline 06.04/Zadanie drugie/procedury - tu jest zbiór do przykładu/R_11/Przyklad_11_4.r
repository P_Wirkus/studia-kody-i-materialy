#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

z <- read.csv2("kawy_obs18_zm9.csv", header=TRUE, row.names=1)
attach(z)
options(OutDec=",")
fa <- factanal(z, factors=3)
fal <- factanal(formula(paste("~", paste(names(z), collapse="+"))), factors=3, scores="Bartlett")$scores
print("Analiza czynnikowa metod� najwi�kszej wiarygodno�ci z rotacj� varimax", quote=FALSE)
print(fa)
print(fal)
detach(z)