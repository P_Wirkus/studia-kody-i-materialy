#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(GPArotation)
z <- read.csv2("kawy_obs18_zm9.csv", header=TRUE, row.names=1)
rt <- GPForth(factanal(z, factors=3, rotation="none")$loadings, normalize=FALSE, eps=1e-5, maxit=1000, method="quartimax")
options(OutDec=",")
print("Analiza czynnikowa metod� najwi�kszej wiarygodno�ci z rotacj� quartimax", quote=FALSE)
print(rt)