#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Miros�awa Sztemberg-Lewandowska     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Funkcja do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

KMO <- function(z, cor=TRUE, digits){
if (cor==TRUE)
  { l <- sum(z^2)-dim(z)[2]
    m <- sum(z^2)+sum((cor2pcor(z))^2)-2*dim(z)[2]
    KMO <- l/m
  }
else
  { l <- sum((cor(z))^2)-dim(z)[2]
    m <- sum((cor(z))^2)+sum((cor2pcor(cor(z)))^2)-2*dim(z)[2]  
    KMO <- l/m
  }
 return(KMO)
}