#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Joanna i Micha� Trz�siok     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(splines)
library(MASS)
data(Boston)
attach(Boston)
options(OutDec=",")
model.splines <- bs(Boston$rm, df=7, intercept=TRUE)
print(model.splines)
o <- order(Boston$rm)
par(mfrow=c(2,2))
plot(Boston$rm[o], Boston$medv[o], xlab="liczba pokoi", ylab="mediana ceny", main="df=3", col="slategrey")
lines(Boston$rm[o], fitted(lm(Boston$medv[o]~bs(Boston$rm[o], df=3))))
plot(Boston$rm[o], Boston$medv[o], xlab="liczba pokoi", ylab="mediana ceny", main="df=5", col="slategrey")
lines(Boston$rm[o], fitted(lm(Boston$medv[o]~bs(Boston$rm[o], df=5))))
plot(Boston$rm[o], Boston$medv[o], xlab="liczba pokoi", ylab="mediana ceny", main="df=15", col="slategrey")
lines(Boston$rm[o], fitted(lm(Boston$medv[o]~bs(Boston$rm[o], df=15))))
plot(Boston$rm[o], Boston$medv[o], xlab="liczba pokoi", ylab="mediana ceny", main="df=35", col="slategrey")
lines(Boston$rm[o], fitted(lm(Boston$medv[o]~bs(Boston$rm[o], df=35))))
detach(Boston)