#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Joanna i Micha� Trz�siok     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# Uwaga - przeprowadzenie poni�szej procedury w znacznym stopniu obci��a procesor oraz pami�� komputera - oczekiwanie na wyniki mo�e trwa� kilkana�cie minut
library(gbm)
library(MASS)
data(Boston)
options(OutDec=",")
# Losowy podzia� zbioru ucz�cego na 5 cz�ci i obliczenie b��du �redniokwadratowego metod� sprawdzania krzy�owego 
MSE.cv <- c()
set.seed(111)   # Ustawienie generatora liczb losowych
wekt.losowy <- sample(1:nrow(Boston), nrow(Boston))
for (i in 1:5){
  indeks.walidac <- wekt.losowy[(floor((i-1)*length(wekt.losowy)/5)+1):floor(i*length(wekt.losowy)/5)]
  cv.walidac <- Boston[indeks.walidac,]
  cv.uczacy <- Boston[-indeks.walidac,]
  modelGBM <- gbm(medv ~ ., distribution="gaussian", data=cv.uczacy, n.trees=10000, cv.folds=5, interaction.depth=3, verbose=FALSE)
  getOption("device")()
  l.drzew <- gbm.perf(modelGBM, method="cv")
  walidac.bez.y <- subset(cv.walidac, select=-medv) 
  y.teoret <- predict(modelGBM, walidac.bez.y, n.trees=l.drzew) 
  MSE <- sum((cv.walidac$medv-y.teoret)^2)/length(y.teoret)
  MSE.cv <- c(MSE.cv,MSE)
  MSE <- NULL 
}
print(paste("B��d �redniokwadratowy modelu MART:", round(mean(MSE.cv), digits=3)), quote=FALSE)
print("Charakterystyki modelu zbudowanego na ca�ym zbiorze ucz�cym:", quote=FALSE)
modelGBM <- gbm(medv ~ ., distribution="gaussian", data=Boston, n.trees=10000, cv.folds=5, interaction.depth=3, verbose=FALSE)
getOption("device")()
l.drzew <- gbm.perf(modelGBM, method="cv")
modelGBM <- gbm(medv ~ ., distribution="gaussian", data=Boston, n.trees=l.drzew, cv.folds=5, interaction.depth=3, verbose=FALSE)
print(paste("Liczba funkcji sk�adowych (drzew) w modelu:", l.drzew), quote=FALSE) 
print(summary(modelGBM))