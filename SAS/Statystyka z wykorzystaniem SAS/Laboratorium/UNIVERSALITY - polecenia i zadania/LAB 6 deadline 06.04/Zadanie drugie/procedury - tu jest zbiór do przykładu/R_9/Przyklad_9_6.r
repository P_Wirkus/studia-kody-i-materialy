#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(randomForest)
library(MASS)
data(Boston)
options(OutDec=",")
set.seed(123)
# zakodowanie zmiennej zero-jedynkowej "chas"
Boston[,4] <- as.factor(Boston[,4])
# podzia� na zbi�r ucz�cy i testowy
l.obs <- nrow(Boston)
l.mod <- 100
test <- sample(1:l.obs, size=round(l.obs/3), replace=FALSE)
bos.ucz <- Boston[-test,]
bos.test <- Boston[test,]
# budowa modelu
rf.bos <- randomForest(medv~., data=bos.ucz, ntree=l.mod, mtry=4, do.trace=10, keep.forest=TRUE)
# waga poszczeg�lnych zmiennych
wz <- round(importance(rf.bos), 2)
waga <- wz[order(wz, decreasing=TRUE),]
print("Waga poszczeg�lnych zmiennych:", quote=FALSE)
print(as.data.frame(waga))