#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(ipred) 
library(ElemStatLearn)
data(spam)
options(OutDec=",")
set.seed(123)
l.obs <- nrow(spam)
l.zm <- ncol(spam)-1
l.zm2 <- floor(l.zm/2)
# Podzia� na zbi�r ucz�cy i testowy:
test <- sample(1:l.obs, round(l.obs/3), replace=FALSE)
spam.ucz <- spam[-test,]
spam.test <- spam[test,]
# model dodatkowy 5-NN
comb.knn <- list(list(model=function(...) ipredknn(...,k=5), predict=predict.ipredknn))
# model zagregowany
model.agr <- bagging(spam ~ ., data=spam.ucz, nbagg=20, comb=comb.knn)
# obliczanie b��du
y.pred <- predict(model.agr, newdata=spam.test, type="class")
blad.bd <- 1-sum(y.pred==spam.test$spam)/nrow(spam.test)
print("B��d dla metody bundling:", quote=FALSE)
print(blad.bd)
# klasyczny model bagging 
bag.spam <- bagging(spam ~ ., data=spam.ucz, nbagg=20)
# obliczanie b��du
bag.spam.pred <- predict(bag.spam,newdata=spam.test, type="class")
blad.bg <- 1-sum(bag.spam.pred==spam.test$spam)/nrow(spam.test)
print("B��d dla metody bagging:", quote=FALSE)
print(blad.bg)