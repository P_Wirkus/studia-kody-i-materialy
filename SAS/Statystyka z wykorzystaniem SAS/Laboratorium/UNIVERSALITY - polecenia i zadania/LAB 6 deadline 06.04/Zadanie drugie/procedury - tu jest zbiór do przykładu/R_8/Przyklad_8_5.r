#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
library(tree)
options(OutDec=",")
# drzewo regresyjne  
drzewo.cpus <- tree(log10(perf)~mmax+cach, data=cpus)
print(summary(drzewo.cpus))
print(drzewo.cpus)
# wykres drzewa
plot(drzewo.cpus)
text(drzewo.cpus)
# czekanie na naci�ni�cie klawisza
print("Naci�nij dowolny klawisz ...", quote=FALSE)
readline()
# podzia� przestrzeni zmiennych
partition.tree(drzewo.cpus)