#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
library(tree)
attach(cpus)
options(OutDec=",")
# drzewo regresyjne dla jednej zmiennej obja�niaj�cej
model <- tree(log10(perf)~cach)
# posta� funkcji schodkowej
w <- seq(min(cach), max(cach), 0.5)
y.pred <- predict(model, list(cach=w))
plot(cach, log10(perf))
lines(w, y.pred)
detach(cpus)