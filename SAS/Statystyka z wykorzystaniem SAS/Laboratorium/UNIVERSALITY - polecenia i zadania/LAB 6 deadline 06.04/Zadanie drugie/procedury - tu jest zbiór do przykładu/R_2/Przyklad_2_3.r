#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

x <- read.csv2("Dane_3_1.csv", header=TRUE, row.names=1)
options(OutDec=",")
m <- mean(x)
print("Wektor warto�ci przeci�tnych", quote=FALSE)
print(m)
v <- var(x)
print("Macierz kowariancji", quote=FALSE)
print(v)
c <- cor(x)
print("Macierz korelacji", quote=FALSE)
print(c)