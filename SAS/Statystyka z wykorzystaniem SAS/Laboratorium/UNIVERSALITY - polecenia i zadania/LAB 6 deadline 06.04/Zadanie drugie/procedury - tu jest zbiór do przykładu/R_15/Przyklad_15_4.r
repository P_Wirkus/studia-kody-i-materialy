#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Ewa Witek    Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mclust)
x <- read.csv2("dane_15_1.csv", header=TRUE, row.names=1)
options(OutDec=",")
MBC <- Mclust(x)
print("Model optymalny:", quote=FALSE)
print(MBC)
print("Sk�adowe funkcji Mclust:", quote=FALSE)
print(names(MBC))
print("Warto�� kryterium informacyjnego BIC dla modelu optymalnego:", quote=FALSE)
print(MBC$bic)
print("Warto�� funkcji najwi�kszej wiarygodno�ci:", quote=FALSE)
print(MBC$loglik)
print("Podzia� obiekt�w na klasy dla modelu optymalnego:", quote=FALSE)
print(MBC$classification)