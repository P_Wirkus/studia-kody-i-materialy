#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Ewa Witek    Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mclust)
x <- read.csv2("dane_15_1.csv", header=TRUE, row.names=1)
y <- read.csv2("klasy_15_1.csv", header=TRUE, row.names=1)
options(OutDec=",")
me <- me(modelName="VVV", data=x, z=unmap(y[,1]))
print("Estymatory prawdopodobie�stw warunkowych dla pierwszych pi�ciu obiekt�w:", quote=FALSE)
print(me$z[1:5,])
print("Wagi dla poszczeg�lnych klas:", quote=FALSE)
print(me$parameters$pro)
print("Warto�ci przeci�tne dla trzech zmiennych w pi�ciu klasach:", quote=FALSE)
print(me$parameters$mean)
print("Macierz kowariancji dla klasy trzeciej:", quote=FALSE)
print(me$parameters$variance$sigma[, , 3])
print("Warto�� funkcji wiarygodno�ci:", quote=FALSE)
print(me$loglik)