#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Ewa Witek    Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(mclust)
x <- read.csv2("dane_15_1.csv", header=TRUE, row.names=1)
options(OutDec=",")
BIC <- mclustBIC(x)
wyniki <- summary(BIC, data=x)
print(BIC)
print(wyniki)