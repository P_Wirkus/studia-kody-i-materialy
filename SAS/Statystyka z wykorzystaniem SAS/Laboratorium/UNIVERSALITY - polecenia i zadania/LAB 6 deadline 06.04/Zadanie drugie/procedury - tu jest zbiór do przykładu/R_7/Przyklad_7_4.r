#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(nnet)
library(class)
options(OutDec=",")
# zmiana nazw zmiennych
names(iris) <- c("ddk","sdk","dp","sp","klasa")
# zmiana na jed noliterowe
iris$klasa <- factor(rep(c("s","c","v"), rep(50,3)))
attach(iris)
# zbior danych do rysunku
min.sp <- min(sp)
max.sp <- max(sp)
min.dp <- min(dp)
max.dp <- max(dp)
irys <- matrix(nrow=150, ncol=2)
irys[,1] <- iris$sp
irys[,2] <- iris$dp
x <- seq(min.sp, max.sp, length=100)
y <- seq(min.dp, max.dp, length=100)
kl <- factor(iris$klasa)
# matryca dla rysunku
irysT <- expand.grid(sp=x, dp=y)
n <- length(x)
# skalowanie
ss <- scale(irys, FALSE, c(max.sp,max.dp))
ssT <- scale(irysT, FALSE, c(max.sp,max.dp))
# b�edy klasyfikacji
# 1-NN 
y.pred <- knn(irys, irys, kl, k=1)
blad <- 1-sum(y.pred==kl)/length(kl)
print("B��d klasyfikacji dla modelu 1-NN:", quote=FALSE)
print(blad)
# 10-NN 
y.pred <- knn(irys, irys, kl, k=10)
blad <- 1-sum(y.pred==kl)/length(kl)
print("B��d klasyfikacji dla modelu 10-NN:", quote=FALSE)
print(blad)
# rysunek
par(mfrow=c(1,2))
y.pred <- knn(ss, ssT, kl, k=1)
plot(sp, dp, type="n", ylab='D�ugo�� p�atka', xlab='Szeroko�� p�atka', xlim=c(min.sp,max.sp), ylim=c(min.dp,max.dp), main="1-NN")
text(sp, dp, as.character(kl))
contour(x, y, matrix(as.numeric(y.pred=="s"), n), add=TRUE, levels=0.5, labex=0, drawlabels=FALSE)
contour(x, y, matrix(as.numeric(y.pred=="v"), n), add=TRUE, levels=0.5, labex=0, drawlabels=FALSE)
# edycja
ed <- multiedit(irys, kl, k = 1, V = 3, I = 5, trace = TRUE)
wyrzucone <- kl[-ed]
print("Obserwacje usuni�te:",quote=FALSE)
print(wyrzucone)
# rysunek c.d.
irys2 <- irys[ed,]
kl2 <- kl[ed]
plot(sp[ed], dp[ed], type="n", ylab='D�ugo�� p�atka', xlab='Szeroko�� p�atka', xlim=c(min.sp,max.sp), ylim=c(min.dp,max.dp), main="Po edycji")
text(sp[ed], dp[ed], as.character(kl2))
contour(x, y, matrix(as.numeric(y.pred=="s"), n), add=TRUE, levels=0.5, labex=0, drawlabels=FALSE)
contour(x, y, matrix(as.numeric(y.pred=="v"), n), add=TRUE, levels=0.5, labex=0, drawlabels=FALSE)
par(mfrow=c(1,1))
detach(iris)