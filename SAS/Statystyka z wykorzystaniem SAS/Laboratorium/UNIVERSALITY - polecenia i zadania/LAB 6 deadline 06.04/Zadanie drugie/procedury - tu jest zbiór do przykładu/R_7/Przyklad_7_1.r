#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(nnet)
library(MASS)
options(OutDec=",")
# zmiana nazw zmiennych
names(iris) <- c("ddk","sdk","dp","sp","klasa")
# zmiana na jednoliterowe nominalne
iris$klasa <- factor(rep(c("s","c","v"), rep(50,3)))
attach(iris)
# budowa modelu
lda.iris <- lda(klasa~sp+dp, data=iris)
print(lda.iris)
plot(lda.iris, cex=1.0)
detach(iris)