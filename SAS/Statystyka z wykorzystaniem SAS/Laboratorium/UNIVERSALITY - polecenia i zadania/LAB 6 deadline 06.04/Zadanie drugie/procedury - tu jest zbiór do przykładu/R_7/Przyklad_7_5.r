#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Eugeniusz Gatnar     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(nnet)
options(OutDec=",")
# zmiana nazw zmiennych
names(iris) <- c("ddk","sdk","dp","sp","klasa")
# zmiana na jednoliterowe
iris$klasa <- factor(rep(c("s","c","v"),rep(50,3)))
attach(iris)
set.seed(115)
# zbior danych do rysunku
min.sp <- min(sp)
max.sp <- max(sp)
min.dp <- min(dp)
max.dp <- max(dp)
irys <- matrix(nrow=150, ncol=2)
irys[,1] <- iris$sp
irys[,2] <- iris$dp
x <- seq(min.sp, max.sp, length=100)
y <- seq(min.dp, max.dp, length=100)
kl <- factor(iris$klasa)
# matryca dla rysunku
irysT <- expand.grid(sp=x, dp=y)
n <- length(x)
# przygotowanie zmiennej zale�nej w postaci wymaganej przez sie� neuronow� 
kli <- class.ind(kl)
# b�edy klasyfikacji
nn.iris <- nnet(irys, kli, skip=TRUE, softmax=TRUE, size=3, col=2, decay=0.01, maxit=1000)
print(summary(nn.iris))
y.pred <- predict(nn.iris, irys, type="class")
blad <- 1-sum(y.pred==kl)/length(kl)
print("B��d klasyfikacji dla modelu sieci neuronowej z 3 neuronami w warstwie ukrytej:", quote=FALSE)
print(blad)
# rysunek
plot(sp, dp, type="n", ylab='D�ugo�� p�atka', xlab='Szeroko�� p�atka', xlim=c(min.sp, max.sp), ylim=c(min.dp, max.dp))
text(sp, dp, as.character(kl))
y.pred <- predict(nn.iris, irysT, type="raw")
z <- y.pred[,1]-pmax(y.pred[,2], y.pred[,3])
contour(x, y, matrix(z, n), add=TRUE, levels=0.5, labex=0, drawlabels=FALSE)
detach(iris)