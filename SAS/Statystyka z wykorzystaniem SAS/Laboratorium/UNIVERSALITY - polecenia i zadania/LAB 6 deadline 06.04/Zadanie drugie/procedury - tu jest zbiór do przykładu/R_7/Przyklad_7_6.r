#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Micha� Trz�siok     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
library(class)
library(e1071)
data(iris)
names(iris) <- c("ddk","sdk","dp","sp","klasa")
attach(iris)
options(OutDec=",") 
svm.iris <- svm(klasa ~ ., data=iris, kernel = "radial", gamma=0.5, cost=10, cross=10)
print(summary(svm.iris))
# ocena jako�ci klasyfikacji
y.teoret <- fitted(svm.iris) 
print("Tablica zgodno�ci klasyfikacji:", quote=FALSE)
print(table(iris$klasa, y.teoret))
print("Wektory no�ne to obserwacje o numerach:", quote=FALSE)
print(svm.iris$index)
detach(iris)