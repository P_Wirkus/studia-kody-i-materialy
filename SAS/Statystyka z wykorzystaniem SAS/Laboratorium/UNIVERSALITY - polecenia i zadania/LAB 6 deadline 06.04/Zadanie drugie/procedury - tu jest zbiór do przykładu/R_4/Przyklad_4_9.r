#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Dorota Rozmus     Akademia Ekonomiczna w Katowicach
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

czas_wykonania <- read.csv2("czas_wykonania.csv", header=TRUE, row.names=1)
czas_wykonania[,2] <- as.factor(czas_wykonania[,2])
options(OutDec=",")
attach(czas_wykonania)
post_m1 <- aov(czas~plec+kwalifikacje)
czas_hsd <- TukeyHSD(post_m1, which=c("plec", "kwalifikacje"))
print(czas_hsd)
par(mfrow=c(2,1))
# Zmniejsza margines na rysunku:
par(mar=c(4.8,3.5,2.8,1.5))
plot.TukeyHSD(czas_hsd)
detach(czas_wykonania)