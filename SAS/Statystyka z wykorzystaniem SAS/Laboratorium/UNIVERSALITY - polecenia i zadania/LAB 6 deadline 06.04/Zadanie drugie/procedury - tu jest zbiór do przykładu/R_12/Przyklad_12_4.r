#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Artur Zaborski     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(rgl)
library(MASS)
dane <- read.csv2("napoje_1.csv", header=TRUE, row.names=1)
x <- as.matrix(dane)
options(OutDec=",")
d <- dist(x, method="euclidean")
nap <- sammon(d, k=3, trace=FALSE)
plot3d(nap$points, xlab="x", ylab="y", zlab="z", size=4)
text3d(nap$points[,1], nap$points[,2], nap$points[,3]+0.05, row.names(nap$points))
print(nap)