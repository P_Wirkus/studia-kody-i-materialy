#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Artur Zaborski     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach
#*  licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej
#*  informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(SensoMineR)
source("indscal_m.r")
a <- read.csv2("szkoly.csv", header=TRUE, row.names=1)
school <- as.matrix(a)
options(OutDec=",")
inds <- indscal(school)
par(mfrow=c(1,2), pty="s")
plot(inds$points, main="Konfiguracja wsp�lna", font.main=1, cex.main=1, xlab="Wymiar 1", ylab="Wymiar 2", asp=1, cex=0.8, pch=20)
text(inds$points, labels=rownames(school), cex=0.8, pos=4, offset=0.2)
abline(v=0, lty=2)
abline(h=0, lty=2)
plot(inds$W, main="Konfiguracja wag", font.main=1, cex.main=1, xlim=c(0,1), ylim=c(0,1), xlab="Wymiar 1", ylab="Wymiar 2")
text(inds$W, labels=paste("y", 1:nrow(inds$W), sep=""), cex=0.8, pos=4, offset=0.2)
print(inds)