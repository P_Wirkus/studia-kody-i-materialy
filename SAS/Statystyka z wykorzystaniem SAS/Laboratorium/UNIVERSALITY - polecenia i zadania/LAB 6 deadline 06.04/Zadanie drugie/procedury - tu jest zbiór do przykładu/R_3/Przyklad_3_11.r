#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

# Utworzenie obszaru dla czterech rysunk�w
par(mfrow=c(2,2), pty="s")
options(OutDec =",")
# Funkcja sin(x) i ln(x) na jednym rysunku
curve(sin(x), col="red", xlim=c(0,4*pi), ylim=c(-1,log(4*pi)), ylab="sin(x) i ln(x)")
curve(log(x), col="blue", add=TRUE, xlim=c(0,4*pi), ylab="sin(x) i ln(x)")
title("a)", font.main=1)
# Funkcja sqrt(x*(1-x))
curve(sqrt(x*(1-x)), 0, 1, ylab=expression(sqrt(x(1-x))))
title("b)", font.main=1)
# Funkcja pot�gowa
curve(2*x^0.4, ylab=expression(2*x^0.4), 0, 4)
title("c)", font.main=1)
# Wykres y=1/x z wykorzystaniem funkcji plot
x <- seq(0, 1, 0.01)
y <- 1/x
plot(x, y, type="l")
text(0.2, 40, labels="y=1/x")
title("d)", font.main=1)