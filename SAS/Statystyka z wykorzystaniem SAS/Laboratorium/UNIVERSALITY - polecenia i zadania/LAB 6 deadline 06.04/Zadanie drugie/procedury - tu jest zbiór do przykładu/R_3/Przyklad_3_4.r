#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(KernSmooth)
x <- read.csv2("Dane_3_1.csv", header=TRUE, row.names=1)
# Estymacja funkcji g�sto�ci rozk�adu zmiennych x1 i x2 (dwie pierwsze kolumny danych z tab. 3.1)
d <- bkde2D(x[,1:2], bandwidth=sapply(x[,1:2], dpik))
# Narysowanie oszacowanej funkcji g�sto�ci
persp(x=d$x1, y=d$x2, z=d$fhat, zlab="g�sto��", xlab="x1", ylab="x2", theta=35, phi=25)