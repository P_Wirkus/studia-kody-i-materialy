#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

x <- read.csv2("Dane_3_1.csv", header=TRUE, row.names=1)
attach(x)
# Utworzenie obszaru dla czterech rysunk�w
par(mfrow=c(2,2), pty="s")
# Wykres rozrzutu z punktami
plot(x4, x5, lwd=2)
title("a)", font.main=1)
# Wykres rozrzutu z punktami i lini� regresji
plot(x4, x5, lwd=2)
abline(lm(x5~x4), lwd=2)
title("b)", font.main=1)
# Wykres rozrzutu z punktami i numerami obiekt�w
plot(x4, x5,lwd=2)
text(x4, x5, pos=1)
title("c)", font.main=1)
# Wykres rozrzutu z liniami pionowymi
plot(x4, x5, type="h", lwd=2)
title("d)", font.main=1)
detach(x)