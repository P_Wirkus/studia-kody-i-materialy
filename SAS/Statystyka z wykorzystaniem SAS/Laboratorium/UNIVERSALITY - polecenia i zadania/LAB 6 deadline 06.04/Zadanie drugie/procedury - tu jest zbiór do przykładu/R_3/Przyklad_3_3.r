#***********************************************************************************************************************************************
#*  
#*  (C) 2009     Marek Walesiak     Uniwersytet Ekonomiczny we Wroc�awiu
#*  
#*  Przyk�ad do ksi��ki:
#*  "Statystyczna analiza danych z wykorzystaniem programu R", PWN, Warszawa 2009.
#*  
#*  Kod poni�szy mo�e by� modyfikowany, kopiowany i rozprowadzany na warunkach licencji GPL 2 (http://gnu.org.pl/text/licencja-gnu.html), 
#*  a w szczeg�lno�ci pod warunkiem umieszczenia w zmodyfikowanym pliku widocznej informacji o dokonanych zmianach, wraz z dat� ich dokonania. 
#*  
#***********************************************************************************************************************************************

library(MASS)
data(anorexia)
attach(anorexia)
# Utworzenie obszaru dla dw�ch rysunk�w
par(mfrow=c(1,2), pty="s")
options(OutDec=",")
# Histogram
hist(Prewt, breaks=nclass.FD(Prewt), main="", col="slategrey", xlab="waga nastolatek w funtach", ylab="cz�sto��")
title("a)", font.main=1)
# Histogram z oszacowan� funkcj� g�sto�ci na wykresie
hist(Prewt, freq=FALSE, breaks=nclass.FD(Prewt), main="", xlab="waga nastolatek w funtach", ylab="g�sto��")
title("b)", font.main=1)
lines(density(Prewt, kernel="gaussian", width=10, n=150))
detach(anorexia)