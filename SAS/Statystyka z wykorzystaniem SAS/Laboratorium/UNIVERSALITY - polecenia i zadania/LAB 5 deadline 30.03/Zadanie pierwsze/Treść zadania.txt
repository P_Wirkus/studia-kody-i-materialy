# Zadanie 1 (Dzielnice):
Mając do dyspozycji tabelę 'mieszkania.sas7bdat', w której umieszczone zostały informacje  dotyczące 
mieszkań w dużym mieście, proszę przeprowadzić analizę wariancji dla klasyfikacji pojedynczej ze względu 
na dzielnicę (zmienna czynnik określa jedną z czterech dzielnic tego miasta).

Odpowiedz na pytania:
1. czy powierzchnia mieszkań zależy od miejsca zamieszkania?
2. czy cena mieszkań zależy od miejsca zamieszkania?

Proszę przyjąć poziom istotności równy 0.02.

Wskazówki: anova, class, model, alpha

Pobierz załącznik
UWAGA - pobrany załącznik jest w tym folderze !!!

