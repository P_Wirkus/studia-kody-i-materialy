ZAD. 1. Ogólnie ok, ale bardzo niewiele osób zastosowało dokładnie takie samo formatowanie jak w zadaniu (nie odejmowałam za to punktów). Aby to zrobić można było stworzyć tabelę atrybutów z linku przy zadaniu (https://support.sas.com/kb/48/465.html).

ZAD. 2 Liczność próbki dla testu Pearson i Fishera

Badamy najpierw moc testu chi^2 Pearsona dla dwóch proporcji
(zob.https://support.sas.com/kb/24/298.html)

W zadaniu mamy "załóż, że różnica proporcji między próbkami wynosi 0.05" (potem zmienione na 0.2) - chodziło tu o "zerową różnicę" (nullpropdifference).

Domyślnie wynosi ona zero. Wówczas H0: p1-p2=0.
W naszym przypadku H0: p1-p2=005
