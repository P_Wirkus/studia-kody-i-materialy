library(tidyverse)
library(cluster)
library(factoextra)
library(dendextend)
par(mfrow=c(1,1))

df <- USArrests
str(df)

df <- na.omit(df)
summary(df)

data <- as.data.frame(scale(df))

summary(data)

####################################
#Grupowanie hierarchiczne | hierarchiczna analiza klastr�w (HCA z ang. hierarchical cluster analysis)

diss <- dist(data, method = "euclidian") 

#hclust

hc1 <- hclust(diss, method = "centroid") # Centroid linkage
hc2 <- hclust(diss, method = "single")   # Single (Minimum) linkage
hc3 <- hclust(diss, method = "complete") # Complete (Maximum) linkage
hc4 <- hclust(diss, method = "average")  # Average (Mean) linkage
hc5 <- hclust(diss, method = "ward.D2")  # Ward's minimum variance 

#dendrogramy

plot(hc1, cex = 0.6, hang = -1, 
     main = "Cetroid linkage dendrogram", sub = "", xlab = "") 
plot(hc2, cex = 0.6, hang = -1, 
     main = "Single linkage dendrogram", sub = "", xlab = "")   
plot(hc3, cex = 0.6, hang = -1, 
     main = "Complete linkage dendrogram", sub = "", xlab = "")  
plot(hc4, cex = 0.6, hang = -1, 
     main = "Average linkage dendrogram", sub = "", xlab = "") 
plot(hc5, cex = 0.6, hang = -1, 
     main = "Ward's minimum variance dendrogram", sub = "", xlab = "")  

#por�wnanie

dend3 <- as.dendrogram(hc3)
dend5 <- as.dendrogram(hc5)

tanglegram(dend3, dend5,
           cex_main = 1,
           cex_main_left = 0.5,
           cex_main_right = 0.5, 
           margin_inner = 10,
           lwd = 2,
           main = "Comparison of Complete and Ward linkage methods")

dend1 <- as.dendrogram(hc1)

tanglegram(dend1, dend5,
           cex_main = 1,
           cex_main_left = 0.5,
           cex_main_right = 0.5, 
           margin_inner = 10,
           lwd = 2,
           main = "Comparison of Centroid and Ward linkage methods")

#agnes

m <- c( "average", "single", "complete", "ward")
names(m) <- c( "average", "single", "complete", "ward")

#ac

choice <- function(x) {
  agnes(diss,
        diss = T,
        method = x)$ac
}

map_dbl(m, choice)

#hc1_a <- agnes(diss, diss = T, method = "single")
#hc2_a <- agnes(diss, diss = T, method = "complete")
#hc3_a <- agnes(diss, diss = T, method = "average")
hc4_a <- agnes(diss,
               diss = T,
               method = "ward")

#pltree(hc1_a, cex = 0.6, hang = -1, main = "Single linkage dendrogram", xlab = "", sub = "")
#pltree(hc2_a, cex = 0.6, hang = -1, main = "Complete linkage dendrogram", xlab = "", sub = "")
#pltree(hc3_a, cex = 0.6, hang = -1, main = "Average linkage dendrogram", xlab = "", sub = "")
par(mfrow=c(2,1))
par(mar = rep(2, 4))
pltree(hc4_a, cex = 0.6, hang = -1, main = "AGNES: Ward's minimum variance dendrogram", xlab = "", sub = "")

plot(hc5, cex = 0.6, hang = -1, 
     main = "HCLUST: Ward's minimum variance dendrogram", sub = "", xlab = "")

#ile klastr�w?

par(mfrow=c(1,1))

plot(hc5, cex = 0.6, hang = -1, 
     main = "HCLUST: Ward's minimum variance dendrogram", sub = "", xlab = "")


groups <- cutree(hc5, k = 4)  

table(groups) 

df$cluster <- groups

plot(hc5, cex = 0.6, xlab = "", sub = "", main = "Dendrogram with 4 clusters")
rect.hclust(hc5, k = 4, border = 2:5)

df$cluster <- as.factor(df$cluster)

summary <- df %>%
  group_by(cluster) %>%
  summarise(n = n(),
            mean_mur = mean(Murder),
            mean_ass = mean(Assault),
            mean_UP = mean(UrbanPop),
            mean_rap = mean(Rape))

##############################
#Algorytm centroid�w | k-�rednich (ang. k-means)
df <- read.csv("C:/Users/uzytkownik/Documents/Projekt/Country-data.csv")

df <- df %>% 
  remove_rownames %>% 
  column_to_rownames(var="country")

set.seed(9999)
kmeans(df, centers = 2, iter.max = 10, nstart = 1)

#ile klastr�w?

#elbow

fviz_nbclust(df, FUNcluster = kmeans, method = "wss", k.max = 10)

#average silhoutte method

fviz_nbclust(df, FUNcluster = kmeans, method = "silhouette", k.max = 10)

#gap statistic method

gap_stat <- clusGap(df, FUNcluster = kmeans, nstart = 25, K.max = 10, B = 50)
fviz_gap_stat(gap_stat)

set.seed(9999)
km <- kmeans(df, 5, nstart = 50, iter.max = 100)
print(km)

aggregate(df, by=list(cluster=km$cluster), mean)

df <- cbind(df, cluster = km$cluster)
head(df)

km$size

km$centers

fviz_cluster(km,
             data = df,
             main = "5 clusters",
             labelsize = 8,
             pointsize = 0.5)

#################################
#Algorytm PAM (ang. Partitioning Around Medoids)
telco <- read.csv("C:/Users/uzytkownik/Documents/Projekt/Telco.csv")

df <- telco
summary(df)
df$SeniorCitizen <- as.factor(ifelse(df$SeniorCitizen == 1, "Yes", "No"))
prop.table(table(df$Churn))

df$customerID <- NULL
names(df)[names(df) == "tenure"] <- "Months"

sum(is.na(df))
is.na(df)
df[!complete.cases(df),]
df[is.na(df)] <- 0

clust <- subset(df, df$Churn == "Yes")
clust$Churn <- NULL
clust$TotalCharges <- NULL

hist(clust$Months, breaks = 40)
hist(clust$MonthlyCharges, breaks = 40)

clust$Months <- log(clust$Months)
clust$MonthlyCharges <- log(clust$MonthlyCharges)

#gower distance matrix
diss_matrix <- daisy(clust,
                     metric = "gower")
summary(diss_matrix)

#ile klastr�W?
silhouette <- c()
silhouette <- c(silhouette, NA)
for(i in 2:10){
  pam_clusters <- pam(as.matrix(diss_matrix),
                      diss = TRUE,
                      k = i)
  silhouette <- c(silhouette, pam_clusters$silinfo$avg.width)
}

silhouette <- as.data.frame(silhouette)
silhouette$k <- c(1:10)

ggplot(data=silhouette, aes(x=k, y=silhouette, group=1)) +
  geom_line(linetype = "longdash", color = "forestgreen", size = 0.8)+
  geom_point(color = "forestgreen", size = 2)+
  labs(x = "Number of clusters", y = "Average Silhouette Width")+ 
  scale_x_continuous(breaks=seq(1, 10, 1))

pam <- pam(diss_matrix, diss = TRUE, k = 3)

clust_sum <- df[pam$medoids, ]

