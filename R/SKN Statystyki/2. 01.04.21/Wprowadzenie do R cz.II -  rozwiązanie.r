#w przypadku gdy zamiast polskich znaków pojawiają się dziwne szlaczki należy zmienić kodowanie skryptu na utf-8 file->reopen with encoding -> UTF-8

#rozpoczęcie definicji funkcji za pomocą polecenia "function(argumenty)", argumenty:
#liczba_zielonych - liczba kości zielonych w pojedynczym losowaniu
#liczba_zoltych - liczba kości zółtych w pojedynczym losowaniu
#liczba_fioletowych - liczba kości fioletowych w pojedynczym losowaniu
#liczba_czerwonych - liczba kości czerwonych w pojedynczym losowaniu
#n_losowan - liczba losowań
#czy_um - czy przy ustalaniu wyniku rzutu uzwględniamy umiejętność bohatera - argument musi przyjąć wartość TRUE lub FALSE
rzut_kością<-function(liczba_zielonych,liczba_zoltych,liczba_fioletowych,liczba_czerwonych,n_losowan,czy_um){

  #wektor reprezentujący liczbę punktów sukcesów/punktów porażek. Każdy element wektora reprezentuje jedną ściankę kości. 
  #zielona kość - 8 ścian - pukty sukcesu
zielona<-c(0,1,1,2,0,0,1,0)
#zielona kość - 12 ścian - pukty sukcesu
fioletowa<-c(0,1,2,0,0,0,0,1)
#zielona kość - 8 ścian - pukty porażki
żółta<-c(0,1,1,2,2,0,1,1,1,0,0,1)
#zielona kość - 12 ścian - pukty porażki
czerwona<-c(0,1,1,2,2,0,0,1,1,0,0,1)

#każdy rodzaj kości losowany jest oddzielnie, a liczba punktów sukcesu/porażki dopisywana do odpowiedniego wektora. Na koniec odpowiadające pozycje z wektorów są sumowane

#zdefiniowanie pustego obiektu do którego przypisywane będą wyniki rzutu
ciag_zielony<-c()
#pętla ustalająca wynik rzutu z danego rodzaju kości, w tym wypadku zielony 
#liczba powtórzeń równa jest wartości w parametrze n_losowan
for( i in 1:n_losowan){
  #funkcja sample() pozwala wybrać losowo n elementów ze zbioru. Przyjme dwa argumenty. pierwszy argument to wektor elementów, a drugi to liczba elementów
  rzut<-sample(zielona,liczba_zielonych)
  #sumowana jest liczba punktów sukcesów
  wynik<-sum(rzut)
  #liczba punktów sukcesów dopisywana jest ciągu sum wyników
  ciag_zielony<-append(ciag_zielony,wynik)
}
#logika jest analogiczna do poprzedniej pętli
ciag_zolty<-c()
for(i in 1:n_losowan){
  rzut<-sample(żółta,liczba_zoltych)
  wynik<-sum(rzut)
  ciag_zolty<-append(ciag_zolty,wynik)
}

#gdy parametr czy_um=TRUE uzględniona jest logika usuwania jednej,najgorszej kości. 
#W tym wypadku chodzi oto, że z wyniku rzutu usuwana jest jedna fiowelota kośćz największą liczbą punktów porażki
#gdy parametr przyjmuje inna wartość logika pętli analogiczna do poprzednich pętli
ciag_fioletowy<-c()
#bramka if() sprawdzająca wartość parametru
if(czy_um==TRUE){
#blok wykonywany gdy czy_um=true
    for( i in 1:n_losowan){
  #funkcja sample() pozwala wybrać losowo n elementów ze zbioru. Przyjme dwa argumenty. pierwszy argument to wektor elementów, a drugi to liczba elementów
  rzut<-sample(fioletowa,liczba_fioletowych)
  # sumowany jest wynik na kościach. W nawiasach kwadratowych wpisana jest instrukcja która sprawdza, które kości mają najwięcej punktów porażki i  usuwana jest pierwsza z nich
  #"-" pozwala wybrać wszystkie elementy oprócz podanego elementu
  #funkcja which() pozwala wybrać określić położenie obiektów w wektorze, które spełniają podany warunek
  wynik<-sum(rzut[-which(rzut==max(rzut))[1]])
  ciag_fioletowy<-append(ciag_fioletowy,wynik) 
  }
}else{ 
  #blok instrukcji wykonywany gdy czy_um nie równa się true
for( i in 1:n_losowan){
  #logika jest analogiczna do poprzedniej pętli
  rzut<-sample(fioletowa,liczba_fioletowych)
  wynik<-sum(rzut)
  ciag_fioletowy<-append(ciag_fioletowy,wynik)
}
}
#logika jest analogiczna do poprzedniej pętli
ciag_czerwony<-c()
for(i in 1:n_losowan){
  rzut<-sample(czerwona,liczba_czerwonych)
  wynik<-sum(rzut)
  ciag_czerwony<-append(ciag_czerwony,wynik)
}
#sumowane są odpowiadające rzuty (ten sam numer rzutu) z każdego rodzaju kości
#funkcja which() zwraca pozycje elementów wektora,które spełniają warunek
#długośc wektora pozycji jest równa liczbie elementów, które spełniają warunek.
#iloraz długośc wektora pozycji i liczbie wszystkich losowań określa odsetek losowań zakończonych sukcesem (liczba punktów sukcesu >0)
return(length(which((ciag_zielony+ciag_zolty-ciag_fioletowy-ciag_czerwony)>0))/n_losowan)
}
