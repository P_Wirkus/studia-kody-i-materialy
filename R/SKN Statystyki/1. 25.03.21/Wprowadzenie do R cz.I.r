####Warsztaty nr 1 - Wprowadzenie do R####
####25.03.2021 - Maciek Piskorz####

#Jezeli zamiast polskich znakow wyswietlaja Ci się artefakty - spróbuj:
#File -> Reopen with encoding... -> UTF-8 -> OK

#wykasuj dane z poprzedniej sesji
ls() #lista obecnie zdefiniowanych obiektów
rm(y) #usuwanie obiektu
rm(list=ls()) #usunięcie wszystkich obiektów
cat("\014")


####Podstawy obs?ugi R ####
#ustawienie nowego folderu roboczego
setwd("/Users/Maciek/Documents/R/Warsztaty_z_R")

getwd() #aktualny folder roboczy

base::print("hello world")

?plot #opisuje funkcje i podaje przykłady zastosowań
?rnorm

2+2*2^2 #r moze byc kalkulatorem
exp(1) #liczba e
log(exp(2),exp(1)) #log - logarytm naturalny, log2() - logarytm o podstawie 2
5%%3 #reszta z dzielenia
5%/%3 #liczba całkowita z dzielenia

x=1
x<-1
log(base=8, 2)
log(8,base=2)
log(base=2,x=8)

z<- as.Date("03-02-1997",format="%d-%m-%y") #zapis daty
TF<-2==2 #sprawdza wartość logiczną
class(x)
is.numeric(x)
is.character(x)
as.Date(12,origin="1900-01-01") #przeskocz x dni do przodu

dataseq<- seq(from=as.Date("2003-02-19"),to=as.Date("2010-02-19"),by="1 year")

dataseq
#as=potraktuj zmienną jako

x.char<-as.character(x) #zmiana zmiennej numerycznej na tekstową
x.ts<-as.ts(x) #szereg czasowy
x.ts<- ts(x,start=2000, freq=1)
?ts
summary(x)
summary(x.char)

exp(10) #logarytm o podstawie naturalnej

sqrt(10)  #pierwiastek

abs(-10.74) #wartość bezwzględna

floor(10.75) #zaokrąglamy w dół do jedności

ceiling(10.75) #zaokrągla w górę

round(3.141592653, digits=4) #zaokrągla do %digits% miejsc po przecinku


########## SKALARY #############

a<- 2*4
b<- 6*3
c<- 3*3
d<-a+b/c
f<- d^2

############# WEKTORY  ###########

#  $$$ Wektory tworzy się przez dodanie litery c(), w środku wpisujemy to co chcemy umieścić w wektorze

x<- c(2,3,4,5,6)

y<- 1:8000 #tworzy ciąg liczb od 1 do 8000 -  naturalnych
length(x) #podaje długość wektora
length(y) 

x<-sqrt(x) #dzialania na wektorach
log(x,2)
x=x/2

y[c(1,3,4)] #indeksowanie
x[2:5]
x[-c(4,5)]
head(x,2)
tail(x,2)
x<-x*4
x>=3 #testowanie wlasciwosci elementow
all(x>=5) #czy wszystkie
 
any(x>=3) #czy któryś
which(x>=2)
which(x>=8)
x2<-x^2

#porownywanie elementow wektora
TF2=x==x2

TF2

q<- c("a", "c", "d")

############### SEKWENCJE ##############

aa<- rep(2,10) #liczba 2 powtarza się 10 razy, pierwszy argument- co powtarzamy, drugi ile razy
summary(aa)
bb<- rep(1:4, each=4) #to samo, each=4 daje nam powtórzenie każdego elementu 4 krotnie
cc<- rep(1:4, times=4) #czterokrotnie podaje liczby od 1 do 4, dzięki times


aa
bb
cc



seq(1,20) #sekwencja liczb od 1 do 20,  od do
seq(1,20, length.out=40) #sekwencja liczb od 1 do 20 ale w 40 elementach, równe odstępy
seq(1,20, by=0.5) 

rep(seq(1,10, by=0.1),2) # argument by jest podziałką, którą odmierzamy wartości w sekwencji

ee<- 1:10
ff<- 1:5
gg<- c(ee,ff,1:3)

gg #połączyliśmy tu trzy wektory w jeden długi wektor- od 1 do 10, od 1 do 5 i od 1 do 3


########## WYCIĄGANIE DANYCH  ###########

ee

ee[3:5] #wyciąga liczby od 3 do 5, nawias kwadratowy [] wyciąga części zbioru
ee[c(2,5,8,9)]
ee[-c(2,5)]  #z argumentem -c możemy wyciągać wektor z wektora, wycinać go

v<- c(65,74,67,86,97,65)

sort(v) #sortowanie malejące

sort(v, decreasing= FALSE) #sortowanie rosnące

##### PODSTAWOWE STATYSTYKI NASZEGO WEKTORA

sum(v)
summary(v)
unique(v) #wszystkie unikalne wartości

zwierzaki <- c("kot", "pies")

zwierzaki

class(zwierzaki) #komenda podaje nam jaka jest klasa naszego atrybutu

##### KRYTERIA WYSZUKIWANIA ######

d<- c(-12,6,17,23,48,56,NA)

which(d<0) #pokazuje na której pozycji w wektorze stoi liczba mniejsza niż 0

which(d==0) #nie ma wartości zerowych 

which(d!=0) #pokazuje które wartości są różne od zera, wszystkie pozycje oprócz pozycji NA

which(is.na(d)) #wskazuje pozycję NA, 7 miejsce
which(d>0 $ d>10) #która większa i która większa
which(d<0 | d<20) #która większa lub która mniejsza

which (d==max(d)) #nic nie zwróci pytając o najwieksze, bo jest NA

which(d==max(d, na.rm=T)) #na.rm to na removement- nie uwzględniając NA
d[6]

which(d==min(d, na.rm=T)) #wyszukujemy namniejszą wartość, na której pozycji stoi

which(is.na(d)) #nasza wartość NA gdzie się znajduje
d[7] 

d[7]<- 16  #### zmieniliśmy 7 wektor zmieniamy z NA na 16


#### PODSTAWOWE STATYSTYKI #######

d
median(d)
mean(d)
quantile(d)
sd(d) #odchylenie standardowe

#####
###### OBIEKTY DWUWYMIAROWE- MACIERZE #########

rm(list=ls())

m1<- matrix(data= 1:12, nrow=3, ncol=4) #tworzymy macierz wypełnioną od 1 do 12

m2<- matrix(data= 1:12, nrow=3, ncol=4, byrow=T) 

#jak dodamy byrow to kolejne wypełnienia od lewej do prawej a jak bez (lub bycol) to góra dół
t(m1) %*% m2

t(m1)  #tak transponujemy macierz
m1

t(m1)%*%m2  #tak można przemnożyć przez siebie macierze

########### WYCIĄGANIE DANYCH Z MACIERZY ########
m1
m1[1,2] #wyciągamy wartość 1 kolumny, 2 wiersza- obiekt 2 wymiarowy 

m1[,2] #puste na początku- nie ma konkretnego wiersza więc wyciąga wszystkie wiersze dla 2 kolumny

m1[1,] #analogicznie dla kolumn, wszystkie kolumny dla jednego wiersza

m1[1,1]<- 15 #definiujemy do co chcemy jako 1 wiersz 1 kolumna u nas w macierzy
m1

pp<- 1:3
qq<- 1:4
cbind(m1,pp) #cbind dokłada dodatkową kolumnę
rbind(m1,qq) # rbind dokłada dodatkowy wiersz, w ten sposób można łatwo łączyć macierze

##########   DATA.FRAME ############
#tabela danych zachowuje się jak macierz, może jednak zawierać dowolny typ danych
x1<- 1:5
x2<- letters[1:5]
x3<- rnorm(20)
data.frame_1<- data.frame(x1,x2,x3)

View(data.frame_1)

str(data.frame) #struktura naszych danych- co to jest ile ma obserwacji i ile zmiennych, i
#każda wartość wierszy i kolumn jak wyglądają i z czego się składają

summary(data.frame_1)[,3] #prosimy o statystyki tylko dla 3 kolumny, wszystkich wierszy


########################### PRACA DOMOWA #################################
# 
# 1. Napisz komendę ustawiająca pulpit Twojego komputera jako Working Directory
# 2. Stwórz cztery wektory, z następującymi wartościami każdy (oznacz je kolejnymi literami alfabetu zaczynając od a):
#    1. (1,9,12,3)
#    2. (21,3,422,5)
#    3. (10,2,14,16)
#    4. (100.25, 9.5, 125.75, 140)
# 3. Podaj komendy dzięki którym otrzymasz średnią, medianę oraz sumę z tych wektorów
# 4. Przekształć wektor a i c w macierz. Tą samą operację przeprowadź na wektorach b i d
# 5. Posortuj wartości wektora b w kolejności malejącej
# 6. Podaj komendę, która wskaże które elementy wektora c są mniejsze niż 10
# 7. Stwórz wektory (oznacz je kolejnymi literami alfabetu zaczynając od e) z następującymi wartościami:
#     1. („Ala”, „ma”)
#     2. („super”,”kotka”)
# 8. wpisz komendę, dzięki której dowiesz się jaki typ danych zawiera wektor e
# 9. Połącz wektor e z wektorem f
# 10. Stwórz data frame, który zawierał będzie trzy kolumny: 
#     1. kolumna x: wartości od 10 do 20, 
#     2. kolumna y: pierwsze 20 liter alfabetu
#     3. kolumna z: rozkład normalny z 20
# 11. Teraz zmień ilość liter alfabetu na 32. Co się zmieniło?

#Prace domowa prosimy przesylac na adres skns.projekt@gmail.com :) 