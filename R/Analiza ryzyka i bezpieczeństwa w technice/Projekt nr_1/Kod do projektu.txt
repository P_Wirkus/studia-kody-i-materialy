# PROJEKT3.
library(stats)
# uklad ma dzialac do czasu T;
N = 500
T = 0.6
# generujemy rozklady
X1 = rexp(N,rate=10);
X2 = rgamma(N,0.1,scale=2);
Y = runif(N, 0 ,0.2);
licznik = 0;
for(i in 1 : N)
{
  if(X1[i] < T && X1[i] + Y[i] <= X2[i])
  {
    X1[i] = rexp(1,rate=10) + X1[i] + Y[i];
  }
  if(X1[i]>= T || X2[i] >= T)
  {
    licznik = licznik + 1;
  }
}
PP = licznik / N