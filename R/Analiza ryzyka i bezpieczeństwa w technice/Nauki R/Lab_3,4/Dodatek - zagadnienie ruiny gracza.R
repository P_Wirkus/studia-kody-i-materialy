# Zagadnienie ruiny gracza 
# [0,a]  z kapita�em pocz�tkowym gracza z
# prawdopodobie�stwo wygrania 1 wynosi p, przegrania 1 wynosi q=1-p
#
# prawdopodobie�stwo ruiny q(z)=((q/p)^a-(q/p)^z)/((q/p)^a-1) dla q!=1/2
# prawdopodobie�stwo ruiny q(z)=1-z/a dla q=p=1/2
#
# warto�� oczekiwana wygranej W(z)=a*(1-q(z))-z dla q!=1/2
# warto�� oczekiwana wygranej W(z)=0 dla q=p=1/2
#
# d�ugo�� gry D(z)=z/(q-p)-(a/(q-p))*(1-(q/p)^z)/(1-(q/p)^a) dla q!=1/2
# d�ugo�� gry D(z)=z(a-z) dla q=p=1/2

#dane
q=1/2      # prawdopodobie�stwo przegrania 1
a=10       # pula gry
z=9        # kapita� pocz�tkowy 0<=z<=a

#zmienne
p=1-q

################### symulacja pojedynczej gry

X0=z
X=c(X0)
i=1
while(X[i]>0 && X[i]<10){
X[i+1]=X[i]+sample(c(-1,1),size = 1, replace = TRUE, prob = c(q,p))    # zamiast sample mo�na u�y� dowolnej innej metody
                                                                       # prosta zmiana pozwala na inne rozk�ady r
i=i+1
}
X
plot(X,ylim=c(0,10))

#############################################################################
################## program pojedyncza gra, zwraca przebieg rozgrywki - wektor
ruina<-function(q,a,z){
X0=z
X=c(X0)
i=1
while(X[i]>0 && X[i]<a){
X[i+1]=X[i]+sample(c(-1,1),size = 1, replace = TRUE, prob = c(q,1-q))
i=i+1
}
return(X)
}

ruina(1/2,10,9)

################# symulacyjne wyznaczenie d�ugo�ci gry
dlugosc=0
for(i in 1:10000){	# �rednia d�ugo�� na podst. 10000 przebieg�w gry
Y=ruina(1/2,10,9)
dlugosc=dlugosc+length(Y)
}
dlugosc/10000


