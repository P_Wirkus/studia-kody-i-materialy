# 1
Uklad <- function(n)
{
  arch = runif(n,0, 32)
  mur = runif(n,10,60)
  ogr = rexp(n,0.04)
  elek = rexp(n, 0.025)
  hydr = rgamma(n,1,1/20)
  mala = runif(n,15,45)
  sprza = rexp(n,0.008)
  
  wektor_dni = c()
  for(i in 1 : n)
  wektor_dni[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
  return(wektor_dni) 
}
Uklad(10) # Przyk�ad dla n = 10
# 2
ParseData <- function(wektor_dni,n, czas)
{
  return(data.frame(c("N","PP", "Sr", "Var"),c(n,mean(wektor_dni >= czas), mean(wektor_dni),var(wektor_dni))))
}

# 3 niezawodno�� teoretyczna
NTeoretyczna <- function(t)
{
  x1 = 1 - punif(t,0, 32)
  x2 = 1 - punif(t,10,60)
  x3 = 1 - pexp(t,0.04)
  x4 = 1 - pgeom(t, 0.025)
  x5 = 1 - pgamma(t,1,1/20)
  x6 = 1 - punif(t,15,45)
  x7 = 1 - pexp(t,0.008)
  return(x1*x2*x4*x6*x7 + x1*x2*x5*x6*x7 + x1*x3*x7 - x1*x2*x4*x5*x6*x7 - x1*x2*x4*x6*x7*x3 - x1 * x2 *x5 * x6 * x7 * x3 + x1 * x2 * x3* x4*x5 * x6 * x7)
}

# 4
EXX_NTeoretyczna <- function(t)
{
  x1 = 1 - punif(t,0, 32)
  x2 = 1 - punif(t,10,60)
  x3 = 1 - pexp(t,0.04)
  x4 = 1 - pgeom(t, 0.025)
  x5 = 1 - pgamma(t,1,1/20)
  x6 = 1 - punif(t,15,45)
  x7 = 1 - pexp(t,0.008)
  return(2*t*(x1*x2*x4*x6*x7 + x1*x2*x5*x6*x7 + x1*x3*x7 - x1*x2*x4*x5*x6*x7 - x1*x2*x4*x6*x7*x3 - x1 * x2 *x5 * x6 * x7 * x3 + x1 * x2 * x3* x4*x5 * x6 * x7))
}

# 5
PlotTE <- function(data)
{
  plot(ecdf(data), lwd = 2)
  x <- seq(0, max(data) + 1, by = 0.01)
  lines(x, 1 - NTeoretyczna(x), col = 2, lty = 2, lwd = 2)
}

# 6
RegresjaTE <- function(data)
{
  x <- seq(0, max(data) + 1, by = 0.01)
  x1 <- 1 - NTeoretyczna(x)
  y1 <- ecdf(data)(x)
  reg <- lm(y1 ~ x1)
  print(summary(reg))
  plot(x1, y1,type = "l", col = "2")
  abline(reg)
}

#7
data100 = Uklad(100)
ParseData(data100, 100, 30)
data1000 = Uklad(1000)
ParseData(data1000, 1000, 30)
data10000 = Uklad(10000)
ParseData(data10000,10000, 30)

#8 
hist(data100, main="Histogram czasu �ycia dla n = 100",xlab="ilo�� dni", ylab="cz�stotliwo��")
hist(data1000, main="Histogram czasu �ycia dla n = 1000", xlab="ilo�� dni", ylab="cz�stotliwo��")
hist(data10000, main="Histogram czasu �ycia dla n = 10000", xlab="ilo�� dni", ylab="cz�stotliwo��")

#9 - s� b��dy
PlotTE(data100)
RegresjaTE(data100)
PlotTE(data1000)
RegresjaTE(data1000)
PlotTE(data10000)
RegresjaTE(data10000)

#10
EX = integrate(NTeoretyczna, lower = 0, upper = Inf, subdivisions=2000)$value
EXX = integrate(EXX_NTeoretyczna,0,Inf)$value
VarX = EXX - EX^2

#11 - miary
Struktura <- function(x1,x2,x3,x4,x5,x6,x7)
{
  return(x1*x2*x4*x6*x7 + x1*x2*x5*x6*x7 + x1*x3*x7 - x1*x2*x4*x5*x6*x7 - x1*x2*x4*x6*x7*x3 - x1 * x2 *x5 * x6 * x7 * x3 + x1 * x2 * x3* x4*x5 * x6 * x7)
}

Miary <- function()
{
  t = 30
  
  x1 = 1 - punif(t,0, 32)
  x2 = 1 - punif(t,10,60)
  x3 = 1 - pexp(t,0.04)
  x4 = 1 - pgeom(t, 0.025)
  x5 = 1 - pgamma(t,1,1/20)
  x6 = 1 - punif(t,15,45)
  x7 = 1 - pexp(t,0.008)
  
  IA_1 = Struktura(1,x2,x3,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  IA_2 = Struktura(x1,1,x3,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  IA_3 = Struktura(x1,x2,1,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  IA_4 = Struktura(x1,x2,x3,1,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  IA_5 = Struktura(x1,x2,x3,x4,1,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  IA_6 = Struktura(x1,x2,x3,x4,x5,1,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  IA_7 = Struktura(x1,x2,x3,x4,x5,x6,1) - Struktura(x1,x2,x3,x4,x5,x6,x7)
  
  IB_1 = Struktura(1,x2,x3,x4,x5,x6,x7) - Struktura(0,x2,x3,x4,x5,x6,x7)
  IB_2 = Struktura(x1,1,x3,x4,x5,x6,x7) - Struktura(x1,0,x3,x4,x5,x6,x7)
  IB_3 = Struktura(x1,x2,1,x4,x5,x6,x7) - Struktura(x1,x2,0,x4,x5,x6,x7)
  IB_4 = Struktura(x1,x2,x3,1,x5,x6,x7) - Struktura(x1,x2,x3,0,x5,x6,x7)
  IB_5 = Struktura(x1,x2,x3,x4,1,x6,x7) - Struktura(x1,x2,x3,x4,0,x6,x7)
  IB_6 = Struktura(x1,x2,x3,x4,x5,1,x7) - Struktura(x1,x2,x3,x4,x5,0,x7)
  IB_7 = Struktura(x1,x2,x3,x4,x5,x6,1) - Struktura(x1,x2,x3,x4,x5,x6,0)
  
  model <- c("Arch", "Mur", "Ogr", "Ele", "Hydr", "Mal","Spr")
  IA_teoret <- c(IA_1,IA_2,IA_3,IA_4,IA_5,IA_6,IA_7) 
  IB_teoret <- c(IB_1,IB_2,IB_3,IB_4,IB_5,IB_6,IB_7)
  IA_do_IB_teo = IA_teoret/IB_teoret 
  return(data.frame(model,IA_teoret,IB_teoret,IA_do_IB_teo))  
}

#12 - miary EMP
MiaryEmp <- function()
{
  n = 10000
  t = 30
  
  z <- c()
  arch = runif(n,0,32)
  mur = runif(n,10,60)
  ogr = rexp(n,0.04)
  elek = rexp(n, 0.025)
  hydr = rgamma(n,1,1/20)
  mala = runif(n,15,45)
  sprza = rexp(n,0.008)
  
  for(i in 1 : n)
  z[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
  P = mean(z >= t)
  
  czas_1_dziala <- c()
  czas_2_dziala <- c()
  czas_3_dziala <- c()
  czas_4_dziala <- c()
  czas_5_dziala <- c()
  czas_6_dziala <- c()
  czas_7_dziala <- c()
  
  czas_1_nie_dziala <- c()
  czas_2_nie_dziala <- c()
  czas_3_nie_dziala <- c()
  czas_4_nie_dziala <- c()
  czas_5_nie_dziala <- c()
  czas_6_nie_dziala <- c()
  czas_7_nie_dziala <- c()
  
  for(i in 1:n)
  {
    czas_1_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), t)
    czas_2_dziala[i] = min(max(min(t,max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_3_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(t,sprza[i])), arch[i])
    czas_4_dziala[i] = min(max(min(mur[i],max(t,hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_5_dziala[i] = min(max(min(mur[i],max(elek[i],t),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_6_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),t, sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_7_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], t),min(ogr[i],t)), arch[i])
    
    czas_1_nie_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), 0)
    czas_2_nie_dziala[i] = min(max(min(0,max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_3_nie_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(0,sprza[i])), arch[i])
    czas_4_nie_dziala[i] = min(max(min(mur[i],max(0,hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_5_nie_dziala[i] = min(max(min(mur[i],max(elek[i],0),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_6_nie_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),0, sprza[i]),min(ogr[i],sprza[i])), arch[i])
    czas_7_nie_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], 0),min(ogr[i],0)), arch[i])
  }
  
  P_1_dziala = mean(czas_1_dziala >= t)
  P_2_dziala = mean(czas_2_dziala >= t)
  P_3_dziala = mean(czas_3_dziala >= t)
  P_4_dziala = mean(czas_4_dziala >= t)
  P_5_dziala = mean(czas_5_dziala >= t)
  P_6_dziala = mean(czas_6_dziala >= t)
  P_7_dziala = mean(czas_7_dziala >= t)
  
  P_1_nie_dziala <- mean(czas_1_nie_dziala >= t)
  P_2_nie_dziala <- mean(czas_2_nie_dziala >= t)
  P_3_nie_dziala <- mean(czas_3_nie_dziala >= t)
  P_4_nie_dziala <- mean(czas_4_nie_dziala >= t)
  P_5_nie_dziala <- mean(czas_5_nie_dziala >= t)
  P_6_nie_dziala <- mean(czas_6_nie_dziala >= t)
  P_7_nie_dziala <- mean(czas_7_nie_dziala >= t)
  
  IA_emp <- c(P_1_dziala - P,P_2_dziala - P,P_3_dziala - P,P_4_dziala - P,P_5_dziala - P, P_6_dziala - P,P_7_dziala - P)
  IB_emp <- c(P_1_dziala - P_1_nie_dziala, P_2_dziala - P_2_nie_dziala,P_3_dziala - P_3_nie_dziala,P_4_dziala - P_4_nie_dziala,P_5_dziala - P_5_nie_dziala, P_6_dziala - P_6_nie_dziala,P_7_dziala - P_7_nie_dziala)
  
  model <- c("Arch", "Mur", "Ogr", "Ele", "Hydr", "Mal","Spr")
  IA_do_IB_emp = IA_emp/IB_emp 
  return(data.frame(model,IA_emp,IB_emp,IA_do_IB_emp))    
}

#13
  #t = 30
  #dla jednego wybranego elementu to powiedzmy dla Ogrodnika
TestMiar <- function()
{
  wynik_IA_t = c()
  wynik_IA_e = c()
  
  n = 10000
  t = 0
  for(k in 1:1001)
  {
    x1 = 1 - punif(t,0, 32)
    x2 = 1 - punif(t,10,60)
    x3 = 1 - pexp(t,0.04)
    x4 = 1 - pgeom(t, 0.025)
    x5 = 1 - pgamma(t,1,1/20)
    x6 = 1 - punif(t,15,45)
    x7 = 1 - pexp(t,0.008)
    
    wynik_IA_t[k] = Struktura(x1,x2,1,x4,x5,x6,x7) - Struktura(x1,x2,x3,x4,x5,x6,x7)
    
    z = c()
    
    arch = runif(n,0, 32)
    mur = runif(n,10,60)
    ogr = rexp(n,0.04)
    elek = rexp(n, 0.025)
    hydr = rgamma(n,1,1/20)
    mala = runif(n,15,45)
    sprza = rexp(n,0.008)
    
    for(i in 1:n)
      z[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
    
    P = mean(z >= t)
    czas_3_dziala = c()
    
    for(i in 1 : n)
      czas_3_dziala[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(t,sprza[i])), arch[i])
    
    P1_dziala = mean(czas_3_dziala >= t)
    wynik_IA_e[k] = P1_dziala - P
    t = 0.03 * k
  }
  
  wynik_IA_e = sort(wynik_IA_e)
  wynik_IA_t = sort(wynik_IA_t)
  czasy = seq(0,30, by=0.03)
  par(mfrow=c(1,2))
  plot(czasy,wynik_IA_e,type="l",col="5")
  plot(czasy,wynik_IA_t,col="2")
  return(ks.test(wynik_IA_e,wynik_IA_t))
}

#14 miary_wykresy
miary = Miary()
miaryEmp = MiaryEmp()
TestMiar()

x <- seq(1,7,by=1)

plot(x, miaryEmp$IA_do_IB_emp)
points(miary$IA_do_IB_teo, col = "red")

plot(x, miaryEmp$IA_emp)
points(miary$IA_teoret, col = "red")

plot(x, miaryEmp$IB_emp)
points(miary$IB_teoret, col = "red")

#15 - funkcja_odnowy
UkladOdnowa <- function(n, czas)
{
  arch = runif(n,0, 32)
  mur = runif(n,10,60)
  ogr = rexp(n,0.04)
  elek = rexp(n, 0.025)
  hydr = rgamma(n,1,1/20)
  mala = runif(n,15,45)
  sprza = rexp(n,0.008)
  
  wektor_dni = c()
  naprawy = 0
  naprawa = runif(1,0, 2)
  for(i in 1 : n)
  {
    while(TRUE)
    {
      wektor_dni[i] = min(max(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]),min(ogr[i],sprza[i])), arch[i])
      if(wektor_dni[i] >= czas)
      {
        break;
      }
      else if(sprza[i] >= czas && arch[i] >= czas && min(min(mur[i],max(elek[i],hydr[i]),mala[i], sprza[i]), arch[i]) + naprawa < czas)
      {
        naprawy = naprawy + 1
        ogr[i] = ogr[i] + naprawa + rexp(1,0.04)
      }
      else
        break;
    }
  }
  
  return(list("wektor" = wektor_dni,"naprawy" = naprawy))
}

#16 - tu potrzebna funkcja do odnowy
var.test(var(data100),VarX)
var.test(var(data1000),VarX)
var.test(var(data10000),VarX)

#17 - tu potrzebna funkcja do odnowy
dataOdn = UkladOdnowa(10000,30)
sr_dataOdn = mean(dataOdn$wektor)
var_dataOdn = var(dataOdn$wektor)
sr_naprawy = dataOdn$napraw / 10000