library("graphics")


#Zadanie 1

F <- function(x)
{
  return(exp(-exp(-(x-2)/3)))
}
f <- function(x)
{
  return(exp(-((x-2)/3 + exp(-(x-2)/3))))
}

Odwrotna <- function(x)
{
  return(2 - 3*log(-log(x)))
}

Ciag <-function(n)
{
  U = runif(n)
  return(Odwrotna(U))
}

x<-seq(0, 1,0.001)
layout(matrix(c(1,2),nrow=1), width=c(4,1)) 
par(mar=c(5,4,4,0)) #No margin on the right side
matplot(x,cbind(F(x),Odwrotna(x)),type="l",col=c("blue","red"), ylab = "y")
par(mar=c(5,0,4,2)) #No margin on the left side
plot(c(0,1),type="n", axes=F, xlab="", ylab="")
legend("center", c("Dystybuanta", "Dystrybuanta odwrotna"),col=seq_len(2),cex=0.8,fill=seq_len(2))

C = Ciag(4000)

h<-hist(C, breaks=100, col="red", xlab="Wartosci",ylab = "Czestotliwosc",
        main="")
xfit<-seq(min(C),max(C),length=40)
yfit<-f(xfit)
yfit <- yfit*diff(h$mids[1:2])*length(x)
lines(xfit, yfit, col="blue", lwd=2)


#zadanie 2

fun <- function(x)
{
  return(sin(x**2 + 4*x + 4)/((1 + x)**2))
}

x<-seq(10, 16, 0.001)
matplot(x,fun(x),type="l",col=c("red"), ylab = "f(x)")
N = 1000000
h<-function(x)
  
{
    return(sin(x**2 + 4*x + 4))
}

inverse_norm <-function(x)
{
    return(10 - sqrt(-2*log(x * sqrt(2 * pi))))
}

sample = inverse_norm(runif(N, 0, 1/sqrt(2*pi)))
h_sample=apply(as.matrix(sample),1,FUN=h)
mean(h_sample) 

