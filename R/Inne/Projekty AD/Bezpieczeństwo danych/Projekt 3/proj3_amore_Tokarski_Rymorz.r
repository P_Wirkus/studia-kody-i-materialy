install.packages("AMORE")
library(AMORE)
install.packages("Rfast")
library(Rfast)

#sprawdzenie katalogu roboczego
getwd()
X_train = read.csv("fashion-mnist_train.csv")
X_train[] = lapply(X_train,as.numeric)
Y_train = X_train[,1] + 1
X_train = X_train[,2:785]

X_test = read.csv("fashion-mnist_test.csv")
X_test[] <- lapply(X_test,as.numeric)
Y_test = X_test[,1] + 1
X_test = X_test[,2:785]


temp = matrix(rep(0, 60000 * 10), nrow = 60000, ncol = 10)
for(i in 1:60000)
{
  temp[i,Y_train[i]] = 1
}
Y_train = temp


temp = matrix(rep(0, 10000 * 10),nrow = 10000, ncol = 10)
for(i in 1:10000)
{
  temp[i,Y_test[i]] = 1
}
Y_test = temp


siec<-newff(n.neurons=c(784, 32, 10),
            learning.rate.global=0.01,
            momentum.global=0.5,
            hidden.layer="sigmoid",
            output.layer="purelin",
            method="BATCHgd",
            error.criterium="LMS")

wynik<-train(siec,
             X_train,
             Y_train,
             error.criterium="LMS",
             report=TRUE,
             show.step=10,
             n.shows=30,
             n.threads = -1)

plot(wynik$Merror,type="l",xlab="Ileracja (x10)",
     ylab="B��d", col="darkred")

test_sieci = sim(wynik$net,X_train)
test_sieci = rowMaxs(test_sieci)

(sum(Y_train == test_sieci) / 60000) * 100

test_sieci = sim(wynik$net,X_test)
test_sieci = rowMaxs(test_sieci)

(sum(Y_test == test_sieci) / 10000) * 100

