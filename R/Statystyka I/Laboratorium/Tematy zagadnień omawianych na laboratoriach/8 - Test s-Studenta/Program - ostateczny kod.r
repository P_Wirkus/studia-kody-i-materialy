library(dplyr)

# Zadanie nr_1
x <-c(175,168,168,190,156,181,182,175,174,179)
y <-c(185,169,173,173,188,186,175,174,179,180)
shapiro.test(x)
shapiro.test(y)
var.test(x,y, alternative = c("two.sided"))
t.test(x,y)


# Zadanie nr_2
shapiro.test(mtcars$mpg)
a <- filter(mtcars,am==0)
b <- filter(mtcars,am==1)
mean(a$mpg)
mean(b$mpg)

t.test(a$mpg, b$mpg)

# Zadanie nr_3

mean(chickwts$weight)
var(chickwts$weight)
sd(chickwts$weight)


horsebean = chickwts[chickwts$feed == "horsebean", 1]
linseed = chickwts[chickwts$feed == "linseed", 1]
soybean = chickwts[chickwts$feed == "soybean", 1]
sunflower = chickwts[chickwts$feed == "sunflower", 1]
meatmeal = chickwts[chickwts$feed == "meatmeal", 1]
casein = chickwts[chickwts$feed == "casein", 1]

v1 = c("feed", "horsebean", "linseed", "soybean", "sunflower", "meatmeal", "casein")
v2 = c("Mean", mean(horsebean$weight),mean(linseed$weight),mean(soybean$weight),mean(sunflower$weight),mean(meatmeal$weight),mean(casein$weight))
v3 = c("Var", var(horsebean$weight),var(linseed$weight),var(soybean$weight),var(sunflower$weight),var(meatmeal$weight),var(casein$weight))
v4 = c("Sd", sd(horsebean$weight),sd(linseed$weight),sd(soybean$weight),sd(sunflower$weight),sd(meatmeal$weight),sd(casein$weight))

data.frame(v1,v2,v3,v4)

ks.test(horsebean,linseed)
ks.test(horsebean,soybean)
ks.test(horsebean,sunflower)
ks.test(horsebean,meatmeal)
ks.test(horsebean,casein)

ks.test(linseed,soybean)
ks.test(linseed,sunflower)
ks.test(linseed,meatmeal)
ks.test(linseed,casein)

ks.test(soybean,sunflower)
ks.test(soybean,meatmeal)
ks.test(soybean,casein)

ks.test(sunflower,meatmeal)
ks.test(sunflower,casein)

ks.test(casein,meatmeal)


