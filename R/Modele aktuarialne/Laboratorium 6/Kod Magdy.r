# aproksymacja zmiennej S = S1 + S2,
# S1 = X1 + .. + XN
# S2 = Y1 + .. + YM
# gdzie N ma rozklad Poissona(lambda1), M ma rozklad Poissona(lambda2)
# X, Y - zmienne losowe o rozk�adzie geometrycznym z parametrami kolejno theta1, theta2.

# a) theta1 = theta2 = 0.2
# b) theta1 =0.2, theta2 = 0.4

# dla aproksymacji rozkladem gamma
# alfa = (4*lambda* (E(X^2))^3) /(E(X^3))^2
# beta = (2*E(X^2))/E(X^3)
# x0 = lambda*E(X) - (2*lambda*(E(X^2))
lambda1 = 1
lambda2 = 2
theta1 =0.2
theta2 = 0.4
lambda = lambda1 + lambda2

# funkcja tworzaca momenty dla geom(theta)
M <- function(t, theta){
(theta*exp(t))/(1 - (1-theta)*exp(t))
}
# potrzebujemy pochodnych funkcji tworzacej momenty w 0

# pierwsza pochodna
M1 <- function(t, theta){
 (theta*exp(t))/((theta - 1)*exp(t) +1)^2
}
# druga pochodna
M2 <- function(t, theta){
 (theta*exp(t))/((theta - 1)*exp(t) +1)^2 - (2*(theta -1)*theta*exp(2*t)) / ((theta - 1)*exp(t) + 1)^3
}

# trzecia pochodna
M3 <- function(t,theta){
(((6*theta*(theta-1)^2)*exp(3*t))/((theta-1)*exp(t)+1)^4 - (6*theta*(theta-1)*exp(2*t))/((theta-1)*exp(t)+1)^3 + (theta*exp(t))/((theta-1)*exp(t)+1)^2)
}
EX <- M1(0,theta1)
EX2 <- M2(0,theta1)
EX3 <- M3(0,theta1)

# wersja 2: rozne thety
EX <- M1(0,theta1/3 + 2*theta2/3)
EX2 <- M2(0,theta1/3 + 2*theta2/3)
EX3 <- M3(0,theta1/3 + 2*theta2/3)


#alfa = (4*(lambda1 + lambda2)* (EX2)^3) /(EX3)^2
alfa = (4*lambda*(EX2)^3) /(EX3)^2
beta = (2*EX2)/EX3
x0 = lambda*EX - (2*lambda*EX2)

#alfa = 148.31543
#beta = 3.6309219
#x0 = -36.34787123

# czy aby nie powinny byc na odwrot alfa i beta???
gestosc <- function(x,alfa,beta){
  ((exp(-beta*x)*x^(alfa -1))*beta^alfa)/(gamma(alfa))
}
xn <- seq(0, 0.14, 0.01)
fg <- c()
fg1 <- c()
for(i in 1:length(xn)){
  fg[i] <- gestosc(xn[i],alfa,beta) 
  fg1[i] <- pgamma(xn[i],alfa,beta) 
}
lines(xn,fg)
plot(xn, fg)

#--------------------------------------------------------
# co� jest nie tak, policzy� to 