(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      7660,        236]
NotebookOptionsPosition[      7418,        223]
NotebookOutlinePosition[      7759,        238]
CellTagsIndexPosition[      7716,        235]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"N", "[", 
          RowBox[{"Pi", ",", "7"}], "]"}], "\n", "3.141593", "\n", 
         RowBox[{"N", "[", 
          RowBox[{"E", ",", "7"}], "]"}], "\n", "2.718282", "\n", 
         RowBox[{"N", "[", 
          RowBox[{
           RowBox[{"Sin", "[", "1.5", "]"}], ",", "7"}], "]"}], "\n", 
         "0.997495", "\n", 
         RowBox[{"N", "[", 
          RowBox[{
           RowBox[{"Tan", "[", 
            RowBox[{"5", "/", "3"}], "]"}], ",", "9"}], "]"}], "\n", 
         RowBox[{"-", "10.3987783"}], "\n", 
         RowBox[{"Solve", "[", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"-", "4"}], " ", 
             RowBox[{"x", "^", "2"}]}], "-", 
            RowBox[{"125", "*", "x"}], "+", 
            RowBox[{"1", " ", "\:f22e", " ", "0"}]}], ",", "x"}], "]"}], "\n", 
         RowBox[{
          RowBox[{"::", "x"}], "\[Rule]", "1"}], "\n", "8", "\n", 
         RowBox[{
          RowBox[{
           RowBox[{"J", "-", "125", "-", 
            RowBox[{"15", " ", "641", " ", "N"}]}], ">"}], ",", " ", 
          RowBox[{
           RowBox[{":", "x"}], "\[Rule]", "2"}]}], "\n", 
         RowBox[{"125", "+", 
          RowBox[{"15", " ", "641"}]}], "\n", 
         RowBox[{
          RowBox[{">>", 
           RowBox[{"Solve[x^2", "+", 
            RowBox[{"5", "*", "x"}], "+", 
            RowBox[{"3", " ", "\:f22e", " ", "0"}]}]}], ",", "x"}]}], "]"}], 
       "\n", 
       RowBox[{
        RowBox[{"::", "x"}], "\[Rule]", "1"}], "\n", "2", "\n", 
       RowBox[{
        RowBox[{
         RowBox[{"J", "-", "5", "-", 
          RowBox[{"13", " ", "N"}]}], ">"}], ",", " ", 
        RowBox[{
         RowBox[{":", "x"}], "\[Rule]", "1"}]}], "\n", "2", "\n", 
       RowBox[{
        RowBox[{"J", "-", "5", "+", 
         RowBox[{"13", " ", "N"}]}], ">>", 
        RowBox[{"Solve[(1", "+", "\[ImaginaryI]"}]}]}], ")"}], " ", 
     RowBox[{"x", "^", "2"}]}], "+", 
    RowBox[{"12", " ", "\[ImaginaryI]", " ", "\:f22e", " ", "0"}]}], ",", 
   "x"}], "]"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"::", "x"}], "\[Rule]", 
   RowBox[{
    RowBox[{
     RowBox[{"-", 
      RowBox[{"-", "6"}]}], "-", 
     RowBox[{"6", " ", "\[ImaginaryI]"}]}], ">"}]}], ",", " ", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{":", "x"}], "\[Rule]", 
     RowBox[{
      RowBox[{"-", "6"}], "-", 
      RowBox[{"6", " ", "\[ImaginaryI]"}]}]}], ">>", 
    RowBox[{"r", "=", 
     RowBox[{"735", "/", "100"}]}]}], ";"}]}], "\n", 
 RowBox[{
  RowBox[{"P", "=", 
   RowBox[{"4", " ", "Pi", "*", 
    RowBox[{"r", "^", "2"}]}]}], ";"}], "\n", 
 RowBox[{"N", "[", 
  RowBox[{"P", ",", "13"}], "]"}], "\n", "678.8667565142", "\n", 
 RowBox[{
  RowBox[{"r", "=", 
   RowBox[{"735", "/", "100"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"V", "=", 
   RowBox[{
    RowBox[{"4", "/", "3"}], " ", "Pi", "*", 
    RowBox[{"r", "^", "3"}]}]}], ";"}], "\n", 
 RowBox[{"N", "[", 
  RowBox[{"V", ",", "14"}], "]"}], "\n", "1663.2235534598", "\n", 
 RowBox[{
  RowBox[{"r", "=", 
   RowBox[{"7", "/", "2"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"h", "=", 
   RowBox[{"29", "/", "2"}]}], ";"}], "\n", 
 RowBox[{"l", "=", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"7", "/", "2"}], ")"}], "^", "2"}], "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"29", "/", "2"}], ")"}], "^", "2"}]}], 
   "]"}]}], "\n", "445", "\n", "2", "\n", 
 RowBox[{"V", "=", 
  RowBox[{
   RowBox[{"1", "/", "3"}], "*", "Pi", "*", 
   RowBox[{"r", "^", "2"}], "*", "h"}]}], "\n", 
 RowBox[{"1421", " ", "\[Pi]"}], "\n", "24", "\n", 
 RowBox[{"Pb", "=", 
  RowBox[{
  "Pi", "*", "r", "*", 
   "l"}]}], "\n", "7", "\n", "2", "\n", "445", "\n", "2", "\n", 
 RowBox[{"\[Pi]", ";"}], "\n", 
 RowBox[{
  RowBox[{"R", "=", "2"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"r", "=", 
   RowBox[{"6", "/", "5"}]}], ";"}], "\n", 
 RowBox[{"V", "=", 
  RowBox[{
   RowBox[{"Pi", "/", "6"}], "*", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"4", "*", "Pi", "*", 
      RowBox[{"r", "^", "2"}]}], ")"}], "^", "3"}]}]}], "\n", 
 RowBox[{"497", " ", "664", " ", "\[Pi]4"}], "\n", 
 RowBox[{
  RowBox[{"15", " ", "625"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{"pole", " ", "powierzchni", " ", "bocznej"}], "*)"}], "\n", 
  RowBox[{"(*", 
   RowBox[{"pole", " ", "powierzchni", " ", "bocznej"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"Pi", ",", "Integers"}], "]"}], "]"}], "\n", "False", "\n", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"Pi", ",", "Rationals"}], "]"}], "]"}], "\n", 
 RowBox[{"False", "\n", 
  RowBox[{"(*", 
   RowBox[{"liczby", " ", "przestOEpne"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"Pi", ",", "Reals"}], "]"}], "]"}], "\n", "True", "\n", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"Pi", ",", "Complexes"}], "]"}], "]"}], "\n", "False", "\n", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"E", ",", "Integers"}], "]"}], "]"}], "\n", "False", "\n", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"E", ",", "Rationals"}], "]"}], "]"}], "\n", 
 RowBox[{"False", "\n", 
  RowBox[{"(*", 
   RowBox[{"liczby", " ", "przestOEpne"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"E", ",", "Reals"}], "]"}], "]"}], "\n", "True", "\n", 
 RowBox[{"2", " ", "Laboratoria", " ", "1.", "nb"}], "\n", 
 RowBox[{"TrueQ", "[", 
  RowBox[{"Element", "[", 
   RowBox[{"E", ",", "Complexes"}], "]"}], "]"}], "\n", "False", "\n", 
 RowBox[{"TrueQ", "[", 
  RowBox[{
   RowBox[{"Sqrt", "[", "2", "]"}], "<", 
   RowBox[{"Log", "[", 
    RowBox[{"2", ",", "3"}], "]"}]}], "]"}], "\n", "True", "\n", 
 RowBox[{
  RowBox[{"TrueQ", "[", 
   RowBox[{
    RowBox[{"[", 
     RowBox[{"A", "\[VerticalSeparator]", "\[VerticalSeparator]", "B"}], 
     "]"}], "&&", 
    RowBox[{"[", 
     RowBox[{
      RowBox[{"!", "A"}], "\[VerticalSeparator]", "\[VerticalSeparator]", 
      RowBox[{"!", "B"}]}], "]"}]}], "]"}], "\n", 
  RowBox[{"(*", "wektory", "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"v", "=", 
  RowBox[{"{", 
   RowBox[{"1", ",", "2", ",", "3", ",", "4"}], "}"}]}], "\n", 
 RowBox[{"A", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"2", ",", "3", ",", "4"}], "}"}]}], "}"}]}], "\n", 
 RowBox[{"v", "//", "MatrixForm"}], "\n", 
 RowBox[{"MatrixForm", "[", "v", "]"}], "\n", 
 RowBox[{"A", "//", "MatrixForm"}], "\n", "A", "\n", 
 RowBox[{"Laboratoria", " ", "1.", "nb", " ", "3"}]}], "Input",
 CellChangeTimes->{{3.750922703879006*^9, 3.750922703879006*^9}}]
},
WindowSize->{716, 781},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"8.0 for Microsoft Windows (64-bit) (October 6, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 6857, 201, 1572, "Input"]
}
]
*)

(* End of internal cache information *)

