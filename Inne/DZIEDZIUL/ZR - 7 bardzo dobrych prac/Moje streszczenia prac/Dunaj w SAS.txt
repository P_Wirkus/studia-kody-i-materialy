Temat: Ryzyko inwestycji na rynku kawy i kakao. Optymalny portfel ze względu na VAR i ES.
Cel pracy: przedstawienie sposobów mierzenia ryzyka oraz dobór odpowiedniej dywersykacji portfela inwestycyjnego,
           przy której ryzyko inwestycji będzie minimalne


Rozdział I - "Analiza danych" ; 
             cel: opis przedmiotów inwestycji, notowania surowców na otwarcie i zamnknięcie miesiąca, stopy strat dla aktyw, statystyki opisowe i analiza rozkładu

UWAGA: Notowania giełdowe spółki można ściągnąć ze strony Bankier.pl, potem wyciąć wycinarką i mamy, 
       np. link: https://www.bankier.pl/inwestowanie/profile/quote.html?symbol=KAWA
       Można też w Excelu stworzyć wykres z interwałem miesięcznym, najeżdżając na wartość w każdym miesiącu i ręcznie wklepać wartości.
Stopy strat - na ich podstawie oblicza się ryzyko inwestycji
            - wzór: L = (W_o - W_z)/W_o * 100% ; wynik może być ujemny lub dodatni
            - "-" wartość oznacza ZYSK w danym miesiącu
            - "+" wartość oznacza STRATĘ w danym miesiącu
Statystyka opisowa - jeden z pierwszych kroków do podjęcia analizy zebranych danych
                   - ocena ryzyka inwestycji rozpoczyna się od wyznaczenia charakterystyk liczbowych dla stóp strat
                   - ozn.: X - badana cecha populacji i EX < oo


Rozdział II - "Ryzyko inwestycji" ; 


              cel: opis własności koherentnych miar ryzyka wraz z opisem VAR i ES
Rozdział III - "Kopuły" ; 


               cel: teoria o f-cji łączącej, tw. używane przy analizie inwestycji łącznej, podstawowy podział kopuł
Rozdział IV - "Portfele inwestycyjne" ; 
               cel: analiza inwestycji pojedynczych i łącznych, wyznaczanie VAR i ES dla portfeli 1-składnikowych i dla inwestycji łącznych,
                    obliczanie ryzyka dla portfeli zdywersyfikowanych, porównanie wybranych portfeli w celu znalezienia tego o najmniejszym ryzyku
