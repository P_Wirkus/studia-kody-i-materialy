data microsoft;
set stopy;
keep microsoft;
run;
/* Gumbell */
data microsoft1;
n=50;
k=1000;
do j=1 to n;
a=0;
do i = 1 to k;
b=rand('exponential');
if b>a then a=b;
c=a;
end;
max=c;
output;
end;
run;
title ’Limit distribution ’;
proc univariate data=microsoft1;
var max;
histogram / gumbel
;
run;
/* Frechet */
data microsoft2;
n=500;
k=1000;
do j=1 to n;
a=0;
do i = 1 to k;
b=rand('F',4,3);
if b>a then a=b;
c=a;
end;
max=1/c;
output;
end;
run;
title ’Limit distribution ’;
proc univariate data=microsoft2;
var max;
histogram / weibull
;
run;
/* Weibull */
data microsoft3;
n=5000;
k=10000;
do j=1 to n;
a=0;
do i = 1 to k;
b=rand('uniform');
if b>a then a=b;
c=a;
end;
max=c;
maksimum=max-1;
odwrotne=-maksimum;
output;
end;
run;
title ’Limit distribution ’;
proc univariate data=microsoft3;
var odwrotne;
histogram / weibull
;
run;
/* Ogony */
data microsoft4;
n=1000;
u=3;
do i=1 to n;
wsk=0;
b=rand('exponential')-u;
if b>0 then wsk=1 ;
if wsk=1 then output ;
end;
proc severity data=microsoft4 obj=cvmobj print=all plots=pp;
loss b;
dist BURR EXP GAMMA GPD IGAUSS LOGN PARETO
WEILBULL ;
cvmobj = _cdf_(b);
cvmobj = (cvmobj -_edf_(b))**2;
run;