%%%%% KOD 1 - SAS %%%%% 
% Kod użyty do wyrysowania wykresów powierzchniowych i poziomicowych kopuł W, PI oraz M.

data kopuly;
krok=0.01;
do u = 0 to 1 by krok ;
    do v = 0 to 1 by krok;
      W = max(u+v-1,0);
      I = u*v;
      M = min(u,v);
      output;
   end;
end;
drop  krok;
run;

/* wykresy powierzchniowe */
title "Kopuła W";
proc g3d data=kopuly;
   plot v*u=W/
	side
	ctop=black
	rotate=25
	caxis=black;
run;
title "Kopuła I";
proc g3d data=kopuly;
   plot v*u=I/
	side
	ctop=black
	rotate=25
	caxis=black;
run;
title "Kopuła M";
proc g3d data=kopuly;
   plot v*u=M/
	side
	ctop=black
	rotate=25
	caxis=black;
run;

/* kod tworzący wykresy powierzchniowe i poziomicowe kopuł W, Pi, M */
axis1 length =48;
axis2 length =30;
proc gcontour data=kopuly;
    plot v*u=W/
		clevels=black black black black black black
		levels=0.01 to 1 by 0.2
		caxis=black
		haxis=axis1
		vaxis=axis2;
run;

proc gcontour data=kopuly;
    plot v*u=I/
		clevels=black black black black black black
		levels=0.01 to 1 by 0.2
		caxis=black
		haxis=axis1
		vaxis=axis2;
run;
proc gcontour data=kopuly;
    plot v*u=M/
		clevels=black black black black black black
		levels=0.01 to 1 by 0.2
		caxis=black
		haxis=axis1
		vaxis=axis2;
run;

%%%%% KOD 2 - SAS %%%%% 
% Kod użyty do zdefiniowania własnych funkcji wyliczających wartości funkcji pochodnych cząstkowych dla kopuły Gaussa, Claytona oraz Gumbela.

/* definiowanie funkcji */
options cmplib=sasuser.funcs;
proc fcmp outlib=sasuser.funcs.math;
	function Gauss(u,v,t);
		wynik=cdf("Normal", (quantile("Normal", v) - t*quantile("Normal", u))/(sqrt(1-t**2)));
		return(wynik);
	endsub;
	function Clayton(u,v,t);
		wynik=(u**(-t-1))*((u**(-t)+v**(-t)-1)**(-1/t-1));
		return(wynik);
	endsub;
	function Gumbel(u,v,t);
		wynik=1/u*((-log(u))**(t-1))*(((-log(u))**t+(-log(v))**t)**(1/t-1))*(exp(-((-log(u))**t+(-log(v))**t)**(1/t)));
		return(wynik);
	endsub;
run;

%%%%% KOD 3 - SAS %%%%% 
%  Kod użyty wyliczenia rozkładu warunkowego zmiennej losowej Y|X=x, gdy x=1200.

data warunkowa;
x=cdf('Gamma',1200,100/9,90);
do Tau=0,0.1,0.3,0.5;
if Tau=0 then t=0.00001;
else t=2*Tau/(1-Tau);
f_y1=0;
f_y2=0;
l=2.5; /*lambda_poissona*/
do y=1 to 12;
	f_y2=f_y2+(exp(-l)*l**y)/(fact(y)*(1-exp(-l)));
	z=x**(-t-1);
	if y=1 then do
		P_y1=Clayton(x,f_y2,t);
		f_y1=f_y2;
		output;
	end;
	else do;
		P_y1=Clayton(x,f_y2,t)-Clayton(x,f_y1,t);
		f_y1=f_y2;
		output;
	end;
end;
end;
label t='Theta';
drop x z f_y1 f_y2 l;
run;

title 'Kopuła Claytona';
proc sgplot data=warunkowa;
yaxis label="Prawdopodbieństwo" grid;
xaxis label="Liczba szkód";
vbar y / response=P_y1 stat=sum group=Tau groupdisplay=cluster;
run;

%%%%% KOD 4 - SAS %%%%% 
%  Kod użyty wyliczenia warunkowej funkcji prawdopodobieństwa Y|X=1200 dla kopuły Gaussa, Claytona oraz Gumbela dla ustalonego współczynnika tau Kendall'a \tau=0.3.

data warunkowa2;
x=cdf('Gamma',1200,100/9,90);
pi=constant("pi");
t=0.3; /*tau=0.3*/
t_Ga=sin(pi*t/2); /* theta kopuły Gaussa */
t_C=2*t/(1-t); /* theta kopuły Claytona */
t_G=1/(1-t); /* thera kopuły Gumbela */
f_y1=0;
f_y2=0;
l=2.5; /*lambda poissona*/
do y=1 to 12;
	f_y2=f_y2+(exp(-l)*l**y)/(fact(y)*(1-exp(-l)));
	if y=1 then do
		P_yGa=Gauss(x,f_y2,t_Ga);
		P_yC=Clayton(x,f_y2,t_C);
		P_yG=Gumbel(x,f_y2,t_G);
		f_y1=f_y2;
		output;
	end;
	else do;
		P_yGa=Gauss(x,f_y2,t_Ga)-Gauss(x,f_y1,t_Ga);
		P_yC=Clayton(x,f_y2,t_C)-Clayton(x,f_y1,t_C);
		P_yG=Gumbel(x,f_y2,t_G)-Gumbel(x,f_y1,t_G);
		f_y1=f_y2;
		output;
	end;
end;
label t='Tau - Kendala';
drop x pi l t f_y1 f_y2;
run;

data warunkowa3;
length kopula $10;
set warunkowa2 (in=zmienna1) warunkowa2 (in=zmienna2) warunkowa2 (in=zmienna3);
zm1=zmienna1;
zm2=zmienna2;
zm3=zmienna3;
if zm1=1 then do
	P=P_yGa;
	Theta=t_Ga;
	kopula="Gauss";
end;
if zm2=1 then do
	P=P_yC;
	Theta=t_C;
	kopula="Clayton";
end;
if zm3=1 then do
	P=P_yG;
	Theta=t_G;
	kopula="Gumbel";
end;
format kopula $10.;
drop t_Ga t_G t_C P_yGa P_yC P_yG zm1 zm2 zm3;
run;


title 'Porównanie kopuł dla tau=0.3';
proc sgplot data=warunkowa3;
yaxis label="Prawdopodbieństwo" grid;
xaxis label="Liczba szkód";
vbar y / response=P stat=sum group=kopula groupdisplay=cluster;
run;

%%%%% KOD 5 - SAS %%%%% 
%  Kod użyty do wyliczenia funkcji gęstości łącznej szkody z polisy dla kopuły Gaussa, Claytona oraz Gumbela i dla 3 różnych wartościach parametru tau Kendall'a równych 0.1, 0.3, 0.5.

data Lgauss;
pi=constant("pi");
do tau=0.1, 0.3, 0.5; 
t_Ga=sin(pi*tau/2); /*Theta*/
l_poiss=2.5;  /*lambda poissona*/
do i=100 to 4700 by 10;
f_y1=0;
f_y2=0;
f_policy=0;
do y=1 to 11;
	/*zmienne pomocnicze*/
	x=cdf('Gamma',i/y,100/9,90);
	xx=pdf('Gamma',i/y, 100/9,90);
	f_y2=f_y2+(exp(-l_poiss)*l_poiss**y)/(fact(y)*(1-exp(-l_poiss)));
	if y=1 then do
		f=xx/y*Gauss(x,f_y2,t_Ga);
		f_y1=f_y2;	
	end;
	else do;
		f=xx/y*(Gauss(x,f_y2,t_Ga)-Gauss(x,f_y1,t_Ga));
		f_y1=f_y2;	
	end;
	f_policy=f_policy+f;
end;
output;
end;
end;
run;

title 'Kopuła Gaussa';
proc sgplot data= Lgauss;
yaxis label="Gęstość" max=.00065;
xaxis label="L";
series x=i y=f_policy/group=tau;
run;

data Lclayton;
do tau=0.1, 0.3, 0.5; 
t_C=2*tau/(1-tau); /*Theta*/
l_poiss=2.5;  /*lambda poissona*/
do i=100 to 4700 by 10;
f_y1=0;
f_y2=0;
f_policy=0;
do y=1 to 11;
	/*zmienne pomocnicze*/
	x=cdf('Gamma',i/y,100/9,90);
	xx=pdf('Gamma',i/y, 100/9,90);
	f_y2=f_y2+(exp(-l_poiss)*l_poiss**y)/(fact(y)*(1-exp(-l_poiss)));
	if y=1 then do
		f=xx/y*Clayton(x,f_y2,t_C);
		f_y1=f_y2;	
	end;
	else do;
		f=xx/y*(Clayton(x,f_y2,t_C)-Clayton(x,f_y1,t_C));
		f_y1=f_y2;	
	end;
	f_policy=f_policy+f;
end;
output;
end;
end;
run;

title 'Kopuła Claytona';
proc sgplot data= Lclayton;
yaxis label="Gęstość" max=.00065;
xaxis label="L";
series x=i y=f_policy/group=tau;
run;

data Lgumbel;
do tau=0.1, 0.3, 0.5; 
t_G=1/(1-tau); /*Theta*/
l_poiss=2.5;  /*lambda poissona*/
do i=100 to 4700 by 10;
f_y1=0;
f_y2=0;
f_policy=0;
do y=1 to 11;
	/*zmienne pomocnicze*/
	x=cdf('Gamma',i/y,100/9,90);
	xx=pdf('Gamma',i/y, 100/9,90);
	f_y2=f_y2+(exp(-l_poiss)*l_poiss**y)/(fact(y)*(1-exp(-l_poiss)));
	if y=1 then do
		f=xx/y*Gumbel(x,f_y2,t_G);
		f_y1=f_y2;	
	end;
	else do;
		f=xx/y*(Gumbel(x,f_y2,t_G)-Gumbel(x,f_y1,t_G));
		f_y1=f_y2;	
	end;
	f_policy=f_policy+f;
end;
output;
end;
end;
run;

title 'Kopuła Gumbela';
proc sgplot data= Lgumbel;
yaxis label="Gęstość" max=.00065;
xaxis label="L";
series x=i y=f_policy/group=tau;
run;

%%%%% KOD 6 - SAS %%%%% 
%  Symulacja 1000 danych z rozkładu Gamma oraz ZTP oraz oszacowanie gęstości łącznej szkody z polisy L (procedura proc kde) przy założeniu niezależności między średnią wartością szkody X oraz liczby szkód Y.

data niezalezny;
do i=1 to 1000;
x=RAND('GAMMA', 100/9, 90);
wejscie=1;
do while(wejscie=1);
	y=RAND('POISSON',2.5);
	if (y<>0) then do;
		L=x*y;
		output;
		wejscie=0;
	end;
end;
end;
run;

proc kde data=niezalezny;
   univar L/ plots=(density);
run;


%%%%% KOD 7 - R %%%%% 
%   Kod użyty do wyznaczenia wartości oczekiwanych łącznej szkody z polisy L dla tau=0.1,0.3,0.5 w programie R.
%   wymaga instalacji następujących pakietów

install.packages("VineCopula")
install.packages("CopulaRegression")
install.packages("MASS") 
install.packages("readxl")
library(VineCopula)
library(CopulaRegression)
library(MASS)
library(readxl)

mu=1000
delta=9/100
lambda=2.5
tau=c(0.1,0.3,0.5)
theta_gaus=c()
theta_cla=c()
theta_gu=c()
for (i in 1:3){
  theta_gaus= c(theta_gaus,BiCopTau2Par(tau=tau[i],family=1))
  theta_cla= c(theta_cla, BiCopTau2Par(tau=tau[i],family=3))
  theta_gu= c(theta_gu, BiCopTau2Par(tau=tau[i],family=4))
}
w_ga=c()
w_cl=c()
w_gu=c()
for (i in 1:3){
w_ga=c(w_ga, epolicy_loss(mu, delta, lambda, theta_gaus[i], 1, y.max = 300,zt=TRUE,compute.var=FALSE)$mean)
w_cl=c(w_cl, epolicy_loss(mu, delta, lambda, theta_cla[i], 3, y.max = 300,zt=TRUE,compute.var=FALSE)$mean)
w_gu=c(w_gu, epolicy_loss(mu, delta, lambda, theta_gu[i], 4, y.max = 300,zt=TRUE,compute.var=FALSE)$mean) 
}
w_ga
w_cl
w_gu

%%%%% KOD 8 - SAS %%%%% 
%  GLM dla średniej wartości szkody (regresja Gamma) oraz GLM dla liczby szkód (regresja Poissona). Stworzono osobne modele regresji za pomocą procedury proc genmod programu SAS.

PROC IMPORT OUT = car
DATAFILE = "C:\Users\Sysia\Desktop\car.xlsx"
DBMS = xlsx REPLACE;
sheet="car";
getnames=yes;
RUN ;

/* poziomy odniesienia */
proc freq data=car;
tables gender agecat area veh_age/ nocum;
run;

/* regresja gamma */
proc Genmod data=car;
class gender (ref='F') agecat (ref='4') area (ref='C');
Model claimcst0= gender agecat area / dist=gamma link=log ;
output out=fitted p=pred;

/* regresja poissona */
proc Genmod data=car;
class gender (ref='F') agecat (ref='4') area (ref='C');
Model numclaims= gender agecat  area / dist=poi link=log ;
output out=fitted p=pred;
run;
run;


%%%%% KOD 9 - R %%%%% 
%  Model regresji opartej na kopule Gaussa, gdzie brzegowe modele regresji to: regresja Gamma dla średniej wartości szkody i regresja ZTP dla liczby szkód. Model stworzony za pomocą funkcji copreg w programie R.

#dane wejściowe
dane<- data.matrix(read_xlsx("/Users/Sysia/Desktop/Rzeczy/STUDIA MAG/Praca dyplomowa/dane/car.xlsx", sheet = "cars", range = "A2:M4625", col_names = FALSE,
                             col_types = NULL, na = "", trim_ws = TRUE, skip = 0, n_max = Inf,
                             guess_max = 4625))
n<-length(dane[,1])
y<-dane[,1]
x<-dane[,2]
exposure<-rep(1,n)
R<-S<-cbind(rep(1,n),dane[,3:13])

#model
model_Gaus<-copreg(x,y,R,S,family=1,exposure,sd.error=TRUE,joint=TRUE,zt=TRUE)
model_Gaus

#statystka Walda i p-value
alfy0=model_Gaus$alpha0
bety0=model_Gaus$beta0
stalfy0=model_Gaus$sd.alpha0
stbety0=model_Gaus$sd.beta0
alfy=model_Gaus$alpha
bety=model_Gaus$beta
stalfy=model_Gaus$sd.alpha
stbety=model_Gaus$sd.beta
walda0=c() #statystyka Walda a0
waldb0=c() #statystyka Walda b0
walda=c() #statystyka Walda a
waldb=c() #statystyka Walda b
for (i in 1:12){
    walda0=c(walda0,(alfy0[i])^2/(stalfy0[i])^2)
    waldb0=c(waldb0,(bety0[i])^2/(stbety0[i])^2)
    walda=c(walda,(alfy[i])^2/(stalfy[i])^2)
    waldb=c(waldb,(bety[i])^2/(stbety[i])^2)
}
pa0=c() #pvalue a0
pb0=c() #pvalue b0
pa=c() #pvalue a
pb=c() #pvalue b
for (i in 1:12){
    pa0=c(pa0,1-pchisq(walda0[i],1))
    pa=c(pa,1-pchisq(walda[i],1))
    pb0=c(pb0,1-pchisq(waldb0[i],1))
    pb=c(pb,1-pchisq(waldb[i],1))
}
