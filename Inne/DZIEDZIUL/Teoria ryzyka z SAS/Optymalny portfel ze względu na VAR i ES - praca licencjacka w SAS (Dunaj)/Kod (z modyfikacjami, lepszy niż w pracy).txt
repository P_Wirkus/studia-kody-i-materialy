
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KOD 1.
Kod do wyliczenia stop strat kakao i kawy.
/* tworzymy plik tymczasowy importujac dane z dowolnego arkusza
 kalkulacyjnego wpisujemy nazwe pliku, sciezke i nazwe arkusza*/
 
 
proc import out=kawa                                                                                                             
 datafile="/home/karol/Dokumenty/dydaktyka/prace studentow/bootstrapDunaj/kawa.xlsx"
dbms=xlsx Replace;
sheet="Arkusz3";
getnames=yes;
run;

proc import out=kakao                                                                                                             
 datafile="/home/karol/Dokumenty/dydaktyka/prace studentow/bootstrapDunaj/kakao.xlsx"
dbms=xlsx Replace;
sheet="Arkusz1";
getnames=yes;
run;

                                                                                                                                       data kakao1;                                                                                                                      
set kakao;                                                                                                                        
L_kakao=round(((kakao_o-kakao_z)/kakao_o)*100,0.0001);                                                                                  
run;

                                                                                                                                         
data kawa1;                                                                                                                        
set kawa;                                                                                                                          
L_kawa=round(((kawa_o-kawa_z)/kawa_o)*100,0.0001);                                                                                       
run;                                                                                                                                     
                                                                                                                                  
                                                                                                                                  
                                                                                                                                  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KOD 2.
Kod generuj?cy statystyki opisowe, wyniki dopasowania rozk?adów, 
histogramy oraz warto?ci kwantyli dla stop strat kakao i kawy.


                                                                                                                                        
proc univariate data=kakao1;                                                                                                      
var L_kakao;                                                                                                                            
histogram L_kakao/normal;                                                                                                               
run; 

proc univariate data=kawa1;                                                                                                        
var L_kawa;                                                                                                                              
histogram L_kawa/normal;                                                                                                                 
run;

ZADANIE 1: Wyznaczyć rozkład dla danych kawy


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KOD 2.5

For your convenience, PROC SEVERITY provides the following 
10 different predefined distributions (the name in 
the parentheses is the name to use in the DIST statement): 
Burr (BURR), exponential (EXP), gamma (GAMMA), generalized 
Pareto (GPD), inverse Gaussian or Wald (IGAUSS), lognormal (LOGN), 
Pareto (PARETO), Tweedie (TWEEDIE), scaled Tweedie (STWEEDIE), 
and Weibull (WEIBULL). 
Cramér-von Mises estimator of the parameters 

data kakao2;
set kakao1;
L_kakao=L_kakao+100;
run;

proc severity data=kakao2 obj=cvmobj print=all plots=pp;
loss L_kakao;
dist BURR EXP GAMMA GPD IGAUSS LOGN PARETO TWEEDIE STWEEDIE WEILBULL;
cvmobj = _cdf_(L_kakao);
   cvmobj = (cvmobj -_edf_(L_kakao))**2;
run;
                                                                                                                                     
                                                                                                                                     
                                                                                                                                     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KOD 3.
Kod s?u??cy do wyliczenia warto?ci Expected Shortfall dla stop strat kakao i kawy.                                                                                                                                   

proc sort data=kakao1 out=kakao_sort;                                                                                        
by descending L_kakao;                                                                                                                   
proc means data=kakao_sort (obs=3) mean ;                                                                                          
var L_kakao;                                                                                                                                                                                                                               
run;

                                                                                                                                    proc sort data=kawa1 out=kawa_sort;                                                                                          
by descending L_kawa;                                                                                                                    
proc means data=kawa_sort (obs=3) mean ;                                                                                           
var L_kawa;                                                                                                                              
run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 KOD Wielowymiarowy rozkład normalny


data cork;
  merge kawa1 kakao1 ;
  run;

proc iml;
  * Read data into IML ;
  use cork;
  read all ;
  * combine L_kawa L_kakao into a matrix X ;
  y = L_kawa || L_kakao;

  print y;
  n = nrow(y) ;
  p = ncol(y) ;
  dfchi = p*(p+1)*(p+2)/6 ;
  q = i(n) - (1/n)*j(n,n,1);
s = (1/(n))*t(y)*q*y ; s_inv = inv(s) ;
g_matrix = q*y*s_inv*t(y)*q;
beta1hat = ( sum(g_matrix#g_matrix#g_matrix) )/(n*n);
beta2hat =trace( g_matrix#g_matrix )/n ;
kappa1 = n*beta1hat/6 ;
kappa2 = (beta2hat - p*(p+2) ) / sqrt(8*p*(p+2)/n) ;
pvalskew = 1 - probchi(kappa1,dfchi) ;
pvalkurt = 2*( 1 - probnorm(abs(kappa2)) );
print s ;
print s_inv ;
print
"TESTS:";
print "Based on skewness:" beta1hat kappa1 pvalskew ;
print " Based on kurtosis" beta2hat kappa2 pvalkurt;
run;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Kod u?yty do estymacji g?sto?ci
 wektora losowego L = ( L_kawa, L_kakao)
 
title " Estymacja gestosci" ;
ods graphics on ;
proc kde data =cork;
title
’ Wykresy kde ’ ;
bivar L_kawa L_kakao / plots =all ;
run ;


%%%%%%%%%%%%%%%%%%%%%%
 Korzystaj?c z obliczonej macierz korelacji użyj kodu z wyk?adu do generacji 
danych ze znanego rozk?adu normalnego do wygenerownia 500 danych z rozk?adu normalnego 
dla L = ( L_kawa, L_kakao). Nastepnie zmodyfikuj kod Makro(poniżej) do wyznaczenia Metod? 
Monte Carlo portfela optymalnego (wskazówka zamiast set transformacja wstawić set newdata 
oraz L=&a*col1+(1-&a)*col2.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
zadanie 2: Narysowa? funkcj? VaR_{0.95} L(a)dla a z odcinka [0,1]

Wyznaczy? portfel optymalny metod? analityczn?, patrz wyklad rozdzial 2.1 czyli
skorzysta? z programu

data VaRt;

   a=0;
   delta=0.01;
   quan =quantile('NORMAL',0.95); 
   output;

       do i = 0 to 100;
	      VaR=(-0.7388)*a+(-0.1666)*(1-a)+sqrt(97.82*a**2+ 2*23.61*(1-a)*a+50.53*(1-a)**2)*quan;
	     a =a+delta;
	    output;
	   end;
   run;


   Symbol value=none interpol=sms line=1  width=2;
   title"Trajectory VaRt";
   proc gplot data=VaRt;
   plot VaR*a;
   run;
   
   Porfele rynkowe podobieństwo do metody Markowitza
   
   data market;
   a=0;
   delta=0.01;
   quan =quantile('NORMAL',0.95); 
   output;

       do i = 1 to 100;
	      VaR=(-0.7388)*a+(-0.1666)*(1-a)+sqrt(97.82*a**2+ 2*23.61*(1-a)*a+50.53*(1-a)**2)*quan;
	     a =a+delta;
	      nu=(-0.7388)*a+(-0.1666)*(1-a);
	    output;
	   end;
   run;


   Symbol value=none interpol=join line=1  width=2;
   title"Trajectory market E-Var";
   proc gplot data=market;
   plot nu*VaR;
   run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
KOD .
Programy dopasowuj?ce kopu?y do danych stop strat kakao i kawy.

/*tworzenie nowego zbioru poprzez połączenie zbiorów kakao1 i kawa1 
oraz pozostawienie tylko zmiennych L_kakao i L_kawa*/               
data laczna;                                                                                                                      
set kakao1 (keep=L_kakao);                                                                                                        
set kawa1 (keep=L_kawa);                                                                                                          
run; 

proc copula data=laczna;                                                                                                          
title "Kopuła Gumbela";                                                                                                                 
var L_kakao L_kawa;                                                                                                                     
fit gumbel/outcopula=gumbel_parametry;                                                                                            
run;                                                                                                                                    
                                                                                                                                        
proc copula data=laczna;                                                                                                          
title "Kopuła Franka";                                                                                                                  
var L_kakao L_kawa;                                                                                                                     
fit frank/outcopula=frank_parametry;                                                                                              
run;                                                                                                                                    
                                                                                                                                        
proc copula data=laczna;                                                                                                          
title "Kopuła Claytona";                                                                                                                
var L_kakao L_kawa;                                                                                                                     
fit clayton/outcopula=clayton_parametry;                                                                                          
run;
%%%%%%%%%%%%%%%%%%%

Zadania3.  sprawdzi? dopasowanie do kopu?y t- studenta oraz kopu?y normalnej.
zastosowa? metod? CAL (opcja dla fit statement)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KOD 5.
Programy u?yte do symulacji danych z kopu?y.

proc copula;                                                                                                                            
title "Kopuła Gumbela";                                                                                                                 
var u v;                                                                                                                                
define g_cop gumbel (theta=1.306482);                                                                                                   
simulate g_cop/seed=1234                                                                                                                
ndraws=1000                                                                                                                             
outuniform=gumbel_jednorodne                                                                                                      
plots= (datatype=UNIFORM distribution=CDF);                                                                                             
run;



proc copula;                                                                                                                            
title "Kopuła Franka";                                                                                                                  
var u v;                                                                                                                                
define f_cop frank (theta=2.200958);                                                                                                    
simulate f_cop/seed=1234                                                                                                                
ndraws=1000                                                                                                                             
outuniform=frank_jednorodne                                                                                                       
plots= (datatype=UNIFORM distribution=CDF);                                                                                             
run;

proc copula;                                                                                                                            
title "Kopuła Claytona";                                                                                                                
var u v;                                                                                                                                
define c_cop clayton (theta=0.450366);                                                                                                  
simulate c_cop/seed=1234                                                                                                                
ndraws=1000                                                                                                                             
outuniform=clayton_jednorodne                                                                                                     
plots= (datatype=UNIFORM distribution=CDF);                                                                                             
run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Zadanie4.  Symulowa? dane z rozk?adu t-studenta. Opcje w DEFINE Statement (COR=SAS-data-set (czyli macierz), THETA=value)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
KOD 6.
Kod dokonuj?cy transformacji danych wylosowanych z kopu?y 
Gumbela za pomoc? dystrybuanty odwrotnej.

data transformacja;                                                                                                               
set gumbel_jednorodne;                                                                                                            
L_kakao=quantile('Normal',u,-0.1667,7.168673);                                                                                          
L_kawa=quantile('Weibull',v,28,218.4529)-215;                                                                                           
run;    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Makro

%macro Kopula (a) ;
data div;
set transformacja;
L=&a*L_kawa+(1-&a)*L_kakao ;
run ;
proc univariate data= div ;
var L ;
run ;

proc sort data= div  out =div_sort;
by descending L ;
proc means data= div_sort ( obs =50) mean ;
var L ;
run ;
%mend ;
%Kopula ( 0 )
%Kopula ( 0.1 )
%Kopula ( 0.2 )
%Kopula ( 0.3 )
%Kopula ( 0.4 )
%Kopula ( 0.5 )
%Kopula ( 0.6 )
%Kopula ( 0.7 )
%Kopula ( 0.8 )
%Kopula ( 0.9 )
%Kopula ( 1 )



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




KOD 8.
Kod generuj?cy wykresy warto?ci stop strat dla inwestycji ??cznej.

ods graphics on;                                                                                                                        
proc kde data=transformacja;                                                                                                      
bivar L_kakao L_kawa/ plots=all;                                                                                                        
run;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Rozwi?zania
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Zadanie 1

proc univariate data=kawa1;                                                                                                       
var L_kawa;                                                                                                                             
histogram/weibull(theta=-215,c=28);                                                                                                     
run;                                                                                                                                    

Zadanie 2. Inny sposob rozwiazania

proc iml ;
  n = 5000;
/*deklarowanie macierzy*/
  sigma = { 97.82 23.61,
  23.61 50.53
   };
mu = {-0.7388, -0.1666};
p = nrow(sigma);
m = repeat(t(mu),n) ;
g =root(sigma);
z =normal(repeat(0,n,p)) ;
/*albo mo?na z =normal(j(n,p,0)) ; */
ymat = z*g + m ;

/*Tworzenie porfeli*/
delta=0.01;
num=101;
A=j(2,num,0);
do k=1 to num;
A[1,k]=(k-1)*delta;
A[2,k]=1-(k-1)*delta;
end;

ymatrix=ymat*A;

call qntl(q, ymatrix, 0.95); /* compute  95th quantiles */
x=1:num;
x1=t(x);
x2=t(q);
kwantyle=x1||x2;
print kwantyle;

/* przepisanie danych z macierzy do pliku*/
create newdata from kwantyle;
append from kwantyle;
close newdata;



   Symbol value=none interpol=join line=1  width=2;
   title"Trajectory VaR";
   proc gplot data=newdata;
   plot col2*col1;
   run;

Zadanie 3.

data laczna;                                                                                                                      
set kakao1 (keep=L_kakao);                                                                                                        
set kawa1 (keep=L_kawa);                                                                                                          
run;

proc copula data=laczna;                                                                                                          
title "Kopuła t";                                                                                                                
var L_kakao L_kawa;                                                                                                                     
fit t/ outcopula=t_parametry;                                                                                          
run;

proc copula data=laczna;                                                                                                          
title "Kopuła normal";                                                                                                                
var L_kakao L_kawa;                                                                                                                     
fit normal/ outcopula=normal_parametry;                                                                                          
run;

proc copula data=laczna;                                                                                                          
title "Kopuła Claytona";                                                                                                                
var L_kakao L_kawa;                                                                                                                     
fit clayton/METHOD=CAL;                                                                                          
run;

Zadanie 4.

proc iml ;
 
/*deklarowanie macierzy*/
  P = {1 0.3877,
  0.3877 1
   };
   
   create Sasp from P;
append from P;
close Sasp;

proc copula;                                                                                                                            
title "Kopuła t-studenta";                                                                                                                
var u v;                                                                                                                                
define c_cop t (corr=Sasp df=30);                                                                                                  
simulate c_cop/seed=1234                                                                                                                
ndraws=1000                                                                                                                             
outuniform=t_jednorodne                                                                                                     
plots= (datatype=UNIFORM distribution=CDF);                                                                                             
run;

%%%%%%%%%%%%%%%%%%%%%%%
Ilustracja - Rysowanie wynikow


proc print data=t_jednorodne;
var u v;
run;

data transformacja;                                                                                                               
set t_jednorodne;                                                                                                            
L_kakao=quantile('Normal',u,-0.1667,7.168673);                                                                                          
L_kawa=quantile('Weibull',v,28,218.4529)-215;                                                                                           
run; 

ods graphics on;                                                                                                                        
proc kde data=transformacja;                                                                                                      
bivar L_kakao L_kawa/ plots=all;                                                                                                        
run;