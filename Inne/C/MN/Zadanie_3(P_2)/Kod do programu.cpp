#include <iostream>
#include <cstdlib>

float** RozkladLU(float** Macierz, unsigned int Dim)
{
	float** wynik = new float* [Dim];
	for (unsigned int i = 0; i < Dim; i++)
		wynik[i] = new float[Dim];

	for (int i = 0; i < Dim; i++)
	{
		for (int j = 0; j <= i; j++)
			wynik[j][i] = Macierz[j][i];

		for (int j = i + 1; j < Dim; j++)
		{
			double w = Macierz[j][i] / Macierz[i][i];

			wynik[j][i] = w;
			for (int k = 0; k < Dim; k++)
				Macierz[j][k] -= w * Macierz[i][k];
		}
	}
	return wynik;
}

void WypiszRozwiazanie(float* Wektor, unsigned int Dim)
{
	std::cout << "Rozwiazanie : " << std::endl;
	for (unsigned int i = 0; i < Dim; i++)
		std::cout << "x" << i << " : " << Wektor[i] << std::endl;
}

void WypiszMacierz(float** Macierz, unsigned int Dim)
{
	for (unsigned int i = 0; i < Dim; i++)
	{
		std::cout << std::endl;
		for (unsigned int j = 0; j < Dim; j++)
			std::cout << Macierz[i][j] << " ";
	}
}

void WypiszLU(float** Macierz, unsigned int Dim)
{
	std::cout << "Macierz U";
	for (unsigned int i = 0; i < Dim; i++)
	{
		std::cout << std::endl;
		for (unsigned int j = 0; j < Dim; j++)
		{
			if (i <= j)
				std::cout << Macierz[i][j] << " ";
			else
				std::cout << 0 << " ";
		}
	}

	std::cout << std::endl << std::endl << "Macierz L";
	for (unsigned int i = 0; i < Dim; i++)
	{
		std::cout << std::endl;
		for (unsigned int j = 0; j < Dim; j++)
		{
			if (i > j)
				std::cout << Macierz[i][j] << " ";
			else if (i == j)
				std::cout << 1 << " ";
			else
				std::cout << 0 << " ";
		}
	}
}

float* RozwiazanieUkladu(float** Macierz, float* Wektor, unsigned int Dim)
{
	float* wynik = new float[Dim];
	wynik[0] = Wektor[0];

	float tymczasowy = 0.0;
	for (int i = 1; i < Dim; i++)
	{
		tymczasowy = 0;
		for (int j = 0; j < i; j++)
			tymczasowy += Macierz[i][j] * wynik[j];

		wynik[i] = Wektor[i] - tymczasowy;
	}

	wynik[Dim - 1] /= Macierz[Dim - 1][Dim - 1];

	for (int i = Dim - 2; i >= 0; i--)
	{
		tymczasowy = 0;
		for (int j = i + 1; j < Dim; j++)
			tymczasowy += Macierz[i][j] * wynik[j];

		wynik[i] = (wynik[i] - tymczasowy) / Macierz[i][i];
	}

	return wynik;
}

void Wynik(float** macierz, float* wektor, unsigned int dim)
{
	float** Rozklad = RozkladLU(macierz, dim);
	WypiszLU(Rozklad, dim);
	std::cout << std::endl << std::endl;

	float* Rozwiazanie = RozwiazanieUkladu(Rozklad, wektor, dim);
	WypiszRozwiazanie(Rozwiazanie, dim);
	std::cout << std::endl;

	for (unsigned int i = 0; i < dim; i++)
		delete[] Rozklad[i];
	delete[] Rozklad;
	delete Rozwiazanie;
}


int main()
{
	float** Przyklad = nullptr;
	float* Wektor = nullptr;

    //Przyklad 1
	Przyklad = new float*[2];
	for (unsigned int i = 0; i < 2; i++)
		Przyklad[i] = new float[2];
	Wektor = new float[2];

	Przyklad[0][0] = 4;
	Przyklad[0][1] = 5;

	Przyklad[1][0] = 7;
	Przyklad[1][1] = 8;

	Wektor[0] = 9;
	Wektor[1] = 6;

	std::cout << "Przyklad 1" << std::endl << std::endl;
	Wynik(Przyklad, Wektor, 2);
	std::cout << "-----------" << std::endl << std::endl;

	for (unsigned int i = 0; i < 2; i++)
		delete[] Przyklad[i];
	delete[] Przyklad;
	delete[] Wektor;
	//----------//
    //Przyklad 2
	Przyklad = new float* [3];
	for (unsigned int i = 0; i < 3; i++)
		Przyklad[i] = new float[3];
	Wektor = new float[3];

	Przyklad[0][0] = 1;
	Przyklad[0][1] = 5;
	Przyklad[0][2] = 9;

	Przyklad[1][0] = 4;
	Przyklad[1][1] = 8;
	Przyklad[1][2] = 7;

	Przyklad[2][0] = 2;
	Przyklad[2][1] = 6;
	Przyklad[2][2] = 3;

	Wektor[0] = 5;
	Wektor[1] = 5;
	Wektor[2] = 5;

	std::cout << "Przyklad 2" << std::endl << std::endl;
	Wynik(Przyklad, Wektor, 3);
	std::cout << "-----------" << std::endl << std::endl;

	for (unsigned int i = 0; i < 3; i++)
		delete[] Przyklad[i];
	delete[] Przyklad;
	delete[] Wektor;
	//----------//
    //Przyklad 3
	Przyklad = new float* [4];
	for (unsigned int i = 0; i < 4; i++)
		Przyklad[i] = new float[4];
	Wektor = new float[4];

	Przyklad[0][0] = 9;
	Przyklad[0][1] = 8;
	Przyklad[0][2] = 7;
	Przyklad[0][3] = 6;

	Przyklad[1][0] = 5;
	Przyklad[1][1] = 4;
	Przyklad[1][2] = 3;
	Przyklad[1][3] = 2;

	Przyklad[2][0] = 1;
	Przyklad[2][1] = 9;
	Przyklad[2][2] = 8;
	Przyklad[2][3] = 7;

	Przyklad[3][0] = 6;
	Przyklad[3][1] = 5;
	Przyklad[3][2] = 4;
	Przyklad[3][3] = 3;

	Wektor[0] = 7;
	Wektor[1] = 4;
	Wektor[2] = 1;
	Wektor[3] = 5;

	std::cout << "Przyklad 3" << std::endl << std::endl;
	Wynik(Przyklad, Wektor, 4);
	std::cout << "-----------" << std::endl << std::endl;

	for (unsigned int i = 0; i < 4; i++)
		delete[] Przyklad[i];
	delete[] Przyklad;
	delete[] Wektor;
	//----------//
    //Przyklad 4
	Przyklad = new float* [3];
	for (unsigned int i = 0; i < 3; i++)
		Przyklad[i] = new float[3];
	Wektor = new float[3];

	Przyklad[0][0] = 0.0000000000000000000000000000000000000005;
	Przyklad[0][1] = 0.0000000000000000000000000000000000000003;
	Przyklad[0][2] = 0.0000000000000000000000000000000000000002;

	Przyklad[1][0] = 0.0000000000000000000000000000000000000001;
	Przyklad[1][1] = 0.0000000000000000000000000000000000000002;
	Przyklad[1][2] = 0.0000000000000000000000000000000000000000;

	Przyklad[2][0] = 0.0000000000000000000000000000000000000003;
	Przyklad[2][1] = 0.0000000000000000000000000000000000000000;
	Przyklad[2][2] = 0.0000000000000000000000000000000000000004;

	Wektor[0] = 0.0000000000000000000000000000000000000010;
	Wektor[1] = 0.0000000000000000000000000000000000000005;
	Wektor[2] = -0.0000000000000000000000000000000000000002;

	std::cout << "Przyklad 4" << std::endl << std::endl;
	Wynik(Przyklad, Wektor, 3);
	std::cout << "-----------" << std::endl << std::endl;

	for (unsigned int i = 0; i < 3; i++)
		delete[] Przyklad[i];
	delete[] Przyklad;
	delete[] Wektor;
	//----------//
    //Przyklad 5
	Przyklad = new float* [4];
	for (unsigned int i = 0; i < 4; i++)
		Przyklad[i] = new float[4];
	Wektor = new float[4];

	Przyklad[0][0] = 1;
	Przyklad[0][1] = -1;
	Przyklad[0][2] = 2;
	Przyklad[0][3] = 2;

	Przyklad[1][0] = 2;
	Przyklad[1][1] = -2;
	Przyklad[1][2] = 1;
	Przyklad[1][3] = 0;

	Przyklad[2][0] = -1;
	Przyklad[2][1] = 2;
	Przyklad[2][2] = 1;
	Przyklad[2][3] = -2;

	Przyklad[3][0] = 2;
	Przyklad[3][1] = -1;
	Przyklad[3][2] = 4;
	Przyklad[3][3] = 0;

	Wektor[0] = 0;
	Wektor[1] = 1;
	Wektor[2] = 1;
	Wektor[3] = 2;

	std::cout << "Przyklad 5" << std::endl << std::endl;
	Wynik(Przyklad, Wektor, 4);
	std::cout << "-----------" << std::endl << std::endl;

	for (unsigned int i = 0; i < 4; i++)
		delete[] Przyklad[i];
	delete[] Przyklad;
	delete[] Wektor;

	system("pause");
	return 0;
}
