#include<iostream>
#include<string>


int zamiana_dz(std::string liczba)
{
	int x;
	int wynik = 0;
	int p = 1;

	for (int i = liczba.size() - 1; i >= 0; i--)
	{
		if (liczba[i] >= '0' && liczba[i] <= '9')
			x = liczba[i] - '0';
		else 
			x = liczba[i] - 55;
		wynik += x * p;
		p *= 16;
	}

	return wynik;
}

void zamiana_sz(int liczba)
{
	if (liczba == 0)
	{
		std::cout << 0;
		return;
	}
	int wynik = liczba % 16;
	liczba /= 16;
	zamiana_sz(liczba);
	if (wynik < 10) 
		std::cout << wynik;
	else
	{
		char litera = wynik + 55;
		std::cout << litera;
	}
}

int main()
{
	std::string liczba_p;
	std::string liczba_d;
	
	char znak;
	
	int wynik;
	
	std::cout << "Kalkulator liczb szesnastkowych do 2 miejsc znaczących." << std::endl;
	std::cout << "Podaj pierwsza liczbe: " << std::endl;
	std::cin >> liczba_p;
	
	if (liczba_p.size() > 2)
	{
		std::cout << "za duza liczb !!!" << std::endl;
		system("pause");
		return 0;
	}
	
	std::cout << "Podaj działanie: " << std::endl;
	std::cin >> znak;
	std::cout << "Podaj druga liczbe: " << std::endl;
	std::cin >> liczba_d;
	
	if (liczba_d.size() > 2)
	{
		std::cout << "za duza liczb !!!" << std::endl;
		system("pause");
		return 0;
	}
	
	int a = zamiana_dz(liczba_p);
	int b = zamiana_dz(liczba_d);
	
	switch (znak)
	{
		case '+':
			wynik = zamiana_dz(liczba_p) + zamiana_dz(liczba_d);
			break;
		case '-':
			wynik = zamiana_dz(liczba_p) - zamiana_dz(liczba_d);
			break;
		case '*':
			wynik = zamiana_dz(liczba_p) * zamiana_dz(liczba_d);
			break;
		case '/':
			wynik = zamiana_dz(liczba_p) / zamiana_dz(liczba_d);
			break;
		default:
			std::cout << "Brak takiego dzialania" << std::endl;
			break;
	}

	std::cout << "Wynik: ";
	zamiana_sz(wynik);
	std::cout << "" << std::endl;


	system("pause");
	return 0;
}
