/******************************************************************************/
/*  Ilustracja sposobu formatowania liczb przy ich wyswietlaniu               */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
	
  double a = 1/3.0;
	long i = 297646;
	
  // Liczby calkowite   
	printf("%d  %i %u\n",i,i,-i);     
  printf("%010d\n",i);
  printf("%*d\n",8,i);
  printf("%x %X %o\n\n",i,i,i);

	// liczby zmiennoprzecinkowe   
	printf("%f\n",4.4);
  printf("%2.2f\n",334.44856968);
	printf("%e  %E\n",4.4,4.4);
  printf("%2.2e\n",334.44856968);
	printf("%f\n",a);
  a = 2*a*90/M_PI;
	printf("%f\n",a);
			
	system("PAUSE");
	return 0;
}

