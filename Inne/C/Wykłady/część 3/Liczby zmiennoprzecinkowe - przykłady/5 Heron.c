/******************************************************************************/
/*  Ilustracja skutkow w rozwiniecia dwojkowego ulamkow dziesietnych w        */
/*  ulamki dwojkowe.                                                          */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.0 Ryszsard Sobczak 2017-03-20                                    */
/*                   - uproszczenie przykladu                                 */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <math.h>

/* Program obliczajacy pole trojkata */

int main(int argc, char** argv) {

	double a,b,c;
	double p;
	int koniec = 0;
	
	//Wczytanie danych
	while (!koniec){
	
		printf ("Podaj bok a trojkata: ");
		scanf("%lf",&a);
		printf ("Podaj bok b trojkata: ");
		scanf("%lf",&b);
		printf ("Podaj bok c trojkata: ");
		scanf("%lf",&c);
	  if ((a > 0) && (b > 0) && ( c > 0)){
      printf("T1\n");
			//Sprawdzenie warunku trojkata
			if ((a + b > c) && (a + c > b) && (b + c > a)){
			  printf("T2\n");
				//Wyznaczenie pola tojkata
			  p = (a + b + c)/2;
			  printf("T3: %e\n",p);
				printf("Pole = %f\n",sqrt(p*(p - a)*(p - b)*(p - c)));	
			}
			else {
				printf("To nie jest trojkat: %e\n",a + c);
				printf("T4: (a+c) =  %e\n",a+c);
			};
	  }
	  else
	    koniec = 1;
  };
	// while (!koniec);
	return 0;
}

