/******************************************************************************/
/*  Generator liczb pseudolosowych                                            */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.0 Ryszard Sobczak 2017-03-20                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 1000

int i;

FILE *liczby;

int main() {

  // Zainicjowanie generatora liczb pseudolosowych
  srand(time(NULL));
  liczby = fopen("Rand_wynik.csv","w");
  for (i = 0; i < N; i++){   // wiersze
    fprintf(liczby,"%6d;\n ",rand());
  };
  fclose(liczby);
  system("pause");
	return 0;
}
