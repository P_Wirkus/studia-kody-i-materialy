/******************************************************************************/
/*  Ilustracja skutkow w rozwiniecia dwojkowego ulamkow dziesietnych          */
/*     Rozwiazanie ownania kwadratowego w postaci wielomianowej               */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.0 Ryszsard Sobczak 2017-03-20                                    */
/*                   - uproszczenie przykladu                                 */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
// Wartosci generujace niepoprawne rozwiazanie: a=0.1, b=0.6, c=0.9
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
  double a, b, c, delta;
  
  // 1. Odczytanie a,b,c
  printf("Podaj a:");
  scanf("%Lf",&a);      // %Lf - odczytanie zmiennej typu double
  printf("Podaj b:");
  scanf("%Lf",&b);      // %Lf - odczytanie zmiennej typu double
  printf("Podaj c:");
  scanf("%Lf",&c);      // %Lf - odczytanie zmiennej typu double
  // 2. Policzy� delte
  delta = b*b - 4*a*c;
  // 3. Czy delta >= 0
  if (delta >= 0) {
    // 4. Tak: pierwiastek
    // 4a. Czy delta == 0?
    if (delta == 0)
      // 4b. Tak: xo=-b/(2*a)
      printf("Pojedynczy pierwiastek = %E\n",-b/(2*a));
    else {
      // 4d. Nie: x1=...
      //          x2=...
      printf("Dwa pierwiastki:\n");
      printf("  Pierwszy = %E\n",(-b-sqrt(delta)/(2*a)));
      printf("     Drugi = %E\n",(-b+sqrt(delta)/(2*a)));
    }
  }
  else 
  // 5. Nie: niema rozwiazan w zbiorze R
    printf("Brak pierwiastkow. Delta = %e\n",delta);
	return 0;
}
