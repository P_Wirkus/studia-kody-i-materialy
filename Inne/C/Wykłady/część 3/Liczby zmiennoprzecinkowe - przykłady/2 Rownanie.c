/******************************************************************************/
/*  Ilustracja skutkow w rozwiniecia dwojkowego ulamkow dziesietnych          */
/*     Rozwiazanie ownania kwadratowego w postaci wielomianowej               */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 2.0 Ryszsard Sobczak 2017-03-20                                    */
/*                   - uproszczenie przykladu                                 */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  double a,b,c,delta;  //float
  
  printf("Podaj a : ");
  scanf("%lf",&a);
  printf("Podaj b : ");
  scanf("%lf",&b);
  printf("Podaj c : ");
  scanf("%lf",&c);
  delta = b*b - 4*a*c;
  printf("T: delta = %e\n",delta);
  if (delta > 0) {
  	printf("Dwa pierwiastki\n");
	}
	else {
		if (delta < 0) {
			printf("Brak pierwiastkow\n");
		}
		else {
			printf("Jeden pierwiastek\n");
		}
	}
  
	return 0;
}
