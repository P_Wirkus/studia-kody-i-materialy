/******************************************************************************/
/*  Iteracyjny algorytm obliczania pola parabola y=x(10 - x)                  */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux:                         */
/*  Wersja 1.0 Ryszard Sobczak 2017-03-27                                     */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	double x0,xn,dx,S,fn,fn1,x;
	int n,i;

	// Odczytanie x0
	printf("Podaj x0: ");
	scanf("%lf",&x0);
	// Odczytanie xn
	printf("Podaj xn: ");
	scanf("%lf",&xn);
	// Odczytanie n
	printf("Podaj n: ");
	scanf("%d",&n);
	// Wyznaczenie dx
	dx = (xn-x0)/n;
  //printf("T0: x0 = %lf, xn = %lf, dx = %lf\n",x0, xn, dx);
	// Sumujemy trapezy
	S =0;
//	for (x=x0;x<=xn;x+=dx){
//		fn = x*(10-x);
//		fn1 = (x+dx)*(10-(x+dx));
//    S += dx*(fn1+fn)/2;
//    //printf("T1: x = %lf\n",x);
//	}
	for (i=1;i<n;i++){
		x = i*dx;
		fn = x*(10-x);
		fn1 = (x+dx)*(10-(x+dx));
		S += dx*(fn1+fn)/2;
	}
	// Wyswietlamy pole
	printf("Pole = %lf\n",S);
	return 0;
}
