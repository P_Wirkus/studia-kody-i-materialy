/******************************************************************************/
/*  Iteracyjny algorytm obliczania wartosci funkcji ln(x+1) z zastosowaniem   */
/*    schematu Hornera                                                        */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux:                         */
/*  Wersja 1.0 Ryszard Sobczak 2017-05-06                                     */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i Matematyki Stosowanej  */
/******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(){
  int i, n = 100;
  float x = -.99;
  double s;
  
	printf("n = %d, x = %lf\n",n,x);
	s = 1/n;
  for (i = 1; i < n; i++){
    printf("T1:n-i=%2d,ds = %35.13lf\n",n-i,(x*s)/(n-i));
    s = 1.0/(n-i) - x*s;
    printf("T2:i = %2d, s = %35.13lf\n",i,s);
	}
	s = s*x;
  printf("s            = %35.13lf\nln(%6.3lf)   = %35.13lf \n",
	        s, x, log(x+1)); 
  system("PAUSE");
  return EXIT_SUCCESS;
}

