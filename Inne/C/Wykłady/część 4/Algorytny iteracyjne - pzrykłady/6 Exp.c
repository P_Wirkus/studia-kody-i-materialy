/******************************************************************************/
/*  Iteracyjny algorytm obliczania wartosci funkcji e^x z zastosowaniem       */
/*    schematu Hornera                                                        */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux:                         */
/*  Wersja 1.0 Ryszard Sobczak 2017-05-06                                     */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i Matematyki Stosowanej  */
/******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(){
  int i, n = 100;
  float x = 3.567;
  double s;
  
	printf("n = %d, x = %lf\n",n,x);
	s = 1;
  for (i = 0; i < n; i++){
//    printf("T1:n-i=%2d,ds = %35.13lf\n",n-i,(x*s)/(n-i));
    s = 1 + (x*s)/(n-i);
//    printf("T2:i = %2d, s = %35.13lf\n",i,s);
	}
  printf("s            = %35.13lf\nexp(%7.3lf) = %35.13lf \n",
	        s, x, exp(x)); 

  system("PAUSE");
  return EXIT_SUCCESS;
}
