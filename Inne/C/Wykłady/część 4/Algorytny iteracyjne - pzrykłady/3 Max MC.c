/******************************************************************************/
/* Poszukiwanie maksimum funkcji metoda Monte Carlo                           */
/* Wersja 1.0 Ryszard Sobczak 02.2008                                         */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

	#include <stdio.h>
	#include <stdlib.h>
	#include <time.h>

	double y;                            // bie��ca warto�� funkcji
	float a;                            // wsp�czynnik a w funkcji
	float x;                   // bie��ca warto�� argumentu funkcji
	long i=0,n;  // liczba losowanych warto�ci funkcji kryterialnej
	double max, xm;                // znaleziona warto�� maksymalna 
                                  // i odpowiadaj�cy jej argument
	int main() {
	  // 1. zainicjowanie generatora liczb pseudolosowych
	  srand( (unsigned)time( NULL ));
	  // 2. odczytanie z klawiatury warto�ci wsp�czynnika a,
	  printf ("Podaj wspolczynnik a                         : ");
	  scanf("%f",&a);
	  // 3. odczytanie z klawiatury liczby losowa� n 
	  //  (n powinno by� wi�ksze od 0)
	  do {
	    printf ("Podaj liczbe losowan                         : ");
	    scanf ("%Ld",&n);
	    if (n <= 0 )
	      printf("Liczba powinna byc wieksza od zera !!!\n");
	  } while (n <= 0);
	  xm  = -1;
	  max = -1e36;                // najmniejsza warto�� typu float
	  do {
	    // 4. wylosowanie kolejnej warto�ci x 
	    x = a*rand()/RAND_MAX;
	    // 5. wyznaczenie warto�ci funkcji
	    y = x*(a - x);
	    // 6. sprawdzanie czy otrzymana warto�� y jest wi�ksza 
	    //    ni� dotychczasowa maksymalna warto�� funkcji max,
	    if (y > max) {
	      // 7. je�li jest wi�ksza to zapami�ta� t� warto�� funkcji
		      //    i odpowiadaj�c� jej warto�� argumentu x.
	      max = y;
	      xm  = x;
	    };
	    i++;
		  // 8. sprawdzenie czy wylosowano n warto�ci 
 	    //    argumentu funkcji,
	    // 9. je�eli nie wylosowano jeszcze n warto�ci argumentu,
	    //    to do  przej�� do punktu 4.
	  } while (i <= n);
    // 10. je�eli wylosowano ju� n warto�ci argumentu,
	  //     to wy�wietli� wynik poszukiwania
	  printf ("Wartosc maksymalna %f w punkcie %f\n", max, xm);
	  system("PAUSE");
	  return 0;
	}
