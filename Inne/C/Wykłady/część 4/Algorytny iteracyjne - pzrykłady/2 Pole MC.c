/******************************************************************************/
/*  Iteracyjny algorytm obliczania pola pod parabol¹ y=x(10-x)                */
/*          Metoda Monte Carlo                                                */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux:                         */
/*  Wersja 1.0 Ryszard Sobczak 2007-08-16                                     */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

const int MAXRAND=32767;
long i,
     t = 0,                             // licznik losowan z trafieniem w obszar
     n = 0;                                    // licznik losowan nie trafionych
const long L = 1000000;                                        // liczba losowan

double x,y,                                   // wspolrzedne wylosowanego punktu
       pole;                                          // pole mierzonego obszaru

int main () {
  // Zainicjowanie generatora liczb losowych
  srand( (unsigned)time( NULL ) );

  for (i=0; i<L; i++) { 
    // Losowanie wspolrzednych punktu
    x = 10.0*rand()/MAXRAND;
    y = 25.0*rand()/MAXRAND;

    // Spawdzenie czy punkt znajduje sie w mierzonym obszarze
    if (y < x*(10-x)) 
      t++;
    else
      n++;
  };

  // Oszacowanie pola
  pole = 10.0*25.0*t/(t+n);
  // Wyswietlenie wynikow
  printf("Pole=%f (blad=%4.1f%%)\n",pole,100*fabs(1.0-pole/166.6666666));
  system("PAUSE");
  return 0;
}
