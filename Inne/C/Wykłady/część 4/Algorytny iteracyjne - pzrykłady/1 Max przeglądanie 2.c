/******************************************************************************/
/* Poszukiwanie maksimum funkcji metoda przegladania zbioru argumentow        */
/* Wersja 1.0 Ryszard Sobczak 08.2006                                         */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

double y;                                             // biezaca wartosc funkcji
float a;                                             // wspolczynnik a w funkcji
float x,                                    // biezaca wartosc argumentu funkcji
     dx;                                            // zmiana wartosci argumentu 
int i,n;                   // liczba przegladanych wartosci funkcji kryterialnej
double max, xm;                                 // znaleziona wartosc maksymalna 
                                                 // i odpowiadajacy jej argument
int main() {
  printf ("Podaj wspolczynnik a                         : ");
  scanf("%f",&a);
  do {
    printf ("Podaj wartosc zmiany argumentu               : ");
    scanf ("%f",&dx);
    if (dx <=0 )
      printf("Liczba powinna byc wieksza od zera !!!\n");
  } while (dx <= 0);
  xm  = 0;
  x = 0;
  max = -1e36;                                 // najmniejsza wartosc typu float
  n = (int)(a/dx);                  // wyznaczenie liczebnosci zbioru argumentow
  for (i = 0; i < n; i++) {
    x += dx;                                    // zwiekszenie argumentu funkcji
    y = x*(a -x);
    if (y > max) {
      max = y;
      xm  = x;
    };
  };
  printf ("Wartosc maksymalna %f w punkcie %f\n", max, xm);
  system("PAUSE");	
  return 0;
}

