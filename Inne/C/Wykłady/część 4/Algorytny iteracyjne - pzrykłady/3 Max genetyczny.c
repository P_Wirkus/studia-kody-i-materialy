/******************************************************************************/
/*  Poszukiwanie maksimum funkcji z zastosowaniem algorytmu genetycznego      */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux:                         */
/*  Wersja 1.0 Ryszard Sobczak 1997 (w Pascalu)                               */
/*  Wersja 1.1 Ryszard Sobczak 2006 (w C)                                     */
/*  Wersja 1.1 Ryszard Sobczak 2007-08-16                                     */
/*                 zwiększenie dlugosci ciagu 'genow' z 5 do 8'               */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define C 256  // liczba mozliwych gentypow
#define B 8    // liczba genow binarnych
#define P 6    // liczebnosc pokolenia (liczba parzysta)

int nr_c,
    nr_b,
    nr_p;

int populacja [C][B];
int wylosowane [C];
int pot_krzyz1[B];  // genotypy krzyzowanej pary
int pot_krzyz2[B];
int pot_mut[B];     // zmutowany genotyp
int los;
int punkt;
int licz;

int pokolenie [P];  // biezace pokolenie
int dziesietna;
int potomek   [P];  // nastepne pokolenia
int krzyzowani[P];  // osobniki juz skrzyzowane
float x;            // wartosc argumentu funkcji celu dla kazdego genotypu
float cel[P];       // wartosc funkcjo celu kazdego osobnika
float suma;
int udzial [P];     // procentowy udzial w uzyskaniu funkcji celu
int pole;
int ud;
int para;

int z;

int main() {

  // Zbudowanie populacji
  for (nr_c = 0; nr_c < C; nr_c++) {
    dziesietna = nr_c;
    for (nr_b = B-1; nr_b >= 0; nr_b--) {
      populacja[nr_c][nr_b] = dziesietna % 2;      // reszta z dzielenia przez 2
      dziesietna /= 2;                             // dzielenie przez 2
    };
    wylosowane[nr_c] = 0;
  };

  // Losowanie pierwszego pokolenia
  srand( (unsigned)time( NULL ) );

  for (nr_p = 0; nr_p < P; nr_p++) {
    do {
    los = (int)floor((float)C*(rand()-1)/RAND_MAX);
    } while (wylosowane[los]);
    pokolenie[nr_p] = los;
    wylosowane[los] = 1;
  };

  do {
    /********************************************************************/
    /* Krzyzowanie                                                      */
    /********************************************************************/

    srand( (unsigned)time( NULL ) );

    para = 0;
    for (nr_p = 0; nr_p < P; nr_p++)
      krzyzowani[nr_p] = 0;          // nikt sie jeszcze nie krzyzowal

    nr_p = 0;
    printf ("\n\nKrzyzowanie: 1 z ");
    do {
      krzyzowani[nr_p] = 1;          // pierwszy element pary

      // Losowanie drugiego osobnika pary wsrod tych osobnikow populacji, 
      // ktorzy jeszcze sie nie krzyzowali
      do {
        los = (int)floor((float)P * (rand()-1)/RAND_MAX);
      } while (krzyzowani[los]);
      krzyzowani[los] = 1;

      // Losowanie dlugosci wymienianego fragmentu genotypu
      punkt = (int)floor((float)B * (rand()-1)/RAND_MAX);
      printf ("%d w %d",los+1, punkt+1);

      // Krzyzowanie z partnerem
      for (nr_b = 0; nr_b <= punkt-1; nr_b++) {
        pot_krzyz1[nr_b]=populacja[pokolenie[nr_p]][nr_b];
        pot_krzyz2[nr_b]=populacja[pokolenie[los]][nr_b];
      };
      for (nr_b = punkt; nr_b < B; nr_b++) {
        pot_krzyz1[nr_b]=populacja[pokolenie[los]][nr_b];
        pot_krzyz2[nr_b]=populacja[pokolenie[nr_p]][nr_b];
      };

      // Poszukiwanie w zbiorze genotypow numerow nowych osobnikow
      for (nr_c = 0; nr_c < C; nr_c++) {
        licz=0;
        for (nr_b = 0; nr_b < B; nr_b++) 
          if (populacja[nr_c][nr_b] == pot_krzyz1[nr_b])
            licz++;
        if (licz == B) 
          potomek[nr_p] = nr_c;
      };

      for (nr_c = 0; nr_c < C; nr_c++) {
        licz=0;
        for (nr_b=0; nr_b < B; nr_b++)
          if (populacja[nr_c][nr_b] == pot_krzyz2[nr_b]) 
            licz++;
        if (licz == B) 
          potomek[los] = nr_c;
      };
      para++;

      // Poszukiwanie kolejnego osobnika do krzyzowania
      if (2*para < P) {
        nr_p = 0;
        while (krzyzowani[nr_p]) 
          nr_p++;
        printf (" i %d z ",nr_p+1);
      };
    } while (!(2*para == P));
    // Koniec krzyzowania

    printf ("\n");
    for (nr_p = 0; nr_p < P; nr_p++) {
      for (nr_b = 0; nr_b < B; nr_b++) 
        if (populacja[pokolenie[nr_p]][nr_b] == 1) 
          printf("1");
        else 
          printf("0");
      pokolenie[nr_p] = potomek[nr_p];
      printf("       ");
      for (nr_b = 0; nr_b < B; nr_b++) 
        if (populacja[pokolenie[nr_p]][nr_b] == 1) 
          printf("1");
        else 
          printf("0");
      printf("\n");
    };

    /********************************************************************/
    /* Mutacja                                                          */
    /********************************************************************/

    printf("\nMutacja: ");

    if ((int)floor((float)C * (rand()-1)/RAND_MAX) > (3.0*C/4)) {
      los = (int)floor((float)P * (rand()-1)/RAND_MAX);
      punkt = (int)floor((float)B * (rand()-1)/RAND_MAX);
      printf ("w %d 'gen' %d\n",los+1,punkt+1);
      for (nr_b=0; nr_b < B; nr_b++) {
        pot_mut[nr_b] = populacja[pokolenie[los]][nr_b];
      };
      if (pot_mut[punkt] == 1)
        pot_mut[punkt] = 0;
      else 
        pot_mut[punkt] = 1;

      for (nr_c = 0; nr_c < C; nr_c++) {
        licz=0;
        for (nr_b = 0; nr_b < B; nr_b++) 
          if (populacja[nr_c][nr_b] == pot_mut[nr_b])
            licz++;
        if (licz == B) 
          potomek[los] = nr_c;
      };

      for (nr_p = 0; nr_p < P; nr_p++)
        pokolenie[nr_p] = potomek[nr_p];
    }
    else 
      printf ("brak mutacji w tym pokoleniu\n");
    for (nr_p = 0; nr_p < P; nr_p++) {
      for (nr_b=0; nr_b<B; nr_b++)
        if (populacja[pokolenie[nr_p]][nr_b]==1) 
          printf("1");
        else 
          printf("0");
      printf("\n");
    };
    
    /********************************************************************/
    /* Selekcja                                                         */
    /********************************************************************/

    // Okreslenie udzialu poszczegolnych osobnikow 
    //   w osiagnieciu celu
    suma = 0.0;
    printf ("\nUdzial w funkcji celu i reprodukcja\n");
    printf ("x          f(x)    blad         udzial  pokolenie nr_osobnika\n");

    for (nr_p = 0; nr_p < P; nr_p++) {
      x = (float)(10.0*pokolenie[nr_p]/C);
      cel[nr_p] = x*(10 - x);
      suma += cel[nr_p];
    };

    if (suma) 
      for (nr_p = 0; nr_p < P; nr_p++) 
        udzial[nr_p] = (int)floor(100.0*cel[nr_p]/suma);
    else
      for (nr_p = 0; nr_p < P; nr_p++) 
        udzial[nr_p] = 100/P;

    // Losowanie nastepnego pokolenia
    srand( (unsigned)time( NULL ) );
    for (nr_p = 0; nr_p < P; nr_p++) {
      los = (int)(100.0 * (rand()-1)/RAND_MAX);
      pole = udzial[0];
      ud = 0;
      do {
        if (los < pole) {
          potomek[nr_p] = pokolenie[ud];
          ud = P;
        }
        else
          pole += udzial[++ud];
      } while (ud < P);
    };

    for (nr_p = 0; nr_p < P; nr_p++)
      pokolenie[nr_p]=potomek[nr_p];

    for (nr_p = 0; nr_p < P; nr_p++) {
      printf("%f  %6.2f  %9.5f%%   ",(float)(10.0*pokolenie[nr_p]/C),cel[nr_p],100.0*(1-cel[nr_p]/25));
      printf ("%6.2f%%  ",fabs(udzial[nr_p]));
      for (nr_b = 0; nr_b < B; nr_b++)
        if (populacja[pokolenie[nr_p]][nr_b] == 1) 
          printf("1");
        else 
          printf("0");
      printf("  %3d\n",pokolenie[nr_p]);
    };

    printf("1 - kontynuacja/... - koniec");
    scanf("%d",&z);
  }while (z==1);
  return 0;
}
