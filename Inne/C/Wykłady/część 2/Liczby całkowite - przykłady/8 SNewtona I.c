/******************************************************************************/
/*  Zakres wyznaczania wartosci symbolu Newtona.
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2017-03-02                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main() {
  int i, n, k;
  unsigned long long 	Sn = 1,    // n!
			Sk = 1,    // k!
			Snk = 1;   // (n-k)!

  printf ("Podaj n (< 21): ");
  scanf ("%d",&n);
  printf ("Podaj k (< 21): ");
  scanf ("%d",&k);
  for (i = 1; i <= n; i++)
    Sn *= i;
  printf ("Wartosc  Sn = %llu \n",Sn);
  for (i = 1; i <= k; i++)
    Sk *= i;
  printf ("Wartosc  Sk = %llu \n",Sk);
  for (i = 1; i <= (n - k); i++) 
    Snk *= i;
  printf ("Wartosc Snk = %llu \n",Snk);
  printf ("Wartosc symbolu Newtona (%d,%d) = %llu \n",n,k,Sn/(Sk*Snk));
  system("PAUSE");

  unsigned long long SN;         // Wartosc symbolu Newtona
  for (k = 0; k <=n; k++)  {
  	SN = 1;
    for (i = 1; i <= k; i++){
      SN = SN*(n - (k - i))/i;
//      SN = ((n - (k - i))/i)*SN;
//      printf ("k = %d, i = %d, SN = %llu \n",k,i,SN);
    };
    printf ("Wartosc symbolu Newtona (%d,%2d)= %llu \n",n,k,SN);
  };
  system("PAUSE");

  printf ("Odwrotna kolejnosc mnozenia ilorazow\n");
  for (k = 0; k <=n; k++)  {
  	SN = 1;
  	for (i = k; i > 0; i--) {
      SN = SN * (n - (k - i))/i;
//      printf ("k = %d, i = %d, SN = %llu \n",k,i,SN);
    };
    printf ("Wartosc symbolu Newtona (%d,%2d)= %llu \n",n,k,SN);
  };

  return 0;
}
