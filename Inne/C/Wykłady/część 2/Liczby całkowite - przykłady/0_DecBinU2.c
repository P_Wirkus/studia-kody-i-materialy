/******************************************************************************/
/* Program zamiany calkowitej liczby dziesietnej na liczbe binarna            */
/* Opis programu:                                                             */
/*   Wczytana wartosc liczby calkowitej z zakresu <-32768, 32767> jest        */
/*   zamieniana na ciag 16 znakow '0' albo '1' tworzacych na ekranie          */
/*   binarna reprezentacje liczby dziesietnej. Do otrzymania wartosci         */
/*   poszczegolnych pozycji ciagu reprezentujacego liczbe binarna             */
/*   zastosowano podane ponizej algorytmy:                                    */
/*                                                                            */
/*   Otrzymanie reprezentacji binarnej dla calkowitych liczb dodatnich:       */
/*                                                                            */
/*   "Prostym sposobem znajdowania kolejnych cyfr rozwiecia dwojkowego        */
/*   dodatniej calkowitej liczby dziesietnej jest znajdowanie reszt z         */
/*   kolenych dzielen przez dwa ciagu liczb zaczynajacego od liczby,          */
/*   ktorej rozwiniecia dwojkowego poszukujemy, a kolejne wyrazy sa           */
/*   czesciami calkowitymi z poprzednich dzielen."                            */
/*            "Propedeutyka informatyki" Wl.Turski, PWN, 1975                 */
/*                                                                            */
/*   Otrzymanie reprezentacji binarnej (system z uzupelnieniowym do           */
/*   dwoch) dla calkowitych ujemnych liczb dziesietnych:                      */
/*                                                                            */
/*   "Bity  liczby binarnej (otrzymanej z  modulu ujemnej liczby dzie-        */
/*   sietnej) nalezy przegladac zaczynajac od prawej pozycji. Dla kaz-        */
/*   dego 0 az do piewrszej jedynki nalezy przepisac 0. Pierwsza na-          */
/*   potkana jedynke nalezy przepisac bez zmian. Nastepnie dla kazdego        */
/*   napotkanego 0 nalezy napisac 1, a dla kazdego 1 napisac 0. Po            */
/*   przepisaniu wartosci modulu nalezy zapis poprzedzic jedynka ozna-        */
/*   czajaca liczbe ujemna."                                                  */
/*      "Arytmetyka maszyn cyfrowych" Ivan Flores, WNT, 1970                  */
/*                                                                            */
/* Uruchomienie programu:                                                     */
/*   decbinu2                                                                 */
/*                                                                            */
/* Wersja: Data:       Opis:                                                  */
/*   1           1999                                                         */
/*   2     19.08.2003  Uporzadkowanie komentarza objasniajacego               */
/*                                                                            */
/* Autor:                                                                     */
/*   R.Sobczak  Katedra Matematyki Dyskretnej Wydzial FTiMS PG                */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int dziesietna;                                             // liczba dziesietna 
int    bit;                           // biezaca wartosc pozycji liczby binarnej 
int    i = 15;                               // numer pozycji w liczbie binarnej 

char binarna[17] = {'0','0','0','0','0','0','0','0',\
                   '0','0','0','0','0','0','0','0','\0'};
                                             // lancuch znakowy z liczba binarna 

int nie_bylo_jedynki = 1;             // flaga informujaca o rozpoznaniu w ciagu 
                                                   // binarnym pierwszej jedynki 

int main()
{
  do
  {
    printf("Podaj liczbe calkowita dziesietnie <-32768,32767>: ");
    scanf("%d",&dziesietna);
  } while ((dziesietna > 32767) || (dziesietna < -32768));

  if (dziesietna >= 0)         // liczba dziesietna jest dodatnia lub rowna zero
  {
    // Zamiana na liczbe binarna
    do
    {
      bit = dziesietna % 2;                        // reszta z dzielenia przez 2
      binarna[i--] = bit+'0';
      dziesietna >>= 1;                           // dzielenie calkowitoliczbowe
                                                          // dziesietnej przez 2
    } while (dziesietna);
  }
  else                                          // liczba dziesietna jest ujemna
  {
    dziesietna = abs(dziesietna);                    // modul liczby dziesietnej
    do
    {
      bit = dziesietna % 2;                        // reszta z dzielenia przez 2
      if (nie_bylo_jedynki)           // czy byla juz jedynka w liczbie binarnej
      {
        // Nie bylo jedynki 
        if (bit)                                     // czy trafiono na jedynke?
          nie_bylo_jedynki = 0;                             // tak, jest jedynka
      }
      else
      {
        // Byla juz jedynka 
        if (!bit)                                            // zanegowanie bitu
          bit = 1;
        else
          bit = 0;
      };

      binarna[i--] = bit+'0';
      dziesietna >>= 1;                           // dzielenie calkowitoliczbowe
                                                          // dziesietnej przez 2
    } while (dziesietna);

    while (i >= 0)                      // uzupelnienie liczby ujemnej jedynkami
      binarna[i--] = '1';
  }; /* if */

  // Wyswietlenie liczby binarnej
  printf("Ta sama liczba calkowita w kodzie z uzupelnieniem do 2: %s\n",binarna);
  system("PAUSE");
  return 0;
}
