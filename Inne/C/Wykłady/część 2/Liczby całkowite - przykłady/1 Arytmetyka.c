/******************************************************************************/
/*  Liczby calkowite i arytmetyka liczb calkowitych                           */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 2.00 Ryszard Sobczak 2017-03-06                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/******************************************************************************/

#include <stdio.h>

void main() {
	int a, b;
  a = 800;
  b = 30;
  printf("a = %d\n",a);
  printf("b = %d\n",b);
  printf("a + b = %d\n",a + b);   // dodawanie
  printf("a - b = %d\n",a - b);   // odejmowanie
  printf("a * b = %d\n",a * b);   // mnozenie
  printf("a / b = %d\n",a / b);   // dzielenie calkowitoliczbowe
  printf("a %% b = %d\n",a % b);  // reszta z dzielenia
  a = (a + b)*3 + 2;
  printf("a = (a + b)*3 + 2 = %d\n",a);  // priorytety dzialan
  a = 8000;
	b = 245365;
  printf("a = %d\n",a);
  printf("b = %d\n",b);
	a = a*b*b;
	printf("Wynik a*b*b = %d\n",a);  // przepelnienie calkowitoliczbowe
//  system("PAUSE");
}
