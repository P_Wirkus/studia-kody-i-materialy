/******************************************************************************/
/*  Wyznaczania wartosci liczb Fibonacciego.                                  */
/*  Uruchomienie z debuggerem                                                 */
/*  Wersja 1.00 Ryszard Sobczak 2017-03-29                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main() {
	int Fi,Fi1,Fi2,i,n;
	
	printf("Podaj numer liczby Fibonacciego : ");
	scanf("%d",&n);
	Fi2 = 0;
	Fi1 = 1;
	for (i = 2; i <= n; i++){
		Fi = Fi1 + Fi2;
		Fi2 = Fi1;
		Fi1 = Fi;
		printf("%d-a liczba Fibonacciego jest rowna = %d\n",i,Fi);
	};
	system("pause");
	return 0;
}

