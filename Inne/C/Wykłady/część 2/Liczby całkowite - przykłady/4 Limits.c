/******************************************************************************/
/*  Najwieksze i najmniejsze wartosci calkowite.                              */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 2.00 Ryszard Sobczak 2017-03-06                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej                          */
/*    i Matematyki Stosowanej                                                 */
/******************************************************************************/

#include <stdio.h>
#include <limits.h>

int main() {
	printf("INT_MAX    = %d\n",INT_MAX);
	printf("INT_MIN    = %d\n",INT_MIN);
	printf("UINT_MAX   = %u\n",UINT_MAX);
	printf("LONG_MAX   = %ld\n",LONG_MAX);
	printf("LONG_MIN   = %ld\n",LONG_MIN);
	printf("ULONG_MAX  = %lu\n",ULONG_MAX);
	printf("LLONG_MAX  = %lld\n",LLONG_MAX);
	printf("LLONG_MIN  = %lld\n",LLONG_MIN);
	printf("ULLONG_MAX = %llu\n",ULLONG_MAX);
	return 0;
}

