/******************************************************************************/
/*  Ilustracja sposobu stosowania 'uproszcznej' wersji operatora podstawiania */
/*  Uruchomienie w srodowisku MS DOS, Windows, Linux                          */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */
/*  Wersja 1.10 Ryszard Sobczak 2017.04.09                                    */
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int a, b;

int main() {
  
  a = 800;
  b = 58;
  printf("a = %d, b = %d\n",a,b);
  printf("a +  b =%d\n",a + b);
  printf("a -  b =%d\n",a - b);
  printf("a *  b =%d\n",a * b);
  printf("a %%  b =%d\n",a % b);
  printf("a +=  b a=%d\n",a += b);                       // rownowazne a = a + b
  printf("a -=  b a=%d\n",a -= b);                       // rownowazne a = a - b
  printf("a %%=  b a=%d\n",a %= b);                      // rownowazne a = a % b
  printf("a *=  b a=%d\n",a *= b);                       // rownowazne a = a * b
  printf("a +=  3 a=%d\n",a += 3);                       // rownowazne a = a + 3
  printf("a -=  3 a=%d\n",a -= 3);                       // rownowazne a = a - 3
  printf("a %%=  3 a=%d\n",a %= 3);                      // rownowazne a = a % 3
  printf("a *=  3 a=%d\n",a *= 3);                       // rownowazne a = a * 3
  printf("a++   a=%d\n",a++);            // rownowazne a = a + 1 po wyswietleniu
  printf("a--   a=%d\n",a--);            // rownowazne a = a - 1 po wyswietleniu
  printf("++a   a=%d\n",++a);                // rownowazne a = a + 1 przed wysw.
  printf("--a   a=%d\n",--a);                // rownowazne a = a - 1 przed wysw.

  system("pause");
  
  // Wyjasnienie ostatnich czterech wynikow
  a = 1;
  b = ++a;
  printf("Przypadek 1: a=%d, b=%d\n",a,b);
  a = 1;
  b = a++;
  printf("Przypadek 2: a=%d, b=%d\n",a,b);
  
  a = 1; 
  b = 1;
  a = a++ + ++a; 
  printf("a=%d b=%d\n",a,b);
  
  system("PAUSE");
  return 0;
}

