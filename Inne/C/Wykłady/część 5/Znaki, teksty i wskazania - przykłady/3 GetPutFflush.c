/******************************************************************************/
/*  Zastosowanie funkcji getchar(), putchar() i fflush(stdio)                 */
/*  Wersja 1.00 Ryszard Sobczak 2005-01-02                                    */                                                   
/*  Wersja 1.10 Ryszard Sobczak 2017-04-09                                    */                                                   
/*  Politechnika Gdanska, Wydzial Fizyki Technicznej i                        */
/*    Matematyki Stosowanej                                                   */
/*  Scenariusz 1: znak,<enter>                                                */
/*  Scenariusz 2: znak,znak,znak,znak,<enter>                                 */
/*  Scenariusz 3: znak,znak,znak,znak,znak,znak,znak,<enter>                  */
/*  Scenariusz 4: N=1;znak,znak,<enter>                                       */
/*  Scenariusz 5: N=1;znak,<enter>                                            */
/******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main () {
	int z, i = 0;
	const int N = 1;

  do {
    z = getchar();        // po wcisnieciu klawisza Enter rozpoczyna wczytywanie
                                         // kolejnych znakow z bufora klawiatury 
    i++;
		printf("Znak: ");
    if (z == 10) 
		  printf("LF Wartosc odpowiadajaca znakowi w kodzie ASCII -  10\n");
		else {
		  putchar(z);
      printf("  Wartosc odpowiadajaca znakowi w kodzie ASCII - %3d\n",z);
		}
		if (i == N) {
			fflush(stdin);
			i = 0;
		}
  } while (z != 65);                           // rownowazne } while (z != 'A');

  printf("\n");
  system("PAUSE");
}

