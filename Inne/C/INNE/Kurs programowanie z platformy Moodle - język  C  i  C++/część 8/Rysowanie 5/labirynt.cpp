#include "stdafx.h"
#include "conio.h"
#include "windows.h"
#include "iostream.h"
#include "process.h"
//----------------------------------------------------------------
#define MAX_X 20
#define MAX_Y 20
//----------------------------------------------------------------
typedef struct{
	int pos_x;
	int pos_y;
	int energia;
	int skarby;
} Ludzik;
//----------------------------------------------------------------
void clrscr(){
	system("cls");
}
//----------------------------------------------------------------
bool wczytajPlansze(char *nazwaPliku,char *p){
	FILE *plik = fopen(nazwaPliku,"r");
	if(plik==NULL) return false;
	char znaczek;
	while(!feof(plik)){
		znaczek= fgetc(plik);
		if(znaczek!='\n') *(p++) = znaczek;
	}
	fclose(plik);
	return true;
}
//----------------------------------------------------------------
void wyswietlPlansze(char p[MAX_Y][MAX_X],Ludzik *m){
	clrscr();
	for(int i=0;i<MAX_Y;i++){
		for(int j=0;j<MAX_X;j++) printf("%c",p[i][j]);
		printf("\n");
	}
	printf("Pozycja: (%d,%d)   Skarby:%d   Energia:%d\n",m->pos_x,m->pos_y,m->skarby,m->energia);
	printf("Kierunki: p(rawo) l(ewo) g(ora) d(ol) q(uit) o(d nowa)\n");
}
//----------------------------------------------------------------
Ludzik* poszukajLudzika(char p[MAX_Y][MAX_X]){
	for(int i=0;i<MAX_Y;i++) for(int j=0;j<MAX_X;j++){
		if(p[i][j]=='L'){
			Ludzik *stworek = new Ludzik;
			stworek->pos_x = j;
			stworek->pos_y = i;
			stworek->energia = 1;
			stworek->skarby = 0;
			return stworek;
		}
	}
	return NULL;
}
//----------------------------------------------------------------
bool moznaWPrawo(char p[MAX_Y][MAX_X],Ludzik *m){
	if(m->pos_x+1==MAX_X){
		printf("Wyszedlbys poza plansze! Tracisz 1 punkt energii.\n");
		m->energia--;
		getch();
		return false;
	}
	if(p[m->pos_y][m->pos_x+1]=='#'){
		printf("Wszedles na sciane! Tracisz 1 punkt energii.\n");
		m->energia--;
		getch();
		return false;
	}
	return true;
}
//----------------------------------------------------------------
int main(int argc, char* argv[]){
	char mapa[MAX_Y][MAX_X]={{' '}};
	Ludzik *klemens;

	if(wczytajPlansze("labirynt1.txt",*mapa)==false){
		printf("Brak pliku z plansz� lub uszkodzony plik.");
		getch();
		return 1;
	}

	klemens = poszukajLudzika(mapa);
	if(klemens==NULL){
		printf("Wadliwa plansza - brakuje ludzika.");
		getch();
		return 2;
	}

	char kierunek;

	while(true){
		wyswietlPlansze(mapa,klemens);
		kierunek=getch();
		if(kierunek=='q') break;
		else if(kierunek=='o'){
			wczytajPlansze("labirynt1.txt",*mapa);
			klemens = poszukajLudzika(mapa);
		}
		else if(kierunek=='p'){
			if(moznaWPrawo(mapa,klemens)==true){
				mapa[klemens->pos_y][klemens->pos_x]=' ';
				klemens->pos_x++;
				if(mapa[klemens->pos_y][klemens->pos_x]=='*') klemens->skarby++;
				else if(mapa[klemens->pos_y][klemens->pos_x]=='J') klemens->energia++;
				mapa[klemens->pos_y][klemens->pos_x]='L';
			}
		}

	}

	printf("Do widzenia !!!\n");
	getch();
	return 0;
}

