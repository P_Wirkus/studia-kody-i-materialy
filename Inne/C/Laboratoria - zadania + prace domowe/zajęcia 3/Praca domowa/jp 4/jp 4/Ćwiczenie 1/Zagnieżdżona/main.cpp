#include <iostream>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) 
{
	int i,j;
	printf (" Tabliczka mnozenia : ");
	for (i = 1; i <= 10; i ++)
	{
		printf ("\n Wiersz %2d: \n", i);
		for (j = 1; j <= 10; j ++)
		{
			printf (" %3d ", j*i);
		}
	}
	return 0;
}

