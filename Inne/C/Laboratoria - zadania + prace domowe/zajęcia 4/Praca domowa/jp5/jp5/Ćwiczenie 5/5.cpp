# include <stdio.h>
# define ROZMIAR 9 // deklaracja stalej
int main ( void )

//	Przyj�o si�, �e procedura od funkcji r�ni si� tym,
//	�e ta pierwsza nie zwraca �adnej warto�ci. Zatem, aby stworzy� procedur� nale�y napisa�:
//	void identyfikator (typ1 argument1, typ2 argument2, typn argument_n)
//	{
//  /* instrukcje */
//	}
//	oid (z ang. pusty, pr�ny) jest s�owem kluczowym maj�cym kilka znacze�,
//	w tym przypadku oznacza "brak warto�ci".
//	Generalnie, w terminologii C poj�cie "procedura" nie jest u�ywane,
//	m�wi si� raczej "funkcja zwracaj�ca void".
//	Je�li nie podamy typu danych zwracanych przez funkcj� kompilator domy�lnie przyjmie typ int,
//	 cho� ju� w standardzie C99 nieokre�lenie warto�ci zwracanej jest b��dem.

{
	int tablica [ ROZMIAR ];
	int i;
	int temp ;
	for (i = 0; i < ROZMIAR ; i ++)
	{
		tablica [i] = i*i;
	}
	// wypisywanie
	for (i = 0; i < ROZMIAR ; i ++)
	{
		printf ("%d. element tablicy to: %d\n", i, tablica [i]);
	}
	// zamiana kolejnosci elementow
	for (i = 0; i < ROZMIAR / 2; i ++)
	{
		temp = tablica [i];
		tablica [i] = tablica [ ROZMIAR - i - 1];
		tablica [ ROZMIAR - i - 1] = temp ;
	}
	printf ("\n\ nTablica odwrocona :\n");
	// wypisywanie
	for (i = 0; i < ROZMIAR ; i ++)
	{
		printf ("%d. element tablicy to: %d\n", i, tablica [i]);
	}
	getchar ();
	return (0);
}

