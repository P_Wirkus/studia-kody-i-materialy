#include <iostream>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

#define ROZMIAR 20

int main(int argc, char** argv) 
{
	int fib[ROZMIAR], i;
	
	printf ("Ile liczb Fibonacciego mam wyznaczyc: ");
	scanf ("%d",ROZMIAR);
	
	fib[0]=1;		//Pierwsze dwa wyrazy maja stala wartosc, niezalezne od n
	fib[1]=1;		//Pierwsze dwa wyrazy maja stala wartosc, niezalezne od n
	
	for (int i=2; i<ROZMIAR; i++) //Bo dwa wiersze wyrazy za juz dane
							//Pierwsza FOR jest po to, aby wczytac wyniki w pamieci
	{
		fib[i] = fib[i-1] + fib[i-2];
	}
	
	for (int i=0; i<ROZMIAR; i++) //Wyrzucenie wynikow na ekran,
							//wszystkich wyrazow, stad petla od 0 do(n-1)
	{
		printf ("Wyraz nr: %d ma wartosc: %d", i, fib[i]);
	}
}
