#include <iostream>
#include <windows.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),4);
    printf("    .*. \n");
    printf("    ./\\. \n");
    printf("   ./  \\. \n");
    printf("  ./    \\. \n");
    printf(" ./      \\. \n");
    printf("|         |\n");
    printf("|         | \n");
    printf("|         | \n");
    printf(".__________.  \n");

    printf("      | \n");
    printf("    --O-- \n");
    printf("      | \n");

for (int k =0; k<10; k++)
{
    printf("**********\n");
}

for (int k=0; k<20; k++)
{
	std::cout<<2*k<<std::endl;
}

for (int k=0; k<20; k++)
{
	std::cout<<k*k<<std::endl;
}


	std::cin.get();	
	return 0;
}
