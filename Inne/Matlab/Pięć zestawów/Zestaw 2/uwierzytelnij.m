% Zadanie 4
% u�ywali�my tu Pesel z Wikipedii

% funkcja sprawdzaj�ca poprawno�� numeru PESEL
function [] = uwierzytelnij(pesel)
% rozbijamy pesel na wektor 11 cyfr
cyfry = [0 0 0 0 0 0 0 0 0 0 0];
for i=1:11
    cyfry(12-i) = (mod(pesel,10^i)-mod(pesel,10^(i-1)))/(10^(i-1));
end
% obliczamy sum� kontroln� numeru PESEL
% suma =9*cyfry(1)+7*cyfry(2)+3*cyfry(3) cyfry(4)+9*cyfry(5)+7*cyfry(6)+3*cyfry(7)+cyfry(8)+9*cyfry(9)+7*cyfry(10)
mnoznik = [9 7 3 1 9 7 3 1 9 7 0];
suma = dot(mnoznik,cyfry);
x = mod(suma,10);
disp(suma);
disp(x);
% koniec pierwszej cz�ci
% sprawdzenie ostatnich cyfr PESEL i sumy kontrolnej
if (x == cyfry(11))
    % sprawdzenie parzysto�ci 10 cyfry
    if (mod(cyfry(10),2)==0)
        disp('Poda�a Pani poprawny nr PESEL.');
    else
        disp('Poda� Pan poprawny nr PESEL.');
    end
else
    disp('B��dny PESEL!');
end