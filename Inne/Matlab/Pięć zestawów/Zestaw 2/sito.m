% funkcja implementuj�ca algorytm sita Erastotenesa
function []=sito(n)
% tworzymy wektor liczb od 1 do n
wektor = 1:n;
wektor(1) = 0;
% w�a�ciwa implementacja algorytmu
for i=2:floor(sqrt(n)) % warunek ko�ca p�tli - sqrt(n)
   if wektor(i)~=0
      j=2*i;
      while (j<=n) % ta p�tla "wykre�la" wielokrotno�ci liczby i
          wektor(j)=0;
          j=j+i;
      end
   end
end
% wy�wietlanie wektora liczb pierwszych
for i=1:n
    if wektor(i)~=0 % wy�wietlamy tylko niezerowe elementy wektora
        disp(wektor(i));
    end
end