# Created by Octave 4.4.1, Wed Dec 05 11:46:21 2018 GMT <unknown@SEBASTIAN77>
# name: A
# type: matrix
# rows: 3
# columns: 3
 1 2 3
 4 5 6
 7 8 9


# name: B
# type: matrix
# rows: 3
# columns: 3
 10 11 12
 13 14 15
 16 17 18


# name: C
# type: matrix
# rows: 3
# columns: 3
 84 90 96
 201 216 231
 318 342 366


# name: D
# type: matrix
# rows: 3
# columns: 3
 1 4 7
 2 5 8
 3 6 9


# name: Id
# type: diagonal matrix
# rows: 3
# columns: 3
1
1
1


# name: V
# type: matrix
# rows: 1
# columns: 3
 224 256 288


# name: Wyznacznik
# type: scalar
6.6613381477509392e-16


# name: a
# type: matrix
# rows: 1
# columns: 3
 1 2 3


# name: ans
# type: matrix
# rows: 1
# columns: 3
 28 40 54


# name: b
# type: matrix
# rows: 1
# columns: 3
 4 5 6


# name: c
# type: matrix
# rows: 1
# columns: 3
 7 8 9


# name: d
# type: matrix
# rows: 1
# columns: 3
 12 15 18


# name: e
# type: matrix
# rows: 1
# columns: 3
 -10 -11 -12


# name: iA
# type: matrix
# rows: 3
# columns: 3
 -0.63888888888888806 -0.16666666666666602 0.30555555555555508
 -0.055555555555555414 5.5511151231257827e-17 0.055555555555555532
 0.52777777777777701 0.16666666666666613 -0.194444444444444


# name: iA2
# type: matrix
# rows: 3
# columns: 3
 -4503599627370499 9007199254740992 -4503599627370495.5
 9007199254740998 -18014398509481984 9007199254740991
 -4503599627370498 9007199254740992 -4503599627370495.5


