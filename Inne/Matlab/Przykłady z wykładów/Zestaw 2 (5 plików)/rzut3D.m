% plik bez zadnych komentarzy
global g; g = 9.81;
global R; R = pi/180;

n_tr = 3;
d_alpha = 30/n_tr;

v0 = 10;

n_phi = 5;
d_phi = 360/n_phi;

X = zeros(100,n_tr*n_phi);
Y = zeros(100,n_tr*n_phi);
Z = zeros(100,n_tr*n_phi);
clc
clf
for m=1:n_phi;
    phi = (m-1)*d_phi*R;
    for n=1:n_tr
        k = n*d_alpha*R;
   
        tmax = 2*v0*sin(k)/g;
        t=linspace(0,tmax);
   
        [X(:,n_tr*(m-1)+n)...
            Y(:,n_tr*(m-1)+n)...
            Z(:,n_tr*(m-1)+n)]...
            =rzut3Dtr(v0,k,t,phi+n);
    end
end

hold on

plot3(X,Y,Z,'LineWidth',2)

t= linspace(0,2*pi);
X = zeros(100,n_tr);
Y = zeros(100,n_tr);
Z = zeros(100,n_tr);

for n=1:n_tr
    k = n*d_alpha*R;
    xmax = v0*v0*sin(2*k)/g;
    X(:,n)=xmax*cos(t);
    Y(:,n)=xmax*sin(t);
end

plot3(X,Y,Z)

view(3)
grid on
hold off

