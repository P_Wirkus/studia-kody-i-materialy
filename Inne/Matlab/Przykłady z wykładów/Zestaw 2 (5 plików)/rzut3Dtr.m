function [xt, yt, zt] = rzut3Dtr(v0,alpha,t,phi)
% Rzut uko�ny - trajektoria 3D
% Detailed explanation should be here
global g;   % ta zmienna musi by� zdefiniowana gdzie� indziej
    
    % wyznaczenie trajektroii zgodnie ze wzorem
    %{
        TODO: 
        poda� �r�d�o
        wyja�ni� symbole
    %}
    xt = v0*t*cos(alpha) *cos(phi);
    yt = v0*t*cos(alpha) *sin(phi);
    zt = v0*t*sin(alpha)-0.5*g*t.^2;
    
end

