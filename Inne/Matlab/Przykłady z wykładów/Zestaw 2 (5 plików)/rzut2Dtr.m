function [xt, zt] = rzut2Dtr(v0,alpha,t)
% Rzut uko�ny - trajektoria 2D
% Detailed explanation should be here
global g;   % ta zmienna musi by� zdefiniowana gdzie� indziej
    
    % wyznaczenie trajektroii zgodnie ze wzorem
    %{
        TODO: 
        poda� �r�d�o
        wyja�ni� symbole
    %}

    xt = v0*t*cos(alpha);
    zt = v0*t*sin(alpha)-g*t.^2/2;
end

%EOF
