% rzut uko�ny 3D - demo while - bez komentarzy
global g; g = 9.81;
global R; R = pi/180;
alpha = 45*R;
v0=10;

X = zeros(100,1);
Y = zeros(100,1);
Z = zeros(100,1);
W = zeros(2,1);
clf
hold on
view(3)

xmax = v0*v0*sin(2*45*R)/g;  
hmax = v0^2/(2*g);


set(gca,'PlotBoxAspectRatio',[1 1 1],...
    'XLim',[-xmax xmax],...
    'YLim',[-xmax xmax],...
    'ZLim',[0 hmax])

range=linspace(0,2*pi);
plot3(xmax*cos(range),xmax*sin(range),0*(range),'Color','red')

koniec = false;
licznik = 1;
while ~koniec
    alpha = input('Podaj alpha [deg]: ');
    
if alpha>0
     phi = input('Podaj phi [deg]: ');
     
     tmax = 2*v0*sin(alpha*R)/g;
     t=linspace(0,tmax);

     [xt yt zt] = rzut3Dtr(10,alpha*R,t,phi*R);
     plot3(xt,yt,zt,'Color',...
         [abs(cos(phi*R)) abs(sin(alpha*R)) 0.3],...
         'LineWidth',2);
      
 sprintf('Rzucono %d raz, parametry:\n alpha = %d\n phi = %d',...
     licznik,alpha,phi)
     
     X(:,licznik)=xt;
     Y(:,licznik)=yt;
     Z(:,licznik)=zt;
     W(:,licznik)=[phi alpha];
     licznik = licznik +1;
else 
     koniec = true;
end

end

hold off    

save('naszerzuty','X','Y','Z','W')
sprintf('Wyniki zapisano do naszerzuty.mat')


