% sumulacja rzutu uko�nego w 2D
%{
  1) wyznaczanie n_tr trajektorii rzutu uko�nego 
  2) zakres kat�w podniesienia alpha0 do 
  3) skrypt korzsta z funkcji rzut2Dtr(v0,alpha,t)

  TODO: 
    1) uodporni� na b��dne dane
%}

%zmienne globalne
global g; g = 9.81;     % przyspieszenie ziemskie
global R; R = pi/180;   % przelicznik stopni na radiany

n_tr = 10;              % ilo�c symulowanych trajektorii

alpha0 = 0;            %pocz�tkowy k�t podniesienia
d_alpha = 30/n_tr;       % krok k�ta podniesienia

v0 = 10;                % pr�dko�� pocz�tkowa


X = zeros(100,n_tr);    % wsp X trajektorii
Z = zeros(100,n_tr);    % wsp Z trajektorii

for n=0:n_tr
   k=alpha0+(d_alpha*n)*R;     % bierz�cy kat rzutu
   
   tmax = 2*v0*sin(k)/g;    % czas trwania bierz�cego rutu
   t=linspace(0,tmax);      % dla tych czas�w wyznaczamy punkty trajektorii

   [X(:,n+1) Z(:,n+1)]=rzut2Dtr(v0,k,t);  %wyznaczenie punkt�w trajektorii
   
end

plot(X,Z)   % narysowanie wyznaczonych trajektorii



k=1;  M=[sprintf('alpha = %d',alpha0);];
while k <= n_tr
   M = char(M,sprintf('alpha = %d',alpha0+(d_alpha*k)));
   k = k+1; 
end 
legend(M)


%EOF
