% B��dy zaokr�gle�
%{
    Na podstawie: 
    ,,Avoiding Common Problems with Floating-Point Arithmetic''
   system pomocy MATLAB
%}

%% u�amki zwyk�e
clc % !!! 
%wywo�anie: Ctrl+Enter
a = 4/3;

sprintf(...
    'a=4/3\nMATLAB:   1-3*(a-1) = %d \nMATLAB:   1-3*a+3  = %d'...
    , 1-3*(a-1) ,  1-3*a +3)

1-3*(a-1) ==  1-3*a +3 % zwraca warto�� logiczn�

%% u�amki dziesi�tne
clc % !!! 
%wywo�anie: Ctrl+Enter

sprintf('Odliczanie do 5, krok a = 0.1, krok b = 0.3')
a = 0.0;
b = 0.0;

for i = 1:5
  a = a + 0.1;
  b = b + 0.3;
end
    if a==0.5
        sprintf('HURRA!')
    else 
        sprintf('BUUUU!')
    end

    if b==1.5
        sprintf('HURRA!')
    else 
        sprintf('BUUUU!')
    end
%% u�amki dziesi�tne
clc % !!! 
%wywo�anie: Ctrl+Enter
sprintf('Odliczanie do 10, krok a = 0.1, krok b = 0.3')    
a = 0.0;
b = 0.0;    
for i = 1:10
  a = a + 0.1;
  b = b + 0.3;
end
    if a==1
        sprintf('HURRA!')
    else 
        sprintf('BUUUU!')
    end

    if b==3
        sprintf('HURRA!')
    else 
        sprintf('BUUUU!')
    end
    
    
%% odejmowanie
clc % !!! 
%wywo�anie: Ctrl+Enter
a = 1e-16 + 1 - 1e-16
b = 1e-16 - 1e-16 + 1

    if a == b
        sprintf('R�wne')
    elseif a<b
        sprintf('a jest mniejsze od b')
    else
        sprintf('a jest wi�ksze od b')
    end
    
%% EOF    
