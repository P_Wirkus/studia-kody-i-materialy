% rozwi�zywanie r�wna� liniowych

%% r�wnania liniowe - mldivide
clc

M = 5;
N = 5;
A = randi([-10,10],[M,N]);
B = randi([-10,10],[M,1]);

% drukowanie macierzy rozszerzonej
temp ='Macierz rozszerzona uk�adu r�wna�:\n';
for m=1:M
    for n=1:N     
        temp = [temp ' ' sprintf('%3i',A(m,n))];
    end
    temp = [temp ' | ' sprintf('%2i',B(m)) '\n'];
end

sprintf(temp)

X=A\B  % dla r�wnania typu AX=B,  
       % A\B w zaleznosci od postaci r-nia korzysta 
       % z macierzy odwrotnej
       % z rok�adu QR, 
       % metody najmniejszych kwadrat�w
       % eliminacji Gaussa  

% mldivide(A,B) % r�wnowa�na forma
disp('   X                   A*X                 B                   AX=B?               A*X-B')

temp=[X A*X B A*X == B A*X-B]; %!!!
format long
disp(temp)
format short

% B'/A  % dla r�wnania typu XA=B
% mrdivide(A,B') % r�wnowa�na forma

%% r�wnania liniowe - metody iteracyjne
clc

M = 5;
N = 5;
A = randi([-10,10],[M,N]);
B = randi([-10,10],[M,1]);

format long
[A\B cgs(A,B) A\B-cgs(A,B)] % cgs - Conjugate Gradient Squared
                            % przyk�ad jednej z wielu metod iteracyjnych 
format short

%% r�wnanie liniowe - rozwi�zania og�lne
clc

M = 3;    % assert M >= N !
N = 5;    % 
A = randi([-10,10],[M,N])
B = randi([-10,10],[M,1])

RORJ = null(A) % r-nie og�lne r-n jednorodnego

RSRN = A\B      % r-nie szczeg�lne r-n niejednorodnego

RORN = @(C) RSRN + RORJ*C; % r-nie ogolne r-n niejednorodnego
                            % parametry podawa� jako kolumne

% dowolne rozwi�zanie r�wnania AX=B jest RORN(C)
C = randi([-1,1],[N-M],1)
RORN(C)

%% r�wnania liniowe - linsolve
clc

A = triu(rand(10,6));
x = [0 1 2 3 4 5]' ;
B = A*x;

%linsolve(A,B)   % korzysta z rozk�adu QR lub LU
                % linsolve(A,B,opts) -- opts pozwala na wyb�r metody
                % dedykowanej dla danego typu macierzy np. dla macierzy
                % g�rnotr�jk�tnych  - opts.UT = true
                % symetrycznych     - opts.SYM = true 
                % itp. ---> help
                
           
y1 = A\B;

opts.UT=true;
y2 = linsolve(A,B,opts);

format longG
[y1 y2 y1-y2]

%% linsolve -- b��dne za�o�enie
clear('opts')
opts.LT=true; % b��dne za�o�enie o macierzy
y3 = linsolve(A,B,opts);

[y1-y2 y1-y3 y2-y3]

format short

%% EOF
