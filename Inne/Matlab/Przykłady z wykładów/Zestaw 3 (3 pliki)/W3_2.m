% komunikacja z u�ytkownikiem

%% wy�wietlanie napis�w, liczb etc
clc
%{ 
    zmienne tekstowe wpisujemy  pomiedzy apostrofami
    do wyswietlania mo�emy u�y�:
    1) sprintf, je�eli chcemy np.: umie�ci� liczby
        kompletny opis z przyk�adami mo��a znale�� w helpie
    2) disp, je�eli chcemy wyswietli� �ancuch znak�w
%}

%% prosty teskst
clc

greetings1 = 'Dzisiaj jest czwartek';
greetings2 = 'Poznajmy MATLAB''a';

disp(greetings1);
disp(greetings2);

sprintf('%s\n%s',greetings2,greetings1)


%% sformatowany tekst
clc
sprintf('Liczba pi (domy�lna posta�): %f',pi) % domy�la precyzja 
sprintf('Liczba pi (2 miejsca po przecinku): %.2f',pi) % 2 miejsca po przecinku
sprintf('Liczba pi (8 znak�w, 2 po przecinku): %8.2f',pi) % 8 znak�w, 2 po przecinku
sprintf('Liczba pi (???): %08.2f',pi) % a to?

% to co wy�ej w jednym poleceniu
sprintf('%s%f\n%s%.3f\n%s%8.3f\n%s%08.5f'...
    ,'Liczba pi (domy�lna posta�)          : ',pi...
    ,'Liczba pi (2 miejsca po przecinku)   : ',pi...
    ,'Liczba pi (8 znak�w, 2 po przecinku) : ',pi...
    ,'Liczba pi (j.w dope�nienienie zerami): ',pi...
    ) % domy�la precyzja 

%% pobieranie danych (linia polece�)

% mo�emy u�y� funkcji input
% inne warte uwagi funkcje: textscan, fscan, sscan
clc

% pro�ba o podanie warto�ci zmiennej
a = input('Podaj warto�c zmiennej a: ');

% a tak mozemy zapyta� u�ytkownika o np. o nazw� pliku do wczytania

filename = input('Podaj nazw� pliku: ','s');

save(filename,'a')


%% EOF
