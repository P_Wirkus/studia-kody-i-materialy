% elementy statystyki

%% elementarna statystyka 1
clc
% rozk�ad jednostajny na odcinku [a,b]
a = 0; 
b = 100;
data1 = a+(b-a)*rand(100,1);

disp('    min       max       mean      median    std       var')
disp([min(data1)...      % minimum 
    max(data1)...   % maksimum
    mean(data1)...  % �rednia
    median(data1)...% mediana
    std(data1)...   % odchylenie standardowe
    var(data1)...   % wariancja
    ])

subplot(2,1,1)
stem(data1)
subplot(2,1,2)
hist(data1,0:100)

%% elementarna statystyka 2
clc
% rozka�d normalny 
mu = 0;
sigma = 1;
data1 = mu+sigma*randn(100,1);

disp('    min       max       mean      median    std       var')
disp([min(data1)...      % minimum 
    max(data1)...   % maksimum
    mean(data1)...  % �rednia
    median(data1)...% mediana
    std(data1)...   % odchylenie standardowe
    var(data1)...   % wariancja
    ])

subplot(2,1,1)
stem(data1)
subplot(2,1,2)
hist(data1,-4:0.1:4)

%EOF
