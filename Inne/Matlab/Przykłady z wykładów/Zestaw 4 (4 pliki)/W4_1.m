% Ca�kowanie numeryczne - funkcje wbudowane
%{
    Demonstracja:
kwadratury:
    - quad          - ca�ka po odcinku
    - dblquad       - ca�ka po prostok�cie, 
                      mo�liwo�� ca�kowania po dowolnym zbiorze
    - triplequad    - ca�ka po sze�cianie
                      mo�liwo�� ca�kowania po dowolnym zbiorze

    - quad2d        - ca�ka po podzbiorze p�aszczyzny
                        a<x<b, c(x)<y<d(x)

meoda trapez�w
    - trapz         - ca�ka po odcinku
%}

%% handler
% uwaga og�lna: do funkcji ca�kujacych z rodziny quad przekazujemy tzw. handler funkcji
% ma to umo�liwi� fukncji ca�kujacej samodzielne obliczanie funkcji
% ca�kowanej
%{

% plik foo1.m
function y = foo1(x)
    y = x.^3-5*x.^2-x+1 
end

%}

% handler tworzymy poprzedz�jac nazw� funkcji przez @
h1 = @foo1;
h2 = @sin;

% mo�emy korzysta� z handlera jak z funkcji 
[h1(0) h2(0)]

%funkcje anonimowe s� handlerami 

foo = @(x) x.^2-x+2

%% ca�kowanie przez kwadratury - quad
clc
[quad(@foo1,0,1) quad(h1,0,1)]
% r�wnowa�ne sposoby, h1 jest handlerem dla foo1

[quad(@sin,0,pi) quad(h2,0,pi)]
% r�wnowa�ne sposoby, h2 jest handlerem dla sin

%% ca�kowanie przez kwadratury - dblquad
clc
I1 = dblquad(@foo2,0,1,0,1); % po prostok�cie [0,1]x[0,1]

h1 = @(x,y)foo2(x,y).*(y<x);
I2 = dblquad(h1,0,1,0,1); % po tr�jk�cie 0<=x<=1, 0<=y<=x

I3 = dblquad(...            
    @(x,y)1.*(x.^2+y.^2<=1)... % pole ko�a, 1.* nie jest konieczne!
    ,-1,1,-1,1);

I4 = 2*dblquad(...                % obj�to�� kuli o promieniu 1
    @(x,y)sqrt(1-x.^2-y.^2).*(x.^2+y.^2<=1)...  
    ,-1,1,-1,1);

[I1 I2 I3 I4]
%% ca�kowanie przez kwadratury - triplequad
clc
h1 = @(x,y,z)y*sin(x)+z*cos(x);
triplequad(h1,0,pi,0,1,-1,1)

%% ca�kowanie quad2d
clc

% obszar ca�kowania - ko�o
a = -1; b = 1;
c = @(x) -sqrt(1-x.^2);
d = @(x) sqrt(1-x.^2);

h1 = @(x,y) 1.*(x<1); % sztucznie zrobiona f-cja sta�a
I1 = quad2d(h1,-1,1,c,d); % pole ko�a

h2 = @(x,y) sqrt(1-x.^2-y.^2);
I2 = 2*quad2d(h2,-1,1,c,d); % obj�to�� kuli

[I1 I2]
%% metoda trapez�w 
% przyk�ady z helpa
clc
K = randi(100);
X = 0:pi/K:pi;
Y = sin(X);
I1 = trapz(X,Y);        % r�wnomierny podzia� przedzia�u ca�kowania na K odcink�w
I2 = pi/K*trapz(Y);     % r�wnowa�ny zapis

X = sort(rand(1,K+1)*pi);   % nier�wnomierny i losowy podzia� przedzia�u ca�kowania na K odcink�w
Y = sin(X);
I3 = trapz(X,Y);        

disp('   K          I1        I2        I3        quad')
disp([K I1 I2 I3 quad(@sin,0,pi)])

%EOF
