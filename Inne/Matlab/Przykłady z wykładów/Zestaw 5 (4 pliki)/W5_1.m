% demonstracja wykres�w 2D

%% wiele wykres�w - spos�b 1
% 'LineWidth',2.5
subplot(3,1,1)
x = linspace(-pi,pi);
Y(1,:) = sin(x);
Y(2,:) = cos(x);
Y(3,:) = sin(x).*sin(x);
Y(4,:) = sin(x).*cos(x);

plot(x,Y,'LineWidth',2.5)

%% wiele wykres�w - spos�b 2

subplot(3,1,2)
x = linspace(-pi,pi);
y1 = sin(x);
y2 = cos(x);
y3 = sin(x).*sin(x);
y4 = sin(x).*cos(x);

plot(x,y1,'r:',x,y2,'b-',x,y3,'g--',x,y4,'k','LineWidth',2.5)

%% wiele wykres�w - spos�b 3
subplot(3,1,3)

set(gca,'XTick',-pi:pi/2:pi)
set(gca,'XTickLabel',{'-pi','-pi/2','0','pi/2','pi'})
xlabel('-\pi \leq \phi \leq \pi')  % mozan uzywa� polece� LaTeX'a!!!
ylabel('sin(\phi)')
title('zadanie z zestawu 4')

x = linspace(-pi,pi);

hold on

y1 = sin(x);
plot(x,y1,'r:','LineWidth',2.5)


y2 = cos(x);
plot(x,y2,'b-')

y3 = sin(x).*sin(x);
plot(x,y3,'g--','LineWidth',2.5)

y4 = sin(x).*cos(x);
plot(x,y4,'k')

legend('sin(x)','cos(x)','sin^2(x)','sin(x)cos(x)')

%% podobrazki
% dzielimy obrazek na 2x2

subplot(2,2,1) %pierwszy podobrazek

x = linspace(-10,10);
y = cos(x/5) + 0.05*sin(x.^2);
plot(x,y)

subplot(2,2,2) % drugi podobrazek

bar(rand(10))

subplot(2,2,3:4) % �aczymy 3 i 4
demo_wykres2D([-10 10])
