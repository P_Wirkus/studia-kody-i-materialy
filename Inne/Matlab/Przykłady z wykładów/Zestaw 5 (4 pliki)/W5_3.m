% wykresy 3D -- siatka i powierzchnia 

[x y] = meshgrid(0:.1:10);

z = sin(x).*x.*y;

subplot(2,2,1)
mesh(z)
title('mesh')

subplot(2,2,2)
meshc(x,y,z)
title('meshc')

subplot(2,2,3)
surf(z)
title('surf')

subplot(2,2,4)
surfc(z)
title('surfc')