function [] = demo_wykres2D(interval)
% descriptions should be here 
%{
 fig_n         which figure we'll use
 interval      interval for x 
%}
    if length(interval)==1
        x = linspace(0,interval);
    else
        x = linspace(interval(1),interval(2));
    end
    
quit = false;       % end-of-the-game flag
aux_str = '';       % user answers

f = @(x) sin(x);

linestyle = '-';
linecolor = 'b';

%let's go
    disp('Dzie� dobry! :-)');
% main loop
while ~quit
clc       
       % ask for next action
 aux_str = sprintf('%s\n%s\n'...
           ,'Rysuj (domyslnie sin(x)) : r'...
           ,'Wyczy��                  : w'...
           ,'Zmie� przedzia�          : p'...
           ,'Zmie� funkcj�            : f'...
           ,'Zmie� styl lini          : ls'...
           ,'Zmie� color lini         : lc'...
           ,'Zako�cz                  : q'...
           );
    disp(aux_str);
    
    aux_str = input('Co robimy?\n # ','s');
        
    switch aux_str
        case 'r'
            plot(x,f(x)...
                ,'LineStyle',linestyle...
                ,'Color',linecolor...
                ,'LineWidth',3.0);       
        case 'w'
            clf(fig_n)
        case 'p'
            disp('');
            interval = input('Podaj vecor [xmin xmax]: ');
            x = linspace(interval(1),interval(2));
        case 'f'
            disp('Funkcja zmiennej x, mnozenie przez .*, potega przez .^');
            aux_str = input('Podaj wz�r: ','s');
            f = inline(aux_str);
        case 'ls'
            % ask for line style 
            linestyle = ask_linestyle();
        case 'lc'
            % ask for line color
            linecolor = ask_linecolor();
        case 'q'
            % are you sure? 
            aux_str = input('Koniec [t/N]? ','s');
            if upper(aux_str) == 'T'
                quit = true;
            end    
    end %switch
          
end %while

    disp('Do zobaczenia!');
        
end

function [ls] = ask_linestyle()
% ask user to select line style of plot
 aux_str = sprintf('Rodzaje lini wykresu:\n%s\n%s\n'...
           ,'ci�g�a (domy�lnie): -'...
           ,'przerywana        : --'...
           ,'kropkowana        : :'...
           ,'kropka-kreska     : -.'...
           );      
    disp(aux_str);
    aux_str = input('Wybierz rodzaj lini: ','s');

    %set selected line style
    switch aux_str
       case '--'
          ls = '--';
       case ':'
          ls = ':';
       case '-.'
          ls = '-.';
       otherwise
          ls = '-';
   end

end

function [lc] = ask_linecolor()
clc
% ask user to select line style of plot
   aux_str = sprintf('Kolor lini wykresu:\n%s\n%s\n%s\n%s\n'...
           ,'niebieska (domy�lnie): n'...
           ,'czerwona             : c'...
           ,'zielona              : z'...
           ,'w�asny (expert!)     : w'...
           );
    disp(aux_str);
        
    aux_str = input('Wybierz kolor wykresu: ','s');

    %set selected line style
    switch aux_str
       case 'n' %blue
          lc = 'b';
       case 'c' %red
          lc = 'r';
       case 'z' %green
          lc = 'g';
        case 'w'           
          lc = input('[R G B]: ');  
       otherwise
          lc = 'b';
   end

end

%EOF