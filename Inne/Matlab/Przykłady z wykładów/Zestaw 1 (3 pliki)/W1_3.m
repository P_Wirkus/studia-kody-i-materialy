% rzut uko�ny 2D - rysowanie wielu trajektorii
%{
    demonstracja:
    - p�tla for
    - rysowanie wielu wykres�w na jednym obrazku
    - sprintf

UWAGA: wymagana funkcja fun_y(t,alpha) z W2_2.mat
%}

X = linspace(0,10.5);         % apriori wybrany przedia�, podzielony na 100 punkt�w
Y = zeros(1,length(X));     % tu bedzeimy przechowywa� punkty trajektorii w punktach X
 
%sprintf('Wymiar Y: %dx%d.',size(Y)) % tylko do cel�w diagnostycznych
 
% prosz� eksperymentowa� z parametrami: ilo�ci� trajektorii, k�tem pocz�tkowym i krokiem 

for n= 0:90             % w zamy�le 90 trajekotrii
                        % od -45deg z krokiem 10deg
                        % uwaga na k�t z przedzialu [90,270]
                        % trajektoria jest wtedy poza obrazkiem
                        
    k= (-45+5*n)*pi/180;  % bierz�cy k�t rzutu
    Y(n+1,:)=fun_y(X,k);  % automatyczne powi�kszanie Y, brak kontroli nad Y!!!
end

%sprintf('Wymiar Y: %dx%d.',size(Y)) % tylko do cel�w diagnostycznych

plot(X,Y)
ylim([-1,5]) % prosz� to zakomentowac i eksperymentowa� z liczbami


%{
    zadanie na dodatkowe punkty dla wytrwa�ych:
    Zmodyfikowa� skrypty (lub napisa� w�asne), aby
    1) uwzgl�dnia�y op�r powietrza
    2) rysowa�y trajektori� dla k�t�w od 90deg do 270deg

    konkurs trwa do 31 grudnia br., do godz 23:59
    zg�oszenia prosz� wysy�a� na m�j adres email
    atrakcyjne nagrody !!!
%}


%EOF
