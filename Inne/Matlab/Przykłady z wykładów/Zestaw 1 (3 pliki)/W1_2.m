% rzut uko�ny 2D - funkcje anonimowe
%{
    demonstracja:
        - funkcje anonimowe (anonymous function)
            pozwalaj� na tworzenie funkcji bez zapisywania do pliku
            patrz help 

 Po wywo�aniu tego skryptu b�d� dostepne funkcje 
    - fun_xt(t)         - wsp x w chwili t   
    - fun_yt(t)         - wsp y w chwili t
    - fun_y(t)          - wsp y w punkcie x
    - fun_y(t,alpha)    - wsp y w punkcie x, przy k�cie rzutu alpha

UWAGA: do prawid�owego zdefiniowania tych funkcji musz� by� zdefiniowane
,,gdzie� indziej'' zmienne: g, v0, alpha (nie dot. fun_y(t,alpha) )
%}

fun_xt = @(t) v0*t*cos(alpha);
fun_yt = @(t) v0*t*sin(alpha)-g*t.^2/2; % .^ oznacza �e pot�gowany jest 
                                        % ka�dy element wektora (element-wise)
                                        
%{
    przyk�ad tzw. przeci��enia funkcji, 
    tzn u�ywamy tej samej nazwy dla funkcji, 
    rozpoznajemy je po ilo�ci argument�w  
%}
                                        
fun_y = @(x) x*tan(alpha)-1/(2*v0^2*cos(alpha)^2)*g*x.^2;
fun_y = @(x,alpha) x*tan(alpha)-1/(2*v0^2*cos(alpha)^2)*g*x.^2;

% z ciekawo�ci naukowej prosz� wymieni� powy�sz� linie na nastepujace
% fun_y = @(x) disp('Jestem fun_y, mam jeden argument.') 
% fun_y = @(x,alpha) disp('Jestem fun_y, mam dwa argumenty.') 

%EOF
