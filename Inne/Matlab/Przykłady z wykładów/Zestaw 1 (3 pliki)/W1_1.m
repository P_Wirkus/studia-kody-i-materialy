% rzut uko�ny 2D 
% brak opor�w, p�aski teren
%{
    demonstracja: 
        zmienne,
        operatory arytmetyczne,
        operator :,
        wektory,    
        wykres
%}

% warunki pocz�tkowe
g = 9.81;           % przyspieszenie ziemskie [m/s^2]
v0 = 10;            % pr�dko�� pocz�tkowa [m/s]
alpha = 30*pi/180;  % k�t rzutu [radian]


% wielko�ci opisuj�ce trajektori�
tw = v0*sin(alpha)/g;           %czas wznoszenia [s]
tc = 2*tw;                      %czas lotu [s]      
xmax = v0*v0*sin(2*alpha)/g;    % maksymalny zasi�g [m]
ymax = (v0*sin(alpha))^2/(2*g); % maksymalna wysoko�� [m]


% dla danych w.p. tworzymy 100 punkt�w w kt�rych b�dziemy oblicza�
% po�o�enie i pr�dko��
% UWAGA: alternatywnie mo�na to zrobi� poleceniem
% t = linspace(0,tc)
% x = linspace(0,xmax)
t = 0:0.01*tc:tc;       % podzia� przedzia�u czasowego
x = 0:0.01*xmax:xmax;   % podzia� przeedzia�u przestrzennego

% tworzymy wektory zawierajace 100 punkt�w reprezentujace odpowiednio: 
% po�o�enie (r-nie parametryczne)
xt = v0*t*cos(alpha);           % sk�adowa pozioma
yt = v0*t*sin(alpha)-g*t.^2/2;  % sk�adowa pionowa
% pr�dko�� chwilowa (r-nie parametryczne)
vy = v0*sin(alpha)-g*t;         % sk�adowa pozioma
vx = v0*cos(alpha);             % sk�adowa pionowa
% r�wnanie toru lotu we wsp. kartezja�skich
y = x*tan(alpha)-1/(2*v0^2*cos(alpha)^2)*g*x.^2;

% wykresy trajektorii
% umieszczamy trzy wykresy na jednym obrazku
subplot(2,2,1)  % podobrazek 1 ---> patrz help
plot(xt,yt)     % w�a�ciwy wykres
xlabel('x(t)')  % etykieta osi x
ylabel('y(t)')  % etykieta osi y

subplot(2,2,3), plot(t,vy), xlabel('t'), ylabel('vy(t)')

subplot(2,2,4), plot(x,y), xlabel('x'), ylabel('y(x)')

% EOF




















